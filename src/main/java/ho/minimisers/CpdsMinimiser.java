/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.minimisers;

import java.util.Collection;
import java.util.HashSet;

import ho.structures.cpds.Cpds;
import ho.structures.cpds.ControlState;
import ho.structures.cpds.SCharacter;
import ho.structures.cpds.StackHead;

public abstract class CpdsMinimiser {

    /**
     * Takes a CPDS and minimises it by some minimisation technique.  Should be
     * a (more or less) exact minimisation.  I.e. preserves initial state
     * reachability.
     *
     * @param cpds the cpds to minimise
     * @param protectedControls a collection of control states that should not
     * be removed (a q in protectedControls is equivalent to <q, a> in
     * protectedHeads for all stack characters a).
     * @param protectedCharacters a collection of stack characters that should
     * not be removed (analogous to protectedControls)
     * @param protectedHeads a collection of stack heads that should not be
     * removed by the minimisation.  E.g. do not combine q1, a ---> q2, b --->
     * q3, c into a single transition q1, a ---> q3, c if (q2, b) is a
     * protected head.
     */
    public abstract void minimise(Cpds cpds,
                                  Collection<ControlState> protectedControls,
                                  Collection<SCharacter> protectedCharacters,
                                  Collection<StackHead> protectedHeads);

    /**
     * Takes a CPDS and minimises it by some minimisation technique.  Should be
     * a (more or less) exact minimisation.  I.e. preserves initial state
     * reachability.
     *
     * @param cpds the cpds to minimise
     * @param protectedHeads a collection of stack heads that should not be
     * removed by the minimisation.  E.g. do not combine q1, a ---> q2, b --->
     * q3, c into a single transition q1, a ---> q3, c if (q2, b) is a
     * protected head.
     */
    public void minimise(Cpds cpds,
                         Collection<StackHead> protectedHeads) {
        minimise(cpds,
                 new HashSet<ControlState>(),
                 new HashSet<SCharacter>(),
                 protectedHeads);
    }

    /**
     * Takes a CPDS and minimises it by some minimisation technique.  Should be
     * a (more or less) exact minimisation.  I.e. preserves initial state
     * reachability.
     *
     * @param cpds the cpds to minimise
     */
    public void minimise(Cpds cpds) {
        minimise(cpds, new HashSet<StackHead>());
    }


}
