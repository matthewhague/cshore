/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.minimisers;

import java.util.Iterator;
import java.util.Set;
import java.util.HashSet;

import org.apache.log4j.Logger;

import ho.util.DoubleMap;
import ho.util.Pair;

import ho.structures.cpds.*;

import ho.managers.Managers;

// A simple forwards reachability analysis that creates a map
//
//     (q, a) --> set of rules that can be used to reach head (q, a)
//

public class Order0RuleElimination {
    static Logger logger = Logger.getLogger(Order0RuleElimination.class);

    // table of heads to rules and characters that might be used/seen on a path
    // to them
    DoubleMap<ControlState,
              SCharacter,
              Pair<Set<Rule>,
                   Set<SCharacter>>> headTable
        = new DoubleMap<ControlState,
                        SCharacter,
                        Pair<Set<Rule>,
                             Set<SCharacter>>>();

    Set<StackHead> worklist = new HashSet<StackHead>();

    Cpds cpds;

    public Order0RuleElimination() { }


    /**
     * @param cpds the cpds to minimise
     * @param initHeads initial heads
     * @return a cpds that only has rules that might occur on a path from
     *         <qinit, [ainit]> (in initHeads) to <qerror, *> (for some stack *).
     */
    public Cpds minimise(Cpds cpds, Set<StackHead> initHeads) {
        this.cpds = cpds;
        headTable.clear();

        for (StackHead head : initHeads)
            processHead(head);

        while (!worklist.isEmpty()) {
            Iterator i = worklist.iterator();
            StackHead head = (StackHead)i.next();
            i.remove();
            processHead(head);
        }

        Cpds newCpds = new Cpds();

        ControlState qerror = Managers.csManager.makeControl(Cpds.error_state);

        for (Pair<Set<Rule>,
                  Set<SCharacter>> rcs : headTable.values(qerror))
            for (Rule r : rcs.first())
                newCpds.addRule(r);

        return newCpds;
    }


    private Pair<Set<Rule>, Set<SCharacter>> getAddEntry(ControlState q,
                                                         SCharacter a) {
        Pair<Set<Rule>, Set<SCharacter>> rcs = headTable.get(q, a);
        if (rcs == null) {
            rcs = new Pair<Set<Rule>,
                           Set<SCharacter>>(new HashSet<Rule>(),
                                            new HashSet<SCharacter>());
            rcs.second().add(a);
            headTable.put(q, a, rcs);
        }
        return rcs;
    }

    /**
     * Notes that the rule might be used on a path to the given head.  Adds <q,
     * a> to the worklist if the addition resulted in a change.
     *
     * @param q  a control state
     * @param a  a stack character
     * @param r  the rule that was used to get here
     * @param newRcs the rules and chars that might have been used on a path to
     *               the head <q, a> (excluding r)
     */
    private void updateEntry(ControlState q,
                             SCharacter a,
                             Rule r,
                             Pair<Set<Rule>,
                                  Set<SCharacter>> newRcs) {
        Pair<Set<Rule>, Set<SCharacter>> rcs = getAddEntry(q, a);

        boolean change = false;
        change |= rcs.first().add(r);
        change |= rcs.first().addAll(newRcs.first());
        change |= rcs.second().addAll(newRcs.second());

        if (change)
            worklist.add(new StackHead(q, a));
    }


    private void processHead(StackHead h) {
        ControlState q = h.getControl();
        SCharacter a = h.getCharacter();

        for (PushRule r : cpds.getPushRulesFwd(q, a))
            processPush(r);
        for (PopRule r : cpds.getPopRulesFwd(q, a))
            processPop(r);
        for (CPushRule r : cpds.getCPushRulesFwd(q, a))
            processCPush(r);
        for (CollapseRule r : cpds.getCollapseRulesFwd(q, a))
            processCollapse(r);
        for (RewRule r : cpds.getRewRulesFwd(q, a))
            processRew(r);
        for (RewLinkRule r : cpds.getRewLinkRulesFwd(q, a))
            processRewLink(r);
        for (GetArgRule r : cpds.getGetArgRulesFwd(q, a))
            processGetArg(r);
        for (GetOrder0ArgRule r : cpds.getGetOrder0ArgRulesFwd(q, a))
            processGetOrder0Arg(r);
        for (EvalNonTermRule r : cpds.getEvalNonTermRulesFwd(q, a))
            processEvalNonTerm(r);
        for (AltRule r : cpds.getAltRulesFwd(q, a))
            processAlt(r);
    }


    private void processPush(PushRule r) {
        ControlState q1 = r.getControl1();
        SCharacter a = r.getCharacter();
        ControlState q2 = r.getControl2();
        updateEntry(q2, a, r, getAddEntry(q1, a));
    }


    private void processPop(PopRule r) {
        ControlState q1 = r.getControl1();
        SCharacter a = r.getCharacter();
        ControlState q2 = r.getControl2();
        Pair<Set<Rule>, Set<SCharacter>> rcs = getAddEntry(q1, a);

        for (SCharacter b : rcs.second())
            updateEntry(q2, b, r, rcs);
    }

    private void processCPush(CPushRule r) {
        ControlState q1 = r.getControl1();
        SCharacter a = r.getCharacter();
        ControlState q2 = r.getControl2();
        SCharacter b = r.getPushCharacter();
        updateEntry(q2, b, r, getAddEntry(q1, a));
    }


    private void processCollapse(CollapseRule r) {
        ControlState q1 = r.getControl1();
        SCharacter a = r.getCharacter();
        ControlState q2 = r.getControl2();
        Pair<Set<Rule>, Set<SCharacter>> rcs = getAddEntry(q1, a);

        for (SCharacter b : rcs.second())
            updateEntry(q2, b, r, rcs);
    }


    private void processRew(RewRule r) {
        ControlState q1 = r.getControl1();
        SCharacter a = r.getCharacter();
        ControlState q2 = r.getControl2();
        SCharacter b = r.getRewCharacter();
        updateEntry(q2, b, r, getAddEntry(q1, a));
    }


    private void processRewLink(RewLinkRule r) {
        ControlState q1 = r.getControl1();
        SCharacter a = r.getCharacter();
        ControlState q2 = r.getControl2();
        SCharacter b = r.getRewCharacter();
        updateEntry(q2, b, r, getAddEntry(q1, a));
    }


    private void processGetArg(GetArgRule r) {
        ControlState q1 = r.getControl1();
        SCharacter a = r.getCharacter();
        ControlState q2 = r.getControl2();
        SCharacter b = r.getRewCharacter();
        updateEntry(q2, b, r, getAddEntry(q1, a));
    }


    private void processGetOrder0Arg(GetOrder0ArgRule r) {
        ControlState q1 = r.getControl1();
        SCharacter a = r.getCharacter();
        ControlState q2 = r.getControl2();
        SCharacter b = r.getRewCharacter();
        updateEntry(q2, b, r, getAddEntry(q1, a));
    }


    private void processEvalNonTerm(EvalNonTermRule r) {
        ControlState q1 = r.getControl1();
        SCharacter a = r.getCharacter();
        ControlState q2 = r.getControl2();
        SCharacter b = r.getPushCharacter();
        updateEntry(q2, b, r, getAddEntry(q1, a));
    }


    private void processAlt(AltRule r) {
        ControlState q1 = r.getControl1();
        SCharacter a = r.getCharacter();
        for (StackHead h : r.getDestinations()) {
            ControlState q2 = h.getControl();
            SCharacter b = h.getCharacter();
            updateEntry(q2, b, r, getAddEntry(q1, a));
        }
    }

}
