/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.minimisers;

import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;

import org.apache.log4j.Logger;

import ho.util.UnsafeMultiMap;
import ho.util.NaiveUnsafeMultiMap;

import ho.structures.cpds.*;

import ho.managers.Managers;

/**
 * Minimises a cpds by looking for rule pairs
 *
 *     (q1, a, rew(b), q2)
 *     (q2, b, o, q3)
 *
 * where (q2, b) is not a protected head, q2 is not a protected control, b is
 * not a protected character, and either
 *
 *     + (q1, a, rew(b), q2) is the only rule leading to the head (q2, b), or
 *     + (q2, b, o, q3) is the only rule leaving (q2, b)
 *
 * and replaces the rule pair with the single rule
 *
 *     (q1, a, o, q3)
 *
 * and provided we don't pop to that control state, and that o is not a push
 * (because then b matters).
 *
 */
public class CpdsRuleCompression extends CpdsMinimiser {

    static Logger logger = Logger.getLogger(CpdsRuleCompression.class);

    // sets of heads that have either a single rule leading in
    // or a single rule leading out, and are not protected.
    private Set<StackHead> singleIn = new HashSet<StackHead>();
    private Set<StackHead> multiIn = new HashSet<StackHead>();
    private Set<StackHead> hasOut = new HashSet<StackHead>();
    // control states that may be popped to (these should be protected since we
    // don't know what the top stack character will be)
    private Set<ControlState> popControls = new HashSet<ControlState>();
    // we can merge an eligible rewrite r with all rules except pushes
    // so if we have a push following it, we should keep r
    // still merging the other rules should lead to fewer steps in the
    // saturation
    private Set<StackHead> hasPush = new HashSet<StackHead>();

    // Cos error is a protected control state too
    private ControlState qerror = Managers.csManager.makeControl(Cpds.error_state);

    // for storing the arguments to minimise
    private Cpds cpds;
    private Collection<ControlState> protectedControls;
    private Collection<SCharacter> protectedCharacters;
    private Collection<StackHead> protectedHeads;


    public CpdsRuleCompression() { }


    public void minimise(Cpds cpds,
                         Collection<ControlState> protectedControls,
                         Collection<SCharacter> protectedCharacters,
                         Collection<StackHead> protectedHeads) {
//        logger.warn("CpdsRuleCompression uses cpds get*Rules methods.  These will be inefficient for large cpdss.");
        this.cpds = cpds;
        this.protectedControls = protectedControls;
        this.protectedCharacters = protectedCharacters;
        this.protectedHeads = protectedHeads;
        while (mainLoop()) {
            // repeat main loop until fixed point
        }
    }


    ///////////////////////////////////////////////////////////////////
    // Privates


    /**
     * Sets up singleIn and hasOut to hold heads that either have a single
     * rule in or a single rule out, and do not contain a protected component.
     */
    private final void setUpSets() {
        multiIn.clear();
        singleIn.clear();
        popControls.clear();
        hasOut.clear();
        hasPush.clear();

        for (Rule r : cpds.getRules()) {
            // In -- if we know the destination, keep it
            // if we don't, then we have to protect the destination control
            // since we may pop to any head with it as the control
            StackHead sh = new StackHead();
            if (r.getDestinationHead(sh)) {
                if (singleIn.remove(sh)) {
                    multiIn.add(sh);
                } else if (!multiIn.contains(sh)) {
                    singleIn.add(sh);
                }
            } else {
                popControls.add(r.getControl2());
            }


            // Out
            sh = new StackHead(r.getControl1(),
                               r.getCharacter());
            hasOut.add(sh);
        }

        for (PushRule r : cpds.getPushRules()) {
            StackHead sh = new StackHead(r.getControl1(),
                                         r.getCharacter());
            hasPush.add(sh);
        }

        for (CPushRule r : cpds.getCPushRules()) {
            StackHead sh = new StackHead(r.getControl1(),
                                         r.getCharacter());
            hasPush.add(sh);
        }

        for (EvalNonTermRule r : cpds.getEvalNonTermRules()) {
            StackHead sh = new StackHead(r.getControl1(),
                                         r.getCharacter());
            hasPush.add(sh);
        }

    }


    /**
     * Runs the main loop: looks for pairs of rules the can be rewritten to just
     * one (and rewrites them).
     *
     * @return true if a change occurred
     */
    private boolean mainLoop() {
        setUpSets();

        Set<Rule> toAdd = new HashSet<Rule>();
        Set<Rule> toRemove = new HashSet<Rule>();
        // if we have q a --> q b and q b --> q c
        // then we need to make sure we don't find
        // q b --> q c and q c --> q d and end up with the rules
        // q a --> q c and q b --> q d, breaking reachability
        // so after merging a rule, we make sure it's destination head is not
        // removed by any later merges
        Set<StackHead> toPreserve = new HashSet<StackHead>();

        for (byte k = cpds.getOrder(); k >= 1; --k) {
            for (RewRule r : cpds.getRewRules()) {
                if (ruleEligible(r, toPreserve)) {
                    compressBy(r, toAdd, toRemove, toPreserve);
                }
            }
        }

        return doUpdate(toAdd, toRemove);
    }


    /**
     * @param r a rule to check
     * @param toPreserve a set of additional stack heads that should not be
     * removed
     * @param true if the rule is a candidate for the first in a compression
     */
    private boolean ruleEligible(RewRule r,
                                 Set<StackHead> toPreserve) {
        ControlState q = r.getControl2();
        SCharacter a = r.getRewCharacter();
        StackHead sh = new StackHead(q, a);
        boolean is = !Managers.csManager.equal(q, qerror) &&
                     !protectedControls.contains(q) &&
                     !protectedCharacters.contains(a) &&
                     !protectedHeads.contains(sh) &&
                     !toPreserve.contains(sh) &&
                     !popControls.contains(q) &&
                     singleIn.contains(sh) &&
                     hasOut.contains(sh);
        return is;
    }


    /**
     * @param r the rule to do compression for (the first rewrite rule in pair)
     * @param toAdd set to add the new rules to
     * @param toRemove set to add the rules that need removing to
     * @param toPreserve the set to add heads that should be preserved by future
     * compressions in the same mainLoop
     */
    private void compressBy(RewRule r,
                            Set<Rule> toAdd,
                            Set<Rule> toRemove,
                            Set<StackHead> toPreserve) {
        ControlState q = r.getControl2();
        SCharacter a = r.getRewCharacter();

        StackHead sh = new StackHead(q, a);
        if (!hasPush.contains(sh)) {
            toRemove.add(r);
        }
        // preserve the start
        StackHead start = new StackHead(r.getControl1(),
                                        r.getCharacter());
        toPreserve.add(start);


        for (RewRule r2 : cpds.getRewRulesFwd(q, a)) {
            RewRule rnew = new RewRule(r2);
            mergeR1IntoR2(r, rnew);
            // as long as we're not replacing the same rule by itself
            if (!rnew.equals(r2)) {
                toAdd.add(rnew);
            }
            toRemove.add(r2);

            // preserve the end
            StackHead preserve = new StackHead();
            rnew.getDestinationHead(preserve);
            toPreserve.add(preserve);
        }

        for (RewLinkRule r2 : cpds.getRewLinkRulesFwd(q, a)) {
            RewLinkRule rnew = new RewLinkRule(r2);
            mergeR1IntoR2(r, rnew);
            toAdd.add(rnew);;
            toRemove.add(r2);

            // preserve the end
            StackHead preserve = new StackHead();
            rnew.getDestinationHead(preserve);
            toPreserve.add(preserve);
        }

        for (PopRule r2 : cpds.getPopRulesFwd(q, a)) {
            PopRule rnew = new PopRule(r2);
            mergeR1IntoR2(r, rnew);
            toAdd.add(rnew);;
            toRemove.add(r2);
        }

        for (CollapseRule r2 : cpds.getCollapseRulesFwd(q, a)) {
            CollapseRule rnew = new CollapseRule(r2);
            mergeR1IntoR2(r, rnew);
            toAdd.add(rnew);;
            toRemove.add(r2);
        }

        for (GetArgRule r2 : cpds.getGetArgRulesFwd(q, a)) {
            GetArgRule rnew = new GetArgRule(r2);
            mergeR1IntoR2(r, rnew);
            toAdd.add(rnew);
            toRemove.add(r2);
        }

        for (GetOrder0ArgRule r2 : cpds.getGetOrder0ArgRulesFwd(q, a)) {
            GetOrder0ArgRule rnew = new GetOrder0ArgRule(r2);
            mergeR1IntoR2(r, rnew);
            toAdd.add(rnew);
            toRemove.add(r2);
        }

        for (AltRule r2 : cpds.getAltRulesFwd(q, a)) {
            AltRule rnew = new AltRule(r2);
            mergeR1IntoR2(r, rnew);
            toAdd.add(rnew);
            toRemove.add(r2);
        }

    }

    /**
     * Merges r1 into r2, updating r2 to have r1's initial control and character
     *
     * @param r1 the rewrite rule on the left
     * @param r2 the rule on the right
     */
    private void mergeR1IntoR2(RewRule r1, Rule r2) {
        r2.setControl1(r1.getControl1());
        r2.setCharacter(r1.getCharacter());
    }



    /**
     * Removes and adds the given rules
     *
     * Note: removes all of toAdd from toRemove, side-effect
     * (Thus removes, then adds.)
     *
     * @param toAdd add these new rules
     * @param toRemove remove these rules
     * @return true if update changes
     */
    private boolean doUpdate(Set<Rule> toAdd,
                             Set<Rule> toRemove) {
        toRemove.removeAll(toAdd);

        for (Rule r : toRemove) {
            cpds.removeRule(r);
        }

        for (Rule r : toAdd) {
            cpds.addRule(r);
        }

        return toRemove.size() > 0 || toAdd.size() > 0;
    }

}
