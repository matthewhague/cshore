/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.minimisers;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.HashSet;

import org.apache.log4j.Logger;


import ho.structures.cpds.*;

import ho.managers.Managers;

// A simple forwards reachability analysis that creates a map
//
//     (q, a) --> set of rules that can be used to reach head (q, a)
//

public class CoarseOrder0RuleElimination {
    static Logger logger = Logger.getLogger(CoarseOrder0RuleElimination.class);

    // table of heads to rules and characters that might be used/seen on a path
    // to them
    Map<ControlState, Set<Rule>> csTable
        = new HashMap<ControlState, Set<Rule>>();

    Set<ControlState> worklist = new HashSet<ControlState>();

    Cpds cpds;

    public CoarseOrder0RuleElimination() { }


    /**
     * @param cpds the cpds to minimise
     * @param initHeads initial heads
     * @return a cpds that only has rules that might occur on a path from
     *         <qinit, [ainit]> (in initHeads) to <qerror, *> (for some stack *).
     */
    public Cpds minimise(Cpds cpds, Set<StackHead> initHeads) {
        this.cpds = cpds;
        csTable.clear();

        for (StackHead head : initHeads)
            processControl(head.getControl());

        while (!worklist.isEmpty()) {
            Iterator i = worklist.iterator();
            ControlState q = (ControlState)i.next();
            i.remove();
            processControl(q);
        }

        Cpds newCpds = new Cpds();

        ControlState qerror = Managers.csManager.makeControl(Cpds.error_state);

        for (Rule r : getAddEntry(qerror))
            newCpds.addRule(r);

        return newCpds;
    }


    private Set<Rule> getAddEntry(ControlState q) {
        Set<Rule> rs = csTable.get(q);
        if (rs == null) {
            rs = new HashSet<Rule>();
            csTable.put(q, rs);
        }
        return rs;
    }

    /**
     * Notes that the rule might be used on a path to the given head.  Adds <q,
     * a> to the worklist if the addition resulted in a change.
     *
     * @param q  a control state
     * @param r  the rule that was used to get here
     * @param newRs the rules that might have been used on a path to
     *              the state q (excluding r)
     */
    private void updateEntry(ControlState q,
                             Rule r,
                             Set<Rule> newRs) {
        Set<Rule> rs = getAddEntry(q);

        boolean change = false;
        change |= rs.add(r);
        change |= rs.addAll(newRs);

        if (change)
            worklist.add(q);
    }


    private void processControl(ControlState q) {

        for (SCharacter a : cpds.getCharacters()) {
            for (PushRule r : cpds.getPushRulesFwd(q, a))
                processRule(r);
            for (PopRule r : cpds.getPopRulesFwd(q, a))
                processRule(r);
            for (CPushRule r : cpds.getCPushRulesFwd(q, a))
                processRule(r);
            for (CollapseRule r : cpds.getCollapseRulesFwd(q, a))
                processRule(r);
            for (RewRule r : cpds.getRewRulesFwd(q, a))
                processRule(r);
            for (RewLinkRule r : cpds.getRewLinkRulesFwd(q, a))
                processRule(r);
            for (GetArgRule r : cpds.getGetArgRulesFwd(q, a))
                processRule(r);
            for (GetOrder0ArgRule r : cpds.getGetOrder0ArgRulesFwd(q, a))
                processRule(r);
            for (EvalNonTermRule r : cpds.getEvalNonTermRulesFwd(q, a))
                processRule(r);
            for (AltRule r : cpds.getAltRulesFwd(q, a))
                processAltRule(r);
        }
    }


    private void processRule(Rule r) {
        ControlState q1 = r.getControl1();
        ControlState q2 = r.getControl2();
        updateEntry(q2, r, getAddEntry(q1));
    }

    private void processAltRule(AltRule r) {
        ControlState q1 = r.getControl1();
        for (StackHead h : r.getDestinations()) {
            ControlState q2 = h.getControl();
            updateEntry(q2, r, getAddEntry(q1));
        }
    }
}
