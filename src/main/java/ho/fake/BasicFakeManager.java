/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.fake;

import java.util.Map;
import ho.fake.target.O1TargetComplete;

import org.apache.log4j.Logger;

import ho.fake.target.SATransGetter;
import ho.fake.target.TargetCompleteAdder;
import ho.fake.target.TargetProcessor;
import ho.lazystates.LazyState;
import ho.lazystates.LazyStateO1;
import ho.managers.Managers;
import ho.structures.cpds.CPushRule;
import ho.structures.cpds.PushRule;
import ho.structures.cpds.Rule;
import ho.structures.cpds.SCharacter;
import ho.structures.cpds.AltRule;
import ho.structures.sa.SAState;
import ho.structures.sa.SATransition;
import ho.structures.sa.SATransitionO1;
import ho.util.ManagedSet;
import ho.util.MapFactory;
import ho.util.Pair;
import ho.util.QuadMap;
import ho.util.Triple;
import ho.util.TripleMap;
import ho.util.UnsafeDoubleMultiMap;
import ho.util.UnsafeMultiMap;
import ho.util.UnsafeMultiMapToQuad;
import ho.fake.target.SimpleTargetProcessor;
import ho.util.UnsafeMultiMapToDouble;
import ho.util.UnsafeMultiMapToTriple;
import ho.witness.PrincipalJustification;



/**
 * FakeManager keeps track of completed targets and sources and executes the
 * call back to add transitions.
 *
 */
public class BasicFakeManager implements TargetCompleteAdder, FakeManager {

    private final ManagedSet<SATransitionO1> emptyO1TranSet = Managers.satransitionO1Manager.makeEmptySet();

    private Logger logger = Logger.getLogger(BasicFakeManager.class);

    private O1TargetComplete disposableO1TargetComplete = new O1TargetComplete(null, null, null);

    /**
     *  Keep track of complete targets for states of order > 1, indexed
     *  by their initLHS and then with (labellingStates, dest) in image of
     *  the map.
     */
    private UnsafeMultiMapToDouble<ManagedSet<SAState>, ManagedSet<SAState>, ManagedSet<SAState>>
        completeTargetTable = MapFactory.newUnsafeMapToDouble();

    /**
     * Target tables for order-1 (as above) indexed by stack alphabet elements. (As usual,
     * labellingStates are the CollapseStates at order-1).
     */
    private Map<SCharacter, UnsafeMultiMap<ManagedSet<SAState>, O1TargetComplete>>
    completeTargetTablesO1 = MapFactory.newMap();

    /*
     * Keep track of sources of order > 1. A source from state represented by LazyState q
     * to a set Q, associated with order-1 transitions with stack character a and
     * via labelling state q' (which might be null if no such labelling state required)
     * is represented by a map Q -> (q, q', a).
     *
     * (note that a does not constrain order->1 transitions in any way... it is only reserved
     * to be propagaged down to level-1).
     */
    private UnsafeMultiMapToTriple<ManagedSet<SAState>, LazyState, SAState, SCharacter>
        sourceTable = MapFactory.newUnsafeMapToTriple();

    /*
     * Keep track of sources for order = 1. A source from LazyState q to a set Q
     * with stack character a is represented by map Q -> (q, a).
     *
     */
    private UnsafeDoubleMultiMap<ManagedSet<SAState>, SCharacter, LazyStateO1>
        sourceTableO1 = MapFactory.newUnsafeDoubleMultiMap();


    /*
     * Maps mapping sources (in same order as components are recorded in tables above)
     * onto justification for them being added. We keep separate from tables above
     * as justifying rule is not a "defining feature" of a source.
     *
     */
    private QuadMap<ManagedSet<SAState>, LazyState, SAState, SCharacter, PrincipalJustification>
    sourceJustificationTable = MapFactory.newQuadMap();

    private TripleMap<ManagedSet<SAState>, SCharacter, LazyStateO1, PrincipalJustification>
        sourceJustificationTableO1 = MapFactory.newTripleMap();

    /**********/

    private TargetProcessor targetProcessor;

    /**
     *
     * @param transGetter callback for getting already added transitions
     * @param order the order of the cpds we're managing for
     */
    public BasicFakeManager(SATransGetter transGetter) {
        this.targetProcessor = new SimpleTargetProcessor(this, transGetter);
    }

    public void addFake(LazyState q, SCharacter a, SAState qtran,
            ManagedSet<SAState> Q, PrincipalJustification justif) {
        //String msg = "Adding Fake q: " + (q == null? "null" : q.toString())
        //        + "\n a : " + (a == null? "null" : a.toString())
        //        + "\n qtran : " + (qtran == null? "null" : qtran.toString())
        //        + "\n Q: " + (Q == null? "null" : Q.toString());
        // logger.debug(msg + "\n");
        //NB Order is important. Need to add source before add target
        //in case adding target results in new complete targets (possible
        //for more advanced implementations of target management, exploiting
        //subsets etc) which might then need firing against the source.
        if(Q.isEmpty()) {
            //If countdown is empty, no point in wasting time
            // adding stuff to maps etc... can create next
            // fake immediately.
            ManagedSet<SAState> newCountDown = (qtran == null) ?
                    Q
                    : Managers.sManager.makeSingleton(qtran);
            if(q.getStateOrder() > 2)
                addFake(q.nextLazyState(Q), a, null, newCountDown, justif);
            else
                addFakeO1(q.nextLazyStateO2(Q), a, newCountDown, justif);
        }
        else {
            addSource(Q, q, qtran, a, justif);
            targetProcessor.addTarget(Q);
        }
    }


    public void addFakeO1(LazyStateO1 q, SCharacter a,
            ManagedSet<SAState> Q, PrincipalJustification justif) {

        if(Q.isEmpty())
            // don't waste time adding stuff to fake tables etc, as can create
            // immediately. (Justifying set of O1-transitions should be empty rather than null
            // as will still originate from a push transition).
            q.complete(a, Q, Q, justif, emptyO1TranSet);
        else {
            addSourceO1(Q, a, q, justif);
            targetProcessor.addTargetO1(a, Q);
        }

    }

    public void addTargetComplete(ManagedSet<SAState> initLHS,
            ManagedSet<SAState> labellingStates, ManagedSet<SAState> dest) {
        if(addCompleteTarget(initLHS, labellingStates, dest)) {
            Iterable<Triple<LazyState, SAState, SCharacter>> sources = sourceTable.get(initLHS);
            if(sources != null)
                for(Triple<LazyState, SAState, SCharacter> source : sources) {
                    LazyState q = source.first();
                    SAState   sourceLabel = source.second();
                    SCharacter a = source.third();
                    PrincipalJustification justif = sourceJustificationTable.get(initLHS, q, sourceLabel, a);
                    fireCompleteTargetAgainstSource(q, sourceLabel, a, labellingStates, dest, justif);
                }
        }
    }

    public void addTargetCompleteO1(SCharacter sChar,
            ManagedSet<SAState> initLHS, ManagedSet<SAState> labellingStates,
            ManagedSet<SAState> dest, ManagedSet<SATransitionO1> justif) {


        if(addCompleteTargetO1(sChar, initLHS, labellingStates, dest, justif)) {
            Iterable<LazyStateO1> lazyStates =  sourceTableO1.get(initLHS, sChar);


            if(lazyStates != null)
                for (LazyStateO1 q : lazyStates) {
                    PrincipalJustification princJustif = sourceJustificationTableO1.get(initLHS, sChar, q);
                    q.complete(sChar, labellingStates, dest, princJustif, justif );
                }
        }
    }


    public void updateFake(SATransition t) {
        targetProcessor.update(t);
    }

    public void updateFakeO1(SATransitionO1 t) {
            targetProcessor.updateO1(t);
    }

    /**
     *
     * @param initLHS
     * @param labellingStates
     * @param dest
     * @return    True iff complete target not previously added
     */
    private final boolean addCompleteTarget(ManagedSet<SAState>initLHS,
            ManagedSet<SAState>labellingStates, ManagedSet<SAState> dest) {
        Boolean alreadyExists = completeTargetTable.hasTuple(initLHS, labellingStates, dest);
        if(!alreadyExists)
            completeTargetTable.put(initLHS, labellingStates, dest);

        return !alreadyExists;
    }

    private final boolean addCompleteTargetO1(SCharacter a,
            ManagedSet<SAState> initLHS, ManagedSet<SAState> labellingStates,
            ManagedSet<SAState> dest, ManagedSet<SATransitionO1> justif) {

        UnsafeMultiMap<ManagedSet<SAState>, O1TargetComplete>
            completeTargetTableO1;


        if(completeTargetTablesO1.containsKey(a))
            completeTargetTableO1 = completeTargetTablesO1.get(a);
        else {
            completeTargetTableO1 = MapFactory.newUnsafeMultiMap();
            completeTargetTablesO1.put(a, completeTargetTableO1);
        }
        disposableO1TargetComplete.set(initLHS, labellingStates, dest);
        Boolean alreadyExists = completeTargetTableO1.hasInImage(disposableO1TargetComplete);
        if(!alreadyExists)
            completeTargetTableO1.put(initLHS, new O1TargetComplete(initLHS, labellingStates, dest));

        return !alreadyExists;

    }

    /**
     * Add source and process against existing completed targets, if new.
     * Assumes order > 1 source.
     * @param rhs
     * @param q
     * @param labellingState
     * @param a
     * @param justif    Justification for creating this source. (Will not change current
     *                     justification of a source if source already exists).
     */
    private final void addSource(ManagedSet<SAState> rhs, LazyState q, SAState labellingState,
            SCharacter a, PrincipalJustification justif) {
        assert(q.getStateOrder() > 1) : "Expected order of state to be > 1";
        if(!sourceTable.hasTuple(rhs, q, labellingState, a)) {
            sourceTable.put(rhs, q, labellingState, a);
            sourceJustificationTable.put(rhs, q, labellingState, a, justif);

            Iterable<Pair<ManagedSet<SAState>, ManagedSet<SAState>>> completeTargets
                = completeTargetTable.get(rhs);

            if(completeTargets != null)
                for(Pair<ManagedSet<SAState>, ManagedSet<SAState>> completeTarget :
                    completeTargetTable.get(rhs))
                    fireCompleteTargetAgainstSource(q, labellingState, a, completeTarget.first(),
                            completeTarget.second(), justif);

        }
        return;
    }

    /**
     * Add source (order 1) and process against existin completed targets, if new.
     * @param rhs
     * @param a
     * @param q
     * @param rule justifying rule (will not change justification of existing source; only recorded if source new).
     */
    private final void addSourceO1(ManagedSet<SAState> rhs, SCharacter a, LazyStateO1 q,
            PrincipalJustification justif) {
        if (!sourceTableO1.hasTuple(rhs, a, q)) {
            sourceTableO1.put(rhs, a, q);
            sourceJustificationTableO1.put(rhs, a, q, justif);

            UnsafeMultiMap<ManagedSet<SAState>, O1TargetComplete>
                targetMap = completeTargetTablesO1.get(a);
            Iterable<O1TargetComplete> completeTargets = null;
            if (targetMap != null)
                completeTargets = targetMap.get(rhs);

            if(completeTargets != null)
                for(O1TargetComplete completeTarget :
                    completeTargets) {
                    q.complete(a, completeTarget.getLabellingStates(), completeTarget.getDest(), justif,
                            completeTarget.getJustifSet());
                }

        }
            return;
    }

    /**
     * Make the necessary updates (inform LazyState of new transition to be created and
     * create a new fake) corresponding to a completed target and its source. Only
     * for sources for states of order > 1
     * @param q                LHS of source
     * @param sourceLabel    label of source transition (can be null if no such additional label)
     * @param a                Stack character associated with source
     * @param labellingStates    Labelling states assimilated in completed target
     * @param dest                RHS states assimialted in completed target
     * @param justif        PrincipalJustification for the source.
     *
     */
    private final void fireCompleteTargetAgainstSource(
            LazyState q, SAState sourceLabel, SCharacter a,
            ManagedSet<SAState> labellingStates, ManagedSet<SAState> dest,
            PrincipalJustification justif) {
        /*
         * The new transition from this completed-target/source pair
         * q--sourceLabel-->initLHS-- labellingStates-->dest
         * is
         * q--newState-->dest
         * (using terminology including that defined below... in particular
         * newState is unique label for q--->dest transition).
         *
         * We need to iterate fake construction down to the new level
         * using newState and labellingStates + {sourceLabel}.
         */
        assert(q.getStateOrder() > 1) : "Expected state of order > 1";
        ManagedSet<SAState> newLabellingStates =
            (sourceLabel == null) ?
                    labellingStates
                    : Managers.sManager.add(labellingStates, sourceLabel);



        if(q.getStateOrder() > 2) {
            LazyState newState = q.nextLazyState(dest);
            this.addFake(newState, a, null, newLabellingStates, justif);
        }
        else {
            LazyStateO1 newState = q.nextLazyStateO2(dest);
            this.addFakeO1(newState, a, newLabellingStates, justif);
        }
        return;
    }

    /**
     * Resets the manager
     *
     * @param transGetter new trans getter
     */
    public void reset(SATransGetter transGetter) {
        completeTargetTable = MapFactory.newUnsafeMapToDouble();
        completeTargetTablesO1 = MapFactory.newMap();
        sourceTable = MapFactory.newUnsafeMapToTriple();
        sourceTableO1 = MapFactory.newUnsafeDoubleMultiMap();
        targetProcessor = new SimpleTargetProcessor(this, transGetter);
        sourceJustificationTable = MapFactory.newQuadMap();
        sourceJustificationTableO1 = MapFactory.newTripleMap();
    }

    public void setOrder(byte order) {
        targetProcessor.setOrder(order);
    }




}
