/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.fake;

import ho.fake.target.SATransGetter;

import ho.lazystates.LazyState;
import ho.lazystates.LazyStateO1;
import ho.structures.sa.SATransition;
import ho.structures.sa.SAState;
import ho.structures.sa.SATransitionO1;
import ho.structures.cpds.CPushRule;
import ho.structures.cpds.PushRule;
import ho.structures.cpds.SCharacter;
import ho.structures.cpds.Cpds;
import ho.util.ManagedSet;
import ho.witness.PrincipalJustification;



/**
 * A FakeProcessor provides the interface to the main algorithm for adding
 * fake transitions. It is responsible for initiating the creation of targets
 * and sources.
 */
public interface FakeManager {

	/**
	 * @param t Newly processed transition with which fake rules need updating
	 *
	 */
	public void updateFake( SATransition t );

	public void updateFakeO1 (SATransitionO1 t);

	/**
	 * Adds a fake rule for order k > 1
	 * @param q		Description of order-k state of fake transition
	 * @param a	Stack symbol to be propagating to level 1
	 * @param qtran Order-(k-1) state to collect at level below
	 *				This may also be set to null if no such
	 *				state needs to be propagated, but from outside the fake world
	 *				I don't think this will ever be the case.
	 * @param Q	Set of order-k states as target of fake transition.
	 * @param justif	The principal justification for adding the fake.
	 *
	 */
	public void addFake(LazyState q,
			            SCharacter a,
                        SAState qtran,
                        ManagedSet<SAState> Q,
                        PrincipalJustification justif);



	/**
	 * Adds fake rule for order-1
	 * @param q		Description of initial order-1 state of fake transition
	 * @param a		Stack symbol being read by transitions
	 * @param Q		Set of order-1 states as target of fake transition
	 * @param justif	The Principal justification for adding the fake
	 */
	public void addFakeO1(LazyStateO1 q,
                          SCharacter a,
			              ManagedSet<SAState> Q,
                          PrincipalJustification justif);

    /**
     * Resets the manager
     *
     * @param transGetter new trans getter
     */
    public void reset(SATransGetter transGetter);

    /**
     * Sets the order of the manager
     *
     * @param order the order
     */
    public void setOrder(byte order);

}

