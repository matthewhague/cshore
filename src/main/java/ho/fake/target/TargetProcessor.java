/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.fake.target;


import ho.structures.cpds.SCharacter;
import ho.structures.sa.SAState;
import ho.structures.sa.SATransition;
import ho.structures.sa.SATransitionO1;
import ho.util.ManagedSet;


/**
 *
 * A TargetProcessor provides interface to outside-world for adding targets.
 * Completeted targets (i.e. those whose countDown has been rendered empty)
 * are reported to the outside world via a callback provided by an
 * implementation of TargetCompleteAdder.
 *
 */
public interface TargetProcessor {

	/**
	 * Used to create a new target with a particular starting point for order > 1. This should in turn
	 * be compared against transitions already processed and added to the stack automaton
	 * (which are retrieved using a callback provided by SATransGetter) and should create
	 * new targets, as necessary, in the light of this, including using callback provided
	 * by TargetCompleteAdder to report any newly completed targets.
	 * @param Q	The starting point for a new fake rule, specifying the set of states
	 *			that must be collected in the LHSs of the transitions being assimilated
	 *			in order to create a new fake rule. This set *must* be non-empty as we
	 *			are assuming we only want fake constuctions of the form q-->Q-->Q' where
	 *			Q is non-empty.  For order > 1
	 */
	public void addTarget( ManagedSet<SAState> Q );

	/**
	 * Version of addTarget for states of order = 1, must also supply stack character
	 * with which this target will be associated.
	 */
	public void addTargetO1( SCharacter sChar, ManagedSet<SAState> Q );



	/**
	 * Update the targets of the fakes with a particular stack automaton transition
	 * that is being newly processed. Any completed targets will be provided by a
	 * callback. For order >1
	 * @param tran		The new stack automaton transition of order > 1
	 */
	public void update( SATransition tran );


	/**
	 * Version for order-1 transitions.
	 * @param tran		The new stack automaton transition of order = 1
	 */
	public void updateO1 ( SATransitionO1 tran );

    /**
     * @param order the order of the manager (this is needed because order-n
     * transitions are restricted to singletons)
     */
    public void setOrder(byte order);
}
