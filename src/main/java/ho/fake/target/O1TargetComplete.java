/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.fake.target;

import ho.managers.Managers;
import ho.structures.sa.SAState;
import ho.structures.sa.SATransitionO1;
import ho.util.ManagedSet;


/**
 * Keeps track of  completed order-1 targets during Fake Management:
 * (initLHS, labellingStates, dest) and also annotates with justification.
 *
 */
public class O1TargetComplete {
	private ManagedSet<SAState> initLHS;

	private ManagedSet<SAState> labellingStates;
	private ManagedSet<SAState> dest;

	private int hashCode;

	// This does not form part of definition of equality, and is an annotation
	// for collecting O1-transitions responsible for (partially) completing this target.
	// We make this a managedSet to indicate intention to be immutable; we
	// will be copying set and adding to it etc.

	private ManagedSet<SATransitionO1> justifSet = Managers.satransitionO1Manager.makeEmptySet();

	public O1TargetComplete(ManagedSet<SAState> initLHS,
			ManagedSet<SAState> labellingStates, ManagedSet<SAState> dest) {
		super();
		this.initLHS = initLHS;

		this.labellingStates = labellingStates;
		this.dest = dest;

		final int prime = 31;
		int result = 1;

		result = prime * result + ((dest == null) ? 0 : dest.hashCode());
		result = prime * result + ((initLHS == null) ? 0 : initLHS.hashCode());
		result = prime * result
				+ ((labellingStates == null) ? 0 : labellingStates.hashCode());
		this.hashCode = result;
	}

	public void setJustifSet(ManagedSet<SATransitionO1> justifSet) {
		this.justifSet = justifSet;
	}


	public ManagedSet<SAState> getInitLHS() {
		return initLHS;
	}








	public ManagedSet<SAState> getLabellingStates() {
		return labellingStates;
	}




	public ManagedSet<SAState> getDest() {
		return dest;
	}







	public ManagedSet<SATransitionO1> getJustifSet() {
		return justifSet;
	}




	@Override
	public int hashCode() {
		return this.hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		O1TargetComplete other = (O1TargetComplete) obj;

		if (dest == null) {
			if (other.dest != null)
				return false;
		} else if (!dest.equals(other.dest))
			return false;
		if (initLHS == null) {
			if (other.initLHS != null)
				return false;
		} else if (!initLHS.equals(other.initLHS))
			return false;
		if (labellingStates == null) {
			if (other.labellingStates != null)
				return false;
		} else if (!labellingStates.equals(other.labellingStates))
			return false;
		return true;
	}


	/**
	 * Method only to be used when checking for membership of sets via "reusable object"
	 * (Note in particular that "justif" is irrelevant for this as not involved in equality checking).
	 *
	 */
	public void set(ManagedSet<SAState> initLHS,
			ManagedSet<SAState> labellingStates, ManagedSet<SAState> dest) {

		this.initLHS = initLHS;

		this.labellingStates = labellingStates;
		this.dest = dest;


		final int prime = 31;
		int result = 1;

		result = prime * result + ((dest == null) ? 0 : dest.hashCode());
		result = prime * result + ((initLHS == null) ? 0 : initLHS.hashCode());
		result = prime * result
				+ ((labellingStates == null) ? 0 : labellingStates.hashCode());
		this.hashCode = result;
	}

}
