/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.fake.target;

import java.util.HashSet;
import ho.util.UnsafeMultiMapToQuin;
import java.util.Map;

import org.apache.log4j.Logger;

import ho.structures.cpds.SCharacter;
import ho.structures.sa.SAState;
import ho.structures.sa.SATransitionO1;
import ho.util.Quad;
import ho.util.UnsafeMultiMap;
import ho.util.UnsafeMultiMapToQuad;
import ho.util.ManagedSet;
import ho.util.MapFactory;


/**
 *
 *
 *  (We now have an O1Target object as need to keep track of O1-transitions
 * that have partially completed a target but this does not form part of its identity
 * so Quad is insufficient... However this target object does not contain information
 * about the stack character as this is catered for by TargetTableO1... in effect
 * O1Target is a Quad as before together with an annotation of O1-states.).
 *
 * ...so... COMMENT BELOW IS OBSOLETE for O1-targets (but still applies for higher-order targets)...
 *
 * For performance reasons (to avoid overhead of allocating objects for each target)
 * targets only exist `logically' in sets/maps. (I actually think our TripleHashMap
 * implementation might eventually amount to a HashSet of `explicit targets (quadruples)
 * but this perhaps gives a bit more flexibility).
 * A target is really a quadruple (initLHS, countDown, labellingStates, dest)
 * all of type ManagedSet<SAState>. initLHS is the `starting point' (which should
 * always be non-empty...otherwise there was no point in creating a target), which is
 * what countDown begins with. As transitions are collected, their LHSs should be removed from
 * countDown  (by TargetProcessor) and their labellingStates collected in labellingStates
 * and RHSs collected in dest. Both of these last two should begin empty.
 *
 * When countDown becomes empty, the target is referred to as `complete'; it is not stored
 * by this manager but rather when created by TargetProcessor should be reported using
 * a callback provided by TargetCompleteAdder.
 *
 * Elements returned by this should be viewed as immutable to any utilising
 * class.
 *
 * Ideally would using type aliasing with quadruples for Target s, but in absence of this
 * feature in Java, using a Target class that extends Quad and upcasting... hoping for optimiser magic!
 *
 */
class TargetManager {
    private Logger logger = Logger.getLogger(TargetManager.class);

    static private final byte UNKNOWN_ORDER = -1;

    /*
     * Keep track of incomplete targets have already been added. Indexed by states in
     * their countdown (domain of map) and quadruples representing targets in image.
     * We do not track complete targets as this should be handled by callback.
     */
    private UnsafeMultiMapToQuad< SAState, ManagedSet<SAState>, ManagedSet<SAState>, ManagedSet<SAState>,
    ManagedSet<SAState> > targetTable
        = MapFactory.newUnsafeMapToQuad();

    /*
     * For Order-1 transitions we need to group by stack character (as this restricts
     * which transitions are collected).
     */
    private Map<SCharacter, UnsafeMultiMap< SAState, O1Target>> targetTablesO1
        = MapFactory.newMap();

    private Iterable<Quad<ManagedSet<SAState>, ManagedSet<SAState>, ManagedSet<SAState>,
    ManagedSet<SAState>>> emptyIterable =
        new HashSet<Quad<ManagedSet<SAState>, ManagedSet<SAState>, ManagedSet<SAState>,
            ManagedSet<SAState>>>();

    private Iterable<O1Target> emptyIterableO1 =
        new HashSet<O1Target>();

    //Used for membership testing of O1Targets
    private O1Target disposableO1Target = new O1Target(null, null, null, null);

    private byte order = UNKNOWN_ORDER;


    void setOrder(byte order) {
        this.order = order;
    }


    /**
     * Update targetTable to include the specified target
     * target.
     * @param initLHS    Should be non-empty
     * @param targetTable    The targetTable to update (may be that for higher-order
     *                         transitions, or it may be one of the order-1 targetTables
     *                         associated with a particular stack character).
     * @return             True iff not previously added (i.e. is new).
     */
     private final boolean addTarget (
        ManagedSet<SAState> initLHS,
        ManagedSet<SAState> countDown,
        ManagedSet<SAState> labellingStates,
        ManagedSet<SAState> dest,
        UnsafeMultiMapToQuad<SAState,
                             ManagedSet<SAState>,
                             ManagedSet<SAState>,
                             ManagedSet<SAState>,
                             ManagedSet<SAState>> targetTable
     ) {
        assert (!initLHS.isEmpty())
        : "Trying to have an initLHS that is empty";

        boolean targetIsNew = true;
        if (targetTable.hasInImage(initLHS, countDown, labellingStates, dest))
            targetIsNew = false;
        else
            for (SAState q : countDown)
                targetTable.put(q, initLHS, countDown, labellingStates, dest);

        return targetIsNew;
    }

     /**
      * Version of addTarget for when dealing with O1-transitions
      */
     private final boolean addTarget (ManagedSet<SAState> initLHS,
                                      ManagedSet<SAState> countDown,
                                      ManagedSet<SAState> labellingStates,
                                      ManagedSet<SAState> dest,
                                      UnsafeMultiMap<SAState, O1Target> targetTable,
                                      ManagedSet<SATransitionO1> justif) {
            assert (!initLHS.isEmpty())
            : "Trying to have an initLHS that is empty";

            boolean targetIsNew = true;
            disposableO1Target.set(initLHS, countDown, labellingStates, dest);
            if (targetTable.hasInImage(disposableO1Target))
                targetIsNew = false;
            else {
                O1Target newTarget = new O1Target(initLHS, countDown, labellingStates, dest);
                newTarget.setJustifSet(justif);
                for (SAState q : countDown)
                    targetTable.put(q, newTarget);
            }

            return targetIsNew;
        }

     /**
         * Update targetTable to include the specified target
         * target when target is for states order > 1
         * @param initLHS    Should be non-empty
         * @return             True iff not previously added (i.e. is new).
         */
     final boolean addTarget ( ManagedSet<SAState> initLHS, ManagedSet<SAState> countDown,
                ManagedSet<SAState> labellingStates, ManagedSet<SAState> dest) {

         if (!filterTarget(initLHS, countDown, labellingStates, dest))
            return addTarget(initLHS,
                             countDown,
                             labellingStates,
                             dest,
                             targetTable);
        else
            return false;
     }

     /**
         * Update targetTable to include the specified target
         * target when target is for states of order = 1
         * @param initLHS    Should be non-empty
         * @param sChar  Stack character to assicate with this target
         * @return             True iff not previously added for this stack character(i.e. is new
         *                     for this stack character).
         */
    final boolean addTargetO1 ( SCharacter sChar, ManagedSet<SAState> initLHS, ManagedSet<SAState> countDown,
               ManagedSet<SAState> labellingStates, ManagedSet<SAState> dest, ManagedSet<SATransitionO1> justif) {
        if (!filterTarget(initLHS, countDown, labellingStates, dest)) {
            UnsafeMultiMap< SAState, O1Target > targetTableToUse
               = targetTablesO1.get(sChar);

            if(targetTableToUse == null) {
                targetTableToUse = MapFactory.newUnsafeMultiMap();
                targetTablesO1.put(sChar, targetTableToUse);
            }


            int size = 0;
            for (UnsafeMultiMap< SAState, O1Target > tt : targetTablesO1.values()) {
                size += tt.size();
            }

            boolean added = addTarget(initLHS,
                                      countDown,
                                      labellingStates,
                                      dest,
                                      targetTableToUse,
                                      justif);

            return added;
        } else {
            return false;
        }
    }

    /**
     * The result of this function should *not* be modified (i.e. should
     * be treated as immuatable).
     * @param q     An SAState order > 1
     * @return        Iterable of all existing targets that still have
     *                 a state q in their countdown.
     */
    final Iterable<Quad<ManagedSet<SAState>, ManagedSet<SAState>, ManagedSet<SAState>,
        ManagedSet<SAState>>>  getTargets( SAState q ) {

        Iterable<Quad<ManagedSet<SAState>, ManagedSet<SAState>, ManagedSet<SAState>,
        ManagedSet<SAState>>> toReturn = targetTable.get(q);
        if(toReturn == null)
            return emptyIterable;
        else
            return toReturn;
    }

    /**
     * The result of this function should *not* be modified (i.e. should
     * be treated as immuatable).
     * @param q     An SAState order = 1
     * @param sChar A stack character
     * @return        Iterable of all existing targets associated with sChar
     *                 that still have
     *                 a state q in their countdown.
     */
    final Iterable<O1Target>  getTargetsO1( SAState q, SCharacter sChar  ) {

        Iterable<O1Target> toReturn;

        UnsafeMultiMap< SAState, O1Target >
                firstLevel = targetTablesO1.get(sChar);

        if (firstLevel == null)
            toReturn = emptyIterableO1;
        else {
            toReturn = firstLevel.get(q);
            if (toReturn == null)
                toReturn = emptyIterableO1;
        }
        return toReturn;
    }


    /**
     * @param initLHS    Should be non-empty
     * @return             True iff not previously added (i.e. is new).
     */
    final boolean filterTarget(ManagedSet<SAState> initLHS,
                               ManagedSet<SAState> countDown,
                               ManagedSet<SAState> labellingStates,
                               ManagedSet<SAState> dest) {
        // Don't filter size > 1, because we might be alternating...
        // TODO: check whether we're alternating...

        //if (order != UNKNOWN_ORDER &&
        //    dest.size() > 1) {
        //    boolean filter = dest.choose().getStateOrder() == order;
        //    if (filter)
        //        logger.debug("Filter!");
        //    return filter;
        //}
        return false;
    }


}
