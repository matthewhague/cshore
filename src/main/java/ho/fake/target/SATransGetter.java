/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.fake.target;



import ho.structures.sa.SAState;
import ho.structures.cpds.SCharacter;
import ho.structures.sa.SATransition;
import ho.structures.sa.SATransitionO1;


/**
 * A callback to get already added transitions.
 *
 */
public interface SATransGetter {
	 /**
     * @param q the source state of the transitions to iterate over order > 1
     * @return an iterator over the transitions from state q that have
     *			already been processed and added to the automaton
     */
	public Iterable<SATransition> oldTranIterator ( SAState q );

	/**
     * @param q the source state of the transitions to iterate over order = 1
     * @param a the labelling character of the transitions to iterate over
     * @return an iterator over the transitions from state q that have
     *			already been processed and added to the automaton
     */
	public Iterable<SATransitionO1> oldTranO1Iterator ( SAState q, SCharacter a );
}
