/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.fake.target;

import org.apache.log4j.Logger;

import ho.managers.Managers;
import ho.structures.cpds.SCharacter;
import ho.structures.sa.SAState;
import ho.structures.sa.SATransition;
import ho.structures.sa.SATransitionO1;
import ho.util.ManagedSet;
import ho.util.Quad;



public class SimpleTargetProcessor implements TargetProcessor {
	private final ManagedSet<SAState> emptyStateSet = ho.managers.Managers.sManager.makeEmptySet();
	private final ManagedSet<SATransitionO1> emptyTranO1Set = ho.managers.Managers.satransitionO1Manager.makeEmptySet();
	private final SATransGetter sATransGetter;
	private final TargetCompleteAdder targetCompleteAdder;
	private final TargetManager targetManager;
	private final Logger logger = Logger.getLogger(SimpleTargetProcessor.class);


	public SimpleTargetProcessor(TargetCompleteAdder targetCompleteAdder,
                                 SATransGetter sATransGetter) {
		this.targetCompleteAdder = targetCompleteAdder;
		this.sATransGetter = sATransGetter;
		this.targetManager = new TargetManager();
	}

    /**
     * @param order the order to set the manager to
     */
    public void setOrder(byte order) {
        targetManager.setOrder(order);
    }

	public void addTarget ( ManagedSet<SAState> Q ) {
		assert ( !Q.isEmpty() ) : "Should only attempt to add targets starting" +
								"with non-empty countDown";
		if(targetManager.addTarget(Q, Q, emptyStateSet, emptyStateSet ))
			processTarget( Q, Q, emptyStateSet, emptyStateSet  );
		return;
	}

	public void addTargetO1(SCharacter sChar, ManagedSet<SAState> Q) {
		assert ( !Q.isEmpty() ) : "Should only attempt to add targets starting" +
									"with non-empty countDown";
		if(targetManager.addTargetO1(sChar, Q, Q, emptyStateSet, emptyStateSet, emptyTranO1Set ))
			processTargetO1( sChar, Q, Q, emptyStateSet, emptyStateSet, emptyTranO1Set  );
		return;

	}

	public final void update( SATransition tran ) {
		for(  Quad<ManagedSet<SAState>, ManagedSet<SAState>, ManagedSet<SAState>, ManagedSet<SAState>>
			target : targetManager.getTargets(tran.getSource()) )
			updateTargetAgainstTransition( tran, target.first(), target.second(),
					target.third(), target.fourth(), false);
		return;
	}

	public void updateO1(SATransitionO1 tran) {
		for(  O1Target target : targetManager.getTargetsO1(tran.getSource(), tran.getLabellingCharacter()))

			updateTargetAgainstTransitionO1( tran, target.getInitLHS(), target.getCountDown(),
					target.getLabellingStates(), target.getDest(), target.getJustifSet(), false);

		return;

	}





	/**
	 * Used when a new target is created that needs to be compared against transitions
	 * already added to the stack automaton, which might lead to the creation of further
	 * targets.
	 * @Input parameters... describe target to be added, order > 1.
	 */
	private final void processTarget( ManagedSet<SAState> initLHS, ManagedSet<SAState> countDown,
			ManagedSet<SAState> labellingStates, ManagedSet<SAState> dest) {
		assert (!countDown.isEmpty()) : "expected incomplete target but got complete target";
		for (SAState q : countDown)
			for (SATransition tran : sATransGetter.oldTranIterator(q))
				updateTargetAgainstTransition( tran, initLHS, countDown, labellingStates,
						dest, true );
		return;
	}


	/**
	 * Used when a new target is created that needs to be compared against transitions
	 * already added to the stack automaton, which might lead to the creation of further
	 * targets.
	 * @Input parameters... describe target to be added, order = 1.
	 */
	private final void processTargetO1( SCharacter sChar,  ManagedSet<SAState> initLHS,
			ManagedSet<SAState> countDown,
			ManagedSet<SAState> labellingStates, ManagedSet<SAState> dest, ManagedSet<SATransitionO1> justif) {
		assert (!countDown.isEmpty()) : "expected incomplete target but got complete target";
		for (SAState q : countDown)
			for (SATransitionO1 tran : sATransGetter.oldTranO1Iterator(q, sChar))
                updateTargetAgainstTransitionO1( tran,
                                                 initLHS,
                                                 countDown,
                                                 labellingStates,
                                                 dest,
                                                 justif,
                                                 true );
		return;
	}

	/**
	 * Creates a new target from the supplied target in the light of the transition.
	 * If resulting target would be complete, the callback is called to report it.
	 * @param tran transition against which to update target order > 1
	 * @param target target to be updated, assumed to be incomplete
	 * @param needsProcessing	Set to true iff any new targets need
	 *							to be processed against existing transitions
	 *							in the stack automaton.
	 *
	 */
	private final void updateTargetAgainstTransition( SATransition tran,
			ManagedSet<SAState> initLHS,
			ManagedSet<SAState> countDown,
			ManagedSet<SAState> labellingStates,
			ManagedSet<SAState> dest,
			boolean needsProcessing ) {
		ManagedSet<SAState> newCountDown
		= Managers.sManager.subtract(countDown, tran.getSource());
		ManagedSet<SAState> newLabellingStates
		= Managers.sManager.add(labellingStates,
				tran.getLabellingState());
		ManagedSet<SAState> newDest
		= Managers.saManager.unionStatesets(dest, tran.getDest());

		// Remember to check that countDown wasn't already empty.
		// If it is then by assumption that don't have empty countDown
		// to begin with, we must have already done requisite processing.
		if (!countDown.isEmpty())
			if (newCountDown.isEmpty())
				targetCompleteAdder.addTargetComplete(initLHS, newLabellingStates, newDest);
			else
				if( targetManager.addTarget(initLHS, newCountDown, newLabellingStates, newDest)
						& needsProcessing )
					processTarget(initLHS, newCountDown, newLabellingStates, newDest) ;

		return;
	}

	/**
	 * Creates a new target from the supplied target in the light of the transition.
	 * If resulting target would be complete, the callback is called to report it.
	 * @param tran transition against which to update target order = 1. It is assumed
	 *				that the labelling character of this order-1 transition is that which should be
	 *				associated with
	 *				this target.
	 * @param target target to be updated, assumed to be incomplete
	 * @param justif *Current* justifying set of target to be updated (tran will be added to this set).
	 * @param needsProcessing	Set to true iff any new targets need
	 *							to be processed against existing transitions
	 *							in the stack automaton.
	 *
	 */
	private final void updateTargetAgainstTransitionO1( SATransitionO1 tran,
			ManagedSet<SAState> initLHS,
			ManagedSet<SAState> countDown,
			ManagedSet<SAState> labellingStates,
			ManagedSet<SAState> dest,
			ManagedSet<SATransitionO1> justif,
			boolean needsProcessing ) {
		// We are assuming that stack symbol uniquely determines order of any
		// link from it



		// Remember to check that countDown wasn't already empty.
		// If it is then by assumption that don't have empty countDown
		// to begin with, we must have already done requisite processing.
		if(!countDown.isEmpty()) {
			ManagedSet<SAState> newCountDown
			= Managers.sManager.subtract(countDown, tran.getSource());
			ManagedSet<SAState> newLabellingStates
			= Managers.saManager.unionStatesets(labellingStates,
					                            tran.getCollapseDest());
			ManagedSet<SAState> newDest
			    = Managers.saManager.unionStatesets(dest, tran.getDest());
			ManagedSet<SATransitionO1> newJustif = Managers.satransitionO1Manager.add(justif, tran);

			if (newCountDown.isEmpty()) {
				targetCompleteAdder.addTargetCompleteO1(tran.getLabellingCharacter(), initLHS, newLabellingStates, newDest, newJustif);
            } else {
				if( targetManager.addTargetO1(tran.getLabellingCharacter(), initLHS, newCountDown, newLabellingStates, newDest, newJustif)
						& needsProcessing ) {
					processTargetO1(tran.getLabellingCharacter(), initLHS, newCountDown, newLabellingStates, newDest, newJustif) ;
                }
            }
		}
		return;
	}
}
