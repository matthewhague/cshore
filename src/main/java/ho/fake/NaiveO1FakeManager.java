/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.fake;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;

import java.lang.StringBuilder;

import org.apache.log4j.Logger;

import ho.structures.sa.SAManager;
import ho.fake.target.SATransGetter;
import ho.fake.target.TargetCompleteAdder;
import ho.fake.target.TargetProcessor;
import ho.fake.target.O1TargetComplete;
import ho.lazystates.LazyState;
import ho.lazystates.LazyStateO1;
import ho.lazystates.LazyStateManager;
import ho.managers.Managers;
import ho.structures.cpds.CPushRule;
import ho.structures.cpds.PushRule;
import ho.structures.cpds.Rule;
import ho.structures.cpds.SCharacter;
import ho.structures.sa.SAState;
import ho.structures.sa.SATransition;
import ho.structures.sa.SATransitionO1;
import ho.util.SetManager;
import ho.util.ManagedSet;
import ho.util.MapFactory;
import ho.util.Pair;
import ho.util.QuadMap;
import ho.util.Triple;
import ho.util.TripleMap;
import ho.util.UnsafeDoubleMultiMap;
import ho.util.UnsafeMultiMap;
import ho.util.UnsafeMultiMapToQuad;
import ho.fake.target.SimpleTargetProcessor;
import ho.util.UnsafeMultiMapToDouble;
import ho.util.UnsafeMultiMapToTriple;
import ho.witness.PrincipalJustification;



/**
 * FakeManager keeps track of completed targets and sources and executes the
 * call back to add transitions.
 *
 * This is a version that does not use order-1 targets, but instead recalculates
 * (the slow way) each time
 *
 */
public class NaiveO1FakeManager implements TargetCompleteAdder, FakeManager {

    private Logger logger = Logger.getLogger(NaiveO1FakeManager.class);

    private static SAManager saManager = ho.managers.Managers.saManager;
    private static SetManager<SAState> sManager = ho.managers.Managers.sManager;
    private static SetManager<SATransitionO1> tManager
        = ho.managers.Managers.satransitionO1Manager;
    static LazyStateManager lazyStateManager
        = ho.managers.Managers.lazyStateManager;

    private final ManagedSet<SATransitionO1> emptyO1TranSet
        = Managers.satransitionO1Manager.makeEmptySet();


    /**
     *  Keep track of complete targets for states of order > 1, indexed
     *  by their initLHS and then with (labellingStates, dest) in image of
     *  the map.
     */
    private UnsafeMultiMapToDouble<ManagedSet<SAState>,
                                   ManagedSet<SAState>,
                                   ManagedSet<SAState>> completeTargetTable
        = MapFactory.newUnsafeMapToDouble();


    /*
     * Keep track of sources of order > 1. A source from state represented by
     * LazyState q to a set Q, associated with order-1 transitions with stack
     * character a and via labelling state q' (which might be null if no such
     * labelling state required) is represented by a map Q -> (q, q', a).
     *
     * (note that a does not constrain order->1 transitions in any way... it is
     * only reserved to be propagaged down to level-1).
     */
    private UnsafeMultiMapToTriple<ManagedSet<SAState>,
                                   LazyState,
                                   SAState,
                                   SCharacter> sourceTable
        = MapFactory.newUnsafeMapToTriple();


    /*
     * Keep track of sources for order = 1. A source from LazyState q to a set Q
     * with stack character a is represented by map Q -> (q, a).
     */
    private UnsafeDoubleMultiMap<SCharacter,
                                 ManagedSet<SAState>,
                                 LazyStateO1> sourceTableO1
        = MapFactory.newUnsafeDoubleMultiMap();

    /*
     * Source look-up by state and char.  I.e. if is source (q, a, Q) with q'
     * \in Q, then there is an entry in this table (q', a) -> {..., Q, ...}
     */
    private UnsafeDoubleMultiMap<SAState,
                                 SCharacter,
                                 ManagedSet<SAState>> reverseSourceTableO1
        = MapFactory.newUnsafeDoubleMultiMap();

    /*
     * Maps mapping sources (in same order as components are recorded in tables
     * above) onto justification for them being added. We keep separate from
     * tables above as justifying rule is not a "defining feature" of a source.
     *
     */
    private QuadMap<ManagedSet<SAState>,
                    LazyState,
                    SAState,
                    SCharacter,
                    PrincipalJustification> sourceJustificationTable
        = MapFactory.newQuadMap();

    private TripleMap<ManagedSet<SAState>,
                      SCharacter,
                      LazyStateO1,
                      PrincipalJustification> sourceJustificationTableO1
        = MapFactory.newTripleMap();



    private class AnImage {
        Set<SAState> qcol;
        Set<SAState> qdest;
        Set<SATransitionO1> justif;

        public AnImage(Set<SAState> qcol,
                       Set<SAState> qdest,
                       Set<SATransitionO1> justif) {
            this.qcol = qcol;
            this.qdest = qdest;
            this.justif = justif;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("( ");
            sb.append(qcol);
            sb.append(" | ");
            sb.append(qdest);
            return sb.toString();
        }
    }

    private class ImageSet implements Iterable<AnImage> {
        LinkedList<AnImage> images = new LinkedList<AnImage>();

        public void add(AnImage i) {
            boolean add = true;
            boolean definiteAdd = false;

            Iterator<AnImage> iter = images.iterator();
            while (iter.hasNext()) {
                AnImage ifoe = iter.next();
                if (!definiteAdd &&
                    i.qcol.containsAll(ifoe.qcol) &&
                    i.qdest.containsAll(ifoe.qdest)) {
                    add = false;
                    break;
                } else if (ifoe.qcol.containsAll(i.qcol) &&
                           ifoe.qdest.containsAll(i.qdest)) {
                    definiteAdd = true;
                    iter.remove();
                }
            }

            if (add)
                images.add(i);
        }

        public Iterator<AnImage> iterator() {
            return images.iterator();
        }

        public int size() {
            return images.size();
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[ ");
            for (AnImage i : images) {
                sb.append("\n  ");
                sb.append(i);
            }
            sb.append(" ]");
            return sb.toString();
        }
    }

    /**********/

    private TargetProcessor targetProcessor;

    /**
     *
     * @param transGetter callback for getting already added transitions
     * @param order the order of the cpds we're managing for
     */
    public NaiveO1FakeManager(SATransGetter transGetter) {
        this.targetProcessor = new SimpleTargetProcessor(this, transGetter);
    }

    public void addFake(LazyState q,
                        SCharacter a,
                        SAState qtran,
                        ManagedSet<SAState> Q,
                        PrincipalJustification justif) {
        //NB Order is important. Need to add source before add target
        //in case adding target results in new complete targets (possible
        //for more advanced implementations of target management, exploiting
        //subsets etc) which might then need firing against the source.
        if(Q.isEmpty()) {
            //If countdown is empty, no point in wasting time
            // adding stuff to maps etc... can create next
            // fake immediately.
            ManagedSet<SAState> newCountDown
                = (qtran == null) ?  Q : Managers.sManager.makeSingleton(qtran);
            if(q.getStateOrder() > 2)
                addFake(q.nextLazyState(Q), a, null, newCountDown, justif);
            else
                addFakeO1(q.nextLazyStateO2(Q), a, newCountDown, justif);
        } else {
            addSource(Q, q, qtran, a, justif);
            targetProcessor.addTarget(Q);
        }
    }


    public void addFakeO1(LazyStateO1 q,
                          SCharacter a,
                          ManagedSet<SAState> Q,
                          PrincipalJustification justif) {

        if(Q.isEmpty())
            // don't waste time adding stuff to fake tables etc, as can create
            // immediately. (Justifying set of O1-transitions should be empty
            // rather than null as will still originate from a push transition).
            q.complete(a, Q, Q, justif, emptyO1TranSet);
        else {
            addSourceO1(Q, a, q, justif);
        }
    }


    public void addTargetComplete(ManagedSet<SAState> initLHS,
                                  ManagedSet<SAState> labellingStates,
                                  ManagedSet<SAState> dest) {
        if(addCompleteTarget(initLHS, labellingStates, dest)) {
            Iterable<Triple<LazyState,
                            SAState,
                            SCharacter>> sources = sourceTable.get(initLHS);
            if(sources != null) {
                for(Triple<LazyState, SAState, SCharacter> source : sources) {
                    LazyState q = source.first();
                    SAState   sourceLabel = source.second();
                    SCharacter a = source.third();
                    PrincipalJustification justif
                        = sourceJustificationTable.get(initLHS,
                                                       q,
                                                       sourceLabel,
                                                       a);
                    fireCompleteTargetAgainstSource(q,
                                                    sourceLabel,
                                                    a,
                                                    labellingStates,
                                                    dest,
                                                    justif);
                }
            }
        }
    }


    public void addTargetCompleteO1(SCharacter sChar,
                                    ManagedSet<SAState> initLHS,
                                    ManagedSet<SAState> labellingStates,
                                    ManagedSet<SAState> dest,
                                    ManagedSet<SATransitionO1> justif) {
        // Do nothing here because we don't use O1 targets
    }


    public void updateFake(SATransition t) {
        targetProcessor.update(t);
    }

    public void updateFakeO1(SATransitionO1 t) {
        SAState q = t.getSource();
        SCharacter a = t.getLabellingCharacter();

        // Do sources
        for (ManagedSet<SAState> dest : reverseSourceTableO1.get(q, a)) {
            if (sourceTableO1.get(a, dest).iterator().hasNext()) {
                ImageSet is = getImagesFrom(dest, a, t);
                for (AnImage trip : is) {
                    ManagedSet<SAState> qcol = sManager.makeSet(trip.qcol);
                    ManagedSet<SAState> qdest = sManager.makeSet(trip.qdest);
                    ManagedSet<SATransitionO1> ts = tManager.makeSet(trip.justif);
                    for (LazyStateO1 l : sourceTableO1.get(a, dest)) {
                        PrincipalJustification justif
                            = sourceJustificationTableO1.get(dest, a, l);
                        l.complete(a, qcol, qdest, justif, ts);
                    }
                }
            }
        }
    }

    /**
     * @param sources a stateset Q
     * @param a       a stack character
     * @param tmust   a transition that must be included
     * @return an iterable over triples (Qcol, Q', T) where we have transitions
     *         sources --- a, Qcol ---> Q'
     */
    private final ImageSet getImagesFrom(ManagedSet<SAState> sources,
                                         SCharacter a,
                                         SATransitionO1 tmust) {
        ImageSet res = new ImageSet();
        // apologies for use of getSet -- apparently java didn't think that
        // people might want to add iterable, not just collections...
        Set<SAState> qcolset
            = new HashSet<SAState>(tmust.getCollapseDest().getSet());
        Set<SAState> qdestset
            = new HashSet<SAState>(tmust.getDest().getSet());
        Set<SATransitionO1> tset = new HashSet<SATransitionO1>();
        tset.add(tmust);
        AnImage init = new AnImage(qcolset, qdestset, tset);

        res.add(init);
        for (SAState q : sources) {
            if (!q.equals(tmust.getSource())) {
                ImageSet resNext = new ImageSet();
                for (SATransitionO1 t : saManager.oldTranO1Iterator(q, a)) {
                    for (AnImage trip: res) {
                        Set<SAState> newQcol
                            = saManager.unionStatesets(trip.qcol,
                                                       t.getCollapseDest().getSet());
                        Set<SAState> newQ1
                            = saManager.unionStatesets(trip.qdest,
                                                       t.getDest().getSet());
                        Set<SATransitionO1> newT
                            = new HashSet<SATransitionO1>(trip.justif);
                        newT.add(t);
                        resNext.add(new AnImage(newQcol, newQ1, newT));
                    }
                }
                res = resNext;
            }
        }

        return res;
    }


    /**
     *
     * @param initLHS
     * @param labellingStates
     * @param dest
     * @return    True iff complete target not previously added
     */
    private final boolean addCompleteTarget(ManagedSet<SAState>initLHS,
                                            ManagedSet<SAState>labellingStates,
                                            ManagedSet<SAState> dest) {
        Boolean alreadyExists = completeTargetTable.hasTuple(initLHS,
                                                             labellingStates,
                                                             dest);
        if(!alreadyExists)
            completeTargetTable.put(initLHS, labellingStates, dest);

        return !alreadyExists;
    }


    /**
     * Add source and process against existing completed targets, if new.
     * Assumes order > 1 source.
     * @param rhs
     * @param q
     * @param labellingState
     * @param a
     * @param justif Justification for creating this source. (Will not change
     *               current justification of a source if source already exists).
     */
    private final void addSource(ManagedSet<SAState> rhs,
                                 LazyState q,
                                 SAState labellingState,
                                 SCharacter a,
                                 PrincipalJustification justif) {
        assert(q.getStateOrder() > 1) : "Expected order of state to be > 1";
        if(!sourceTable.hasTuple(rhs, q, labellingState, a)) {
            sourceTable.put(rhs, q, labellingState, a);
            sourceJustificationTable.put(rhs, q, labellingState, a, justif);

            Iterable<Pair<ManagedSet<SAState>,
                          ManagedSet<SAState>>> completeTargets
                = completeTargetTable.get(rhs);

            if(completeTargets != null) {
                for(Pair<ManagedSet<SAState>,
                         ManagedSet<SAState>> completeTarget
                        : completeTargetTable.get(rhs)) {
                    fireCompleteTargetAgainstSource(q,
                                                    labellingState,
                                                    a,
                                                    completeTarget.first(),
                                                    completeTarget.second(),
                                                    justif);
                }
            }

        }
    }

    /**
     * Add source (order 1) and process against existin completed targets, if new.
     * @param rhs
     * @param a
     * @param q
     * @param rule justifying rule (will not change justification of existing source; only recorded if source new).
     */
    private final void addSourceO1(ManagedSet<SAState> rhs,
                                   SCharacter a,
                                   LazyStateO1 q,
                                   PrincipalJustification justif) {
        for (ManagedSet<SAState> qs : sourceTableO1.keySet(a)) {
            if (sManager.subset(qs, rhs)) {
                for (LazyStateO1 qother : sourceTableO1.get(a, qs)) {
                    if (q == qother) {
                        //logger.debug("Hit on " + rhs);
                        //logger.debug("justif rule: " + justif.getRule());
                        return;
                    }
                }
            } else if (sManager.subset(rhs, qs)) {
                Iterator<LazyStateO1> i = sourceTableO1.get(a, qs).iterator();
                while (i.hasNext()) {
                    if (q == i.next()) {
                        //logger.debug("Rev Hit on " + rhs);
                        //logger.debug("justif rule: " + justif.getRule());
                        i.remove();
                    }
                }
            }
        }

        if (!sourceTableO1.hasTuple(a, rhs, q)) {
            sourceTableO1.put(a, rhs, q);
            sourceJustificationTableO1.put(rhs, a, q, justif);
            for (SAState dest : rhs) {
                reverseSourceTableO1.put(dest, a, rhs);
            }
            processSourceO1(q, a, rhs, justif);
        }
    }


    private final void processSourceO1(LazyStateO1 q,
                                       SCharacter a,
                                       ManagedSet<SAState> rhs,
                                       PrincipalJustification justif) {
        for (AnImage dest: getImages(rhs, a)) {
            ManagedSet<SAState> qcol = sManager.makeSet(dest.qcol);
            ManagedSet<SAState> qdest = sManager.makeSet(dest.qdest);
            ManagedSet<SATransitionO1> ts = tManager.makeSet(dest.justif);
            q.complete(a, qcol, qdest, justif, ts);
        }
    }


    /**
     * @param sources a stateset Q
     * @param a       a stack character
     * @return an iterable over triples (Qcol, Q', T) where we have transitions
     *         sources --- a, Qcol ---> Q'
     */
    private final ImageSet getImages(ManagedSet<SAState> sources,
                                     SCharacter a) {
        ImageSet res = new ImageSet();
        res.add(new AnImage(new HashSet<SAState>(),
                            new HashSet<SAState>(),
                            new HashSet<SATransitionO1>()));
        for (SAState q : sources) {
            ImageSet resNext = new ImageSet();
            for (SATransitionO1 t : saManager.oldTranO1Iterator(q, a)) {
                for (AnImage trip: res) {

                    Set<SAState> newQcol
                        = saManager.unionStatesets(trip.qcol,
                                                   t.getCollapseDest().getSet());
                    Set<SAState> newQ1
                         = saManager.unionStatesets(trip.qdest,
                                                    t.getDest().getSet());
                    Set<SATransitionO1> newT
                        = new HashSet<SATransitionO1>(trip.justif);
                    newT.add(t);
                    resNext.add(new AnImage(newQcol, newQ1, newT));
                }
            }
            res = resNext;
        }
        return res;
    }


    /**
     * Make the necessary updates (inform LazyState of new transition to be
     * created and create a new fake) corresponding to a completed target and
     * its source. Only for sources for states of order > 1
     * @param q               LHS of source
     * @param sourceLabel     label of source transition (can be null if no such additional label)
     * @param a               Stack character associated with source
     * @param labellingStates Labelling states assimilated in completed target
     * @param dest            RHS states assimialted in completed target
     * @param justif          PrincipalJustification for the source.
     */
    private final void fireCompleteTargetAgainstSource(
        LazyState q,
        SAState sourceLabel,
        SCharacter a,
        ManagedSet<SAState> labellingStates,
        ManagedSet<SAState> dest,
        PrincipalJustification justif
    ) {
        /*
         * The new transition from this completed-target/source pair
         * q--sourceLabel-->initLHS-- labellingStates-->dest
         * is
         * q--newState-->dest
         * (using terminology including that defined below... in particular
         * newState is unique label for q--->dest transition).
         *
         * We need to iterate fake construction down to the new level
         * using newState and labellingStates + {sourceLabel}.
         */
        assert(q.getStateOrder() > 1) : "Expected state of order > 1";
        ManagedSet<SAState> newLabellingStates =
            (sourceLabel == null) ?
                    labellingStates
                    : Managers.sManager.add(labellingStates, sourceLabel);

        if(q.getStateOrder() > 2) {
            LazyState newState = q.nextLazyState(dest);
            this.addFake(newState, a, null, newLabellingStates, justif);
        }
        else {
            LazyStateO1 newState = q.nextLazyStateO2(dest);
            this.addFakeO1(newState, a, newLabellingStates, justif);
        }
        return;
    }

    /**
     * Resets the manager
     *
     * @param transGetter new trans getter
     */
    public void reset(SATransGetter transGetter) {
        completeTargetTable = MapFactory.newUnsafeMapToDouble();
        sourceTable = MapFactory.newUnsafeMapToTriple();
        sourceTableO1 = MapFactory.newUnsafeDoubleMultiMap();
        targetProcessor = new SimpleTargetProcessor(this, transGetter);
        sourceJustificationTable = MapFactory.newQuadMap();
        sourceJustificationTableO1 = MapFactory.newTripleMap();
    }

    public void setOrder(byte order) {
        targetProcessor.setOrder(order);
    }

}
