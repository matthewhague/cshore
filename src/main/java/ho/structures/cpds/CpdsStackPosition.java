/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.cpds;

import java.util.ArrayList;

 import java.lang.StringBuilder;

import org.apache.log4j.Logger;


/**
 * For representing positions in the stack.
 *
 * E.g. [[ab][c]]
 *
 * has positions
 *
 *     .[.[.a.b.].[.c.].].
 *
 * marked by . which are (right to left):
 *
 *     (-1, -1, 0)
 *          ]
 *     (-1,  0, 0)
 *          ]
 *     ( 0,  0, 0)
 *          c
 *     ( 1,  0, 0)
 *          [
 *     (-1,  1, 0)
 *          ]
 *     ( 0,  1, 0)
 *          b
 *     ( 1,  1, 0)
 *          a
 *     ( 2,  1, 0)
 *          [
 *     (-1,  2, 0)
 *          [
 *     (-1, -1, 1)
 *
 */
public class CpdsStackPosition {

    static Logger logger = Logger.getLogger(CpdsStackPosition.class);

    byte order;
    ArrayList<Integer> vals;


    /**
     * @param order the order of the stack the position is for
     * @return an object representing a stack position of an order-order stack
     */
    public CpdsStackPosition(byte order) {
        this.order = order;
        vals = new ArrayList<Integer>(((int)order) + 1);
        for (int i = 0; i < (int)order; ++i)
            vals.add(i, -1);
        vals.add(order, 0);
    }

    /**
     * @param a stack position to copy
     * @return a copy of the stack position
     */
    public CpdsStackPosition(CpdsStackPosition pos) {
        this.order = pos.order;
        this.vals = new ArrayList<Integer>(((int)order) + 1);
        for (int i = 0; i <= (int)order; ++i)
            this.vals.add(i, pos.getCoord((byte)i));
    }

    /**
     * @param list of positions order-1 to order-n+1 (p1,...,pn, pn+1)
     * @return a stack position (p1, ..., pn, pn+1) of order-n
     */
    public CpdsStackPosition(int... positions) {
        this.order = (byte)(positions.length - 1);
        this.vals = new ArrayList<Integer>(positions.length);
        for (int i = 0; i < positions.length; ++i)
            this.vals.add(i, positions[i]);
    }

    /**
     * @return the order of the stack the position applies to
     */
    public byte getOrder() {
        return order;
    }

    /**
     * @return the order of the position pointed to e.g. [[a.b]] is an order-1
     * position, [[ab].] is order-2 and [[ab]]. is order-3
     */
    public byte getPositionOrder() {
        byte k = 0;
        while (k < order && getCoord(k) <= -1)
            ++k;
        return (byte)(k + 1);
    }


    /**
     * @return true iff the position is the bottom of the stack it points into
     */
    public boolean isBottom() {
        byte k = getPositionOrder();
        return (getCoord(k) <= 0);
    }

    /**
     * @param k the order of the co-ord to get
     * @return the kth co-ordinate
     */
    public int getCoord(byte k) {
        assert 0 <= k && k <= order
            : "Can't get a co-ordinate of a stack pos outside of 0..order";
        return vals.get((int)k);
    }

    /**
     * @param k the order of the co-ord to set
     * @param val the value to set it to
     */
    public void setCoord(byte k, int val) {
        assert 0 <= k && k <= order
            : "Can't get a co-ordinate of a stack pos outside of 0..order";
        vals.set((int)k, val);
    }

    /**
     * @param k the order of the co-ord to increment by 1
     */
    public void incCoord(byte k) {
        incCoord(k, (byte)1);
    }

    /**
     * @param k the order of the co-ord to increment
     * @param inc the value to set increment it by
     */
    public void incCoord(byte k, int inc) {
        assert k <= order
            : "Can't get a co-ordinate of a stack pos outside of 0..order";
        vals.set((int)k, vals.get((int)k) + inc);
    }

    public boolean equals(Object o) {
        if (o instanceof CpdsStackPosition) {
            CpdsStackPosition pos = (CpdsStackPosition)o;
            return (order == pos.order &&
                    vals.equals(pos.vals));
        }
        return false;
    }


    public int hashCode() {
        return order + vals.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("( ");
        for (byte k = 0; k <= order; ++k) {
            sb.append(vals.get(k));
            sb.append(" ");
        }
        sb.append(")");
        return sb.toString();
    }

}
