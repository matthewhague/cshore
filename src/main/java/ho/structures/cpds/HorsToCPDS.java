/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.structures.cpds;

import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;

import scala.collection.Iterator;
import scala.collection.immutable.List;

import org.apache.log4j.Logger;

import ho.main.Main;
import ho.hors.*;

import ho.util.Pair;

/**
 * Apply to a Hors (which includes a property automaton) to get an equivalent CPDS.
 * Explores from the starting symbol forwards and in this way avoids some subterms
 * that are not subterms of any reachable term. This could be improved a little
 * by a binding analysis (which could probably be integrated with this construction
 * fairly striaghtforwardly.
 * UPDATE: Actually, in the event I don't think much at all, if anything, wil
 * be stripped due to findBindings exploring all subterms. However, it would
 * be worth looking into such restrictions.
 *
 * `Position' being looked up will be > 0 if it is an argument position and = 0 if we
 * are due to evaluate the head symbol.
 *
 */

public class HorsToCPDS {
    static Logger logger = Logger.getLogger(HorsToCPDS.class);

    private static ControlStateManager csManager = ho.managers.Managers.csManager;
    private static SCharacterManager scManager = ho.managers.Managers.scManager;

    Set<Pair<PropAutState, Term>> alreadyProcessed
        = new HashSet<Pair<PropAutState, Term>>();

    Set<Term> alreadyFindBindings = new HashSet<Term>();

    Cpds cpds = new Cpds();
    Hors hors = new Hors();
    byte horsOrder = (byte)0;
    Set<PropAutState> propAutStates = new HashSet<PropAutState>();

    // treat domain values like property automaton states: e.g. state v must
    // evaluate to a stack with "v" on top of its stack
    //
    // Or think of it as the property automaton that only accepts v
    Map<String, PropAutState> domValStates = new HashMap<String, PropAutState>();

    /**
     * @param horsIn    Hors to translate to a CPDS
     * @return    The resulting CPDS (takes into account property automaton in hors)
     */
    public Cpds translate(Hors horsIn) {
        cpds = new Cpds();
        horsOrder = horsIn.getOrder();
        alreadyProcessed.clear();
        alreadyFindBindings.clear();
        propAutStates.clear();
        hors = horsIn;
        Iterator i = hors.getPropAutStates();
        while (i.hasNext()) {
            propAutStates.add((PropAutState)i.next());
        }

        Iterator it = hors.initNonTerms().iterator();
        while (it.hasNext()) {
            Term t = (Term)it.next();
            Iterator ip = hors.initPropAutStates().iterator();
            while (ip.hasNext()) {
                processTerm((PropAutState)ip.next(), t);
            }
        }

        setUpErrorStates(cpds, hors);

        return cpds;
    }

    private PropAutState getDomValState(String v) {
        PropAutState qv = domValStates.get(v);
        if (qv == null) {
            qv = new PropAutState(v);
            domValStates.put(v, qv);
        }
        return qv;
    }

    /**
     * Sets up the cpds control-state/stack element combinations that should
     * have a transition to the fixed error state of a CPDS due to them
     * being failing property automaton states/ failing non-terminals.
     *
     * Note: fail property automaton states are dealt with in terminalChild,
     * except if initial prop aut state is also a fail
     */
    private void setUpErrorStates(Cpds cpds, Hors hors) {
        // Keep track of rules separately when adding to CPDS to avoid ConcurrentModification exception
        // caused by adding to CPDS whilst using iterators.
        Set<RewRule> rules = new HashSet<RewRule>();
        ControlState qerror = csManager.makeControl(Cpds.error_state);

        Iterator i = hors.initPropAutStates().iterator();
        while (i.hasNext()) {
            PropAutState initState = (PropAutState)i.next();
            if (hors.failPropAutStates().contains(initState)) {
                Iterator i2 = hors.initNonTerms().iterator();
                while (i2.hasNext()) {
                    Term initNonTerm = (Term)i2.next();
                    ControlState cs
                        = csManager.makeControl(initState, 0, false);
                    SCharacter sourceChar
                        = scManager.makeCharacter(initNonTerm);

                    rules.add(new RewRule(cs,
                                          sourceChar,
                                          qerror,
                                          sourceChar,
                                          new ToError()));
                }
            }
        }

        for (SCharacter sChar : cpds.getCharacters()) {
            if (sChar instanceof SCharacterTerm) {
                SCharacterTerm sc = (SCharacterTerm)sChar;
                Term h = sc.getTerm().getHead();
                if (h instanceof NonTerm) {
                    NonTerm nt = (NonTerm)h;
                    if(hors.failNonTerms().contains(nt)) {
                        for (ControlState controlState : cpds.getControls()) {
                            if (controlState instanceof ControlStateHors) {
                                ControlStateHors cs = (ControlStateHors)controlState;
                                if (cs.getPosition() == 0) {
                                    //cs.getPosition == 0 is good as do not need to pollute
                                    // CPDS with transitions to error from configurations
                                    // representing in-middle-of-finding-variable binding.
                                    rules.add(new RewRule(controlState,
                                                          sChar,
                                                          qerror,
                                                          sChar,
                                                          new ToError()));
                                }
                            }
                        }
                    }
                }
            }
        }

        Iterator<Domain> domi = hors.getDomains();
        while (domi.hasNext()) {
            Domain dom = (Domain)domi.next();
            Iterator<String> si = dom.getValues().iterator();
            while (si.hasNext()) {
                String v = (String)si.next();
                SCharacter av = getDomValSC(v, dom);
                ControlState q = getDomValCS(v);
                rules.add(new RewRule(q, av, qerror, av, new ToError()));
            }
        }

        for (RewRule rule : rules) {
            cpds.addRewRule(rule);
        }
    }

    /**
     * @param dv domain value name
     * @return control state for that domain value
     */
    private ControlState getDomValCS(String dv) {
        return csManager.makeControl(getDomValState(dv), 0, false);
    }

    /**
     * @param dv domain value name
     * @param dom the domain of the value
     * @return stack character for that domain value
     */
    private SCharacter getDomValSC(String dv, Domain dom) {
        DomainType domType = new DomainType(dom);
        return scManager.makeCharacter(new DomConst(dv, domType));
    }


    /**
     * Since CPDS is stateful, making this of type Unit. (Could do a `less mutable' version
     * in a monadic setting... hopefully implicits clean up state passing a bit... not sure
     * whether this is good Scala etiquette or not). Corresponding elimiation of some unreachable
     * property automaton states.
     *
     * @param  cpds      Cpds to be modified
     * @param     hors    Hors being translated
     * so inefficient, so better to compute only once and pass as argument)
     * @propAutState                property automaton state to process in combination with...
     * @param     term                 Hors term to process (add corresponding bits to Cpds)
     *
     */
    HashSet<Pair<PropAutState, Term>> termWorklist
        = new HashSet<Pair<PropAutState, Term>>();
    boolean recursingTerms = false;

    private void processTerm(PropAutState propAutState,
                            Term term) {
        if (recursingTerms) {
            termWorklist.add(new Pair<PropAutState, Term>(propAutState, term));
        } else {
            recursingTerms = true;
            processTermBody(propAutState, term);
            while (!termWorklist.isEmpty()) {
                java.util.Iterator<Pair<PropAutState, Term>> i
                    = termWorklist.iterator();
                Pair<PropAutState, Term> pt = (Pair<PropAutState, Term>)i.next();
                i.remove();
                processTermBody(pt.first(), pt.second());
            }
            recursingTerms = false;
        }
    }


    private void processTermBody(PropAutState propAutState,
                                 Term term) {
        Pair<PropAutState, Term> pt
            = new Pair<PropAutState, Term>(propAutState, term);
        if(!alreadyProcessed.contains(pt)) {
            alreadyProcessed.add(pt);

            Term head = term.getHead();
            List<Term> argList = term.getArgsOfHead();

            // Add all transitions corresponding to looking up value to which a variable
            // is bound by looking at subterm of term
            findBindings(term, argList);

            if (head instanceof NonTerm) {
                NonTerm nt = (NonTerm)head;
                if(head.getOrder() == 0) {
                    // term must equal head and so no arguments, so can just rewrite
                    scala.Option<Term> rhs = hors.getRhs(nt);
                    if (rhs instanceof scala.Some) {
                        Term t = ((scala.Some<Term>)rhs).get();
                        evalOrd0NonTerm(propAutState, term, t);
                        processTerm(propAutState, t);
                    }
                } else {
                    scala.Option<Term> rhs = hors.getRhs(nt);
                    if (rhs instanceof scala.Some) {
                        Term t = ((scala.Some<Term>)rhs).get();
                        evalHONonTerm(propAutState, term, t);
                        processTerm(propAutState, t);
                    }
                }
            } else if (head instanceof Terminal) {
                Terminal tml = (Terminal)head;
                // Nothing to do for order-0 terminal as dead end.
                if (tml.getOrder() > 0) {
                    for(int pos = 1; pos <= tml.getArity(); ++pos){
                        scala.Option<PropAutState> opt
                            = hors.getNextState(propAutState, tml, pos);
                        if (opt instanceof scala.Some) {
                            PropAutState nextState
                                = ((scala.Some<PropAutState>)opt).get();
                            terminalChild(propAutState, term, pos, nextState);
                            processTerm(nextState, term);
                        }
                    }
                }
            } else if (head instanceof Var) {
                Var v = (Var)head;
                if(v.getOrder() == 0)
                    callOrd0Variable(propAutState, v, term, v.pos());
                else
                    callHOVariable(propAutState, v, term, v.pos());
            } else if (head instanceof NonDet) {
                NonDet nonDet = (NonDet)head;
                evalNonDet(propAutState,
                           nonDet,
                           nonDet.choice1(),
                           nonDet.choice2());
                processTerm(propAutState, nonDet.choice1());
                processTerm(propAutState, nonDet.choice2()) ;
            } else if (head instanceof CaseTerm) {
                processCaseTerm((CaseTerm)head, propAutState);
            }
            // nothing to do in other cases Note App should never be head symbol
            // (One could create a NonAppTerm type to avoid this case, but when
            // I tried things elsewhere seemed to start looking messier...
        }
    }

    /**
     * processTerm for the case statements
     *
     * @param ct the case term
     * @param paq the propautstate
     */
    private void processCaseTerm(CaseTerm ct, PropAutState paq) {
        // wouldn't have type checked if not domain type
        Term cond = ct.getCond();
        DomainType dt = (DomainType)cond.getType();
        List<String> vals = dt.getDomain().getValues();
        List<Term> cases = ct.getCases();

        if (vals.size() != cases.size()) {
            throw new RuntimeException("Translating HORS with case/vals mismatch: " + ct);
        } else {
            SCharacter act = scManager.makeCharacter(ct);
            ControlState q = csManager.makeControl(paq, 0, false);

            Iterator<String> vi = vals.iterator();
            Iterator<Term> ci = cases.iterator();
            while (vi.hasNext()) {
                String v = (String)vi.next();
                Term c = (Term)ci.next();

                Set<StackHead> nextHeads = new HashSet<StackHead>();

                // evaluate rest of scheme on branch c
                nextHeads.add(new StackHead(q, scManager.makeCharacter(c)));
                // check that condition evaluates to v
                nextHeads.add(new StackHead(getDomValCS(v),
                                            scManager.makeCharacter(cond)));

                AltRule rule = new AltRule(q, act, nextHeads);
                cpds.addAltRule(rule);

                processTerm(paq, c);
                processTerm(getDomValState(v), cond);
            }
        }
    }

    /**
     * Creates all of the CPDS rules for finding a particular argument of the head
     * of a term. Will recursively call processTerm with new subterms
     * explored.
     *
     * @param     term    The term whose head's argument we want to create rules to find.
     * @param     argList The list of arguments that are syntactic arguments of the head of term
     *                     (could be derived from term itself, but passed to avoid computing
     *                         multiple times). List will have left-most as first argument
     *
     * Note that term.typ.getArity will be the number of arguments of the head of term
     * that are not syntactic arguments and that must be found via a collapse.
     *
     */
    private void findBindings(Term term,
                              List<Term> argList) {
        if(!alreadyFindBindings.contains(term)) {
            alreadyFindBindings.add(term);

            // for regular propautstates
            for(PropAutState propAutState : propAutStates) {
                findBindings(term, argList, propAutState);
            }

            // for domain values
            Iterator<Domain> domi = hors.getDomains();
            while (domi.hasNext()) {
                Domain dom = (Domain)domi.next();
                Iterator<String> si = dom.getValues().iterator();
                while (si.hasNext()) {
                    String v = (String)si.next();
                    PropAutState pas = getDomValState(v);
                    findBindings(term, argList, pas);
                }
            }

        }
    }


    /**
     * For a particular propAutState, creates all of the CPDS rules for finding a particular argument of the head
     * of a term. Will recursively call processTerm with new subterms
     * explored.
     *
     * @param     term    The term whose head's argument we want to create rules to find.
     * @param     argList The list of arguments that are syntactic arguments of the head of term
     *                     (could be derived from term itself, but passed to avoid computing
     *                         multiple times). List will have left-most as first argument
     * @param     propAutState the propAutState
     *
     * Note that term.typ.getArity will be the number of arguments of the head of term
     * that are not syntactic arguments and that must be found via a collapse.
     *
     */

    private void findBindings(Term term,
                              List<Term> argList,
                              PropAutState propAutState) {
        // Stack character corresponding to the term being considered:
        SCharacter sourceSChar = ho.managers.Managers.scManager.makeCharacter(term);
        //First process syntactic arguments
        int pos = 0 ;
        Iterator j = argList.iterator();
        while (j.hasNext()) {
            Term arg = (Term)j.next();
            pos = pos + 1;
            ControlState sourceState
                = ho.managers.Managers.csManager.makeControl(propAutState,
                                                             pos,
                                                             false);
            ControlState targetState
                = ho.managers.Managers.csManager.makeControl(propAutState,
                                                             0,
                                                             false);

            SCharacter targetSChar
                = ho.managers.Managers.scManager.makeCharacter(arg);

            Rule rule;
            if(Main.getUseSCPDS()) {
                if(arg.getOrder() == 0)
                    rule = new GetOrder0ArgRule(sourceState,
                                                sourceSChar,
                                                targetState,
                                                targetSChar,
                                                new FindBinding());
                else
                    rule = new GetArgRule(sourceState,
                                          sourceSChar,
                                          targetState,
                                          targetSChar,
                                          (byte)(horsOrder - arg.getOrder() + 1),
                                          new FindBinding());

            } else {
                if(arg.getOrder() == 0)
                    // Then we are finding binding of an order-0 variable so no need to attach a link
                    rule = new RewRule(sourceState,
                                       sourceSChar,
                                       targetState,
                                       targetSChar,
                                       new FindBinding());
                else
                    // We are looking for binding of a higher-order variable and so we need to
                    // attach a link to stack that would have been copied. Order of variable
                    // must be equal to order of arg.
                    rule = new RewLinkRule(sourceState,
                                           sourceSChar,
                                           targetState,
                                           targetSChar,
                                           (byte)(horsOrder - arg.getOrder() + 1),
                                           new FindBinding());
            }
            cpds.addRule(rule);
            // Remember to process the new argument:
            processTerm(propAutState, arg);
        }

        // Now process arguments that are not syntactic arguments.
        if(term.getArity() > 0) {
            for(int i = (pos + 1); i <= term.getHead().getArity(); ++i) {
                ControlState sourceState
                    = ho.managers.Managers.csManager.makeControl(propAutState,
                                                                 i,
                                                                 false);
                // (i - pos) is the offset from left-most syntactic argument
                // to position of this new argument to be looked up.
                ControlState targetState
                    = ho.managers.Managers.csManager.makeControl(propAutState,
                                                                 (i - pos),
                                                                 false);


                // Note that order of link on term will be horsOrder -
                // term.getOrder + 1
                CollapseRule rule = new CollapseRule(sourceState,
                                                     sourceSChar,
                                                     targetState,
                                                     (byte)(horsOrder - term.getOrder() + 1),
                                                     new FindBinding());
                cpds.addCollapseRule(rule);
            }
        }

    }


    /**
     * When evaluating a order-0 non-terminal we can just rewrite it to its RHS
     * (the property automaton state remains same as have not emitted any
     * terminal symbol).  We keep the `needToPopOne' flag false (as no need to
     * pop one before continuing)
     */
    private void evalOrd0NonTerm(PropAutState propAutState,
                                 Term sourceTerm,
                                 Term targetTerm) {
        ControlState cs
            = ho.managers.Managers.csManager.makeControl(propAutState,
                                                         0,
                                                         false);
        SCharacter sourceChar
            = ho.managers.Managers.scManager.makeCharacter(sourceTerm);
        SCharacter targetChar
            = ho.managers.Managers.scManager.makeCharacter(targetTerm);

        RewRule newRule = new RewRule(cs,
                                      sourceChar,
                                      cs,
                                      targetChar,
                                      new Rewrite());
        cpds.addRewRule(newRule);

        Term th = targetTerm.getHead();
        if (th instanceof NonTerm) {
            NonTerm nt = (NonTerm)th;
            if(hors.failNonTerms().contains(nt)) {
                ControlState errorcs
                    = ho.managers.Managers.csManager.makeControl(Cpds.error_state);
                RewRule erule = new RewRule(cs,
                                            sourceChar,
                                            errorcs,
                                            targetChar,
                                            new ToError());
                cpds.addRewRule(erule);
            }
        }
    }

    /**
     * Similar to above except this time we need to do a push-one (without any link being created)
     */
    private void evalHONonTerm(PropAutState propAutState,
                               Term sourceTerm,
                               Term targetTerm) {
        ControlState cs
            = ho.managers.Managers.csManager.makeControl(propAutState, 0, false);
        SCharacter sourceChar
            = ho.managers.Managers.scManager.makeCharacter(sourceTerm);
        SCharacter targetChar
            = ho.managers.Managers.scManager.makeCharacter(targetTerm);

        if (Main.getUseSCPDS()) {
            EvalNonTermRule r = new EvalNonTermRule(cs,
                                                    sourceChar,
                                                    cs,
                                                    targetChar,
                                                    new Rewrite());
            cpds.addEvalNonTermRule(r);
        } else {
            CPushRule r = new CPushRule(cs,
                                        sourceChar,
                                        cs,
                                        targetChar,
                                        (byte)1,
                                        new Rewrite());
            cpds.addCPushRule(r);
        }

        Term head = targetTerm.getHead();
        if (head instanceof NonTerm) {
            NonTerm nt = (NonTerm)head;
            if(hors.failNonTerms().contains(nt)) {
                ControlState errorcs
                    = ho.managers.Managers.csManager.makeControl(Cpds.error_state);
                RewRule erule = new RewRule(cs,
                                            sourceChar,
                                            errorcs,
                                            targetChar,
                                            new ToError());
                cpds.addRewRule(erule);
            }
        }
    }

    /**
     * @param source    property automaton state that beginning with
     * @param term        Term that is headed by a terminal symbol
     * @param target    The next property automaton state when decending down pos child
     * @param pos    The child that is being explored (used to annotated transition)
     */
    private void terminalChild(PropAutState source,
                               Term term,
                               int pos,
                               PropAutState target) {
        ControlState sourcecs
            = ho.managers.Managers.csManager.makeControl(source, 0, false);
        SCharacter schar = ho.managers.Managers.scManager.makeCharacter(term);
        ControlState targetcs
            = ho.managers.Managers.csManager.makeControl(target, pos, false);

        RewRule rule = new RewRule(sourcecs,
                                   schar,
                                   targetcs,
                                   schar,
                                   new TerminalChild(pos));
        cpds.addRewRule(rule);

        if(hors.failPropAutStates().contains(target)) {
            ControlState errorcs
                = ho.managers.Managers.csManager.makeControl(Cpds.error_state);
            RewRule erule = new RewRule(sourcecs,
                                        schar,
                                        errorcs,
                                        schar,
                                        new ToError());
            cpds.addRewRule(erule);
        }
    }


    /**
     * @param propAutState     The property automaton state that should be maintained
     * @param head     The head of term (which should be a variable of order > 0)
     * @param term     The term whose head is a higher-order variable to be called
     * @param varParameterPos    The position of the formal parameter of a non-terminal to which this variable
     *                             is bound.
     */
    private void callHOVariable(PropAutState propAutState,
                                Var head,
                                Term term,
                                int varParameterPos) {
        //Top symbol remains same throughout both transitions that we create:
        SCharacter sChar = ho.managers.Managers.scManager.makeCharacter(term);

        ControlState sourcecs
            = ho.managers.Managers.csManager.makeControl(propAutState, 0, false);

        // True flag below indicates we've just done higher-order push and are about
        // to start popping to find what variable bound to.
        ControlState middlecs
            = ho.managers.Managers.csManager.makeControl(propAutState, 0, true);
        ControlState targetcs
            = ho.managers.Managers.csManager.makeControl(propAutState,
                                                         varParameterPos,
                                                         false);

        byte varOrder = head.getOrder();

        PushRule rule1 = new PushRule(sourcecs,
                                      sChar,
                                      middlecs,
                                      (byte)(horsOrder - varOrder + 1),
                                      new FindBinding());
        PopRule rule2 = new PopRule(middlecs,
                                    sChar,
                                    targetcs,
                                    (byte)1,
                                    new FindBinding());

        cpds.addPushRule(rule1);
        cpds.addPopRule(rule2);
    }

    /**
     * Expects a term with order-0 variable as its head.
     */
    private void callOrd0Variable(PropAutState propAutState,
                                  Var head,
                                  Term term,
                                  int varParameterPos) {
        //Top symbol remains same throughout both transitions that we create:
        SCharacter sChar = ho.managers.Managers.scManager.makeCharacter(term);

        ControlState sourcecs
            = ho.managers.Managers.csManager.makeControl(propAutState, 0, false);

        ControlState targetcs
            = ho.managers.Managers.csManager.makeControl(propAutState,
                                                         varParameterPos,
                                                         false);

        PopRule rule = new PopRule(sourcecs,
                                   sChar,
                                   targetcs,
                                   (byte)1,
                                   new FindBinding());

        cpds.addPopRule(rule);
    }

    /**
     * Adds CPDS rule for non-deterministic choice
     * @param propAutState    the property automaton state to maintain
     * @param term    The term constituting non-deterministic choice.
     * @param     leftChoice     The left-hand choice
     * @param    rightChoice     The right-hand choice
     */
    private void evalNonDet(PropAutState propAutState,
                            NonDet term,
                            Term leftChoice,
                            Term rightChoice) {
        ControlState cs
            = ho.managers.Managers.csManager.makeControl(propAutState, 0, false);
        SCharacter sourceChar
            = ho.managers.Managers.scManager.makeCharacter(term);
        SCharacter targetCharLeft
            = ho.managers.Managers.scManager.makeCharacter(leftChoice);
        SCharacter targetCharRight
            = ho.managers.Managers.scManager.makeCharacter(rightChoice);

        RewRule newRuleLeft = new RewRule(cs,
                                          sourceChar,
                                          cs,
                                          targetCharLeft,
                                          new LeftNonDetChoice());
        RewRule newRuleRight = new RewRule(cs,
                                           sourceChar,
                                           cs,
                                           targetCharRight,
                                           new RightNonDetChoice());

        cpds.addRewRule(newRuleLeft);
        cpds.addRewRule(newRuleRight);

        Term head = leftChoice.getHead();
        if (head instanceof NonTerm) {
            NonTerm nt = (NonTerm)head;
            if(hors.failNonTerms().contains(nt)) {
                ControlState errorcs
                    = ho.managers.Managers.csManager.makeControl(Cpds.error_state);
                RewRule erule = new RewRule(cs,
                                            sourceChar,
                                            errorcs,
                                            targetCharLeft,
                                            new ToError());
                cpds.addRewRule(erule);
            }
        }

        head = rightChoice.getHead();
        if (head instanceof NonTerm) {
            NonTerm nt = (NonTerm)head;
            if(hors.failNonTerms().contains(nt)) {
                ControlState errorcs
                    = ho.managers.Managers.csManager.makeControl(Cpds.error_state);
                RewRule erule
                    = new RewRule(cs,
                                  sourceChar,
                                  errorcs,
                                  targetCharRight,
                                  new ToError());
                cpds.addRewRule(erule);
            }
        }
    }
}
