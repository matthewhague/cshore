/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.cpds;

import java.lang.StringBuilder;

import java.util.ArrayList;
import java.util.Collection;

import ho.managers.Managers;


/**
 * Representation of rewrite rules (p, a, rew(b), q)
 */
public class RewRule extends Rule {

    SCharacter rewCharacter;
    int hashCode;

    /**
     * Constructs a rule (control1, character, rew(rewCharacter), control2)
     *
     * @param control1 the source control state
     * @param character the source top character
     * @param control2 the destination control state
     * @param rewCharacter the destination top character
     */
    public RewRule(ControlState control1,
                   SCharacter character,
                   ControlState control2,
                   SCharacter rewCharacter) {
        super(control1, character, control2);
        this.rewCharacter = rewCharacter;
        this.hashCode = control1.hashCode() +
                        character.hashCode() +
                        control2.hashCode() +
                        rewCharacter.hashCode();
    }

    public RewRule(ControlState control1,
            SCharacter character,
            ControlState control2,
            SCharacter rewCharacter, RuleAnnotation annotation) {
        super(control1, character, control2, annotation);
        this.rewCharacter = rewCharacter;
        this.hashCode = control1.hashCode() +
                        character.hashCode() +
                        control2.hashCode() +
                        rewCharacter.hashCode();
    }

    public RewRule(RewRule r) {
        super(r);
        this.rewCharacter = r.getRewCharacter();
        this.hashCode = r.hashCode();
    }

    /**
     * @return the character that replaces the previous top character
     */
    public SCharacter getRewCharacter() {
        return rewCharacter;
    }


    public byte getOrder() {
        return (byte)1;
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("(");
        s.append(getControl1());
        s.append(" ");
        s.append(getCharacter());
        s.append(" ");
        s.append("rew(");
        s.append(rewCharacter);
        s.append(")");
        s.append(" ");
        s.append(getControl2());
        s.append(")");
        return s.toString();
    }

    /**
     * This method is inefficient, but we don't expect to use it much.
     *
     * @return the set of characters of the rule
     */
    public Collection<SCharacter> getCharacters() {
        ArrayList<SCharacter> characters = new ArrayList<SCharacter>(2);
        characters.add(0, getCharacter());
        characters.add(1, getRewCharacter());
        return characters;
    }

    public final boolean getDestinationHead(StackHead sh) {
        sh.setControl(getControl2());
        sh.setCharacter(getRewCharacter());
        return true;
    }

    public boolean equals(Object o) {
        if (o instanceof RewRule) {
            RewRule r = (RewRule)o;
            return hashCode == o.hashCode() &&
                   Managers.csManager.equal(getControl1(), r.getControl1()) &&
                   Managers.scManager.equal(getCharacter(), r.getCharacter()) &&
                   Managers.csManager.equal(getControl2(), r.getControl2()) &&
                   Managers.scManager.equal(getRewCharacter(),
                                            r.getRewCharacter());
        } else
            return false;
    }

    public final int hashCode() {
        return hashCode;
    }

    public void accept(RuleVisitor v) { v.visit(this); }

    public Object clone() throws CloneNotSupportedException {
        return new RewRule(this);
    }

}
