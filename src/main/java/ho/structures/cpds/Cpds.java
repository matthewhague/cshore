/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.cpds;

import java.lang.StringBuilder;
import java.lang.Math;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import ho.util.DoubleMap;
import ho.util.NaiveUnsafeDoubleMultiMap;
import ho.util.UnsafeDoubleMultiMap;






/**
 * Representation of a collapsible pushdown system
 */
public class Cpds {

    static Logger logger = Logger.getLogger(Cpds.class);


    /**
     * An Iterable for storing stack characters a such that (qerror, s) with top_1(s) = a
     * might be reachable. (To be computed by e.g. approximate reachability graph).
     */
    private Iterable<SCharacter> errorSChars = null;


    /**
     *	Calling this will setErrorSChars to the set of all stack characters
     * and set every destructive rule to have over-approximation of reachable characters
     * equal to the set of all characters. Thus calling this should result, in effect, in the
     * "original" algorithm being run on the CPDS (without the collapse/pop restriction optimisation).
     *
     *  This is not intended to be efficient (probably only useful for testing)
     *
     */
    public void trivialiseConstraints() {
	this.setErrorSChars(this.getCharacters());
	for(DestructiveRule rule : this.getPopRules())
		for(SCharacter schar : this.getCharacters())
			rule.addAfterSChar(schar);
	for(DestructiveRule rule : this.getCollapseRules())
		for(SCharacter schar : this.getCharacters())
			rule.addAfterSChar(schar);
    }

    /**
     * @return Returns the Iterable intended for storing stack characters a such that (qerror, s) with top_1(s) = a
     * might be reachable. (To be computed by e.g. approximate reachability graph).
     *
     */
    public Iterable<SCharacter> getErrorSChars() {
	return this.errorSChars;
    }

    /**
     * Sets the Iterable intended for storing stack characters a such that (qerror, s) with top_1(s) = a
     * might be reachable.
     *
     * @param errorSChars  A managedset that satisfies the requirement above
     *						(To be computed by e.g. approximate reachability graph).
     */
    public void setErrorSChars(Iterable<SCharacter> errorSChars) {
	this.errorSChars = errorSChars;
    }

    /**
     * An array of maps for indexing rule collections by order and one argument
     */
    private class RuleIndex<K, V> {

        int size = 0;

        Collection<V> empty = new HashSet<V>();

        ArrayList<HashMap<K, HashSet<V>>> array
            = new ArrayList<HashMap<K, HashSet<V>>>();

        public RuleIndex() { }

        /**
         * @param k the index key
         * @param order the order to get the rules for
         * @return a collection of rules for index (order, k)
         */
        public final Collection<V> get(K k, byte order) {
            if (array.size() < order) {
                return empty;
            } else {
                Collection<V> c = array.get(order-1).get(k);
                return (c == null) ? empty : c;
            }
        }

        /**
         * Add a rule with given index
         *
         * @param k the key to index against
         * @param order the order to index against
         * @param v the v to add
         */
        public final boolean add(K k, byte order, V v) {
            ensureSize(order);
            HashMap<K, HashSet<V>> m = array.get(order-1);

            HashSet<V> c = m.get(k);
            if (c == null) {
                c = new HashSet<V>();
                m.put(k, c);
            }

            boolean added = c.add(v);
            if (added)
                size++;
            return added;
        }

        /**
         * Remove a rule with given index
         *
         * @param k the key indexed against
         * @param order the order indexed against
         * @param v the V to remove
         */
        public final boolean remove(K k, byte order, V v) {
            ensureSize(order);
            HashMap<K, HashSet<V>> m = array.get(order-1);

            boolean removed = false;

            HashSet<V> c = m.get(k);
            if (c != null) {
                removed = c.remove(v);
            }

            if (removed)
                size--;

            return removed;
        }


        /**
         * @return the number of rules in the index
         */
        public final int size() {
            return size;
        }

        /**
         * @return collection of all values in the index
         */
        public final Collection<V> values() {
            HashSet<V> vs = new HashSet<V>();
            for (HashMap<K, HashSet<V>> maps : array) {
                for (HashSet<V> rs : maps.values()) {
                    vs.addAll(rs);
                }
            }
            return vs;
        }

        /**
         * @param size assure we have maps for indexes up to size-1
         */
        private final void ensureSize(int size) {
            int curSize = array.size();
            if (curSize < order) {
                array.ensureCapacity(order);
                for (int i = curSize; i < order; i++) {
                    HashMap<K, HashSet<V>> m = new HashMap<K, HashSet<V>>();
                    array.add(i, m);
                }
            }
        }
    }

    /**
     * An array of double maps for indexing rule collections by order and two
     * arguments
     */
    private class DoubleRuleIndex<K1, K2, V> {

        HashSet<V> empty = new HashSet<V>();

        ArrayList<DoubleMap<K1, K2, HashSet<V>>> array
            = new ArrayList<DoubleMap<K1, K2, HashSet<V>>>();

        int size = 0;

        public DoubleRuleIndex() { }

         /**
         * @param k1 the first index key
         * @param k2 the second index key
         * @param order the order to get the rules for
         * @return a collection of rules for index (order, k1, k2)
         */
        public final Collection<V> get(K1 k1, K2 k2, byte order) {
            if (array.size() < order) {
                return empty;
            } else {
                Collection<V> c = array.get(order-1).get(k1, k2);
                return (c == null) ? empty : c;
            }
        }

        /**
         * Add a rule with given index
         *
         * @param k1 the first key to index against
         * @param k2 the second key to index against
         * @param order the order to index against
         * @param v the v to add
         */
        public final boolean add(K1 k1, K2 k2, byte order, V v) {
            ensureSize(order);
            DoubleMap<K1, K2, HashSet<V>> m = array.get(order-1);

            HashSet<V> c = m.get(k1, k2);
            if (c == null) {
                c = new HashSet<V>();
                m.put(k1, k2, c);
            }

            boolean added = c.add(v);
            if (added)
                size++;
            return added;
        }

        /**
         * Remove a rule with given index
         *
         * @param k1 the first key indexed against
         * @param k2 the second key indexed against
         * @param order the order indexed against
         * @param v the V to remove
         */
        public final boolean remove(K1 k1, K2 k2, byte order, V v) {
            ensureSize(order);
            DoubleMap<K1, K2, HashSet<V>> m = array.get(order-1);

            boolean removed = false;

            HashSet<V> c = m.get(k1, k2);
            if (c != null) {
                removed = c.remove(v);
            }

            if (removed)
                size--;

            return removed;
        }


        /**
         * @return the number of rules in the index
         */
        public final int size() {
            return size;
        }


        /**
         * @return collection of all values in the index
         */
        public final Collection<V> values() {
            HashSet<V> vs = new HashSet<V>();
            for (DoubleMap<K1, K2, HashSet<V>> maps : array) {
                for (HashSet<V> rs : maps.values()) {
                    vs.addAll(rs);
                }
            }
            return vs;
        }

        /**
         * @param size assure we have maps for indexes up to size-1
         */
        private final void ensureSize(int size) {
            int curSize = array.size();
            if (curSize < order) {
                array.ensureCapacity(order);
                for (int i = curSize; i < order; i++) {
                    DoubleMap<K1, K2, HashSet<V>> m
                        = new DoubleMap<K1, K2, HashSet<V>>();
                    array.add(i, m);
                }
            }
        }

    }


    public static final String error_state = "error";

    // (p, a, push(k), p') lookup by p', indexed by order
    private RuleIndex<ControlState, PushRule> pushRules
        = new RuleIndex<ControlState, PushRule>();
    // (p, a, pop(k), p') lookup by p', indexed by order
    private RuleIndex<ControlState, PopRule> popRules
        = new RuleIndex<ControlState, PopRule>();
    // (p, a, collapse(k), p') lookup by p'
    private RuleIndex<ControlState, CollapseRule> collapseRules
        = new RuleIndex<ControlState, CollapseRule>();
    // (p, a, rew(b), p') lookup by p', b
    private DoubleRuleIndex<ControlState, SCharacter, RewRule> rewRules
        = new DoubleRuleIndex<ControlState, SCharacter, RewRule>();
    // (p, a, push(b)(k), p') lookup by p', a
    private DoubleRuleIndex<ControlState, SCharacter, CPushRule> cpushRules
        = new DoubleRuleIndex<ControlState, SCharacter, CPushRule>();
    // (p, a, eval-nt(b), p') lookup by p', a
    private DoubleRuleIndex<ControlState, SCharacter, EvalNonTermRule> evalNonTermRules
        = new DoubleRuleIndex<ControlState, SCharacter, EvalNonTermRule>();
    // (p, a, rew(b)(k), p') lookup by p', b
    private DoubleRuleIndex<ControlState, SCharacter, RewLinkRule> rewLinkRules
        = new DoubleRuleIndex<ControlState, SCharacter, RewLinkRule>();
    // (p, a, getarg(b)(k), p') lookup by p', b
    private DoubleRuleIndex<ControlState, SCharacter, GetArgRule> getArgRules
        = new DoubleRuleIndex<ControlState, SCharacter, GetArgRule>();
    // (p, a, getorder0arg(b), p') lookup by p', b
    private DoubleRuleIndex<ControlState, SCharacter, GetOrder0ArgRule> getOrder0ArgRules
        = new DoubleRuleIndex<ControlState, SCharacter, GetOrder0ArgRule>();
    // (p, a, {(p1,a1),...,(pm,am)) lookup by pi, ai (do not use this for rule
    // sizes as it repeats entries m times)
    private DoubleRuleIndex<ControlState, SCharacter, AltRule> altRules
        = new DoubleRuleIndex<ControlState, SCharacter, AltRule>();


    // Indexing the rules in the other direction (forwards)
    // but no need to index by order so use ordinary DoubleMultiMaps:
    // (p, a, push(k), p') lookup by p, a
    private UnsafeDoubleMultiMap<ControlState, SCharacter, PushRule> pushRulesFwd
        = new NaiveUnsafeDoubleMultiMap<ControlState, SCharacter, PushRule>();
    // (p, a, pop(k), p') lookup by p, a
    private UnsafeDoubleMultiMap<ControlState, SCharacter, PopRule> popRulesFwd
	= new NaiveUnsafeDoubleMultiMap<ControlState, SCharacter, PopRule>();

    // (p, a, collapse(k), p') lookup by p, a
    private UnsafeDoubleMultiMap<ControlState, SCharacter, CollapseRule> collapseRulesFwd
	= new NaiveUnsafeDoubleMultiMap<ControlState, SCharacter, CollapseRule>();

    // (p, a, rew(b), p') lookup by p, a
    private UnsafeDoubleMultiMap<ControlState, SCharacter, RewRule> rewRulesFwd
	= new NaiveUnsafeDoubleMultiMap<ControlState, SCharacter, RewRule>();


    // (p, a, push(b)(k), p') lookup by p, a
    private UnsafeDoubleMultiMap<ControlState, SCharacter, CPushRule> cpushRulesFwd
	= new NaiveUnsafeDoubleMultiMap<ControlState, SCharacter, CPushRule>();

    // (p, a, eval-nt(b), p') lookup by p, a
    private UnsafeDoubleMultiMap<ControlState, SCharacter, EvalNonTermRule> evalNonTermRulesFwd
	= new NaiveUnsafeDoubleMultiMap<ControlState, SCharacter, EvalNonTermRule>();

    // (p, a, rew(b)(k), p') lookup by p, a
    private UnsafeDoubleMultiMap<ControlState, SCharacter, RewLinkRule> rewLinkRulesFwd
	= new NaiveUnsafeDoubleMultiMap<ControlState, SCharacter, RewLinkRule>();

    // (p, a, getarg(b)(k), p') lookup by p, a
    private UnsafeDoubleMultiMap<ControlState, SCharacter, GetArgRule> getArgRulesFwd
	= new NaiveUnsafeDoubleMultiMap<ControlState, SCharacter, GetArgRule>();

    // (p, a, rew(b)(k), p') lookup by p, a
    private UnsafeDoubleMultiMap<ControlState, SCharacter, GetOrder0ArgRule> getOrder0ArgRulesFwd
	= new NaiveUnsafeDoubleMultiMap<ControlState, SCharacter, GetOrder0ArgRule>();

    // (p, a, {(p1,a1),...,(pm,am)}) lookup by p, a
    private UnsafeDoubleMultiMap<ControlState, SCharacter, AltRule> altRulesFwd
	= new NaiveUnsafeDoubleMultiMap<ControlState, SCharacter, AltRule>();

    Set<ControlState> controlStates = new HashSet<ControlState>();
    Set<SCharacter> characters = new HashSet<SCharacter>();

    ControlState initControl = null;
    SCharacter initCharacter = null;

    byte order = 0;


    public Cpds() { }

    /*
     * @return the order of the cpds
     */
    public byte getOrder() {
        return order;
    }

    /*
     * @return the control states of the cpds
     */
    public Collection<ControlState> getControls() {
        return controlStates;
    }

    /*
     * @return the stack characters of the cpds
     */
    public Collection<SCharacter> getCharacters() {
        return characters;
    }

    /**
     * @return true if an initial control and schar has been set
     */
    public boolean hasInitConfig() {
        return (initControl != null && initCharacter != null);
    }

    /**
     * @return the initial control state (null if not set)
     */
    public ControlState getInitControlState() {
        return initControl;
    }

    /**
     * @return the initial stack character (null if not set)
     */
    public SCharacter getInitCharacter() {
        return initCharacter;
    }

    /**
     * @param c the new initial control state
     */
    public void setInitControlState(ControlState c) {
        initControl = c;
        if (initControl != null)
            controlStates.add(c);
    }

    /**
     * @param c the new initial stack character
     */
    public void setInitCharacter(SCharacter c) {
        initCharacter = c;
        if (initCharacter != null)
            characters.add(c);
    }

    /**
     * @param rule the rule to add to the CPDS
     */
    public void addRule(Rule rule) {
        // this makes me cringe too, but the visitor pattern is worse...
        // (perhaps we should make this scala and do pattern matching (which is
        // the same really, but still...))

        // Do these two first else they get turned into rew and rewlink (dodgy
        // programming i suppose...)
        final Cpds parent = this;
        rule.accept(new RuleVisitor() {
            public void visit(GetArgRule r) { parent.addGetArgRule(r); }
            public void visit(GetOrder0ArgRule r) { parent.addGetOrder0ArgRule(r); }
            public void visit(PushRule r) { parent.addPushRule(r); }
            public void visit(PopRule r) { parent.addPopRule(r); }
            public void visit(CollapseRule r) { parent.addCollapseRule(r); }
            public void visit(RewRule r) { parent.addRewRule(r); }
            public void visit(CPushRule r) { parent.addCPushRule(r); }
            public void visit(EvalNonTermRule r) { parent.addEvalNonTermRule(r); }
            public void visit(RewLinkRule r) { parent.addRewLinkRule(r); }
            public void visit(AltRule r) { parent.addAltRule(r); }
        });
    }

    /**
     * @param rule the push rule to add to the CPDS
     */
    public void addPushRule(PushRule rule) {
        if (rule != null) {
            updateInfo(rule);
            pushRules.add(rule.getControl2(),
                          rule.getOrder(),
                          rule);
            pushRulesFwd.put(rule.getControl1(), rule.getCharacter(), rule);
        }
    }

    /**
     * @param rule the pop rule to add to the CPDS
     */
    public void addPopRule(PopRule rule) {
        if (rule != null) {
            updateInfo(rule);
            popRules.add(rule.getControl2(),
                         rule.getOrder(),
                         rule);
            popRulesFwd.put(rule.getControl1(), rule.getCharacter(), rule);
        }
    }

    /**
     * @param rule the collapse rule to add to the CPDS
     */
    public void addCollapseRule(CollapseRule rule) {
        if (rule != null) {
            if (rule.getOrder() == 1) {
                logger.error("Collapse of order-1 ignored: " + rule);
            } else {
                updateInfo(rule);
                collapseRules.add(rule.getControl2(),
                                      rule.getOrder(),
                                      rule);
                collapseRulesFwd.put(rule.getControl1(),
                                     rule.getCharacter(),
                                     rule);
            }
        }
    }

    /**
     * @param rule the rew rule to add to the CPDS
     */
    public void addRewRule(RewRule rule) {
        if (rule != null) {
            updateInfo(rule);
            rewRules.add(rule.getControl2(),
                         rule.getRewCharacter(),
                         rule.getOrder(),
                         rule);
            rewRulesFwd.put(rule.getControl1(), rule.getCharacter(), rule);
        }
    }

    /**
     * @param rule the cpush rule to add to the CPDS
     */
    public void addCPushRule(CPushRule rule) {
        if (rule != null) {
            updateInfo(rule);
            cpushRules.add(rule.getControl2(),
                           rule.getPushCharacter(),
                           (byte)rule.getOrder(),
                           rule);
            cpushRulesFwd.put(rule.getControl1(), rule.getCharacter(), rule);
        }
    }

    /**
     * @param rule the eval-nt rule to add to the CPDS
     */
    public void addEvalNonTermRule(EvalNonTermRule rule) {
        if (rule != null) {
            updateInfo(rule);
            evalNonTermRules.add(rule.getControl2(),
                                 rule.getPushCharacter(),
                                 (byte)rule.getOrder(),
                                 rule);
            evalNonTermRulesFwd.put(rule.getControl1(), rule.getCharacter(), rule);
        }
    }

    /**
     * @param rule the rewLink rule to add to the CPDS
     */
    public void addRewLinkRule(RewLinkRule rule) {
        if (rule != null) {
            updateInfo(rule);
            rewLinkRules.add(rule.getControl2(),
                             rule.getRewCharacter(),
                             (byte)rule.getOrder(),
                             rule);
            rewLinkRulesFwd.put(rule.getControl1(), rule.getCharacter(), rule);
        }
    }

    /**
     * @param rule the rewLink rule to add to the CPDS
     */
    public void addGetArgRule(GetArgRule rule) {
        if (rule != null) {
            updateInfo(rule);
            getArgRules.add(rule.getControl2(),
                            rule.getRewCharacter(),
                            (byte)rule.getOrder(),
                            rule);
            getArgRulesFwd.put(rule.getControl1(), rule.getCharacter(), rule);
        }
    }

    /**
     * @param rule the rew rule to add to the CPDS
     */
    public void addGetOrder0ArgRule(GetOrder0ArgRule rule) {
        if (rule != null) {
            updateInfo(rule);
            getOrder0ArgRules.add(rule.getControl2(),
                                  rule.getRewCharacter(),
                                  rule.getOrder(),
                                  rule);
            getOrder0ArgRulesFwd.put(rule.getControl1(), rule.getCharacter(), rule);
        }
    }

    /**
     * @param rule the alt rule to add to the CPDS
     */
    public void addAltRule(AltRule rule) {
        if (rule != null) {
            updateInfo(rule);
            for (StackHead h : rule.getDestinations()) {
                altRules.add(h.getControl(),
                             h.getCharacter(),
                             (byte)1,
                             rule);
            }
            altRulesFwd.put(rule.getControl1(), rule.getCharacter(), rule);
        }
    }



    /**
     * @return the number of rules in the CPDS
     */
    public int getNumRules() {
        return pushRules.size() +
               popRules.size() +
               collapseRules.size() +
               rewRules.size() +
               cpushRules.size() +
               evalNonTermRules.size() +
               rewLinkRules.size() +
               getArgRules.size() +
               getOrder0ArgRules.size() +
               altRulesFwd.size();
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        return toString(s).toString();
    }

    public StringBuilder toString(StringBuilder s) {
        if (hasInitConfig()) {
            s.append("Initial config: <");
            s.append(initControl);
            s.append(", [..[");
            s.append(initCharacter);
            s.append("]..]>\n");
        }
        s.append("Cpds has ");
        s.append(getNumRules());
        s.append(" rules:\n");
        printRules(pushRules.values(), s);
        printRules(popRules.values(), s);
        printRules(collapseRules.values(), s);
        printRules(rewRules.values(), s);
        printRules(cpushRules.values(), s);
        printRules(evalNonTermRules.values(), s);
        printRules(rewLinkRules.values(), s);
        printRules(getArgRules.values(), s);
        printRules(getOrder0ArgRules.values(), s);
        printRules(altRulesFwd.values(), s);

        return s;
    }

    /**
     * TODO: return an iterator that goes through all rather than creating a new
     * set of all rules (though unlikely that we'll need to save on the
     * efficiency here).
     *
     * @return	Collection of all rules
     */
    public Collection<Rule> getRules() {
        Collection<Rule> rules = new HashSet<Rule>();
        rules.addAll(pushRules.values());
        rules.addAll(popRules.values());
        rules.addAll(collapseRules.values());
        rules.addAll(rewRules.values());
        rules.addAll(cpushRules.values());
        rules.addAll(evalNonTermRules.values());
        rules.addAll(rewLinkRules.values());
        rules.addAll(getArgRules.values());
        rules.addAll(getOrder0ArgRules.values());
        rules.addAll(altRulesFwd.values());
        return rules;
    }


    public String cpdsStats() {
        StringBuilder sb = new StringBuilder();
        sb.append("Number of push(k) rules: " + pushRules.size() + "\n");
        sb.append("Number of pop(k) rules: " + popRules.size() + "\n");
        sb.append("Number of cpush(b)(k) rules: " + cpushRules.size() + "\n");
        sb.append("Number of rew(b)(k) rules: " + rewLinkRules.size() + "\n");
        sb.append("Number of rew(b) rules: " + rewRules.size() + "\n");
        sb.append("Number of getArg(b)(k) rules: " + getArgRules.size() + "\n");
        sb.append("Number of getOrder0Arg(b) rules: " + getOrder0ArgRules.size() + "\n");
        sb.append("Number of eval-nt(b) rules: " + evalNonTermRules.size() + "\n");
        sb.append("Number of alternating rules: " + altRulesFwd.size());
        return sb.toString();
    }

    /**
     * @param rules a collection of rules to print to the string builder
     * @param s the string builder to write to
     */
    private void printRules(Collection<? extends Rule> rules,
                            StringBuilder s) {
        for (Rule r : rules) {
            s.append("    ");
            s.append(r.toString());
            s.append("\n");
        }
    }


    /**
     * updates set of characters, control states and order of cpds by rule
     */
    private void updateInfo(Rule r) {
        order = (byte)Math.max(order, r.getOrder());
        controlStates.addAll(r.getControls());
        characters.addAll(r.getCharacters());
    }


    /**
     * @param control the destination control
     * @param order the order
     * @return collection of all rules (_, character, push(order), control)
     *         (never null)
     */
    public final Collection<PushRule> getPushRules(ControlState control,
                                                   byte order) {
        return pushRules.get(control, order);
    }

    /**
     * @param control the destination control
     * @param order the order
     * @return collection of all rules (_, _, pop(order), control)
     *         (never null)
     */
    public final Collection<PopRule> getPopRules(ControlState control,
                                                 byte order) {
        return popRules.get(control, order);
    }

    /**
     * @param control the destination control
     * @param order the order
     * @return collection of all rules (_, _, collapse(order), control)
     *         (never null)
     */
    public final Collection<CollapseRule> getCollapseRules(ControlState control,
                                                           byte order) {
        return collapseRules.get(control, order);
    }

    /**
     * @param control the destination control
     * @param rewCharacter the character to rewrite to
     * @return collection of all rules (_, _, rew(rewCharacter), control)
     *         (never null)
     */
    public final Collection<RewRule> getRewRules(ControlState control,
                                                 SCharacter rewCharacter) {
        // TODO: should probably get rid of constant order...
        return rewRules.get(control, rewCharacter, (byte)1);
    }

    /**
     * @param control the destination control
     * @param pushCharacter the character to push
     * @param order the order
     * @return collection of all rules (_, _, cpush(pushCharacter)(order), control)
     *         (never null)
     */
    public final Collection<CPushRule> getCPushRules(ControlState control,
                                                     SCharacter pushCharacter,
                                                     byte order) {
        return cpushRules.get(control, pushCharacter, order);
    }

    /**
     * @param control the destination control
     * @param pushCharacter the character to push
     * @return collection of all rules (_, _, eval-nt(pushCharacter), control)
     *         (never null)
     */
    public final Collection<EvalNonTermRule> getEvalNonTermRules(ControlState control,
                                                                 SCharacter pushCharacter) {
        return evalNonTermRules.get(control, pushCharacter, (byte)1);
    }

    /**
     * @param control the destination control
     * @param rewCharacter the character to rewrite to
     * @param order the order of the link to create
     * @return collection of all rules (_, _, rew(rewCharacter)(order), control)
     *         (never null)
     */
    public final Collection<RewLinkRule> getRewLinkRules(ControlState control,
                                                         SCharacter rewCharacter,
                                                         byte order) {
        return rewLinkRules.get(control, rewCharacter, order);
    }

    /*
     * @param control the destination control
     * @param rewCharacter the character to rewrite to
     * @param order the order of the link to create
     * @return collection of all rules (_, _, getarg(rewCharacter)(order), control)
     *         (never null)
     */
    public final Collection<GetArgRule> getGetArgRules(ControlState control,
                                                       SCharacter rewCharacter,
                                                       byte order) {
        return getArgRules.get(control, rewCharacter, order);
    }


    /**
     * @param control the destination control
     * @param rewCharacter the character to rewrite to
     * @return collection of all rules (_, _, getorder0arg(rewCharacter), control)
     *         (never null)
     */
    public final Collection<GetOrder0ArgRule> getGetOrder0ArgRules(ControlState control,
                                                                   SCharacter rewCharacter) {
        // TODO: should probably get rid of constant order...
        return getOrder0ArgRules.get(control, rewCharacter, (byte)1);
    }

    /**
     * @param control the destination control
     * @param character the character
     * @return collection of all rules (_, _, {...(control,character)...})
     *         (never null)
     */
    public final Collection<AltRule> getAltRules(ControlState control,
                                                 SCharacter character) {
        return altRules.get(control, character, (byte)1);
    }

    /**
     *
     * @param control		the source control state
     * @param topChar		the top character on which the transition can be fired
     * @return				iterable over all rules of the form (control, topChar, push_k, _)
     *						with k > 1
     */
    public final Iterable<PushRule> getPushRulesFwd(ControlState control,
													SCharacter topChar) {
	return pushRulesFwd.get(control, topChar);
    }

    /**
     *
     * @param control		the source control state
     * @param topChar		the top character on which the transition can be fired
     * @return				iterable over all rules of the form (control, topChar, pop_i, _)
     *						for any i.
     */
    public final Iterable<PopRule> getPopRulesFwd(ControlState control,
													SCharacter topChar) {
	return popRulesFwd.get(control, topChar);
    }

    /**
     *
     * @param control		the source control state
     * @param topChar		the top character on which the transition can be fired
     * @return				iterable over all rules of the form (control, topChar, collapse_i, _)
     *						for any i.
     */
    public final Iterable<CollapseRule> getCollapseRulesFwd(ControlState control,
													SCharacter topChar) {
	return collapseRulesFwd.get(control, topChar);
    }

    /**
     *
     * @param control		the source control state
     * @param topChar		the top character on which the transition can be fired
     * @return				iterable over all rules of the form (control, topChar, rew, _)
     *
     */
    public final Iterable<RewRule> getRewRulesFwd(ControlState control,
													SCharacter topChar) {
	return rewRulesFwd.get(control, topChar);
    }

    /**
     *
     * @param control		the source control state
     * @param topChar		the top character on which the transition can be fired
     * @return				iterable over all rules of the form (control, topChar, cpush(_)(_), _)
     *
     */
    public final Iterable<CPushRule> getCPushRulesFwd(ControlState control,
													SCharacter topChar) {
	return cpushRulesFwd.get(control, topChar);
    }

    /**
     *
     * @param control		the source control state
     * @param topChar		the top character on which the transition can be fired
     * @return				iterable over all rules of the form (control, topChar, eval-nt(_), _)
     *
     */
    public final Iterable<EvalNonTermRule> getEvalNonTermRulesFwd(ControlState control,
                                                                  SCharacter topChar) {
	return evalNonTermRulesFwd.get(control, topChar);
    }


    /**
     *
     * @param control		the source control state
     * @param topChar		the top character on which the transition can be fired
     * @return				iterable over all rules of the form (control, topChar, rewLink(_)(_), _)
     *
     */
    public final Iterable<RewLinkRule> getRewLinkRulesFwd(ControlState control,
													SCharacter topChar) {
	return rewLinkRulesFwd.get(control, topChar);
    }

    /**
     *
     * @param control		the source control state
     * @param topChar		the top character on which the transition can be fired
     * @return				iterable over all rules of the form (control, topChar, getarg(_)(_), _)
     *
     */
    public final Iterable<GetArgRule> getGetArgRulesFwd(ControlState control,
													SCharacter topChar) {
	return getArgRulesFwd.get(control, topChar);
    }

    /**
     *
     * @param control		the source control state
     * @param topChar		the top character on which the transition can be fired
     * @return				iterable over all rules of the form (control, topChar, getorder0arg, _)
     *
     */
    public final Iterable<GetOrder0ArgRule> getGetOrder0ArgRulesFwd(ControlState control,
                                                                    SCharacter topChar) {
	return getOrder0ArgRulesFwd.get(control, topChar);
    }

    /**
     *
     * @param control		the source control state
     * @param topChar		the top character on which the transition can be fired
     * @return				iterable over all rules of the form (control, topChar, {...})
     *
     */
    public final Iterable<AltRule> getAltRulesFwd(ControlState control,
											  SCharacter topChar) {
	return altRulesFwd.get(control, topChar);
    }


    /**
     * @return the set of rewrite rules
     */
    public final Iterable<RewRule> getRewRules() {
        return rewRules.values();
    }

    /**
     * @return the set of push rules
     */
    public final Iterable<PushRule> getPushRules() {
        return pushRules.values();
    }

    /**
     * @return the set of pop rules
     */
    public final Iterable<PopRule> getPopRules() {
        return popRules.values();
    }

    /**
     * @return the set of collapse rules
     */
    public final Iterable<CollapseRule> getCollapseRules() {
        return collapseRules.values();
    }

    /**
     * @return the set of cpush rules
     */
    public final Iterable<CPushRule> getCPushRules() {
        return cpushRules.values();
    }

    /**
     * @return the set of eval-nt rules
     */
    public final Iterable<EvalNonTermRule> getEvalNonTermRules() {
        return evalNonTermRules.values();
    }


    /**
     * @return the set of rewrite link rules
     */
    public final Iterable<RewLinkRule> getRewLinkRules() {
        return rewLinkRules.values();
    }

    /**
     * @return the set of getarg rules
     */
    public final Iterable<GetArgRule> getGetArgRules() {
        return getArgRules.values();
    }

    /**
     * @return the set of getorder0arg rules
     */
    public final Iterable<GetOrder0ArgRule> getGetOrder0ArgRules() {
        return getOrder0ArgRules.values();
    }

    /**
     * @return the set of alternating rules
     */
    public final Iterable<AltRule> getAltRules() {
        return altRulesFwd.values();
    }


    /**
     * Removes a rule from the cpds, if it exists
     *
     * @param rule the rule to remove
     */
    public final void removeRule(Rule rule) {
        final Cpds parent = this;
        rule.accept(new RuleVisitor() {
            public void visit(PushRule r) {
                parent.removePushRule(r);
            }

            public void visit(PopRule r) {
                parent.removePopRule(r);
            }

            public void visit(CollapseRule r) {
                parent.removeCollapseRule(r);
            }

            public void visit(RewRule r) {
                parent.removeRewRule(r);
            }

            public void visit(CPushRule r) {
                parent.removeCPushRule(r);
            }

            public void visit(EvalNonTermRule r) {
                parent.removeEvalNonTermRule(r);
            }

            public void visit(RewLinkRule r) {
                parent.removeRewLinkRule(r);
            }

            public void visit(GetArgRule r) {
                parent.removeGetArgRule(r);
            }

            public void visit(GetOrder0ArgRule r) {
                parent.removeGetOrder0ArgRule(r);
            }

            public void visit(AltRule r) {
                parent.removeAltRule(r);
            }
        });
    }



    /**
     * Removes a push rule from the cpds, if it exists
     *
     * @param rule the rule to remove
     */
    public final void removePushRule(PushRule rule) {
        pushRules.remove(rule.getControl2(),
                         rule.getOrder(),
                         rule);
        pushRulesFwd.remove(rule.getControl1(),
                            rule.getCharacter(),
                            rule);
    }
    /**
     * @param rule the pop rule to remove
     */
    public void removePopRule(PopRule rule) {
        popRules.remove(rule.getControl2(),
                        rule.getOrder(),
                        rule);
        popRulesFwd.remove(rule.getControl1(),
                           rule.getCharacter(),
                           rule);
    }

    /**
     * @param rule the collapse rule to remove from the CPDS
     */
    public void removeCollapseRule(CollapseRule rule) {
        collapseRules.remove(rule.getControl2(),
                             rule.getOrder(),
                             rule);
        collapseRulesFwd.remove(rule.getControl1(),
                                rule.getCharacter(),
                                rule);
    }

    /**
     * @param rule the rew rule to remove from the CPDS
     */
    public void removeRewRule(RewRule rule) {
        rewRules.remove(rule.getControl2(),
                        rule.getRewCharacter(),
                        rule.getOrder(),
                        rule);
        rewRulesFwd.remove(rule.getControl1(),
                           rule.getCharacter(),
                           rule);
    }

    /**
     * @param rule the cpush rule to remove from the CPDS
     */
    public void removeCPushRule(CPushRule rule) {
        cpushRules.remove(rule.getControl2(),
                          rule.getPushCharacter(),
                          rule.getOrder(),
                          rule);
        cpushRulesFwd.remove(rule.getControl1(),
                             rule.getCharacter(),
                             rule);
    }

    /**
     * @param rule the cpush rule to remove from the CPDS
     */
    public void removeEvalNonTermRule(EvalNonTermRule rule) {
        evalNonTermRules.remove(rule.getControl2(),
                                rule.getPushCharacter(),
                                (byte)1,
                                rule);
        evalNonTermRulesFwd.remove(rule.getControl1(),
                                   rule.getCharacter(),
                                   rule);
    }


    /**
     * @param rule the rewLink rule to remove from the CPDS
     */
    public void removeRewLinkRule(RewLinkRule rule) {
        rewLinkRules.remove(rule.getControl2(),
                            rule.getRewCharacter(),
                            rule.getOrder(),
                            rule);
        rewLinkRulesFwd.remove(rule.getControl1(),
                               rule.getCharacter(),
                               rule);
    }

    /**
     * @param rule the getarg rule to remove from the CPDS
     */
    public void removeGetArgRule(GetArgRule rule) {
        getArgRules.remove(rule.getControl2(),
                           rule.getRewCharacter(),
                           rule.getOrder(),
                           rule);
        getArgRulesFwd.remove(rule.getControl1(),
                              rule.getCharacter(),
                              rule);
    }

    /**
     * @param rule the getarg rule to remove from the CPDS
     */
    public void removeGetOrder0ArgRule(GetOrder0ArgRule rule) {
        getOrder0ArgRules.remove(rule.getControl2(),
                                 rule.getRewCharacter(),
                                 rule.getOrder(),
                                 rule);
        getOrder0ArgRulesFwd.remove(rule.getControl1(),
                                    rule.getCharacter(),
                                    rule);
    }


    /**
     * @param rule the rule to remove
     */
    public void removeAltRule(AltRule rule) {
        for (StackHead h : rule.getDestinations()) {
            altRules.remove(h.getControl(),
                            h.getCharacter(),
                            (byte)1,
                            rule);
        }
        altRulesFwd.remove(rule.getControl1(),
                           rule.getCharacter(),
                           rule);
    }


}
