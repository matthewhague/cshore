/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.cpds;

import java.util.Stack;

import java.lang.StringBuilder;
import java.lang.SuppressWarnings;
import java.lang.Iterable;
import java.util.Iterator;

import org.apache.log4j.Logger;

import ho.managers.Managers;


/**
 * A class for simulating CPDSs -- stores an explicit representation of a
 * configuration which can be manipulated with CPDS rules.
 */
public class CpdsConfig {

    static Logger logger = Logger.getLogger(CpdsConfig.class);

    private ControlState control;
    private byte order;
    private CpdsStack stack;


    /**
     * Builds a configuration (control, [...[init]...])
     *
     * @param c the control state of the configuration
     * @param init the initial stack character
     * @param order the order of the stack
     */
    public CpdsConfig(ControlState control, SCharacter init, byte order) {
        this.control = control;
        this.order = order;
        this.stack = new CpdsStack(init, order);
    }


    /**
     * Builds a configuration (control, s)
     *
     * @param c the control state of the configuration
     * @param stack the initial stack
     * @param order the order of the stack
     */
    public CpdsConfig(ControlState control, CpdsStack stack, byte order) {
        this.control = control;
        this.order = order;
        this.stack = stack;
    }

    /**
     * @param c a configuration to create a deep copy of
     */
    public CpdsConfig(CpdsConfig c) {
        this.control = c.getControlState();
        this.order = c.getOrder();
        this.stack = new CpdsStack(c.getStack());
    }



    public ControlState getControlState() {
        return control;
    }

    public CpdsStack getStack() {
        return stack;
    }

    public byte getOrder() {
        return order;
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("<");
        s.append(control);
        s.append(" ");
        s.append(stack);
        s.append(">");
        return s.toString();
    }



    public boolean applyRules(Iterator<Rule> rules) {
        boolean valid = true;
        while (rules.hasNext() && valid) {
            valid = applyRule(rules.next());
        }
        return valid;
    }

    /**
     * Applies the given rule to the configuration, if applicable, else returns
     * false and leaves the configuration unchanged.
     *
     * @param r the rule to apply
     * @return true if the rule was successfully applied, false if it was not
     * applicable
     */
    public boolean applyRule(Rule r) {
        if (r.getControl1() != control ||
            r.getOrder() > order ||
            r.getOrder() < 1)
            return false;

        CharLink cl = stack.getTop1CharLink();
        if (cl == null ||
            cl.a != r.getCharacter() ||
            (r instanceof CollapseRule && r.getOrder() != cl.linkOrder))
            return false;

        if (r instanceof PopRule) {
            stack.applyPop(r.getOrder());
        } else if (r instanceof CollapseRule) {
            stack.applyCollapse(r.getOrder());
        } else if (r instanceof PushRule) {
            stack.applyPush(r.getOrder());
        } else if (r instanceof CPushRule) {
            CPushRule pr = (CPushRule)r;
            stack.applyPush(pr.getPushCharacter(), r.getOrder());
        } else if (r instanceof EvalNonTermRule) {
            EvalNonTermRule pr = (EvalNonTermRule)r;
            stack.applyEvalNonTerm(pr.getPushCharacter());

        // Next two cases exploit the fact the get arg rules are just restricted
        // rewrite rules -- we could actually write a simulation that "forgets"
        // the rest of the stack that we don't care about anymore, but let's do
        // that later if at all.

        } else if (r instanceof RewRule) {
            RewRule pr = (RewRule)r;
            stack.applyRew(pr.getRewCharacter());
        } else if (r instanceof RewLinkRule) {
            RewLinkRule pr = (RewLinkRule)r;
            stack.applyRewLink(pr.getRewCharacter(), r.getOrder());
        } else if (r instanceof GetOrder0ArgRule) {
            GetOrder0ArgRule pr = (GetOrder0ArgRule)r;
            stack.applyGetOrder0Arg(pr.getRewCharacter());
        } else if (r instanceof GetArgRule) {
            GetArgRule pr = (GetArgRule)r;
            stack.applyGetArg(pr.getRewCharacter(), r.getOrder());
        } else if (r instanceof AltRule) {
            logger.error("CpdsConfig doesn't know how to apply an alternating rule " + r);
            System.exit(-1);
        } else {
            logger.error("CpdsConfig doesn't know how to handle rule " + r);
            System.exit(-1);
        }

        // we've made it this far, finish the job
        control = r.getControl2();

        return true;
    }


    public boolean equals(Object o) {
        if (o instanceof CpdsConfig) {
            CpdsConfig c = (CpdsConfig)o;
            return order == c.order &&
                   Managers.csManager.equal(control, c.control) &&
                   stack.equals(c.stack);
        }
        return false;
    }

}
