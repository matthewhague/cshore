/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


/**
 * Representation of a thread interface. That is something of the form, e.g.,
 *
 *     a (q1, q2) (q3, q4) (q5, q6) b
 *
 * which means can start in (q1, [..[a]..]) and have a run to q2
 * then assuming something jumps us to q3 can continue to a4
 * then assuming something jumps us to q5 can continue to q6
 * and at q6 we will have b on top of the stack.
 *
 * Note, if q6 == qerror, we don't specify b
 */


package ho.structures.cpds;

import java.lang.StringBuilder;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

import ho.managers.Managers;
import ho.parsers.CSymbs;

public class ThreadInterface {

    private final ControlState qerror
        = Managers.csManager.makeControl(Cpds.error_state);

    public static final String NIL_CONTROL_NAME = CSymbs.nonIDChar() + "nil";
    public static final String NIL_CHAR_NAME = CSymbs.nonIDChar() + "nil";

    private SCharacter charIn = null;
    private SCharacter charOut = null;
    private List<ControlState> controlStates = new ArrayList<ControlState>();

    public ThreadInterface() { }

    /**
     * A thread interface ending in qerror
     *
     * @param charIn the initial stack character
     * @param controlStates the list q1, q2, q3... of control states ending in
     * qerror
     */
    public ThreadInterface(SCharacter charIn,
                           List<ControlState> controlStates) {
        assert charIn != null &&
               controlStates != null &&
               controlStates.size() > 0 &&
               controlStates.size() % 2 == 0 &&
               qerror.equals(controlStates.get(controlStates.size() - 1));

        this.charIn = charIn;
        this.controlStates.addAll(controlStates);
    }

    /**
     * @param charIn the initial stack character
     * @param charOut the stack character at the end of the run
     * @param controlStates the list q1, q2, q3... of control states ending in
     * qerror
     */
    public ThreadInterface(SCharacter charIn,
                           SCharacter charOut,
                           List<ControlState> controlStates) {
        assert charIn != null &&
               charOut != null &&
               controlStates != null &&
               controlStates.size() > 0 &&
               controlStates.size() % 2 == 0 &&
               qerror.equals(controlStates.get(controlStates.size() - 1));

        this.charIn = charIn;
        this.charOut = charOut;
        this.controlStates.addAll(controlStates);
    }

    public void setCharIn(SCharacter charIn) {
        this.charIn = charIn;
    }

    public void setCharOut(SCharacter charOut) {
        this.charOut = charOut;
    }

    public void setControlPairs(List<ControlState> controlStates) {
        assert controlStates != null && controlStates.size() % 2 == 0;
        this.controlStates.isEmpty();
        this.controlStates.addAll(controlStates);
    }

    public void addControlPair(ControlState qin, ControlState qout) {
        controlStates.add(qin);
        controlStates.add(qout);
    }

    /**
     * @return true if the interface is consistent (i.e. charOut is null iff
     * last control is error)
     */
    public boolean isConsistent() {
        return controlStates.size() % 2 == 0 &&
               ((charIn == null &&
                 charOut == null &&
                 controlStates.size() == 0) ||
                (charOut == null && endError()) ||
                (charOut != null && !controlStates.isEmpty() && !endError()));
    }

    /**
     * @return true if the thread interface is [ ]
     */
    public boolean isEmpty() {
        return controlStates.isEmpty();
    }

    /**
     * @return true if the interface reaches the error state
     */
    public boolean endError() {
        if (controlStates.isEmpty())
            return false;
        return qerror.equals(controlStates.get(controlStates.size() - 1));
    }

    /**
     * @return the number of (qin qout) pairs in the interface
     */
    public int numControlPairs() {
        return controlStates.size() / 2;
    }

    public SCharacter getCharIn() {
        return charIn;
    }

    public SCharacter getCharOut() {
        return charOut;
    }

    public List<ControlState> getControls() {
        return Collections.unmodifiableList(controlStates);
    }

    /**
     * Get the first state of the ith context.  E.g.
     *
     * q1, q2, q3, q4
     *
     * q1 is the first of the 1st context
     * q3 is the first of the 2nd context
     *
     * @param i the number of the context
     * @return the q
     */
    public ControlState getControlIn(int i) {
        assert 1 <= i && i <= controlStates.size() % 2;
        return controlStates.get(2*(i-1));
    }


    /**
     * Get the last state of the ith context.  E.g.
     *
     * q1, q2, q3, q4
     *
     * q2 is the last of the 1st context
     * q4 is the last of the 2nd context
     *
     * @param i the number of the context
     * @return the q
     */
    public ControlState getControlOut(int i) {
        assert 1 <= i && i <= controlStates.size() % 2;
        return controlStates.get(2*(i-1) + 1);
    }

    public String toString() {
        return toString(new StringBuilder()).toString();
    }

    public StringBuilder toString(StringBuilder sb) {
        sb.append("[ ");

        if (charIn != null) {
            sb.append(charIn);
            sb.append(" ");
        }

        for (int i = 0; i < controlStates.size() / 2; ++i) {
            sb.append("(");
            sb.append(controlStates.get(2*i));
            sb.append(" ");
            sb.append(controlStates.get(2*i + 1));
            sb.append(") ");
        }

        if (charOut != null) {
            sb.append(charOut);
            sb.append(" ");
        }

        sb.append("]");

        return sb;
    }


    public boolean equals(Object o) {
        if (o instanceof ThreadInterface) {
            ThreadInterface ti = (ThreadInterface)o;
            return ((charOut == null && ti.charOut == null) ||
                    (charOut != null && charOut.equals(ti.charOut))) &&
                   ((charIn == null && ti.charOut == null) ||
                    (charIn != null && charIn.equals(ti.charIn))) &&
                   controlStates.equals(ti.controlStates);
        }
        return false;
    }

    public int hashCode() {
        int hash = 0;
        if (charIn != null)
            hash += charIn.hashCode();
        if (charOut != null)
            hash += charOut.hashCode();
        return hash + controlStates.hashCode();
    }

    /**
     * Return a thread interface that is essentially equivalent to the current
     * interface except of the form
     *
     *      (a (q1 q'1) ... (qbound q'bound) b)
     *
     * where nil states and chars are used to pad the interface to length
     */
    public ThreadInterface getPaddedInterface(int bound) {
        SCharacter newCharOut = (charOut != null) ? charOut : getNilChar();

        List<ControlState> newControls = new ArrayList<ControlState>(2*bound);
        for (ControlState q : controlStates)
            newControls.add(q);

        ControlState qnil = getNilControl();
        for (int i = 0; i < 2*bound - controlStates.size(); ++i)
            newControls.add(qnil);

        return new ThreadInterface(charIn, newCharOut, newControls);
    }


    /**
     * @return nil control state
     */
    public static ControlState getNilControl() {
        return Managers.csManager.makeControl(NIL_CONTROL_NAME);
    }

    /**
     * @return nil control state
     */
    public static SCharacter getNilChar() {
        return Managers.scManager.makeCharacter(NIL_CHAR_NAME);
    }


}
