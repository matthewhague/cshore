/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.structures.cpds;

import ho.managers.Managers;
import ho.util.ManagedSet;

/**
 * Abstract class for representing destructive rules (pop and collapse).
 * Additionally allows for a "afterwards-top-stack-characters annotation" that is a ManagedSet containing stack characters
 * that might see on top of stack after executing this rule on a reachable configuration
 * *whilst remaining on path to error state* (one might perform this rule on a reachable configuration
 * and result in a configuration that cannot reach an error state). This information could
 * be obtained by, for example, an approximate reachability graph. It would always be sound
 * for this annotation to contain all stack characters.
 *
 */
public abstract class DestructiveRule extends Rule {

	// This set maintains all of the stack characters that might be seen on top following
    // this destructive rule. This information can be filled in by approximate reachability graph
    // and restricts action of saturation algorithm. This does not form part of the equality
    // test of a rule and it is assumed that each pop rule with given defining characteristics
    // (as in standard CPDA) in any given CPDA
    // is unique so that this set is merely an annotation.
    private ManagedSet<SCharacter> afterSChars = Managers.scsetManager.makeEmptySet();

		 public DestructiveRule(ControlState control1,
	             SCharacter character,
	             ControlState control2) {
	     super(control1, character, control2);
	 }

	 public DestructiveRule(ControlState control1,
	             SCharacter character,
	             ControlState control2,
	             RuleAnnotation annotation) {
	     super(control1, character, control2, annotation);
	 }

	 /**
	  * Copies rule but without preserving the "below" annotation.
	  */
	 public DestructiveRule(DestructiveRule r) {
	     super(r);
	 }

	 /**
	  * Reset the annotation of stack characters that might be seen after executing this rule
	  * on a reachable configuration.
	  */
	 public void resetAfterSChars() {
		 this.afterSChars = Managers.scsetManager.makeEmptySet();
	 }

	 /**
	  * Add a stack character to the set of stack characters that might be seen after
	  * performing this rule on a reachable configuration. (To be called by code
	  * that computes such things---e.g. approximate reachability graph code).
	  *
	  * @param schar	The stack character to add.
	  *
	  */
	 public void addAfterSChar(SCharacter schar) {
		 this.afterSChars = Managers.scsetManager.add(this.afterSChars, schar);

	 }

	 /**
	  * Check whether set of stack characters that might be seen after performing this rule
	  * on a reachable configuration (at least the set purporting to be such) contains
	  * a particular stack character.
	  *
	  * @param schar	Stack character to query.
	  * @return
	  */
	 public boolean afterSCharsContains(SCharacter schar) {
		 return this.afterSChars.contains(schar);
	 }



}
