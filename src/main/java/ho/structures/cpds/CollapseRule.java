/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.cpds;

import java.lang.StringBuilder;

import ho.managers.Managers;


/**
 * Representation of rule (p, a, collapse(k), q)
 */
public class CollapseRule extends DestructiveRule {

    byte order;
    int hashCode;

    /**
     * Constructs a rule (control1, character, collapse(order), control2)
     *
     * @param control1 the source control state
     * @param character the source top character
     * @param control2 the destination control state
     * @param order the order of the collapse operation
     */
    public CollapseRule(ControlState control1,
                        SCharacter character,
                        ControlState control2,
                        byte order) {
        super(control1, character, control2);
        this.order = order;
        this.hashCode = control1.hashCode() +
                        character.hashCode() +
                        control2.hashCode() +
                        order;
    }

    /**
     * Constructs a rule (control1, character, collapse(order), control2)
     *
     * @param control1 the source control state
     * @param character the source top character
     * @param control2 the destination control state
     * @param order the order of the collapse operation
     * @param annotation	annotation of the rule
     */
    public CollapseRule(ControlState control1,
                        SCharacter character,
                        ControlState control2,
                        byte order, RuleAnnotation annotation) {
        super(control1, character, control2, annotation);
        this.order = order;
        this.hashCode = control1.hashCode() +
                        character.hashCode() +
                        control2.hashCode() +
                        order;
    }

    /**
     * Copy rule
     */
    public CollapseRule(CollapseRule r) {
        super(r);
        this.order = r.getOrder();
        this.hashCode = r.hashCode();
    }

    /**
     * @return the order of the collapse operation
     */
    public byte getOrder() {
        return order;
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("(");
        s.append(getControl1());
        s.append(" ");
        s.append(getCharacter());
        s.append(" ");
        s.append("collapse(");
        s.append(order);
        s.append(")");
        s.append(" ");
        s.append(getControl2());
        s.append(")");
        return s.toString();
    }

    public final boolean getDestinationHead(StackHead sh) {
        return false;
    }

    public boolean equals(Object o) {
        if (o instanceof CollapseRule) {
            CollapseRule r = (CollapseRule)o;
            return hashCode == o.hashCode() &&
                   Managers.csManager.equal(getControl1(), r.getControl1()) &&
                   Managers.scManager.equal(getCharacter(), r.getCharacter()) &&
                   Managers.csManager.equal(getControl2(), r.getControl2()) &&
                   order == r.getOrder();
        } else
            return false;
    }

    public int hashCode() {
        return hashCode;
    }

    public void accept(RuleVisitor v) { v.visit(this); }

    public Object clone() throws CloneNotSupportedException {
        return new CollapseRule(this);
    }

}
