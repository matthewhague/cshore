/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.cpds;

import java.lang.StringBuilder;

import java.util.Collection;
import java.util.ArrayList;

import ho.managers.Managers;


/**
 * Representation of a rule (p, a, cpush(a)(k), q)
 */
public class CPushRule extends Rule {

    SCharacter pushCharacter;
    byte order;
    int hashCode;

    /**
     * Constructs a representation of a rule (control1, character,
     * cpush(pushCharacter)(order), control2)
     *
     * @param control1 the source control state
     * @param character the source top character
     * @param control2 the destination control state
     * @param pushCharacter the character to push onto the stack
     * @param order the order of the annotation for the new character
     */
    public CPushRule(ControlState control1,
                     SCharacter character,
                     ControlState control2,
                     SCharacter pushCharacter,
                     byte order) {
        super(control1, character, control2);
        this.pushCharacter = pushCharacter;
        this.order = order;
        this.hashCode = control1.hashCode() +
                        character.hashCode() +
                        control2.hashCode() +
                        pushCharacter.hashCode() +
                        order;
    }

    public CPushRule(ControlState control1,
            SCharacter character,
            ControlState control2,
            SCharacter pushCharacter,
            byte order, RuleAnnotation annotation) {
        super(control1, character, control2, annotation);
        this.pushCharacter = pushCharacter;
        this.order = order;
        this.hashCode = control1.hashCode() +
                        character.hashCode() +
                        control2.hashCode() +
                        pushCharacter.hashCode() +
                        order;
    }


    public CPushRule(CPushRule r) {
        super(r);
        this.pushCharacter = r.getPushCharacter();
        this.order = r.getOrder();
        this.hashCode = r.hashCode();
    }

    /**
     * @return the character to be pushed by the rule
     */
    public SCharacter getPushCharacter() {
        return pushCharacter;
    }

    /**
     * @return the order of the annotation created by the rule
     */
    public byte getOrder() {
        return order;
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("(");
        s.append(getControl1());
        s.append(" ");
        s.append(getCharacter());
        s.append(" ");
        s.append("push(");
        s.append(pushCharacter);
        s.append(")");
        if (order > 1) {
            s.append("(");
            s.append(order);
            s.append(")");
        }
        s.append(" ");
        s.append(getControl2());
        s.append(")");
        return s.toString();
    }

    /**
     * This method is inefficient, but we don't expect to use it much.
     *
     * @return the set of characters of the rule
     */
    public Collection<SCharacter> getCharacters() {
        ArrayList<SCharacter> characters = new ArrayList<SCharacter>(2);
        characters.add(0, getCharacter());
        characters.add(1, getPushCharacter());
        return characters;
    }

    public final boolean getDestinationHead(StackHead sh) {
        sh.setControl(getControl2());
        sh.setCharacter(getPushCharacter());
        return true;
    }

    public boolean equals(Object o) {
        if (o instanceof CPushRule) {
            CPushRule r = (CPushRule)o;
            return Managers.csManager.equal(getControl1(), r.getControl1()) &&
                   Managers.scManager.equal(getCharacter(), r.getCharacter()) &&
                   Managers.csManager.equal(getControl2(), r.getControl2()) &&
                   Managers.scManager.equal(getPushCharacter(),
                                            r.getPushCharacter()) &&
                   order == r.getOrder();
        } else
            return false;
    }

    public int hashCode() {
        return hashCode;
    }

    public void accept(RuleVisitor v) { v.visit(this); }

    public Object clone() throws CloneNotSupportedException {
        return new CPushRule(this);
    }

}
