/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.cpds;

import ho.hors.PropAutState;
import ho.util.DoubleMap;
import ho.util.DoubleMap;

import java.util.Map;
import java.util.HashMap;

/**
 * Manager class for control states -- control state objects are only to be
 * created and manipulated with this class (maintains uniqueness, so we don't
 * have to objects corresponding to the same control state).
 */
public class ControlStateManager {

    // shouldn't matter too much that hashing strings is slow -- we won't be
    // creating control states during the main algorithm
    Map<String, ControlState> controls = new HashMap<String, ControlState>();
    DoubleMap<PropAutState, Integer, ControlState> controlsHorsNoNeedPopOne
	= new DoubleMap<PropAutState, Integer, ControlState>();

    DoubleMap<PropAutState, Integer, ControlState> controlsHorsNeedPopOne
	= new DoubleMap<PropAutState, Integer, ControlState>();

    /**
     * @param name the name of the control state to make
     * @return a control state object with the given name
     */
    public ControlState makeControl(String name) {
        ControlState c = controls.get(name);
        if (c == null) {
            c = new ControlStateString(name);
            controls.put(name, c);
        }
        return c;
    }

    /**
     * @param propAutState the property automaton state component of control-state
     * @param pos	The component of control-state specifying next argument position to look up in HORS.
     * @param needToPopOne	Component of control-sate which is true iff have just performed higher-order
     *						push and then need to look up what variable bound to (in particular pos will
     *						be 0 when this is true, and will then be set to position of variable).
     * @return a control state formed of the pair propAutState and pos.
     */
    public ControlState makeControl(PropAutState propAutState, Integer pos, boolean needToPopOne) {
	if(needToPopOne) {
		ControlState c = controlsHorsNeedPopOne.get(propAutState, pos);
		if (c == null) {
			c = new ControlStateHors(propAutState, pos, true);
			controlsHorsNeedPopOne.put(propAutState, pos, c);
		}
		return c;
	}
	else {
		ControlState c = controlsHorsNoNeedPopOne.get(propAutState, pos);
		if (c == null) {
			c = new ControlStateHors(propAutState, pos, false);
			controlsHorsNoNeedPopOne.put(propAutState, pos, c);
		}
		return c;
	}
    }


    /**
     * @param c1 the first part of the equality
     * @param c2 the second part of the equality
     * @return true if c1 is the same as c2, false otherwise
     */
    public final boolean equal(ControlState c1, ControlState c2) {
        return (c1 == c2);
    }

    /**
     * Resets the manager
     */
    public final void reset() {
        controls = new HashMap<String, ControlState>();
    }
}
