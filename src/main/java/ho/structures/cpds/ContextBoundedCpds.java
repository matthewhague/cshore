/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.cpds;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;



/**
 * A CPDS along with a context bound and a set of control states where switches
 * might occur.
 *
 * I've separated them in to switch from states and switch to.  I can't think
 * of any situation where it makes sense to have this split, but i'll do it for
 * generality in case i think of something clever later...
 */
public class ContextBoundedCpds {

    private Cpds cpds;
    private Set<StackHead> switchSrcHeads = null;
    private Set<ControlState> switchDestStates = null;
    private int bound = -1;
    private Set<ThreadInterface> checkInterfaces = null;


    /**
     * Do nothing constructor for the purpose of parsing
     */
    public ContextBoundedCpds() { }

    /**
     * @param cpds the cpds
     * @param switchSrcHeads heads where a context switch may switch from
     * @param switchDestStates control states which a context switch may switch too
     * @param bound the number of contexts allowed (bound - 1 switches)
     *
     */
   public ContextBoundedCpds(Cpds cpds,
                             Collection<StackHead> switchSrcHeads,
                             Collection<ControlState> switchDestStates,
                             int bound,
                             Collection<ThreadInterface> checkInterfaces) {
        this.cpds = cpds;
        this.switchSrcHeads = new HashSet<StackHead>(switchSrcHeads);
        this.switchDestStates = new HashSet<ControlState>(switchDestStates);
        this.bound = bound;
        this.checkInterfaces = new HashSet<ThreadInterface>(checkInterfaces);
    }

    public void setCpds(Cpds cpds) {
        this.cpds = cpds;
    }

    public void setSwitchSrcHeads(Collection<StackHead> switchSrcHeads) {
        this.switchSrcHeads = new HashSet<StackHead>(switchSrcHeads);
    }

    public void setSwitchDestStates(Collection<ControlState> switchDestStates) {
        this.switchDestStates = new HashSet<ControlState>(switchDestStates);
    }

    public void setBound(int bound) {
        this.bound = bound;
    }

    public void setCheckInterfaces(Collection<ThreadInterface> checkInterfaces) {
        this.checkInterfaces = new HashSet<ThreadInterface>(checkInterfaces);
    }

    public void addCheckInterface(ThreadInterface threadInterface) {
        if (checkInterfaces == null) {
            checkInterfaces = new HashSet<ThreadInterface>();
        }
        checkInterfaces.add(threadInterface);
    }

    public Cpds getCpds() {
        return cpds;
    }

    public Collection<StackHead> getSwitchSrcHeads() {
        if (switchSrcHeads == null)
            return null;
        else
            return Collections.unmodifiableSet(switchSrcHeads);
    }

    public Collection<ControlState> getSwitchDestStates() {
        if (switchDestStates == null)
            return null;
        else
            return Collections.unmodifiableSet(switchDestStates);
    }

    public int getBound() {
        return bound;
    }

    public Collection<ThreadInterface> getCheckInterfaces() {
        if (checkInterfaces == null)
            return null;
        else
            return Collections.unmodifiableSet(checkInterfaces);
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        return toString(s).toString();
    }

    public StringBuilder toString(StringBuilder s) {
        s.append("bound ");
        s.append(bound);

        s.append("\nswitchfrom ");
        printSet(switchSrcHeads, s);
        s.append("\nswitchto ");
        printSet(switchDestStates, s);

        for (ThreadInterface ti : checkInterfaces) {
            s.append("\ninterface ");
            ti.toString(s);
        }

        if (cpds == null) {
            s.append("\n<null cpds>");
        } else {
            s.append("\n");
            cpds.toString(s);
        }

        return s;
    }

    private void printSet(final Collection<? extends Object> cs,
                          StringBuilder s) {
        if (cs == null) {
            s.append("<null>");
        } else {
            s.append("{ ");
            for (Object c : cs) {
                s.append(c);
                s.append(" ");
            }
            s.append("}");
        }
    }
}
