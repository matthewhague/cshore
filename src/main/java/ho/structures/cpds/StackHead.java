/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.cpds;

import ho.managers.Managers;

/**
 * Representation of a stack head: control state + top of stack character.
 * Mainly used for minimisation where we specify what stack heads we don't want
 * to minimisation to erase.  E.g. we might want to make sure the initial stack
 * <q0, [..[S]..]> is not elided.
 */
public class StackHead {

    private ControlState control;
    private SCharacter character;
    private int hashCode;

    /**
     * Constructs an empty stack head object.  Control and Character should be
     * set before the constructed object is used.
     */
    public StackHead() {
        this.control = null;
        this.character = null;
        this.hashCode = 0;
    }

    /**
     * @param control the control state of the head
     * @param character the stack character of the head
     */
    public StackHead(ControlState control, SCharacter character) {
        assert control != null && character != null
            : "Stack head must have non-null control and character";
        this.control = control;
        this.character = character;
        this.hashCode = control.hashCode() + character.hashCode();
    }


    public final ControlState getControl() {
        return control;
    }

    public final SCharacter getCharacter() {
        return character;
    }

    public final void setControl(ControlState control) {
        assert control != null;
        this.control = control;
        if (character == null)
            this.hashCode = control.hashCode();
        else
            this.hashCode = control.hashCode() + character.hashCode();
    }

    public final void setCharacter(SCharacter character) {
        assert character != null;
        this.character = character;
        if (control == null)
            this.hashCode = character.hashCode();
        else
            this.hashCode = control.hashCode() + character.hashCode();

    }

    public final String toString() {
        return "(" + control + ", " + character + ")";
    }

    public final int hashCode() {
        return hashCode;
    }

    public final boolean equals(Object o) {
        if (o instanceof StackHead) {
            StackHead sh = (StackHead)o;
            return Managers.csManager.equal(control, sh.getControl()) &&
                   Managers.scManager.equal(character, sh.getCharacter());
        } else
            return false;
    }
}
