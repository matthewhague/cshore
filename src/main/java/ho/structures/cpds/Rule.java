/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.cpds;

import java.util.Collection;
import java.util.ArrayList;

/**
 * Common components of the different kinds of rules
 */
abstract public class Rule implements Cloneable {

    ControlState control1;
    SCharacter character;
    ControlState control2;
    RuleAnnotation annotation;

    /**
     * Constructs a rule of the form (control1, character, ?, control2)
     *
     * @param control1 the source control state
     * @param character the source top character
     * @param control2 the destination control state
     */
    public Rule(ControlState control1,
                SCharacter character,
                ControlState control2) {
        this.control1 = control1;
        this.character = character;
        this.control2 = control2;
        this.annotation = new NoRuleAnnotation();
    }

    public Rule(ControlState control1,
                SCharacter character,
                ControlState control2,
                RuleAnnotation annotation) {
        this.control1 = control1;
        this.character = character;
        this.control2 = control2;
        this.annotation = annotation;
    }

    /**
     * Copies rule
     */
    public Rule(Rule r) {
        this.control1 = r.getControl1();
        this.control2 = r.getControl2();
        this.character = r.getCharacter();
        this.annotation = r.getAnnotation();
    }

    /**
     * @return the source control state of the rule
     */
    public ControlState getControl1() { return control1; }

    /**
     * @return the source character of the rule
     */
    public SCharacter getCharacter() { return character; }

    /**
     * @return the destination control state of the rule
     */
    public ControlState getControl2() { return control2; }

    public RuleAnnotation getAnnotation() {return this.annotation;}

    /**
     * Writes to sh the head of the stack after the rule has been applied
     * (e.g. (q, a, push(2), p) has destination head (p, a))
     *
     * @param sh the stack head object to write the information to
     * @return false if the head cannot be determined
     */
    public abstract boolean getDestinationHead(StackHead sh);

    /**
     * @param control1 set control1
     */
    public void setControl1(ControlState control1) {
        this.control1 = control1;
    }

    public void setAnnotation(RuleAnnotation annotation) {
	this.annotation = annotation;
    }

    /**
     * @param control1 set control2
     */
    public void setControl2(ControlState control2) {
        this.control2 = control2;
    }

    /**
     * @param character set character
     */
    public void setCharacter(SCharacter character) {
        this.character = character;
    }





    /**
     * @return the order of the rule
     */
    public abstract byte getOrder();

    /**
     * This method is inefficient, but we don't expect to use it much.
     *
     * @return the set of control states of the rule
     */
    public Collection<ControlState> getControls() {
        ArrayList<ControlState> controls = new ArrayList<ControlState>(2);
        controls.add(0, getControl1());
        controls.add(1, getControl2());
        return controls;
    }

    /**
     * This method is inefficient, but we don't expect to use it much.
     *
     * @return the set of characters of the rule
     */
    public Collection<SCharacter> getCharacters() {
        ArrayList<SCharacter> characters = new ArrayList<SCharacter>(1);
        characters.add(0, getCharacter());
        return characters;
    }


    public abstract void accept(RuleVisitor v);

    public abstract Object clone() throws CloneNotSupportedException;
}
