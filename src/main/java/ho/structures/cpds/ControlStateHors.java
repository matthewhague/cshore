/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.structures.cpds;

import ho.hors.PropAutState;



/**
 * Representation of control states for CPDS derived from HORS (including property automaton).
 * To be built and manipulated through
 * ControlStateManager only -- to allow unique representation (one object per
 * property automaton state, position pair).
 */
public class ControlStateHors implements ControlState {

    PropAutState propAutState;
    int position;
    boolean needToPopOne; // set to true if just peformed higher-order push before looking up variable.

    /**
     * @param propAutState the property automaton state component of control-state
     * @param pos	The component of control-state specifying next argument position to look up in HORS.
     */
    ControlStateHors(PropAutState propAutState, int position, boolean needToPopOne) {
        this.propAutState = propAutState;
        this.position = position;
        this.needToPopOne = needToPopOne;
    }

   public final PropAutState getPropAutState(){
	   return this.propAutState;
   }
   public final int getPosition() {
	   return this.position;
   }
   public final boolean getNeedToPopOne() {
	   return this.needToPopOne;
   }
    public final int hashCode() {
        return System.identityHashCode(this);
    }

    public String toString() {
        return "[" + propAutState.toString() + ", " + position + ", " + needToPopOne + "]";
    }

    public final boolean equals(Object o) {
        return (this == o);
    }

}
