/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.cpds;

import java.lang.StringBuilder;

import java.util.ListIterator;
import java.util.Stack;

import org.apache.log4j.Logger;


/**
 * Implementation of a CPDS stack
 */
public class CpdsStack {

    static Logger logger = Logger.getLogger(CpdsConfig.class);

    private byte order;
    private Stack<Object> stack;

    /**
     * The type of a position: OPEN or CLOSE.  A stack is of the form (with
     * positions marked by ".")
     *
     *    .[.[.a.b.].[.c.].].
     *
     * A position is OPEN if it occurs before a stack character or a "]".  That
     * is, there is more of the current stack before it.  A position is CLOSE if
     * it occurs before "[".  That is, we move out of the stack into the order
     * above.
     *
     * NOPOSITION is for when a given position does not appear in the stack.
     */
    public enum PositionType {
        OPEN, CLOSE, NOPOSITION
    };

    /**
     * Creates a new stack [...[init]...] of given order
     *
     * @param init the initial stack character
     * @param order the order of the new stack
     */
    public CpdsStack(SCharacter init, byte order) {
        this.order = order;
        this.stack = new Stack<Object>();
        CharLink initl = new CharLink(init, (byte)1, 0);
        this.stack.push((Object)initl);
        for (int k = 2; k <= order; ++k) {
            Stack<Object> s = new Stack<Object>();
            s.push((Object)this.stack);
            this.stack = s;
        }
    }

    /**
     * Creates a new empty stack of given order
     *
     * @param order the order
     */
    public CpdsStack(byte order) {
        this.order = order;
        this.stack = new Stack<Object>();
    }

    /**
     * @param s the stack to create a deep copy of
     */
    public CpdsStack(CpdsStack s) {
        this.order = s.order;
        this.stack = deepCopy(s.stack, s.order);
    }


    /**
     * @param s Stack<Object> of stack
     * @param order the order of s
     * @return a new deep copy of s
     */
    private CpdsStack(Stack<Object> s, byte order) {
        this.order = order;
        this.stack = deepCopy(s, order);
    }

    /**
     * @return the order of the stack
     */
    public byte getOrder() {
        return order;
    }



    /**
     * @return return top(1) of stack or null if there is no top(1)
     */
    public SCharacter getTop1() {
        CharLink cl = getTop1CharLink();
        if (cl == null)
            return null;
        else
            return cl.a;
    }

    /**
     * put a new, empty, order-k stack on top of the current top order-(k+1)
     * stack.
     *
     * @param k the order of the new empty stack
     * @return true if successful, false if there was a problem (e.g. no top
     * order-(k+1) stack)
     */
    public boolean pushEmpty(byte k) {
        if (k >= order || k < 1)
            return false;
        Stack<Object> s = getTop((byte)(k + (byte)2));
        if (s == null) {
            return false;
        }

        s.push(new Stack<Object>());
        return true;
    }

    /**
     * put a new character with link on top of stack
     *
     * @param a the character to push
     * @param k the order of the link
     * @param h the height of the link from the bottom of the top(k+1) stack.
     * @return true if successful, false if there was a problem (e.g. no top
     * order-(k+1) stack)
     */
    public boolean pushCharLink(SCharacter a, byte k, int h) {
        if (k > order || k < 1)
            return false;
        Stack<Object> dests = getTop((byte)(k + (byte)1));
        Stack<Object> s = getTop((byte)2);
        // note dests will not be null if s isn't
        if (s == null || dests.size() < h) {
            return false;
        }
        s.push(new CharLink(a, k, h));
        return true;
    }

    /**
     * put a new character on stack for evaluating a nonterminal
     *
     * @param a the character to push
     * @return true if successful, false if there was a problem
     */
    public boolean applyEvalNonTerm(SCharacter a) {
        pushCharLink(a, (byte)1, 0);
        return true;
    }



    /**
     * @return return top(1) CharLink of stack or null if there is no top(1)
     */
    public CharLink getTop1CharLink() {
        Stack<Object> s;
        s = getTop((byte)2);

        if (s == null || (s.size() == 0))
            return null;
        else
            return (CharLink)s.peek();
    }


    /**
     * @param k the order
     * @return true iff top(k) exists
     */
    public boolean hasTop(byte k) {
        return getTop(k) != null;
    }


    /**
     * @param k the order
     * @return the size of top(k), -1 if no such stack
     */
    public int topSize(byte k) {
        Stack<Object> s = getTop(k);
        return s == null ? -1 : s.size();
    }

    /**
     * @param k the order
     * @return a CpdsStack object that is a copy of the top(k) stack, or null if
     *         no such stack
     */
    public CpdsStack getTopCopy(byte k) {
        Stack<Object> s = getTop(k);
        return s == null ? null : new CpdsStack(s, (byte)(k - (byte)1));
    }


    /**
     * @param pos the position
     * @return the type of the position (see public enum PositionType)
     */
    @SuppressWarnings("unchecked")
    public PositionType getPositionType(CpdsStackPosition pos) {
        assert pos.getOrder() == this.getOrder()
            : "Orders of stack and position must match.";

        Stack<Object> s = stack;

        // there's only 1 order-n stack
        int p = pos.getCoord(order);
        if (p > 1 || p < 0)
            return PositionType.NOPOSITION;

        for (byte i = (byte)(order - (byte)1); i > 0; --i) {
            p = pos.getCoord(i);
            // if it's < 0 then we have a position that points just below the
            // current stack, so OPEN
            if (p < 0)
                return PositionType.OPEN;
            // then we're at the top of the current stack
            else if (s.size() == p) {
                // we're at the top of an order-k stack, check that we don't
                // expect a stack above us
                if (pos.getCoord((byte)(i - (byte)1)) < 0)
                    return PositionType.CLOSE;
                else
                    return PositionType.NOPOSITION;
            }
            // if the current co-order is bigger than the stack size
            // we out of the stack
            else if (s.size() < p)
                return PositionType.NOPOSITION;
            else // look into the next stack to find out
                s = (Stack<Object>)s.get(p);
        }

        // now we're in the top order-1 stack
        p = pos.getCoord((byte)0);
        if (p < s.size())
            return PositionType.OPEN;
        else if (p == s.size())
            return PositionType.CLOSE;
        else
            return PositionType.NOPOSITION;
    }

    /**
     * @param pos the position
     * @return the next position in the stack (if pos is already the bottom of
     * the stack, the same position is returned)
     */
    @SuppressWarnings("unchecked")
    public CpdsStackPosition getNextPosition(CpdsStackPosition pos) {
        assert pos.getOrder() == this.getOrder()
            : "Orders of stack and position must match.";

        // E.g. (-1, 2, 0) goes to (n, 1, 0) where n is the height of the stack
        // at (_, 1, 0).
        //
        // If (-1, -1, -1) then return (-1, -1, -1)
        //
        // If (2, 3, 0) return (1, 3, 0)
        //
        // If (-1, 0, 0) return (-1, -1, 0)

        Stack<Object> s = stack;
        Stack<Object> sprev = null;

        CpdsStackPosition nextPos = new CpdsStackPosition(pos);

        // if already at bottom, just return same pos
        if (pos.getCoord(order) == -1)
            return nextPos;

        // else do some calculation
        byte k = order;
        while (k >= 0 && pos.getCoord(k) >= 0) {
            --k;
            sprev = s;
            if (// more to come
                k > 0 &&
                // the next stack position is inside the next stack
                pos.getCoord((byte)(k - 1)) >= 0) {
                // make sure the next stack exists
                if (pos.getCoord(k) < s.size()) {
                    s = (Stack<Object>)s.get(pos.getCoord(k));
                } else {
                    logger.error("CpdsStack.getNextPosition: invalid position " +
                                 pos +
                                 " passed for stack " + this);
                    System.exit(-1);
                }
            }
        }

        // found -1 or end, move back to co-ord that's just before
        ++k;

        if (k == 0) {
            nextPos.incCoord((byte)0, -1);
        } else {
            nextPos.incCoord(k, -1);
            int kcoord = nextPos.getCoord(k);
            // if we haven't dropped out of the stack, update next coord along
            // to reflect size of stack now pointed to
            if (kcoord >= 0) {
                // we know sprev is not null because pos.getCoord(order) >= 0
                if (sprev.size() > 0) {
                    s = (Stack<Object>)sprev.get(kcoord);
                    nextPos.setCoord((byte)(k - 1), s.size());
                } else {
                    nextPos.setCoord((byte)(k - 1), -1);
                }
            }
        }

        return nextPos;
    }



    /**
     * @param pos the position of the stack to retrieve
     * @return the character in the stack at the given position, or null if the
     * position doesn't exist in the stack
     */
    @SuppressWarnings("unchecked")
    public CharLink getPosition(CpdsStackPosition pos) {
        assert pos.getOrder() == this.getOrder()
            : "Orders of stack and position must match.";

        Stack<Object> s = stack;

        // there's only 1 order-n stack
        int p = pos.getCoord(order);
        if (p < 0 || p > 1)
            return null;

        for (byte i = (byte)(order - (byte)1); i > 0; --i) {
            p = pos.getCoord(i);
            if (p < 0 || s.size() <= p)
                return null;
            else // look into the next stack to find out
                s = (Stack<Object>)s.get(p);
        }


        // now we're in the top order-1 stack
        p = pos.getCoord((byte)0);
        if (p < 0 || p >= s.size()) {
            return null;
        } else {
            return (CharLink)s.get(p);
        }
    }

    /**
     * @return a CpdsStackPosition object pinpointing top(1) (supposes it
     * exists)
     */
    @SuppressWarnings("unchecked")
    public CpdsStackPosition getTopPosition() {
        assert hasTop((byte)1)
            : "Can't get top position of a stack with no top.";
        CpdsStackPosition pos = new CpdsStackPosition(order);
        Stack<Object> s = stack;
        for (byte k = (byte)(order-(byte)1); k >= 0; --k) {
            pos.setCoord(k, s.size());
            s = (Stack<Object>)s.peek();
        }

        return pos;
    }


    /**
     * @return the first position in the stack (i.e. the * position in *[.[.a....)
     *         that is the *very* top
     */
    public CpdsStackPosition getFirstPosition() {
        CpdsStackPosition pos = new CpdsStackPosition(order);
        pos.setCoord(order, 1);
        for (byte k = (byte)(order-(byte)1); k >= 0; --k) {
            pos.setCoord(k, -1);
        }

        return pos;
    }

    /**
     * @return the last position in the stack (i.e. the * position in .[...]*)
     *         that is the *very* bottom
     */
    public CpdsStackPosition getLastPosition() {
        CpdsStackPosition pos = new CpdsStackPosition(order);
        for (byte k = order; k >= 0; --k) {
            pos.setCoord(k, -1);
        }
        return pos;
    }


    /**
     * Applies push(k) to current stack (assumes push is applicable)
     *
     * @param k the order
     */
    @SuppressWarnings("unchecked")
    public void applyPush(byte k) {
        assert k > 1 && k <= order : "Can only push(k) for push(2)...push(n)";
        Stack<Object> topk = getTop((byte)(k + (byte)1));
        Stack<Object> top = (Stack)topk.peek();
        topk.push(deepCopy(top, (byte)(k - (byte)1)));
    }

    /**
     * Applies pop(k) to current stack (assumes pop is applicable)
     *
     * @param k the order
     */
    public void applyPop(byte k) {
        assert k >= 1 && k <= order : "Can only pop(k) for pop(1)...pop(n)";
        Stack<Object> topk = getTop((byte)(k + 1));
        topk.pop();
    }

    /**
     * Applies collapse(k) to current stack (assumes collapse is applicable)
     *
     * @param k the order
     */
    public void applyCollapse(byte k) {
        assert k >= 2 && k <= order
            : "Can only collapse(k) for collapse(1)...collapse(n)";
        CharLink cl = getTop1CharLink();
        Stack<Object> kstack;
        kstack = getTop((byte)(cl.linkOrder + (byte)1));
        while (kstack.size() > cl.linkHeight)
            kstack.pop();
    }

    /**
     * Applies rew(b) to current stack (assumes rew is applicable)
     *
     * @param b the character to rewrite top(1) to
     */
    public void applyRew(SCharacter b) {
        Stack<Object> s = getTop((byte)2);
        CharLink cl = (CharLink)s.pop();
        CharLink clb = new CharLink(b, cl.linkOrder, cl.linkHeight);
        s.push(clb);
    }

    /**
     * Applies get-0-arg(b) to current stack (assumes rew is applicable)
     *
     * @param b the character to rewrite top(1) to
     */
    public void applyGetOrder0Arg(SCharacter b) {
        Stack<Object> s = getTop((byte)2);
        CharLink cl = (CharLink)s.pop();
        CharLink clb = new CharLink(b, cl.linkOrder, cl.linkHeight);
        s.push(clb);
    }



    /**
     * Applies push(b)(k) to current stack (assumes push(b)(k) is applicable)
     *
     * @param b the character to push
     * @param k the order of the link
     */
    public void applyPush(SCharacter b, byte k) {
        assert k >= 1 && k <= order
            : "Can only push(b)(k) for 1 <= k <= n";

        // we're not representing order-1 links, so order-1 links should get
        // height 0
        int linkHeight = 0;
        if (k > 1) {
            Stack<Object> kstack;
            kstack = getTop((byte)(k + (byte)1));
            linkHeight = kstack.size() - 1;
        }

        CharLink clb = new CharLink(b, k, linkHeight);
        Stack<Object> top2 = getTop((byte)2);
        top2.push(clb);
    }

    /**
     * Applies rew(b)(k) to current stack (assumes push(b)(k) is applicable)
     *
     * @param b the character to rewrite to
     * @param k the order of the link
     */
    public void applyRewLink(SCharacter b, byte k) {
        assert k > 1 && k <= order
            : "Can only rew(b)(k) for 1 < k <= n";

        Stack<Object> kstack;
        kstack = getTop((byte)(k + (byte)1));
        int linkHeight = kstack.size() - 1;

        CharLink clb = new CharLink(b, k, linkHeight);
        Stack<Object> top2 = getTop((byte)2);
        // assumes we have a character to rewrite...
        top2.pop();
        top2.push(clb);
    }

    /**
     * Applies getarg(b)(k) to current stack (assumes push(b)(k) is applicable)
     *
     * @param b the character to rewrite to
     * @param k the order of the link
     */
    public void applyGetArg(SCharacter b, byte k) {
        assert k > 1 && k <= order
            : "Can only rew(b)(k) for 1 < k <= n";

        Stack<Object> kstack;
        kstack = getTop((byte)(k + (byte)1));
        int linkHeight = kstack.size() - 1;

        CharLink clb = new CharLink(b, k, linkHeight);
        Stack<Object> top2 = getTop((byte)2);
        // assumes we have a character to rewrite...
        top2.pop();
        top2.push(clb);
    }


    public String toString() {
        return stackToString(stack, order);
    }

    public boolean equals(Object o) {
        if (o instanceof CpdsStack) {
            CpdsStack cs = (CpdsStack)o;
            return (order == cs.order) &&
                   (stack.equals(cs.stack));
        }
        return false;
    }

    public int hashCode() {
        return order + stack.hashCode();
    }


    ///////////////////////////////////////////////////////////
    // Privates

    /**
     * Given an order-(order) stack, return top(k)
     *
     * @param k the order of the top(k) command
     * @param order the order of stack
     * @param stack the stack to operate on
     * @return top(k)(stack) or null if k > order or k < 2 (see getTop1 for k ==
     *         1) or if there is no top(k) (because of emtpy top stack)
     */
    @SuppressWarnings("unchecked")
    private static Stack<Object> getTopRecursive(byte k,
                                                 byte order,
                                                 Stack<Object> stack) {
        if (k > order + 1 || k < (byte)2 || stack == null)
            return null;
        else if (k == order + 1)
            return stack;
        else if (k == order)
            if (stack.size() == 0)
                return null;
            else
                return (Stack)stack.peek();
        else {

            if (stack.size() == 0)
                return null;
            else {
                byte o = (byte)(order - (byte)1);
                return getTopRecursive(k, o, (Stack)stack.peek());
            }
        }
    }

    /**
     * @param s the stack to convert
     * @param k the order of the stack to convert
     */
    @SuppressWarnings("unchecked")
    private String stackToString(Stack<Object> s, byte k) {
        StringBuilder sb = new StringBuilder();

        sb.append("[");

        ListIterator<Object> i = s.listIterator(s.size());
        if (k == 1) {
            while (i.hasPrevious()) {
                CharLink cl = (CharLink)i.previous();
                sb.append(cl);
                if (i.hasPrevious())
                    sb.append(" ");
            }
        } else {
            byte subk = (byte)(k - (byte)1);
            while (i.hasPrevious()) {
                Stack<Object> subs = (Stack<Object>)i.previous();
                sb.append(stackToString(subs, subk));
                if (i.hasPrevious())
                    sb.append(" ");
            }
        }

        sb.append("](");
        sb.append(k);
        sb.append(")");

        return sb.toString();
    }


    /**
     * @param stack stack to create a completely independent copy of
     * @param k the order of the stack
     */
    @SuppressWarnings("unchecked")
    private Stack<Object> deepCopy(Stack<Object> stack, byte k) {
        Stack<Object> copy = new Stack<Object>();
        if (k == 1) {
            ListIterator<Object> i = stack.listIterator();
            while (i.hasNext())
                copy.push(new CharLink((CharLink)i.next()));
        } else {
            byte knext = (byte)(k - (byte)1);
            ListIterator<Object> i = stack.listIterator();
            while (i.hasNext())
                copy.push(deepCopy((Stack<Object>)i.next(), knext));
        }

        return copy;
    }



    /**
     * For orders > 1, return top(k) of stack, for k == 1, see getTop1()
     *
     * @param k return top(k)
     * @return return top(k) or null if k > order or k < 2
     */
    private Stack<Object> getTop(byte k) {
        return getTopRecursive(k, order, stack);
    }


}
