/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.cpds;

import java.lang.StringBuilder;

import org.apache.log4j.Logger;


import ho.managers.Managers;

/**
 * Characters on the stack need links, links are given by the order
 * linkOrder and the height linkHeight above the bottom of their
 * order-linkOrder stack they're in.
 *
 * i.e. [ [[a][b]] [[c]] [[d]] ]
 *
 * where a = (a, 3, 1) is a link to [[[d]]]
 */
public class CharLink {

    static Logger logger = Logger.getLogger(CpdsConfig.class);

    SCharacter a;
    byte linkOrder;
    int linkHeight;

    CharLink(SCharacter a, byte linkOrder, int linkHeight) {
        this.a = a;
        this.linkOrder = linkOrder;
        this.linkHeight = linkHeight;
    }

    CharLink(CharLink cl) {
        this.a = cl.a;
        this.linkOrder = cl.linkOrder;
        this.linkHeight = cl.linkHeight;
    }

    /**
     * @return the character
     */
    public SCharacter getCharacter() {
        return a;
    }


    /**
     * @return the order of the link on the character
     */
    public byte getLinkOrder() {
        return linkOrder;
    }


    /**
     * @return the height from the bottom of the stack the link points to
     */
    public int getLinkHeight() {
        return linkHeight;
    }

    public String toString() {
        if (linkOrder > 1) {
            StringBuilder s = new StringBuilder();
            s.append("(");
            s.append(a);
            s.append(" ");
            s.append(linkOrder);
            s.append(" ");
            s.append(linkHeight);
            s.append(")");
            return s.toString();
        } else {
            return a.toString();
        }
    }

    public boolean equals(Object o) {
        if (o instanceof CharLink) {
            CharLink cl = (CharLink)o;
            return Managers.scManager.equal(a, cl.a) &&
                   linkOrder == cl.linkOrder &&
                   linkHeight == cl.linkHeight;
        }
        return false;
    }
}

