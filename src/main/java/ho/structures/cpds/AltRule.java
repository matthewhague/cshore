/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.cpds;

/**
 * Class for an alternating CPDS rule
 *
 *      (p, a, {(rew(a1), p1), ..., (rew(am), pm)})
 */

import java.util.Set;
import java.util.HashSet;
import java.util.Collection;

import ho.managers.Managers;

public class AltRule extends Rule {

    Set<StackHead> destinations;
    int hashCode;

    public AltRule(ControlState control1,
                   SCharacter character,
                   Set<StackHead> destinations) {
        // control2 is null because we have several in destinations
        super(control1, character, null);
        this.destinations = new HashSet<StackHead>(destinations);
        this.hashCode = control1.hashCode() +
                        character.hashCode() +
                        destinations.hashCode();
    }

    public AltRule(ControlState control1,
                   SCharacter character,
                   Set<StackHead> destinations,
                   RuleAnnotation annotation) {
        // control2 is null because we have several in destinations
        super(control1, character, null, annotation);
        this.destinations = new HashSet<StackHead>(destinations);
        this.hashCode = control1.hashCode() +
                        character.hashCode() +
                        destinations.hashCode();
    }

    public AltRule(ControlState control1,
                   SCharacter character,
                   StackHead... destinations) {
        super(control1, character, null);
        this.destinations = new HashSet<StackHead>(destinations.length);
        for (StackHead h : destinations)
            this.destinations.add(h);
        this.hashCode = control1.hashCode() +
                        character.hashCode() +
                        destinations.hashCode();
    }

    public AltRule(AltRule r) {
        super(r);
        this.destinations = new HashSet<StackHead>(r.destinations);
        this.hashCode = r.hashCode();
    }

    /**
     * @return the destination set {(p1, a1), ..., (pm, am)}
     */
    public Iterable<StackHead> getDestinations() {
        return destinations;
    }


    public byte getOrder() {
        return (byte)1;
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("(");
        s.append(getControl1());
        s.append(" ");
        s.append(getCharacter());
        s.append(" ");
        s.append("{");
        for (StackHead h : destinations) {
            s.append(" (");
            s.append(h.getControl());
            s.append(" ");
            s.append(h.getCharacter());
            s.append(")");
        }
        s.append(" })");
        return s.toString();
    }

    /**
     * This method is inefficient, but we don't expect to use it much.
     *
     * @return the set of characters of the rule
     */
    public Collection<SCharacter> getCharacters() {
        Set<SCharacter> characters = new HashSet<SCharacter>(2);
        characters.add(getCharacter());
        for (StackHead h : destinations) {
            characters.add(h.getCharacter());
        }
        return characters;
    }

    public final boolean getDestinationHead(StackHead sh) {
        return false;
    }

    public boolean equals(Object o) {
        if (o instanceof AltRule) {
            AltRule r = (AltRule)o;
            return hashCode == o.hashCode() &&
                   Managers.csManager.equal(getControl1(), r.getControl1()) &&
                   Managers.scManager.equal(getCharacter(), r.getCharacter()) &&
                   destinations.equals(r.destinations);
        } else
            return false;
    }

    public final int hashCode() {
        return hashCode;
    }

    public void accept(RuleVisitor v) { v.visit(this); }

    public Object clone() throws CloneNotSupportedException {
        return new AltRule(this);
    }

}

