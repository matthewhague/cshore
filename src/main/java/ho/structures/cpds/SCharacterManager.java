/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.cpds;

import ho.hors.Term;

import java.util.Map;
import java.util.HashMap;

/**
 * Manager class for stack characters -- Character objects are only to be
 * created and manipulated with this class (maintains uniqueness, so we don't
 * have to objects corresponding to the same character name).
 */
public class SCharacterManager {

    // doesn't matter that looking up a string will be slow -- we'll only do it
    // when parsing the cpds.
    Map<String, SCharacter> chars = new HashMap<String, SCharacter>();

    Map<Term, SCharacter> charsTerm = new HashMap<Term, SCharacter>();

    /**
     * @param name the name of the character to make
     * @return a character object with the given name
     */
    public SCharacter makeCharacter(String name) {
        SCharacter a = chars.get(name);
        if (a == null) {
            a = new SCharacterString(name);
            chars.put(name, a);
        }
        return a;
    }

    public SCharacter makeCharacter(Term term) {
        SCharacter a = charsTerm.get(term);
        if (a == null) {
            a = new SCharacterTerm(term);
            charsTerm.put(term, a);
        }
        return a;
    }

    /**
     * @param c1 the first part of the equality
     * @param c2 the second part of the equality
     * @return true if c1 is the same as c2, false otherwise
     */
    public final boolean equal(SCharacter c1, SCharacter c2) {
        return (c1 == c2);
    }

    /**
     * Resets the manager
     */
    public final void reset() {
        chars = new HashMap<String, SCharacter>();
        charsTerm = new HashMap<Term, SCharacter>();
    }
}
