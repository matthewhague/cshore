/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */




package ho.structures.sa;

import ho.structures.cpds.ControlState;
import ho.util.NaiveUnsafeMultiMap;
import ho.util.UnsafeMultiMap;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

import org.apache.log4j.Logger;


/**
 * A simple container for stack automata -- for now is simply an inefficient set
 * of transitions of ordersK and 1 for the purposes of testing / comparing
 * automata.
 */
public class SA {

    static Logger logger = Logger.getLogger(SA.class);

    private Set<SATransition> orderKContents = new HashSet<SATransition>();
    private Set<SATransitionO1> order1Contents = new HashSet<SATransitionO1>();
    private Map<ControlState, SAOuterState> outerStates = null;

    private UnsafeMultiMap<Integer,SAState> orderKStates = null;

    /**
     * @param orderKContents the initial set of order-K transitions
     * @param order1Contents the initial set of order-1 transitions
     * @param outerStates map from controls to corresponding states of SA
     */
    public SA(Collection<SATransition> orderKContents,
              Collection<SATransitionO1> order1Contents,
              Map<ControlState, SAOuterState> outerStates) {
        this.orderKContents.clear();
        this.order1Contents.clear();
        this.orderKContents.addAll(orderKContents);
        this.order1Contents.addAll(order1Contents);
        this.outerStates
            = new HashMap<ControlState, SAOuterState>(outerStates);
    }

    public SA() { }

    /**
     * @param t an order-K transition to add
     */
    public void addTransition(SATransition t) {
        orderKContents.add(t);
    }

    /**
     * @param t an order-1 transition to add
     */
    public void addTransitionO1(SATransitionO1 t) {
        order1Contents.add(t);
    }

    /**
     * @param outerStates a map from control states to the corresponding
     * automaton states
     */
    public void setOuterStates(Map<ControlState, SAOuterState> outerStates) {
        this.outerStates = new HashMap<ControlState, SAOuterState>(outerStates);
    }

    /**
     * @param q a control state
     * @return the sastate that should begin an accepting run, or null if no
     * info available
     */
    public SAOuterState getOuterState(ControlState q) {
        if (outerStates == null)
            return null;
        else
            return outerStates.get(q);
    }

    /**
     * @return the order-k transitions (with k > 1)
     */
    public Collection<SATransition> getTransitions() {
        return orderKContents;
    }

    /**
     * @return the order-1 transitions
     */
    public Collection<SATransitionO1> getTransitionsO1() {
        return order1Contents;
    }

    public boolean equals(Object o) {
        if (o instanceof SA) {
            SA sa = (SA)o;
            return orderKContents.equals(sa.orderKContents) &&
                   order1Contents.equals(sa.order1Contents);
        } else {
            return false;
        }
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("Order-K:\n");
        for (SATransition t : orderKContents) {
            s.append("  ");
            s.append(t.toStringTransition());
            s.append("\n");
        }
        s.append("Order-1:\n");
        for (SATransitionO1 t : order1Contents) {
            s.append("  ");
            s.append(t);
            s.append("\n");
        }
        return s.toString();
    }


    /**
     * @param k an order
     * @return the order-k states of the sa
     */
    public Collection<SAState> getStates(byte k) {
        if (orderKStates == null) {
            buildOrderKStatesMap();
        }
        return orderKStates.get((int)k);
    }


    /**
     * Run through all transitions and fill orderKStates.get(k) with a set of
     * order-k states
     */
    private void buildOrderKStatesMap() {
        orderKStates = new NaiveUnsafeMultiMap<Integer, SAState>();

        for (SATransitionO1 t : order1Contents) {
            addStateToOrderKStatesMap(t.getSource());
            for (SAState q : t.getDest())
                addStateToOrderKStatesMap(q);
            for (SAState q : t.getCollapseDest())
                addStateToOrderKStatesMap(q);
        }

        for (SATransition t : orderKContents) {
            addStateToOrderKStatesMap(t.getSource());
            for (SAState q : t.getDest())
                addStateToOrderKStatesMap(q);
        }
    }

    /**
     * Adds q to the orderKStatesMap
     *
     * @param q a state
     */
    private void addStateToOrderKStatesMap(SAState q) {
        orderKStates.put((int)(q.getStateOrder()), q);
    }
}

