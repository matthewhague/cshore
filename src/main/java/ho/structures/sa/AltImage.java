
package ho.structures.sa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import ho.managers.Managers;
import ho.util.ManagedSet;
import ho.util.SetManager;

/**
 * an image (Qcol, Q1, ..., Qn)
 */
public class AltImage {

    static SetManager<SAState> sManager = Managers.sManager;

    ArrayList<Set<SAState>> dests;
    Set<SATransitionO1> justif;
    byte order;

    /**
     * Empty image
     *
     * @param order the order of the cpds
     */
    public AltImage(byte order) {
        this.order = order;
        Collection<Set<SAState>> nes
            = Collections.nCopies(order + 1, new HashSet<SAState>());
        dests = new ArrayList<Set<SAState>>(nes);
        justif = new HashSet<SATransitionO1>();
    }

    /**
     * @param t the initial transition to take the image from
     * @param order the order of the cpds
     */
    public AltImage(SATransitionO1 t, byte order) {
        this.order = order;
        dests = new ArrayList<Set<SAState>>(order + 1);
        dests.add(0, new HashSet<SAState>(t.getCollapseDest().getSet()));
        dests.add(1, new HashSet<SAState>(t.getDest().getSet()));
        SATransition q = t.getSource().getTransition();
        for (int k = 2; k < order; ++k) {
            dests.add(k, new HashSet<SAState>(q.getDest().getSet()));
            q = q.getSource().getTransition();
        }
        if (order > 1)
            dests.add(order, new HashSet<SAState>(q.getDest().getSet()));

        justif = new HashSet<SATransitionO1>();
        justif.add(t);
    }

    /**
     * @param i image set to copy
     */
    public AltImage(AltImage i) {
        this.dests = new ArrayList<Set<SAState>>(i.dests.size());
        for (int k = 0; k < i.dests.size(); ++k) {
            dests.set(k, new HashSet<SAState>(i.dests.get(k)));
        }
        justif = new HashSet<SATransitionO1>(i.justif);
    }

    /**
     * @param i image set to copy
     * @param t transition to union to it
     */
    public AltImage(AltImage i, SATransitionO1 t) {
        this.dests = new ArrayList<Set<SAState>>(i.dests.size());
        for (int k = 0; k < i.dests.size(); ++k) {
            dests.add(k, new HashSet<SAState>(i.dests.get(k)));
        }
        justif = new HashSet<SATransitionO1>(i.justif);

        addTransition(t);
    }


    /**
     * Union the image of a new transition with the image
     *
     * @param t the new transition
     */
    public void addTransition(SATransitionO1 t) {
        dests.get(0).addAll(t.getCollapseDest().getSet());
        dests.get(1).addAll(t.getDest().getSet());
        SATransition q = t.getSource().getTransition();
        for (byte k = 2; k < order; ++k) {
            dests.get(k).addAll(q.getDest().getSet());
            q = q.getSource().getTransition();
        }
        if (order > 1)
            dests.get(order).addAll(q.getDest().getSet());

        justif.add(t);
    }

    /**
     * @param k the order of the destination to get
     * @return the order k destination
     */
    public Set<SAState> getDest(byte k) {
        return dests.get(k);
    }

    /**
     * @return the collapse destination
     */
    public Set<SAState> getCollapseDest() {
        return dests.get(0);
    }


    /**
     * @return true if this <= i
     */
    public boolean isSubImage(AltImage i) {
        boolean sub = i.getCollapseDest().containsAll(getCollapseDest());
        for (byte k = 1; k <= order && sub; ++k) {
            sub = (i.getDest(k).containsAll(getDest(k)));
        }
        return sub;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("( ");
        for (Set<SAState> s : dests) {
            sb.append(s);
            sb.append(" ");
        }
        sb.append(")");
        return sb.toString();
    }

}


