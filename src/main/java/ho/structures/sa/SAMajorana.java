/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.sa;

import java.lang.IllegalArgumentException;

import org.apache.log4j.Logger;

import ho.util.ManagedSet;

import ho.lazystates.EagerLazyState;
import ho.managers.Managers;

import ho.structures.cpds.ControlState;

/**
 * States and transitions are essentially the same object, since each
 * order-(k-1) state is associated uniquely to an order-k transition.  Hence, we
 * have a single representation SAMajorana.
 *
 * Only to be created by SAManager.
 */
public class SAMajorana extends EagerLazyState implements SATransition,
                                   SAState {

    static Logger logger = Logger.getLogger(SAMajorana.class);

    SAState source;
    ManagedSet<SAState> dest;
    // the order of the state the majorana represents
    // (transition is order+1)
    byte order;
    int hashCode;

    // because i figure chaining calls all the way back to order-n would be slow
    ControlState control;

    /**
     * Construct a state q' / transition source -- q' --> dest
     *
     * Note, all states (source and those in dest) should have the same order)
     *
     * @param source the source of the transition
     * @param dest the state set that is the destination of the transition
     */
    SAMajorana(SAState source,
               ManagedSet<SAState> dest) {
        assert matchedOrders(source, dest);
        this.source = source;
        this.dest = dest;
        this.order = (byte)(source.getStateOrder() - 1);
        this.hashCode = source.hashCode() + dest.hashCode();
        this.control = source.getControlState();

        assert this.order >= 0;
    }

    public final SAState getSource() {
        return source;
    }

    public final ManagedSet<SAState> getDest() {
        return dest;
    }

    public SAState getLabellingState() {
        return (SAState)this;
    }

    public SATransition getTransition() {
        return (SATransition)this;
    }


    public final byte getStateOrder() {
        return order;
    }

    public final byte getTransitionOrder() {
        return (byte)(order + 1);
    }

    public final int hashCode() {
        return hashCode;
    }


    public final boolean equals(Object o) {
        if (o instanceof SAMajorana) {
            SAMajorana om = (SAMajorana)o;
            return (Managers.saManager.equal(source, om.getSource()) &&
                    Managers.sManager.equal(dest, om.getDest()));
        } else {
            return false;
        }
    }

    public String toString() {
        return "(" + source + ", " + dest + ")";
    }

    public ControlState getControlState() {
        return control;
    }

    public String toStringTransition() {
        return source.toString() + " ------> " + dest.toString();
    }

    public String toStringState() {
        return "(" + source.toString() + ", " + dest.toString() + ")";
    }


    public ManagedSet<SAState> getQk(byte k) {
        byte tOrd = getTransitionOrder();
        if (k < tOrd)
            throw new IllegalArgumentException("Order " + order + " transition does not have a Q" + k);
        else if (k == tOrd)
            return dest;
        else
            return source.getTransition().getQk(k);
    }

    public SAState getSourceK(byte k) {
        byte tOrd = getTransitionOrder();
        if (k < tOrd)
            throw new IllegalArgumentException("getSourceK(" + k + ") on " + this);
        else if (k ==tOrd)
            return source;
        else
            return source.getTransition().getSourceK(k);
    }

    /**
     * Checks a new transition for consistent orders (i.e. all in dest have same
     * order as source
     *
     * @param source the source of the new candidate transition
     * @param dest the destination set of the new candidate transition
     */
    private boolean matchedOrders(SAState source, ManagedSet<SAState> dest) {
        byte k = source.getStateOrder();
        boolean matched = true;
        for (SAState q : dest) {
            matched &= (q.getStateOrder() == k);
            assert (q.getStateOrder() == k)
                : "When creating a new transition, destination orders must match source." +
                   source + ", " + q + ", " + k;
        }
        return matched;
    }

}
