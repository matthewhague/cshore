/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.sa;

import java.lang.StringBuilder;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;

import ho.util.ManagedSet;
import ho.util.SetManager;

import ho.structures.cpds.CharLink;
import ho.structures.cpds.CpdsConfig;
import ho.structures.cpds.CpdsStack;
import ho.structures.cpds.ControlState;
import ho.structures.cpds.SCharacter;
import ho.structures.cpds.CpdsStackPosition;

import ho.managers.Managers;

/**
 * A class for building runs of an SA over a stack -- actual run representation
 * is SALabelledRun
 */
public class SARun {

    static Logger logger = Logger.getLogger(SARun.class);

    static SetManager<SAState> sManager = ho.managers.Managers.sManager;

    CpdsStack stack;
    Map<SCharacter, Set<SATransitionO1>> trans;
    SALabelledRun run = new SALabelledRun();
    ManagedSet<SAState> initialLabel;

    static Set<SATransitionO1> emptyTrans = new HashSet<SATransitionO1>();


    /**
     * @param order1contents the order-1 transitions of the automaton, with the
     * assumption that if a state (q, Q) exists, then the transition q ------> Q
     * exists in the automaton
     */
    public SARun(Collection<SATransitionO1> order1contents) {
        this.stack = null;

        buildTransMap(order1contents);
    }

    /**
     * builds the object's map from characters to transitions
     *
     * @param order1contents the order-1 transitions
     */
    private void buildTransMap(Collection<SATransitionO1> order1contents) {
        trans = new HashMap<SCharacter, Set<SATransitionO1>>();
        for (SATransitionO1 t : order1contents) {
            SCharacter a = t.getLabellingCharacter();
            Set<SATransitionO1> ts = trans.get(a);
            if (ts == null) {
                ts = new HashSet<SATransitionO1>();
                trans.put(a, ts);
            }
            ts.add(t);
        }
    }


    /**
     * Builds the run for the stack in conf, and returns true if the run is
     * accepting for conf
     *
     * @param conf a config
     * @return true if there is an accepting run for conf
     */
    public boolean buildRun(CpdsConfig conf) {
        buildRun(conf.getStack());
        SAOuterState qp = Managers.saManager.getOuterState(conf.getControlState());
        return initialLabel.contains(qp);
    }


    /**
     * Builds the run (fills in the map from stack positions to statesets) for
     * the stack of the given configuration
     *
     * @param stack a stack to build the run for
     * @return the set of states labelling the beginning of the stack
     */
    public ManagedSet<SAState> buildRun(CpdsStack stack) {
        // check if already built first
        if (this.stack != null && this.stack.equals(stack))
            return initialLabel;

        run.resetRun();
        this.stack = stack;
        CpdsStackPosition pos = new CpdsStackPosition(stack.getOrder());
        CpdsStackPosition colPos = new CpdsStackPosition(stack.getOrder());
        // run order is order-1 since outer states read order-(n-1) stacks
        byte runOrder = (byte)(stack.getOrder() - (byte)1);
        byte k = runOrder;

        ManagedSet<SAState> e = sManager.makeEmptySet();

        CpdsStack.PositionType posType = stack.getPositionType(pos);
        boolean done = false;

        while (!done) {
            switch (posType) {
                case OPEN:
                    if (k == 0 && pos.getCoord((byte)0) >= 0) {
                        CharLink cl = stack.getPosition(pos);
                        ManagedSet<SAState> destLabel = run.getLabel(pos);
                        ManagedSet<SAState> colLabel = e;
                        if (cl.getLinkOrder() > 1) {
                            setCollapsePosition(pos,
                                                cl.getLinkOrder(),
                                                cl.getLinkHeight(),
                                                colPos);
                            colLabel = run.getLabel(colPos);
                        }

                        // move to pos above which exists since we're at an open
                        // pos
                        pos.incCoord(k);

                        ManagedSet<SAState> label = e;
                        for (SATransitionO1 t : getTrans(cl.getCharacter())) {
                            if (sManager.subset(t.getDest(), destLabel) &&
                                sManager.subset(t.getCollapseDest(), colLabel)) {
                                label = sManager.add(label, t.getSource());
                            }
                        }
                        run.labelPosition(pos, label);
                    } else if (k == 0) { // && pos.getCoord(0) < 0) {
                        pos.setCoord(k, 0);
                        run.labelPosition(pos, e);
                    } else {
                        // move into next stack and label position with "e"
                        // (the only set of accepting states)
                        // if coming from below, need to increment
                        // if coming from above, we've already incremented
                        if (pos.getCoord(k) < 0) {
                            pos.setCoord(k, (byte)0);
                            run.labelPosition(pos, e);
                        }
                        k--;
                    }
                    break;


                case CLOSE:
                    if (k < runOrder) {
                        // label of current pos has (q, Q)s
                        // move out into stack above
                        // (q, Q) tells us the transitions that could have
                        // accepted the stack we just left
                        // put all the q into the label of new position
                        ManagedSet<SAState> oldLabel = run.getLabel(pos);
                        pos.setCoord(k, -1);
                        ManagedSet<SAState> dest = run.getLabel(pos);

                        k++;
                        pos.incCoord(k);

                        ManagedSet<SAState> newLabel = e;
                        for (SAState q : oldLabel) {
                            SATransition t = q.getTransition();
                            if (sManager.subset(t.getDest(), dest))
                                newLabel = sManager.add(newLabel, t.getSource());
                        }
                        run.labelPosition(pos, newLabel);
                    } else {
                        // we've reached the top of the order-n stack, we're
                        // done
                        initialLabel = run.getLabel(pos);
                        done = true;
                    }

                    break;

                case NOPOSITION:
                    logger.error("SARun.buildRun() fell out of stack:" +
                                 stack);
                    System.exit(-1);
                    break;
            }

            posType = stack.getPositionType(pos);
        }


        return initialLabel;
    }

    public String toString() {
        return run.toString();
    }

    ///////////////////////////////////////////////////////////////////
    // Privates


    /**
     * @param pos a stack position
     * @param linkOrder the order of the link
     * @param linkHeight the height of the stack pointed to
     * @param colPos the object to write the position to after the collapse of
     * order linkOrder has been applied to pos
     */
    private void setCollapsePosition(CpdsStackPosition pos,
                                     byte linkOrder,
                                     int linkHeight,
                                     CpdsStackPosition colPos) {
        assert pos.getOrder() == colPos.getOrder();
        for (byte k = pos.getOrder(); k >= linkOrder; --k) {
            colPos.setCoord(k, pos.getCoord(k));
        }
        // and order-3 link points to the top of an order-2 stack
        colPos.setCoord((byte)(linkOrder - (byte)1), linkHeight);
        for (byte k = (byte)(linkOrder - (byte)2); k >= 0; --k) {
            colPos.setCoord(k, -1);
        }
    }




    /**
     * @param a the character
     * @return the transitions labelled by a
     */
    private Set<SATransitionO1> getTrans(SCharacter a) {
        Set<SATransitionO1> ts = trans.get(a);
        if (ts == null)
            return emptyTrans;
        else
            return ts;
    }




}
