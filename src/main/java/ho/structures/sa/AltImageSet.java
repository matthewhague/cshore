
package ho.structures.sa;

import java.util.Iterator;
import java.util.LinkedList;

public class AltImageSet implements Iterable<AltImage> {

    LinkedList<AltImage> images = new LinkedList<AltImage>();
    byte order;

    public AltImageSet(byte order) {
        this.order = order;
    }

    public void add(AltImage i) {
        boolean add = true;
        boolean definiteAdd = false;

        Iterator<AltImage> iter = images.iterator();
        while (iter.hasNext()) {
            AltImage ifoe = iter.next();

            if (!definiteAdd &&
                ifoe.isSubImage(i)) {
                add = false;
                break;
            } else if (i.isSubImage(ifoe)) {
                definiteAdd = true;
                iter.remove();
            }
        }

        if (add)
            images.add(i);
    }

    public Iterator<AltImage> iterator() {
        return images.iterator();
    }

    public int size() {
        return images.size();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{ ");
        for (AltImage i : this) {
            sb.append(i);
            sb.append(" ");
        }
        sb.append("}");
        return sb.toString();
    }
}


