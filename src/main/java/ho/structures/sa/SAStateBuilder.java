/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.structures.sa;

import ho.structures.SATransitionAdder;

/**
 * A StateBuilder suspends the creation of an actual stack-automaton state for
 * use in cases where we want to describe a state without the overhead of
 * creating all of the associated transitions, and moreover avoids adding
 * any transitions to TNew until it is determined that they actually `lead'
 * to an order-1 transition.
 *
 */
public interface SAStateBuilder {

	/**
	 *
	 * NOTE: This method may be more appropriate as being static.
	 *
	 * Sets the callback to be used for adding transitions when
	 * the state is `completed' to be an order-1 state.
	 * @param transAdder  The callback
	 */
	public void setCallback ( SATransitionAdder transAdder );



}
