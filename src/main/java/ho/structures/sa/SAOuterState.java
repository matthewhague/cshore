/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.sa;

import ho.util.ManagedSet;

import ho.structures.cpds.ControlState;

import ho.lazystates.EagerLazyState;
import ho.managers.Managers;

/**
 * A representation of stack automaton states that occur at order-n (i.e. just
 * represent a control state of the CPDS, and do not label a transition).
 *
 * Only to be created by an SAManager.
 */
public class SAOuterState extends EagerLazyState implements SAState {

    ControlState controlState;
    byte order;

    /**
     * @param controlState the state that this outer transition represents
     * @param order the order of the CPDS the control state is from
     */
    SAOuterState(ControlState controlState, byte order) {
        this.controlState = controlState;
        this.order = order;
    }

    public final byte getStateOrder() {
        return order;
    }

    /**
     * @return the control state contained
     */
    public final ControlState getControlState() {
        return controlState;
    }

    public final int hashCode() {
        return controlState.hashCode();
    }

    public final boolean equals(Object o) {
        if (o instanceof SAOuterState) {
            SAOuterState os = (SAOuterState)o;
            return Managers.csManager.equal(controlState,
                                            os.getControlState());
        } else {
            return false;
        }
    }

    public String toString() {
        return controlState.toString();
    }

    public SATransition getTransition() {
        return null;
    }

    public String toStringState() {
        return controlState.toString();
    }

}
