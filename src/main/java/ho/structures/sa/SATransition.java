/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.sa;

import ho.util.ManagedSet;

/**
 * Representation of an order->1 transition
 */
public interface SATransition {

    // what do we need to do with a transition?

    /**
     * @return the source state of the transtion
     */
    public SAState getSource();

    /**
     * @return the destination state set of the transition
     */
    public ManagedSet<SAState> getDest();

    /**
     * @return the state labelling the transition
     */
    public SAState getLabellingState();

    /**
     * @return the order of the transition
     */
    public byte getTransitionOrder();

    /**
     * @return representation of transition as string
     */
    public String toStringTransition();


    /**
     * @param k the order
     * @return recalling a transition is essentially p ---> Qk', ..., Qn,
     *         returns Qk
     */
    public ManagedSet<SAState> getQk(byte k);

    /**
     * @param k the order
     * @return recalling a transition is essentially p ---> Qk', ..., Qn,
     *         returns (p, Qk+1, ... Qn) state (i.e. order-k source)
     */
    public SAState getSourceK(byte k);

    public boolean equals(Object o);

    public int hashCode();
}
