/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.sa;

import ho.util.ManagedSet;

import ho.managers.Managers;

import ho.structures.cpds.SCharacter;

import ho.witness.PrincipalJustification;

/**
 * Representation of an order-1 transition -- class makes the assumption that
 * two transitions are only equal if they are the same object.  If you build
 * only with SAManager, this will be ensured.
 */
public class SATransitionO1 {

    SAState source;
    SCharacter label;
    ManagedSet<SAState> collapseDest;
    ManagedSet<SAState> dest;
    int hashCode;
    PrincipalJustification principalJustification;
    ManagedSet<SATransitionO1> unwoundJustification;

    /**
     * Constructs a new order-1 transition source -- label, collapseDest --> dest
     *
     * @param source the source state of the transition
     * @param label the character labelling the transition
     * @param collapseDest the collapse target of the transition
     * @param dest the order-1 destination set of the transition
     */
    SATransitionO1(SAState source,
                   SCharacter label,
                   ManagedSet<SAState> collapseDest,
                   ManagedSet<SAState> dest) {
        assert source.getStateOrder() == 1 &&
               assertOrder1(dest) &&
               assertOrders(collapseDest);

        this.source = source;
        this.label = label;
        this.dest = dest;
        this.collapseDest = collapseDest;
        // a-la sets
        this.hashCode = source.hashCode() +
                        label.hashCode() +
                        collapseDest.hashCode() +
                        dest.hashCode();
        this.principalJustification = null;
        this.unwoundJustification = null;
    }

    /**
     * Constructs a new order-1 transition source -- label, collapseDest --> dest
     *
     * @param source the source state of the transition
     * @param label the character labelling the transition
     * @param collapseDest the collapse target of the transition
     * @param dest the order-1 destination set of the transition
     * @param principalJustification simple justification of transition for
     * witness generation
     */
    SATransitionO1(SAState source,
                   SCharacter label,
                   ManagedSet<SAState> collapseDest,
                   ManagedSet<SAState> dest,
                   PrincipalJustification principalJustification) {
        assert source.getStateOrder() == 1 &&
               assertOrder1(dest) &&
               assertOrders(collapseDest);

        this.source = source;
        this.label = label;
        this.dest = dest;
        this.collapseDest = collapseDest;
        // a-la sets
        this.hashCode = source.hashCode() +
                        label.hashCode() +
                        collapseDest.hashCode() +
                        dest.hashCode();
        this.principalJustification = principalJustification;
        this.unwoundJustification = null;
    }

    /**
     * Constructs a new order-1 transition source -- label, collapseDest --> dest
     *
     * @param source the source state of the transition
     * @param label the character labelling the transition
     * @param collapseDest the collapse target of the transition
     * @param dest the order-1 destination set of the transition
     * @param principalJustification simple justification of transition for
     * witness generation
     * @param unwoundJustification unwound justification of transition for
     * witness generation
     */
    SATransitionO1(SAState source,
                   SCharacter label,
                   ManagedSet<SAState> collapseDest,
                   ManagedSet<SAState> dest,
                   PrincipalJustification principalJustification,
                   ManagedSet<SATransitionO1> unwoundJustification) {
        assert source.getStateOrder() == 1 &&
               assertOrder1(dest) &&
               assertOrders(collapseDest);

        this.source = source;
        this.label = label;
        this.dest = dest;
        this.collapseDest = collapseDest;
        // a-la sets
        this.hashCode = source.hashCode() +
                        label.hashCode() +
                        collapseDest.hashCode() +
                        dest.hashCode();
        this.principalJustification = principalJustification;
        this.unwoundJustification = unwoundJustification;
    }


    /**
     * @return the source state of the transition
     */
    public SAState getSource() {
        return source;
    }

    /**
     * @return the destination state set of the transition
     */
    public ManagedSet<SAState> getDest() {
        return dest;
    }

    /**
     * @param k the order
     * @return recalling a transition is essentially p ---> Q1, ..., Qn,
     *         returns Qk
     */
    public ManagedSet<SAState> getQk(byte k) {
        if (k == 1)
            return dest;
        else
            return source.getTransition().getQk(k);
    }

    /**
     * @param k the order
     * @return recalling a transition is essentially p ---> Qk', ..., Qn,
     *         returns (p, Qk+1, ... Qn) state (i.e. order-k source)
     */
    public SAState getSourceK(byte k) {
        if (k == 1)
            return source;
        else
            return source.getTransition().getSourceK(k);
    }



    /**
     * @return the destination of the collapse branch of the transition
     */
    public ManagedSet<SAState> getCollapseDest() {
        return collapseDest;
    }

    /**
     * @return the state labelling the transition
     */
    public SCharacter getLabellingCharacter() {
        return label;
    }

    /**
     * @return pointer to principal justification of transition, or null if the
     * transition was given as part of the original SA.
     */
    public PrincipalJustification getPrincipalJustification() {
        return principalJustification;
    }

    /**
     * @return pointer to the unwound justification of the transition, or null
     * if no such justification is needed/present.
     */
    public ManagedSet<SATransitionO1> getUnwoundJustification() {
        return unwoundJustification;
    }


    /**
     * Override the equals method here for sake of SAManager using a set to
     * store the order-1 transitions (rather than a map from source, character,
     * collapse dest, dest to transition (which would entail four look ups or
     * the creation of a transition object to look up a transition object...)
     */
    public final boolean equals(Object o) {
        if (o instanceof SATransitionO1) {
            SATransitionO1 t = (SATransitionO1)o;
            return (Managers.saManager.equal(source, t.getSource()) &&
                    Managers.scManager.equal(label, t.getLabellingCharacter()) &&
                    Managers.sManager.equal(collapseDest, t.getCollapseDest()) &&
                    Managers.sManager.equal(dest, t.getDest()));
        } else {
            return false;
        }
    }


    public final int hashCode() {
        return hashCode;
    }

    @Override
    public String toString() {
	return source +
               " --- " + label + ", " + collapseDest + " ---> " +
               dest + " [ " + principalJustification + " ] ";
    }

    /**
     * toString using same notation as SATransition
     */
    public String toStringTransition() {
        return this.toString();
    }


    /**
     * Checks that all states in dest are order-1
     *
     * @param dest the destination set of the new candidate transition
     */
    private boolean assertOrder1(ManagedSet<SAState> dest) {
        for (SAState q : dest) {
            assert (q.getStateOrder() == 1)
                : "All dest states of an order-1 transition must be order-1";
        }
        return true;
    }

    /**
     * Checks that all states in collapse dest have same order
     *
     * @param collapseDest the collapse destination set of the new candidate transition
     */
    private boolean assertOrders(ManagedSet<SAState> collapseDest) {
        SAState q = collapseDest.choose();
        if (q != null) {
            byte k = q.getStateOrder();
            for (SAState q2 : collapseDest) {
                assert (q.getStateOrder() == k)
                    : "All collapse destination states must be of the same order";
            }
        }
        return true;
    }


}
