/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.structures.sa;

import java.util.Map;
import java.util.HashMap;

import ho.util.ManagedSet;
import ho.structures.cpds.CpdsStackPosition;
import ho.structures.sa.SAState;

public class SALabelledRun {

    Map<CpdsStackPosition, ManagedSet<SAState>> run =
        new HashMap<CpdsStackPosition, ManagedSet<SAState>>();

    public SALabelledRun() { }

    /**
     * @param pos the position to label
     * @param label the set of states to label it with
     */
    public void labelPosition(CpdsStackPosition pos,
                              ManagedSet<SAState> label) {
        run.put(new CpdsStackPosition(pos), label);
    }


    /**
     * @param pos the position
     * @return the label at that position (or null if no label)
     */
    public ManagedSet<SAState> getLabel(CpdsStackPosition pos) {
        return run.get(pos);
    }

    /**
     * Resets the run back to empty
     */
    public void resetRun() {
        run =  new HashMap<CpdsStackPosition, ManagedSet<SAState>>();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<CpdsStackPosition, ManagedSet<SAState>> kv
                 : run.entrySet()) {
            sb.append(kv.getKey());
            sb.append(" = ");
            sb.append(kv.getValue());
            sb.append("\n");
        }
        return sb.toString();
    }

}
