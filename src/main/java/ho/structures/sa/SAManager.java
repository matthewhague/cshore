/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.structures.sa;

import java.util.Set;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Comparator;

import org.apache.log4j.Logger;

import ho.util.ManagedSet;
import ho.util.SetManager;
import ho.util.DoubleMap;
import ho.util.TripleMap;
import ho.util.QuadMap;
import ho.util.Memoiser2Arg;
import ho.util.Memoiser4ArgByte;
import ho.util.MultiIterator;

import ho.structures.cpds.ControlState;
import ho.structures.cpds.SCharacter;
import ho.structures.cpds.CpdsConfig;
import ho.structures.cpds.CpdsStack;
import ho.structures.cpds.CpdsStackPosition;
import ho.structures.cpds.SCharacterManager;
import ho.structures.cpds.ControlStateManager;

import ho.witness.Witness;
import ho.witness.WitnessExtractor;
import ho.witness.PrincipalJustification;

import ho.fake.target.SATransGetter;
import ho.managers.Managers;


/**
 * A manager class for stack automata things -- stores Tnew and Told for
 * algorithm, provides make new transition methods.
 */
public class SAManager implements SATransGetter {

    static Logger logger = Logger.getLogger(SAManager.class);

    static SetManager<SAState> sManager = Managers.sManager;
    static SCharacterManager scManager = Managers.scManager;
    static ControlStateManager csManager = Managers.csManager;

    public interface IFullTransitionAcceptor {
        /**
         * For iterating over full transitions
         *
         *  q -- a, Qbr --> Qk, ..., Q1
         *
         * provide an IFullTransitionAcceptor to handle each transition found.
         *
         * Note: source state and character not given since it is assumed known
         * by f (i.e. iterate over transitions from q labelled a.
         *
         * @param qsbr the collapse destination set of states
         * @param qsis list of k sets of states, Qk,...,Q1 target of
         *             transition (Qk at 0 index, k is order of q)
         */
        public void accept(ManagedSet<SAState> qsbr,
                           List<ManagedSet<SAState>> qsis);
    }

    public interface IOrderStateAcceptor {
        /**
         * For iterating over transitions up to order k
         *
         *      q -- qk --> Qk, ..., Qk'
         *
         * provide an IOrderStateAcceptor to handle each qk found.
         *
         * @param qk the labelling order-k state stopped at
         */
        public void accept(SAState qk);
    }


    public interface ISimpleTransitionAcceptor {
        /**
         * For handling iteration over simple transitions q -- q' --> Q
         *
         * @param t the transition from q
         */
        public void accept(SATransition t);
    }

    public interface ISimpleTransitionO1Acceptor {
        /**
         * For handling iteration over simple O1 transitions q -- a, Qbr --> Q
         *
         * @param t the transition from q
         */
        public void accept(SATransitionO1 t);
    }

    /**
     * Pair (Qk-1, Qk) for iterating over
     *
     *      Q -- Qk-1 --> Qk   or   Q -- Qbr --> Qk
     *
     * for a given Q.  Qk is the dest, Qk-1 or Qbr is the label.  Qbr is
     * collapse dest when states in Q are order 1.
     */
    public class SimpleEnumeration {
        ManagedSet<SAState> qslab;
        ManagedSet<SAState> qsdest;

        public SimpleEnumeration() {
            this.qslab = sManager.makeEmptySet();
            this.qsdest = sManager.makeEmptySet();
        }

        public SimpleEnumeration(ManagedSet<SAState> qslab,
                                 ManagedSet<SAState> qsdest) {
            this.qslab = qslab;
            this.qsdest = qsdest;
        }

        public ManagedSet<SAState> getLabel() { return qslab; }
        public ManagedSet<SAState> getDest() { return qsdest; }

        public boolean equal(Object o) {
            if (o instanceof SimpleEnumeration) {
                SimpleEnumeration e = (SimpleEnumeration)o;
                return this.qslab.equals(e.qslab) &&
                       this.qsdest.equals(e.qsdest);
            }
            return false;
        }

        public int hashCode() {
            return this.qslab.hashCode() + this.qsdest.hashCode();
        }
    }


    Map<ControlState, SAOuterState> outerStates
        = new HashMap<ControlState, SAOuterState>();
    DoubleMap<SAState, ManagedSet<SAState>, SAMajorana> orderKContents
        = new DoubleMap<SAState, ManagedSet<SAState>, SAMajorana>();
    QuadMap<SAState,
            SCharacter,
            ManagedSet<SAState>,
            ManagedSet<SAState>,
            SATransitionO1> order1Contents
        = new QuadMap<SAState,
                      SCharacter,
                      ManagedSet<SAState>,
                      ManagedSet<SAState>,
                      SATransitionO1>();

    Map<SAState, LinkedList<SATransition>> orderKOldStateTrans
        = new HashMap<SAState, LinkedList<SATransition>>();
    TripleMap<ControlState,
              SCharacter,
              SAState,
              LinkedList<SATransitionO1>> order1OldStateTrans
        = new TripleMap<ControlState,
                        SCharacter,
                        SAState,
                        LinkedList<SATransitionO1>>();

    LinkedList<SATransition> orderKTnew = new LinkedList<SATransition>();
    LinkedList<SATransitionO1> order1Tnew = new LinkedList<SATransitionO1>();

    Iterable<SATransition> emptyIterable = (new LinkedList<SATransition>());
    Iterable<SATransitionO1> emptyIterableO1 = (new LinkedList<SATransitionO1>());


    Memoiser2Arg<SAState, ControlState, SAState> switchMemo
        = new Memoiser2Arg<SAState, ControlState, SAState>();
    Memoiser4ArgByte<SAState,
                     ControlState,
                     ManagedSet<SAState>,
                     SAState> switchUnionMemo
        = new Memoiser4ArgByte<SAState,
                               ControlState,
                               ManagedSet<SAState>,
                               SAState>();

    byte order;

    public static final int NO_SET_BOUND = -1;
    int maxSetSize = NO_SET_BOUND;
    boolean trackNew = true;

    // IMPORTANT: must be updated if sManager is reset (i.e. reset SAManager
    // after SetManager!
    Set<SimpleEnumeration> fromEmptyEnumeration
        = new HashSet<SimpleEnumeration>(Collections.nCopies(1, new SimpleEnumeration()));

    /**
     * Constructs an SAManager -- NOTE: setOrder/reset should be called before
     * using.
     */
    public SAManager() { }

    /**
     * Constructs an SAManager with given order
     */
    public SAManager(byte order) {
        this.order =  order;
    }


    public void setOrder(byte order) {
        this.order = order;
    }

    /**
     * @param trackNew true if new transitions should be stored in a separate
     * Tnew list so that getNewTransition and getNewTransitionO1 will work
     */
    public void setTrackNew(boolean trackNew) {
        this.trackNew = trackNew;
    }

    /**
     * For simple CEGAR restrict the max size of any stateset in a transition of
     * the automaton.  If some Q has more than size states, use instead a subset
     * of Q of given size (arbitrary subset).
     *
     * @param the max size of any stateset in the automaton
     */
    public void setStatesetBound(int size) {
        maxSetSize = size;
    }

    /**
     * Removes any bound on stateset size in the aut (turns out CEGAR)
     */
    public void removeStatesetBound() {
        maxSetSize = NO_SET_BOUND;
    }

    /**
     * @return the bound on the sizes of statesets, NO_SET_BOUND if none
     */
    public int getStatesetBound() {
        return maxSetSize;
    }

    public boolean hasStatesetBound() {
        return (maxSetSize != NO_SET_BOUND);
    }

    /**
     * @param s1 first state set
     * @param s2 second state set
     * @return the union of s1 and s2 up to the stateset bound of samanager
     *         if s1 is already bigger than the bound, just return s1
     */
    public final ManagedSet<SAState> unionStatesets(ManagedSet<SAState> s1,
                                                    ManagedSet<SAState> s2) {
        return sManager.makeSet(unionStatesets(s1.getSet(), s2.getSet()));
    }

    /**
     * @param s1 first state set
     * @param s2 second state set
     * @return the union of s1 and s2 up to the stateset bound of samanager
     *         if s1 is already bigger than the bound, just return s1
     */
    public final Set<SAState> unionStatesets(Set<SAState> s1, Set<SAState> s2) {
        if (hasStatesetBound()) {
            int bound = getStatesetBound();
            if (s1.size() >= bound) {
                return s1;
            } else {
                Set<SAState> result = new HashSet<SAState>(s1);
                Iterator<SAState> i = s2.iterator();
                while (i.hasNext() && result.size() < bound) {
                    result.add(i.next());
                }
                return result;
            }
        } else {
            Set<SAState> result = new HashSet<SAState>(s1);
            result.addAll(s2);
            return result;
        }
    }


    /**
     * clean the SAManager for a new run of the algorithm
     *
     * @param order the order of the automaton to manage
     */
    public void reset(byte order) {
        setOrder(order);
        setTrackNew(true);
        switchMemo = new Memoiser2Arg<SAState, ControlState, SAState>();
        switchUnionMemo
        = new Memoiser4ArgByte<SAState,
                               ControlState,
                               ManagedSet<SAState>,
                               SAState>();
        outerStates.clear();
        orderKContents.clear();
        order1Contents.clear();
        orderKOldStateTrans.clear();
        order1OldStateTrans.clear();
        orderKTnew.clear();
        order1Tnew.clear();
        fromEmptyEnumeration
            = new HashSet<SimpleEnumeration>(Collections.nCopies(1, new SimpleEnumeration()));
    }


    /**
     * @param controlState the control state the new sa state will correspond to
     * @return true iff the control state did not exist
     */
    public boolean addOuterState(ControlState controlState) {
        SAOuterState os = outerStates.get(controlState);
        if (os == null) {
            outerStates.put(controlState,
                            new SAOuterState(controlState, order));
            return true;
        } else
            return false;
    }

    /**
     * @param controlState the control state the new sa state will correspond to
     * @return the outer state object for controlState
     */
    public SAOuterState getAddOuterState(ControlState controlState) {
        SAOuterState os = outerStates.get(controlState);
        if (os == null) {
            os = new SAOuterState(controlState, order);
            outerStates.put(controlState, os);
        }
        return os;
    }


    /**
     * Constructs and adds a new transition q -- a, Qbr --> Q1, ..., Qn, adds
     * new bits to tnew if collecting new transitions.
     *
     * @param q the source state (should already exist)
     * @param a the character label
     * @param qsbr the collapse target
     * @param qsis array of Qn, ..., Q1 with Qn at index 0
     * @return boolean if new transition added
     */
    public boolean addTransition(SAState q,
                                 SCharacter a,
                                 ManagedSet<SAState> qsbr,
                                 List<ManagedSet<SAState>> qsis) {
        // TODO: should probably assert q exists
        assert (consistencyCheck(qsbr) &&
                consistencyCheckList(qsis) &&
                qsis.size() <= order) : "addTransition - inconsistent orders in arguments.";

        SAState prev = q;
        for (int i = 0; i < qsis.size() - 1; i++) {
            SATransition t = getAddTransition(prev, qsis.get(i));
            prev = t.getLabellingState();
        }
        ManagedSet<SAState> dest = qsis.get(qsis.size() - 1);
        return addTransitionO1(prev, a, qsbr, dest);
    }

    /**
     * Constructs and adds a new transition q -- a, 0 --> 0, ..., 0 from q of
     * any order, adding new bits to Tnew if enabled..
     *
     * @param q the source state (should already exist)
     * @param a the character label
     * @return true if new bits added
     */
    public boolean addEmptyTransition(SAState q,
                                      SCharacter a) {
        return addEmptyTransition(q, a, sManager.makeEmptySet());
    }

    /**
     * Constructs and adds a new transition q -- a, qsbr --> 0, ..., 0 from q of
     * any order.
     *
     * @param q the source state (should already exist)
     * @param a the character label
     * @param qsbr the set of states that should appear on the collapse branch
     * @return true if new bits added
     */
    public boolean addEmptyTransition(SAState q,
                                      SCharacter a,
                                      ManagedSet<SAState> qsbr) {
        ManagedSet<SAState> e = sManager.makeEmptySet();
        SAState prev = q;
        for (int i = 0; i < q.getStateOrder() - 1; i++) {
            SATransition t = getAddTransition(prev, e);
            prev = t.getLabellingState();
        }
        return addTransitionO1(prev, a, qsbr, e);
    }


    /**
     * Constructs a new transition q ----> qs, adds it to Tnew if it doesn't
     * already exist
     *
     * @param q the source of the new transition
     * @param qs the destination state set of the new transition
     * @return true iff new transition added
     */
    public boolean addTransition(SAState q,
                                 ManagedSet<SAState> qs) {
	assert (consistencyCheck(qs)) : "getAddTransition - inconsistent orders in qs: " + qs;
        qs = restrictSetSize(qs);
        SAMajorana t = orderKContents.get(q, qs);
        if (t == null) {
            t = new SAMajorana(q, qs);
            orderKContents.put(q, qs, t);
            if (trackNew)
                orderKTnew.addFirst(t);

            return true;
        } else
            return false;
    }

    /**
     * Constructs a new transition q ----> qs, adds it to Tnew if it doesn't
     * already exist
     *
     * @param q the source of the new transition
     * @param qs the destination state set of the new transition
     * @return the transition for q --> qs
     */
    public SATransition getAddTransition(SAState q,
                                         ManagedSet<SAState> qs) {
        qs = restrictSetSize(qs);
        SAMajorana t = orderKContents.get(q, qs);

        assert (consistencyCheck(qs)) : "getAddTransition - inconsistent orders in qs: " + qs;

        if (t == null) {
            t = new SAMajorana(q, qs);
            orderKContents.put(q, qs, t);
            if (trackNew)
                orderKTnew.addFirst(t);
//            logger.debug("New order-" + q.getStateOrder() + ", dest size: " + qs.size() + ", total ord->1 trans: "
//		+ orderKContents.values().size());
        }
        return t;
    }

    //Useful for testing
    public boolean containsTransition(SAState q,
                                      ManagedSet<SAState> qs)
    {
        qs = restrictSetSize(qs);
	    return (orderKContents.get(q, qs) != null);
    }




    /**
     * addTransitionO1(q, a, qsbr, qs, null, null)
     */
    public boolean addTransitionO1(SAState q,
                                   SCharacter a,
                                   ManagedSet<SAState> qsbr,
                                   ManagedSet<SAState> qs) {
        qs = restrictSetSize(qs);
        qsbr = restrictSetSize(qsbr);
        return addTransitionO1(q, a, qsbr, qs, null, null);
    }



    /**
     * addTransitionO1(q, a, qsbr, qs, justif, null)
     */
    public boolean addTransitionO1(SAState q,
                                   SCharacter a,
                                   ManagedSet<SAState> qsbr,
                                   ManagedSet<SAState> qs,
                                   PrincipalJustification justif) {
        qs = restrictSetSize(qs);
        qsbr = restrictSetSize(qsbr);
        return addTransitionO1(q, a, qsbr, qs, justif, null);
	}

    /**
     * Constructs a new transition q -- a, Qbr --> Q, adds it to Tnew if it
     * doesn't already exist
     *
     * Does an anti-chain optimisation, that is q -- a, Qbr --> Q is not
     * added if there is q -- a, Q'br --> Q' with Q' <= Q and Q'br <= Qbr.
     *
     * @param q the source state
     * @param a the character labelling the transition
     * @param qsbr the destination of the collapse link
     * @param qs the (order-1) destination of the transition
     * @param justif the transition that justifies the existence of this one
     * (for counter example generationi)
     * @param unwoundJustif further justification of the current transition
     * @return true iff new transition added
     */
    public boolean addTransitionO1(SAState q,
                                   SCharacter a,
                                   ManagedSet<SAState> qsbr,
                                   ManagedSet<SAState> qs,
                                   PrincipalJustification justif,
                                   ManagedSet<SATransitionO1> unwoundJustif) {
        assert (consistencyCheck(qsbr))
            : "addTransitionO1 - inconsistent orders in qsbr: " + qsbr;
        assert (consistencyCheck(qs))
            : "addTransitionO1 - inconsistent orders in qs: " + qs;

        qs = restrictSetSize(qs);
        qsbr = restrictSetSize(qsbr);

        SATransitionO1 t = order1Contents.get(q, a, qsbr, qs);
        if (t == null) {
            // check if new
            boolean toadd = transitionO1NotSubsumed(q, a, qsbr, qs);
            if (toadd) {
                t = new SATransitionO1(q, a, qsbr, qs, justif, unwoundJustif);
                addAndFilterTransitionO1(t);
            }
            return toadd;
        } else
            return false;
	}

    //Useful for testing
    public boolean containsTransitionO1(SAState q,
            SCharacter a,
            ManagedSet<SAState> qsbr,
            ManagedSet<SAState> qs) {
        qs = restrictSetSize(qs);
        qsbr = restrictSetSize(qsbr);
	return (order1Contents.get(q, a, qsbr, qs) != null);
    }

    /**
     * Constructs a new transition q -- a, Qbr --> Q, adds it to Tnew if it
     * doesn't already exist
     *
     * @param q the source state
     * @param a the character labelling the transition
     * @param qsbr the destination of the collapse link
     * @param qs the (order-1) destination of the transition
     * @return the new transtion q -- a, Qbr --> Q
     */
    public SATransitionO1 getAddTransitionO1(SAState q,
                                             SCharacter a,
                                             ManagedSet<SAState> qsbr,
                                             ManagedSet<SAState> qs) {
        assert (consistencyCheck(qsbr)) : "getAddTransitionO1 - inconsistent orders in qsbr: " + qsbr;
        assert (consistencyCheck(qs)) : "getAddTransitionO1 - inconsistent orders in qs: " + qs;

        qs = restrictSetSize(qs);
        qsbr = restrictSetSize(qsbr);

        SATransitionO1 t = order1Contents.get(q, a, qsbr, qs);
        if (t == null) {
            t = new SATransitionO1(q, a, qsbr, qs);
            order1Contents.put(q, a, qsbr, qs, t);
            if (trackNew)
                order1Tnew.addFirst(t);
        }
        return t;
    }

    /**
     * @param qs set of SAState all of same order, not order 1
     * @return enumeration of all pairs (Q', Q) s.t. qs -- Q' --> Q
     */
    public Set<SimpleEnumeration> enumerateSimple(ManagedSet<SAState> qs) {
        Set<SimpleEnumeration> res = fromEmptyEnumeration;

        for (SAState q : qs) {
            Set<SimpleEnumeration> newres = new HashSet<SimpleEnumeration>();

            for (Map.Entry<ManagedSet<SAState>, SAMajorana> entry : orderKContents.entrySet(q)) {
                SATransition t = entry.getValue().getTransition();
                for (SimpleEnumeration e : res) {
                    ManagedSet<SAState> elab =
                        sManager.add(e.getLabel(), t.getLabellingState());
                    ManagedSet<SAState> edest =
                        sManager.union(e.getDest(),t.getDest());
                    SimpleEnumeration newe =
                        new SimpleEnumeration(elab, edest);

                    newres.add(newe);
                }
            }

            res = newres;
        }

        return res;
    }

    /**
     * @param qs set of SAState all of same order, not order 1
     * @param a the character that has to label the outgoing transitions
     * @return enumeration of all pairs (Qbr, Q) s.t. qs -- a, Qbr --> Q
     */
    public Set<SimpleEnumeration> enumerateSimpleO1(ManagedSet<SAState> qs,
                                                    SCharacter a) {
        Set<SimpleEnumeration> res = fromEmptyEnumeration;

        for (SAState q : qs) {
            Set<SimpleEnumeration> newres = new HashSet<SimpleEnumeration>();

            for (SATransitionO1 t : order1Contents.valuesIterator(q, a)) {
                for (SimpleEnumeration e : res) {
                    ManagedSet<SAState> elab =
                        sManager.union(e.getLabel(), t.getCollapseDest());
                    ManagedSet<SAState> edest =
                        sManager.union(e.getDest(),t.getDest());
                    SimpleEnumeration newe =
                        new SimpleEnumeration(elab, edest);

                    newres.add(newe);
                }
            }

            res = newres;
        }

        return res;
    }

    /**
     * Iterates over simple transitions q -- q' --> Q from q
     *
     * @param q the source state of order > 1
     * @param f an object to deal with transitions found during iteration
     */
    public void iterateSimple(SAState q, ISimpleTransitionAcceptor f) {
        for (Map.Entry<ManagedSet<SAState>, SAMajorana> entry : orderKContents.entrySet(q)) {
            SATransition t = entry.getValue().getTransition();
            f.accept(t);
        }
    }

    /**
     * Iterates over simple o1 transitions (q, ...) -- a, Qbr --> Q ultimately
     * from state q (of any order)
     *
     * @param q the order k state to start from
     * @param a the character on the transitions
     * @param f an object to deal with transitions found during iteration
     */
    public void iterateSimpleO1(SAState q,
                                SCharacter a,
                                ISimpleTransitionO1Acceptor f) {
        if (q.getStateOrder() == 1) {
            for (SATransitionO1 t : order1Contents.valuesIterator(q, a)) {
                f.accept(t);
            }
        } else {
            for (Map.Entry<ManagedSet<SAState>, SAMajorana> entry : orderKContents.entrySet(q)) {
                SATransition t = entry.getValue().getTransition();
                iterateSimpleO1(t.getLabellingState(), a, f);
            }
        }
    }

    /**
     * @param q an order 1 state
     * @param a a character
     * @return the set of transitions q -- a, Qbr --> Q1
     */
    public Iterable<SATransitionO1> getTransitionsO1(SAState q,
                                                     SCharacter a) {
        return order1Contents.valuesIterator(q, a);
    }

    /**
     * Iterates over full transitions from sets of states
     *
     *      Q -- a, Qbr --> Q1, ..., Qk
     *
     * where Q is a set of order k states.
     *
     * @param qs the source set of states all of same order
     * @param a the character that ultimately must label the transitions
     * @param f what to do with each transition found
     */
    public void iterateFull(ManagedSet<SAState> qs,
                            SCharacter a,
                            IFullTransitionAcceptor f) {
        if (qs.isEmpty())
            return;

        byte qorder = qs.choose().getStateOrder();

        List<ManagedSet<SAState>> outerqs
            = new ArrayList<ManagedSet<SAState>>(qorder);
        iterateFullInner(qs, qorder, a, outerqs, f);
    }

    /**
     * Iterates over full transitions from sets of states
     *
     *      Q -- a, Qbr --> Q1, ..., Qk
     *
     * where Q is a set of order k states.
     *
     * @param qs the source set of states all of same order
     * @param qsOrder the intended order of the states in qs (since qs could
     *                be empty)
     * @param a the character that ultimately must label the transitions
     * @param outer the Qk' .. Qk+1 states already iterated over that must
     *              appear at the end of the iterated transitions
     *                  Qk' appears at index 0
     *                  Qk+1 appears at outerindex - 1
     * @param f what to do with each transition found
     */
    public void iterateFullInner(ManagedSet<SAState> qs,
                                 byte qsOrder,
                                 SCharacter a,
                                 List<ManagedSet<SAState>> outer,
                                 IFullTransitionAcceptor f) {
        outer.add(null);
        int last_pos = outer.size() - 1;

        if (qsOrder == 1) {
            Set<SimpleEnumeration> es = enumerateSimpleO1(qs, a);
            for (SimpleEnumeration e : es) {
                outer.set(last_pos, e.getDest());
                f.accept(e.getLabel(), outer);
            }
        } else {
            Set<SimpleEnumeration> es = enumerateSimple(qs);
            for (SimpleEnumeration e : es) {
                outer.set(last_pos, e.getDest());
                iterateFullInner(e.getLabel(),
                                 (byte)(qsOrder - 1),
                                 a,
                                 outer,
                                 f);
            }
        }

        outer.remove(last_pos);
    }

    /**
     * Iterates over transitions from a state up to order-k
     *
     *      q -- q' --> Qk+1, ..., Qk'
     *
     * where q is a set of order k' states.
     *
     * @param q the source state
     * @param k the order to stop iterating at
     * @param f what to do with each transition found
     */
    public void iterateOrderState(SAState q,
                                  byte k,
                                  IOrderStateAcceptor f) {
        if (k == order) {
            // assumes q is order-k = order-n! (else bad call)
            f.accept(q);
        } else if (q.getStateOrder() == k+1) {
            for (Map.Entry<ManagedSet<SAState>, SAMajorana> entry : orderKContents.entrySet(q)) {
                SATransition t = entry.getValue().getTransition();
                f.accept(t.getLabellingState());
            }
        } else {
            for (Map.Entry<ManagedSet<SAState>, SAMajorana> entry : orderKContents.entrySet(q)) {
                SATransition t = entry.getValue().getTransition();
                iterateOrderState(t.getLabellingState(), k, f);
            }
        }
    }


    // For assertions/testing: Returns true iff all elements of set same order:
    private boolean consistencyCheck(ManagedSet<SAState> Q) {
        boolean result = true;

        if(!Q.isEmpty()) {
            byte order = Q.choose().getStateOrder();

            for(SAState q : Q)
                result = result && (q.getStateOrder() == order);


        }

	    return result;
    }



    /**
     * @return true iff there is a new order-k transition
     */
    public final boolean hasNewTransition() {
        return !orderKTnew.isEmpty();
    }

    /**
     * @return true iff there is a new order-1 transition
     */
    public final boolean hasNewTransitionO1() {
        return !order1Tnew.isEmpty();
    }

    /**
     * Removes an order-k transition from Tnew and adds it to Told.
     *
     * @return an order-k transition from Tnew, null if there are none
     */
    public final SATransition getNewTransition() {
        SATransition t = orderKTnew.poll();
        if (t != null) {
            addOldTran(t);
        }
        return t;
    }


    /**
     * Removes an order-1 transition from Tnew, adds it to Told.
     *
     * @return an order-k transition from Tnew, null if there are none
     */
    public final SATransitionO1 getNewTransitionO1() {
        SATransitionO1 t = order1Tnew.poll();
        if (t != null) {
            addOldTranO1(t);
        }
        return t;
    }




    /**
     * @param q1 the first part of the equality
     * @param q2 the second part of the equality
     * @return true iff q1 is the same state as q2
     */
    public final boolean equal(SAState q1, SAState q2) {
        // works because SAManager only ever creates one object for distinct
        // states
        return (q1 == q2);
    }

    /**
     * @param t1 the first part of the equality
     * @param t2 the second part of the equality
     * @return true iff q1 is the same transition as q2
     */
    public final boolean equal(SATransition t1, SATransition t2) {
        // works because SAManager only ever creates one object for distinct
        // transitions
        return (t1 == t2);
    }

    /**
     * @param t1 the first part of the equality
     * @param t2 the second part of the equality
     * @return true iff q1 is the same transition as q2
     */
    public final boolean equal(SATransitionO1 t1, SATransitionO1 t2) {
        // works because SAManager only ever creates one object for distinct
        // transitions
        return (t1 == t2);
    }


    /**
     * @param q the source state of the transitions to iterate over
     * @return an iterator over the transitions from state q
     */
    public final Iterable<SATransition> oldTranIterator(SAState q) {
        Iterable<SATransition> i;
        LinkedList<SATransition> l = orderKOldStateTrans.get(q);
        if (l != null) {
            i = l;
        } else {
            i = emptyIterable;
        }
        return i;
    }

    /**
     * @param q the source state of the transitions to iterate over
     * @param a the character of the transitions to iterate over
     * @return an iterator over the transitions from state q
     */
    public final Iterable<SATransitionO1> oldTranO1Iterator(SAState q,
                                                            SCharacter a) {
        Iterable<SATransitionO1> i;
        LinkedList<SATransitionO1> l = order1OldStateTrans.get(q.getControlState(), a, q);
        if (l != null) {
            i = l;
        } else {
            i = emptyIterableO1;
        }
        return i;
    }

    /**
     * @param p the source control state of the transitions to iterate over
     * @param a the character of the transitions to iterate over
     * @return an iterator over the transitions from state q
     */
    public final Iterable<SATransitionO1> oldTranO1HeadIterator(ControlState p,
                                                                SCharacter a) {
        Iterable<SATransitionO1> i;
        Iterable<LinkedList<SATransitionO1>> l = order1OldStateTrans.get(p, a);
        if (l != null) {
            i = new MultiIterator<SATransitionO1>(l);
        } else {
            i = emptyIterableO1;
        }
        return i;
    }



    /**
     * For adding transitions to Told at order-k
     *
     * @param t the transition to add
     */
    protected void addOldTran(SATransition t) {
        SAState q = t.getSource();
        LinkedList<SATransition> l = orderKOldStateTrans.get(q);
        if (l == null) {
            l = new LinkedList<SATransition>();
            orderKOldStateTrans.put(q, l);
        }
        l.addFirst(t);
    }

    /**
     * For adding transitions to Told at order-1
     *
     * @param t the transition to add
     */
    protected void addOldTranO1(SATransitionO1 t) {
        SAState q = t.getSource();
        SCharacter a = t.getLabellingCharacter();
        ControlState p = q.getControlState();

        // first for old tran iterating
        LinkedList<SATransitionO1> l = order1OldStateTrans.get(p, a, q);
        if (l == null) {
            l = new LinkedList<SATransitionO1>();
            order1OldStateTrans.put(p, a, q, l);
        }
        l.addFirst(t);
    }


    /**
     * @param c a control state
     * @return the outer state for control state c
     */
    public SAOuterState getOuterState(ControlState c) {
        return outerStates.get(c);
    }

    /**
     * @param q the source state
     * @param qs the destination state set
     * @return the transition q ---> qs, or null if doesn't exist
     */
    public SATransition getTransition(SAState q, ManagedSet<SAState> qs) {
        qs = restrictSetSize(qs);
        return orderKContents.get(q, qs);
    }

    /**
     * @param q a source state
     * @param qs a destination state set
     * @return the state labelling the transition q ---> qs, or null if doesn't
     * exist
     */
    public SAState getState(SAState q, ManagedSet<SAState> qs) {
        qs = restrictSetSize(qs);
        return orderKContents.get(q, qs);
    }

    /**
     * @param q the source state
     * @param a the stack character
     * @param qsbr the collapse link destination
     * @param qs the destination
     * @return the transtiion q -- a, qsbr --> qs if exists (else null)
     */
    public SATransitionO1 getTransitionO1(SAState q,
                                          SCharacter a,
                                          ManagedSet<SAState> qsbr,
                                          ManagedSet<SAState> qs) {
        qs = restrictSetSize(qs);
        qsbr = restrictSetSize(qsbr);
        return order1Contents.get(q, a, qsbr, qs);
    }

    /**
     * From a state (p, Qk, .., Qn) create (p', Qk, ..., Qn) -- may involve
     * creating new transitions (that will be put in Tnew)
     *
     * @param q the state (p, Qk, ..., Qn)
     * @param c the new control
     * @param a state (c, Qk, ..., Qn)
     */
    public SAState switchControlStateAdd(SAState q, ControlState c) {
        SAState qnew = switchMemo.get(q, c);
        if (qnew == null) {
            if (q.getStateOrder() == order) {
                qnew = getAddOuterState(c);
            } else {
                SATransition t = q.getTransition();
                SAState qtnew = switchControlStateAdd(t.getSource(), c);
                SATransition tnew = getAddTransition(qtnew, t.getDest());
                qnew = tnew.getLabellingState();
            }
            switchMemo.put(q, c, qnew);
        }
        return qnew;
    }

    /** From a state (p, Qk', .., Qn) create (p', Qk', ..., Qk + Qbr, ..., Qn)
     * -- may involve creating new transitions (that will be put in Tnew)
     *
     * @param q the state (p, Qk, ..., Qn)
     * @param qbr a stateset of order-k
     * @param k the order to adjust
     * @param c the new control
     * @return param a state (c, Qk', ..., Qk+Qbr, ..., Qn)
     */
    public SAState switchControlStateUnionOrdkAdd(SAState q,
                                                  ControlState c,
                                                  ManagedSet<SAState> qbr,
                                                  byte k) {
        SAState qnew = switchUnionMemo.get(q, c, qbr, k);
        if (qnew == null) {
            if (q.getStateOrder() == order) {
                qnew = getAddOuterState(c);
            } else {
                SATransition t = q.getTransition();

                SAState qtnew;
                if (k >= t.getTransitionOrder()) {
                    qtnew = switchControlStateUnionOrdkAdd(t.getSource(),
                                                           c,
                                                           qbr,
                                                           k);
                } else {
                    qtnew = switchControlStateAdd(t.getSource(), c);
                }

                ManagedSet<SAState> destnew;
                if (k == t.getTransitionOrder())
                    destnew = restrictSetSize(sManager.union(t.getDest(), qbr));
                else
                    destnew = t.getDest();
                SATransition tnew = getAddTransition(qtnew, destnew);
                qnew = tnew.getLabellingState();
            }
            switchUnionMemo.put(q, c, qbr, k, qnew);
        }
        return qnew;
    }


    /**
     * Prints the transitions in Told to stdout
     */
    public void printAutomaton() {

        System.out.println("Order-K contents:");
        for (SATransition t : orderKContents.values()) {
            System.out.println("   " + t.toStringTransition());
        }

        System.out.println("Order-1 contents:");
        for (SATransitionO1 t : order1Contents.values()) {
            System.out.println("   " + t.toStringTransition());
        }

    }


    public String shortStats() {
        StringBuilder sb = new StringBuilder();
        sb.append("OK: " + orderKContents.size() + "\n");
        sb.append("O1: ");
        sb.append(order1Contents.size() + "\n");
        return sb.toString();
    }

    /**
     * Prints stats about automaton constructed
     */
    public String automatonStats() {
        StringBuilder sb = new StringBuilder();
        sb.append("Order-K contents:\n");
        sb.append("    Size: " + orderKContents.size() + "\n");

        sb.append("Order-1 contents:\n");
        sb.append("    Size: " + order1Contents.size() + "\n");

        int numBigDests = 0;

        for (SATransition t : orderKContents.values()) {
            if (t.getDest().size() > 1)
                numBigDests++;
        }

        sb.append("Big dests = " + numBigDests + "\n");

        int numBigColDestsO1 = 0;
        int numSingleColDestsO1 = 0;
        int numBigDestsO1 = 0;
        int numSingleDestsO1 = 0;
        int numRedundant = 0;

        for (SATransitionO1 t : order1Contents.values()) {
            if (t.getCollapseDest().size() > 1)
                numBigColDestsO1++;
            if (t.getCollapseDest().size() == 1)
                numSingleColDestsO1++;
            if (t.getDest().size() > 1)
                numBigDestsO1++;
            if (t.getDest().size() == 1)
                numSingleDestsO1++;
        }

        sb.append("Big col dests O1 = " + numBigColDestsO1 + "\n");
        sb.append("Big dests O1 = " + numBigDestsO1 + "\n");
        sb.append("Single col dests O1 = " + numSingleColDestsO1 + "\n");
        sb.append("Single dests O1 = " + numSingleDestsO1 + "\n");

        return sb.toString();
    }



    /**
     * returns the stack automaton built so far (inefficient since it copies
     * internal data structures to a currently clunky (for testing only) SA
     * implementationi)
     *
     * @return the SA built so far
     */
    public SA getSA() {
        SA sa = new SA();
        sa.setOuterStates(outerStates);
        for (SATransition t : orderKContents.values()) {
            sa.addTransition(t);
        }
        for (SATransitionO1 t : order1Contents.values()) {
            sa.addTransitionO1(t);
        }
        return sa;
    }


    /**
     * @param p the initial control state
     * @param a the initial stack character
     * @return true if initial configuration <q,[...[a]...]> is accepted by
     *         the SA built so far
     */
    public boolean accepts(ControlState p, SCharacter a) {
        // note, implementation exploits the fact we don't have final states, so
        // all accepting transitions must go to empty
        //
        // that is, we only accept with p --- a, { } ---> ({ }, ..., { })
        SAState q = getOuterState(p);
        if (q != null) {
            ManagedSet<SAState> e = sManager.makeEmptySet();
            for (int k = order; k > 1; k--) {
                SATransition t = getTransition(q, e);
                if (t == null)
                    return false;
                q = t.getLabellingState();
            }
            SATransitionO1 t = getTransitionO1(q, a, e, e);
            return (t != null);
        }
        return false;
    }

    /**
     * @param p the initial control state
     * @param a the initial stack character
     * @return a witness trace from <p, [..[a]..]> to error state, or null if
     *         none exists
     */
    public Witness getCounterExample(ControlState p, SCharacter a) {
        // note, implementation exploits the fact we don't have final states, so
        // all accepting transitions must go to empty
        //
        // that is, we only accept with p --- a, { } ---> ({ }, ..., { })
        SAState q = getOuterState(p);
        if (q != null) {
            ManagedSet<SAState> e = sManager.makeEmptySet();
            for (int k = order; k > 1; k--) {
                SATransition t = getTransition(q, e);
                if (t == null)
                    return null;
                q = t.getLabellingState();
            }
            SATransitionO1 t = getTransitionO1(q, a, e, e);
            if (t != null) {
                WitnessExtractor we = new WitnessExtractor();
                return we.extractWitness(order, t);
            }
        }
        return null;
    }


    /**
     * @param c a configuration
     * @return true iff the configuration c is accepted by the SA built so far
     */
    public boolean accepts(CpdsConfig c) {
        // this may turn out to be inefficient since calling values on a quadmap
        // requires building a new collection, then sarun will build a new
        // collection...
        SARun run = new SARun(order1Contents.values());
        return run.buildRun(c);
    }


    ////////////////////////////////////////////////////////////////
    // Private methods

    /**
     * Goes through order1contents and finds if the transition would be
     * subsumed.
     *
     * @param q the source state of the new transition
     * @param a the character of the new transition
     * @param qsbr the collapse destination of the new transition
     * @param qs the destination of the new trantiion
     * @return true iff there is not another transition in order1contents of the
     * form q -- a, qsbr' --> qs' with qs' <= qs and qsbr' <= qsbr.  That is,
     * the transition is not redundant
     */
    private boolean transitionO1NotSubsumed(SAState q,
                                            SCharacter a,
                                            ManagedSet<SAState> qsbr,
                                            ManagedSet<SAState> qs) {

        // This code for checking at all orders

        //boolean subsumed = false;
        //for (SATransitionO1 t : oldTranO1HeadIterator(q.getControlState(), a)) {
        //    if (transitionSubsumes(t, q, a, qsbr, qs)) {
        //        subsumed = true;
        //        break;
        //    }
        //}
        //return !subsumed;

        // Code below only compares at order-1 level

        boolean notSubsumed = true;
        QuadMap.K3Iterator<ManagedSet<SAState>,
                           ManagedSet<SAState>,
                           SATransitionO1> i;
        i = order1Contents.keyIterator(q, a);
        if (i != null) {
            while (notSubsumed && i.hasNext()) {
                if (sManager.subset(i.next(), qsbr)) {
                    Iterator<Map.Entry<ManagedSet<SAState>,
                                       SATransitionO1>> i2 = i.getK4Entries().iterator();
                    while (notSubsumed && i2.hasNext()) {
                        Map.Entry<ManagedSet<SAState>,
                                  SATransitionO1> e = i2.next();
                        notSubsumed = !sManager.subset(e.getKey(), qs);
                    }
                }
            }
        }
        return notSubsumed;
    }


    /**
     * @param t1 an order-1 transition
     * @param q
     * @param a
     * @param qsbr
     * @param qs
     * @return true iff t1 <= (q --- a, qsbr ---> qs)
     */
    private boolean transitionSubsumes(SATransitionO1 t1,
                                       SAState q,
                                       SCharacter a,
                                       ManagedSet<SAState> qsbr,
                                       ManagedSet<SAState> qs) {
        if (!scManager.equal(t1.getLabellingCharacter(), a))
            return false;

        SAState q1 = t1.getSource();

        ControlState p1 = q1.getControlState();
        ControlState p2 = q.getControlState();

        if (!csManager.equal(p1, p2))
            return false;

        if (!sManager.subset(t1.getCollapseDest(), qsbr))
            return false;

        if (!sManager.subset(t1.getDest(), qs))
            return false;

        for (byte k = 2; k <= order; ++k) {
            SATransition tout1 = q1.getTransition();
            SATransition tout2 = q.getTransition();

            if (!sManager.subset(tout1.getDest(), tout2.getDest()))
                return false;

            q1 = tout1.getSource();
            q = tout2.getSource();
        }

        return true;
    }

    /**
     * @param t1 an order-1 transition
     * @param t2 an order-1 transition
     * @return true iff t1 <= t2
     */
    private boolean transitionSubsumes(SATransitionO1 t1,
                                       SATransitionO1 t2) {
        return transitionSubsumes(t1,
                                  t2.getSource(),
                                  t2.getLabellingCharacter(),
                                  t2.getCollapseDest(),
                                  t2.getDest());
    }




    /**
     * Adds a new order-1 transition to the SAManager data structures, as well
     * as removes any transition from Tnew or Told that have been subsumed by
     * the new one.  (Note, can also remove from order1contents, but there are
     * things to think about wrt to doing this:
     *     1) If we try to re-add a transition removed from order1contents, we
     *     have to search order1contents again to find that its redundant.
     *
     *     2) It takes more filtering time.
     *
     *     3) Perhaps we should memoize addTransitionO1 to remember what we
     *     decided...
     *
     *     4) Hmm...)
     *
     * @param t the new order-1 transition
     */
    private void addAndFilterTransitionO1(SATransitionO1 t) {
        SAState q = t.getSource();
        SCharacter a = t.getLabellingCharacter();
        ManagedSet<SAState> qsbr = t.getCollapseDest();
        ManagedSet<SAState> qs = t.getDest();

        // filter Told if we're paying attention to these things
        if (trackNew) {
            LinkedList<SATransitionO1> l = order1OldStateTrans.get(q.getControlState(), a, q);
            if (l != null) {
                Iterator<SATransitionO1> i = l.iterator();
                while (i.hasNext()) {
                    SATransitionO1 told = i.next();
                    if (sManager.subset(qsbr, told.getCollapseDest()) &&
                        sManager.subset(qs, told.getDest())) {
                        i.remove();
                    }
                }
            }
        } else {
            // TODO: something clever here?
        }

        // filter Tnew (this is hyper slow i guess)
        // commented out because only 14 hits in kobayashi popl 09 example
//        Iterator<SATransitionO1> i = order1Tnew.iterator();
//        while (i.hasNext()) {
//            SATransitionO1 tnew = i.next();
//            if (equal(tnew.getSource(), q) &&
//                scManager.equal(tnew.getLabellingCharacter(), a) &&
//                sManager.subset(qsbr, tnew.getCollapseDest()) &&
//                sManager.subset(qs, tnew.getDest())) {
//                i.remove();
//                logger.debug("hit!");
//            }
//        }

        order1Contents.put(q, a, qsbr, qs, t);
        if (trackNew)
            order1Tnew.addFirst(t);

        // check if not currently useless
//        Iterator<SAState> destIt = qs.iterator();
//        if (destIt.hasNext()) {
//            Set<SCharacter> goodChars = order1Contents.keySet(destIt.next());
//            while (destIt.hasNext() && !goodChars.isEmpty()) {
//                goodChars.retainAll(order1Contents.keySet(destIt.next()));
//            }
//            if (goodChars.isEmpty())
//                logger.debug("Adding currently useless transition: " + t);
//        }
    }

    /**
     * restricts the size of the given set to the current set bound
     *
     * @param qs the stateset
     * @return a subset of qs respecting the bound on set sizes
     */
    public ManagedSet<SAState> restrictSetSize(ManagedSet<SAState> qs) {
        if (maxSetSize == NO_SET_BOUND || qs.size() <= maxSetSize) {
            return qs;
        } else {
            //logger.debug("Restricting: " + qs);
            Set<SAState> tmpQs = new HashSet<SAState>(maxSetSize);
            Iterator<SAState> i = qs.iterator();
            while (i.hasNext() && tmpQs.size() < maxSetSize)
                tmpQs.add(i.next());
            return sManager.makeSet(tmpQs);
        }
    }

    private boolean consistencyCheckList(List<ManagedSet<SAState>> Qs) {
        for (ManagedSet<SAState> Q : Qs)
            if (!consistencyCheck(Q))
                return false;
	    return true;
    }
}

