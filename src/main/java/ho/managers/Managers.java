/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.managers;

import ho.fake.NaiveO1FakeManager;
import ho.fake.BasicFakeManager;
import ho.fake.FakeManager;
import ho.lazystates.EagerLazyStateManager;
import ho.lazystates.LazyStateManager;
import ho.structures.cpds.ControlStateManager;
import ho.structures.cpds.SCharacter;
import ho.structures.cpds.SCharacterManager;
import ho.structures.sa.SAManager;
import ho.structures.sa.SAState;
import ho.structures.sa.SATransitionO1;

import ho.util.SetManager;
import ho.util.HashSetManager;

/**
 * Various elements of our architecture use "managers", providing a universe of
 * objects that should be manipulated through the manager only.  This is a
 * static class providing easy access to the various managers, rather than
 * passing them around.
 */
public class Managers {
    /**
     * The global control state manager
     */
    public static ControlStateManager csManager = new ControlStateManager();

    /**
     * The global character manager
     */
    public static SCharacterManager scManager = new SCharacterManager();

    /**
     * The global set manager for SAStates
     */
    public static SetManager<SAState> sManager
        = new HashSetManager<SAState>();

    /**
     * The global set manager for stack characters
     */
    public static SetManager<SCharacter> scsetManager
	= new HashSetManager<SCharacter>();


    /**
     * The global set manager for O1-transitions
     */
    public static SetManager<SATransitionO1> satransitionO1Manager
	= new HashSetManager<SATransitionO1>();

    /**
     * The global stack automata manager
     */
    public static SAManager saManager = new SAManager();

    /**
     * The FakeProcessor
     */
    //public static FakeManager fakeManager = new BasicFakeManager(saManager);
    public static FakeManager fakeManager = new NaiveO1FakeManager(saManager);

    /**
     * The LazyStateManager
     */
    public static LazyStateManager lazyStateManager = new EagerLazyStateManager();

    /**
     * Resets all managers
     *
     * Note: resets saManager and fake manager to order-1, so call
     * reset/setOrder again to set the correct order, once known
     */
    public static void reset() {
        csManager.reset();
        scManager.reset();
        sManager.reset();
        saManager.reset((byte)1);
        fakeManager.reset(saManager);
        lazyStateManager.reset();
    }

    /**
     * Resets for another run of the algorithm: i.e. doesn't reset control state
     * stack character managers.
     *
     * Note: resets saManager and fake manager to order-1, so call
     * reset/setOrder again to set the correct order, once known
     */
    public static void algorithmReset() {
        sManager.reset();
        saManager.reset((byte)1);
        fakeManager.reset(saManager);
        lazyStateManager.reset();
    }

}
