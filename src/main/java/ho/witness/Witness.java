/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */




package ho.witness;

import java.util.Iterator;
import java.util.LinkedList;

import java.lang.StringBuilder;

import org.apache.log4j.Logger;

import ho.structures.cpds.Rule;

/**
 * Holds a trace from a given initial state to an error state (really just a
 * list of CPDS rules)
 */
public class Witness {

    static Logger logger = Logger.getLogger(Witness.class);

    LinkedList<Rule> rules = new LinkedList<Rule>();

    /**
     * Build an empty trace
     */
    public Witness() {
        // nothing to do
    }


    /**
     * Add a move to the end of the witness (no check for sanity)
     *
     * @param r the rule to add
     */
    public void appendMove(Rule r) {
        rules.add(r);
    }


    /**
     * @return iterator over the run
     */
    public Iterator<Rule> iterator() {
        return rules.iterator();
    }


    public boolean equals(Object o) {
        if (o instanceof Witness)
            return rules.equals(((Witness)o).rules);
        else
            return false;
    }


    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Rule rule : rules)
            sb.append(rule + "\n");
        return sb.toString();
    }
}
