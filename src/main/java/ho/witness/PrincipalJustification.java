/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.witness;

import ho.structures.cpds.Rule;
import ho.structures.sa.SAState;
import ho.structures.sa.SATransition;
import ho.structures.sa.SATransitionO1;
import ho.util.HashMemoiser1Arg;
import ho.util.ManagedSet;
import ho.util.Memoiser1Arg;

/**
 * Class for representing CPDS rule justifying creation of
 * an SATransition, together with SATransition/O1-transition that should
 * become principal transition when it is unwound.
 *
 */
public class PrincipalJustification {
	private Rule rule; // CPDS rule justifying

	// Note that exactly one of these next two fields should ever be non-null:
	private SATransition tran;
	private SATransitionO1 tranO1;

	private Memoiser1Arg<ManagedSet<SATransitionO1>, SATransitionO1>
				getTranO1FromSetMemo = new HashMemoiser1Arg<ManagedSet<SATransitionO1>, SATransitionO1>();

    public PrincipalJustification(Rule rule) {
        this.rule = rule;
    }

	public PrincipalJustification(Rule rule, SATransition tran) {
		this.rule = rule;
		this.tran = tran;
	}

	public PrincipalJustification(Rule rule, SATransitionO1 tranO1) {
		this.rule = rule;
		this.tranO1 = tranO1;
	}

	public Rule getRule()  {
		return rule;
	}

	public SATransition getTran() {
		return tran;
	}

	public SATransitionO1 getTranO1() {
		return tranO1;
	}

    public String toString() {
        return rule.toString();
    }

}
