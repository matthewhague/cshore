/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */




package ho.witness;

import java.util.LinkedList;

import org.apache.log4j.Logger;

import ho.util.ManagedSet;

import ho.managers.Managers;

import ho.structures.sa.SATransitionO1;
import ho.structures.sa.SAState;

import ho.structures.cpds.SCharacter;
import ho.structures.cpds.Rule;
import ho.structures.cpds.PushRule;
import ho.structures.cpds.PopRule;
import ho.structures.cpds.CollapseRule;
import ho.structures.cpds.RewRule;
import ho.structures.cpds.CPushRule;
import ho.structures.cpds.EvalNonTermRule;
import ho.structures.cpds.RewLinkRule;
import ho.structures.cpds.GetArgRule;
import ho.structures.cpds.GetOrder0ArgRule;
import ho.structures.cpds.GetArgRule;
import ho.structures.cpds.GetOrder0ArgRule;
import ho.structures.cpds.AltRule;


/**
 * Extracts a witness from a stack automaton with an accepting run.
 *
 * Currently only works for initial configurations of the form (p, [..[a]..]),
 * i.e. only a single stack character.
 */
public class WitnessExtractor {

    static Logger logger = Logger.getLogger(WitnessExtractor.class);

    LinkedList<Rule> witness = new LinkedList<Rule>();

    public WitnessExtractor() {
        // Nothing to do.
    }


    /**
     * Extract a counter example for a configuration (p, [..[a]..]) where t is
     * the single order-1 transition that implies the acceptance of the given
     * configuration, i.e.
     *
     *      (p, 0, ..., 0) --- a, 0 ---> 0
     *
     * @param order the order of the cpds for which a witness is to be extracted
     * @param initT the SA transition witnessing acceptance of the desired initial
     * configuration (with singleton stack)
     * @return a trace from (p, [..[a]..]) to the error state
     */
    public Witness extractWitness(byte order, SATransitionO1 initT) {
        Witness w = new Witness();

        ManagedSet<SATransitionO1> initTset
            = Managers.satransitionO1Manager.makeSingleton(initT);
        CpdsStack<ManagedSet<SATransitionO1>> runStack
            = CpdsStackOps.createSingleElementStack(order, initTset);

        SATransitionO1 t = initT;
        PrincipalJustification j = t.getPrincipalJustification();

        while (j != null && w != null) {
            Rule r = j.getRule();
            w.appendMove(r);
            byte k = r.getOrder();
            // i know, instance of...
            if (r instanceof PushRule) {
                ManagedSet<SATransitionO1> unwound = t.getUnwoundJustification();
                t = selectTransition(j.getTran().getLabellingState(),
                                     unwound);
                if (t != null) {
                    ManagedSet<SATransitionO1> justT
                        = Managers.satransitionO1Manager.makeSingleton(t);
                    runStack = CpdsStackOps.rew(unwound, runStack);
                    runStack = CpdsStackOps.push(k, runStack);
                    runStack = CpdsStackOps.rew(justT, runStack);
                }
            } else if (r instanceof PopRule) {
                runStack = CpdsStackOps.pop(r.getOrder(), runStack);
                SAState qk = t.getQk(k).choose();
                t = selectTransition(qk, CpdsStackOps.top(runStack));
            } else if (r instanceof CollapseRule) {
                runStack = CpdsStackOps.collapse(runStack);
                SAState qk = t.getCollapseDest().choose();
                t = selectTransition(qk, CpdsStackOps.top(runStack));
            } else if (r instanceof CPushRule) {
                ManagedSet<SATransitionO1> unwound = t.getUnwoundJustification();
                t = j.getTranO1();
                ManagedSet<SATransitionO1> justT
                    = Managers.satransitionO1Manager.makeSingleton(t);
                runStack = CpdsStackOps.rew(unwound, runStack);
                runStack = CpdsStackOps.cpush(justT, k, runStack);
            } else if (r instanceof EvalNonTermRule) {
                ManagedSet<SATransitionO1> unwound = t.getUnwoundJustification();
                t = j.getTranO1();
                ManagedSet<SATransitionO1> justT
                    = Managers.satransitionO1Manager.makeSingleton(t);
                runStack = CpdsStackOps.rew(unwound, runStack);
                runStack = CpdsStackOps.evalNT(justT, runStack);
            } else if (r instanceof RewRule) {
                t = j.getTranO1();
                ManagedSet<SATransitionO1> nextT
                    = Managers.satransitionO1Manager.makeSingleton(t);
                runStack = CpdsStackOps.rew(nextT, runStack);
            } else if (r instanceof GetOrder0ArgRule) {
                t = j.getTranO1();
                ManagedSet<SATransitionO1> nextT
                    = Managers.satransitionO1Manager.makeSingleton(t);
                runStack = CpdsStackOps.getOrder0Arg(nextT, runStack);
            } else if (r instanceof RewLinkRule) {
                t = j.getTranO1();
                ManagedSet<SATransitionO1> nextT
                    = Managers.satransitionO1Manager.makeSingleton(t);
                runStack = CpdsStackOps.rewLink(nextT, k, runStack);
            } else if (r instanceof GetArgRule) {
                t = j.getTranO1();
                ManagedSet<SATransitionO1> nextT
                    = Managers.satransitionO1Manager.makeSingleton(t);
                runStack = CpdsStackOps.getArg(nextT, k, runStack);
            } else if (r instanceof AltRule) {
                logger.error("Don't know how to build an alternating witness just yet.");
                System.exit(-1);
            } else {
                logger.error("Unrecognised rule type " +
                             "in WitnessExtractor.extractWitness " + r);
                System.exit(-1);
            }

            j = (t != null) ?  t.getPrincipalJustification() : null;
        }

        return w;
    }


    /**
     * @param qmatch a state (p, Qk+1, ..., Qn)
     * @param ts a selection of order-1 transitions
     * @return t from ts such that t = (p, Q2, ..., Qn) --- a, Qbr ---> Q1 for
     *           some Q1...Qk (Qk+1...Qn come from qmatch)
     *           null if none found
     */
    private SATransitionO1 selectTransition(SAState qmatch,
                                            ManagedSet<SATransitionO1> ts) {

        if (qmatch == null) {
            return null;
        }
        byte k = qmatch.getStateOrder();
        for (SATransitionO1 t : ts) {
            if (Managers.saManager.equal(qmatch, t.getSourceK(k))) {
                return t;
            }
        }
        return null;
    }


}
