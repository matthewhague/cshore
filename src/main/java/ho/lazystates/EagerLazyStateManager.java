/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.lazystates;

import ho.managers.Managers;
import ho.structures.cpds.ControlState;
import ho.structures.sa.SAState;
import ho.util.ManagedSet;

/**
 * A non-lazy implementation of lazystates (just forwards to create actual states).
 *
 */
public class EagerLazyStateManager implements LazyStateManager {

	public LazyState getAddLazyState(SAState q, ControlState p) {

		return (LazyState) Managers.saManager.switchControlStateAdd(q, p);
	}

	public LazyStateO1 getAddLazyStateO1(SAState q, ControlState p, Byte k,
			ManagedSet<SAState> qbr) {

		return (LazyStateO1) Managers.saManager.switchControlStateUnionOrdkAdd(q, p, qbr, k);
	}

    public final void reset() { }

    public boolean equal(LazyStateO1 l1, LazyStateO1 l2) {
        return Managers.saManager.equal((SAState)l1, (SAState)l2);
    }
}
