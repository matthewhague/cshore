/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.lazystates;

import ho.structures.cpds.SCharacter;
import ho.structures.sa.SAState;
import ho.structures.sa.SATransitionO1;
import ho.util.ManagedSet;
import ho.witness.PrincipalJustification;


/**
 * A LazyStateO1 represents an SAState of order = 1 and transition together with possibly several
 * other dependent transitions and states that have not yet been created. These should be
 * managed objects and can be given instructions to create new LazyStates (which should be
 * forwarded to their manager). A LazyStateO1 can be given an instruction (to be forwarded
 * to the manager) that actually creates all of the dependent states.
 *
 */
public interface LazyStateO1 {

	/**
	 * Should create an order-1 transition from the order-1 state represented
	 * by this LazyStateO1, together with all dependent states/transitions at
	 * higher-orders
	 * @param sChar			Stack character for the transition
	 * @param collapseDest	Collapse labelling states for the transition
	 * @param dest			Order-1 destinations states for the transition
	 * @param justif		Rule and SATransition t justifying addition of O1-transition about
	 *						to be completed
	 * @param unwoundTransitions	Set of O1-Transitions that justifies transition when
	 *								justified by a push-rule. If not justified by a push-rule then null.
	 *						(Should only ever be non-null when called by fake-stuff).
	 *
	 */
	public void complete ( SCharacter sChar, ManagedSet<SAState> collapseDest,
			ManagedSet<SAState> dest, PrincipalJustification princJustif, ManagedSet<SATransitionO1> unwoundTransitions);

}
