/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.lazystates;

import ho.structures.sa.SAState;
import ho.util.ManagedSet;

/**
 * A LazyState represents an SAState of order > 1 and transition together with possibly several
 * other dependent transitions and states that have not yet been created. These should be
 * managed objects and can be given instructions to create new LazyStates (which should be
 * forwarded to their manager).
 *
 */
public interface LazyState {

	/**
	 * This should only be defined for states of order > 2
	 * @param dest	If this LazyState represents a state q and dest=Q,
	 *				then we should create the lazyState q_Q for the transition
	 *				q---->Q. We assume order of states in dest is equal to order of q
	 *				and that this order > 2
	 * @return		The newly created LazyState
	 */
	public LazyState nextLazyState ( ManagedSet<SAState> dest );

	/**
	 * Similar to nextLazyState, but for when we are representing a
	 * state of order 2. In this case we should return an order-1 state
	 * as the next.
	 *
	 */
	public LazyStateO1 nextLazyStateO2 ( ManagedSet<SAState> dest );

	/**
	 *
	 * @return	The order of the state.
	 */
	public byte getStateOrder();
}
