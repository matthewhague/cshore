/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.lazystates;

import ho.structures.cpds.ControlState;
import ho.structures.sa.SAState;
import ho.util.ManagedSet;

public interface LazyStateManager {
	/**
	 *
	 * @param q Existing SAState (q', Q_n....Q_k)
	 * @param p A Control-state
	 * @return  Lazy state suspending creation of (p, Q_n....Q_k)
	 */
	LazyState getAddLazyState( SAState q , ControlState p);

	/**
	 *
	 * @param q  Existing SAStateO1 (q', Q_n..Q_k..Q_1)
	 * @param p	A control-state
	 * @param k		An order
	 * @param qbr	A set of order-k states
	 * @return	Lazy state suspending creation of (q', Q_n..,Q_k + Qbr,..Q_1)
	 */
	LazyStateO1 getAddLazyStateO1( SAState q , ControlState p, Byte k, ManagedSet<SAState> qbr);

    /**
     * @param l1 a lazy state (order-1)
     * @param l2 a lazy state (order-1)
     * @return true iff l1 is equal to l2
     */
    boolean equal(LazyStateO1 l1, LazyStateO1 l2);

    /**
     * Resets the manager
     */
    void reset();
}
