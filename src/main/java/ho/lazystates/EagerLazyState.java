/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.lazystates;

import ho.managers.Managers;
import ho.structures.cpds.SCharacter;
import ho.structures.sa.SAState;
import ho.structures.sa.SATransition;
import ho.structures.sa.SATransitionO1;
import ho.util.ManagedSet;
import ho.witness.PrincipalJustification;

/**
 * A temporary class for creating Lazy States eagerly before we implement
 * some kind of manager.
 *
 * This should be used as base class for classes implementing actual states.
 *
 */
public abstract class EagerLazyState implements LazyState, LazyStateO1, SAState {


	public LazyState nextLazyState(ManagedSet<SAState> dest) {
		assert(this.getStateOrder() > 2) : "nextLazyState should not be called for order 2 or below";
		SATransition newTransition = Managers.saManager.getAddTransition(this, dest);
		return (LazyState) newTransition.getLabellingState();
	}

	public LazyStateO1 nextLazyStateO2(ManagedSet<SAState> dest) {
		assert(this.getStateOrder() == 2) : "nextLazyStateO2 should only be called for exactly order-2";
		SATransition newTransition = Managers.saManager.getAddTransition(this, dest);
		return (LazyStateO1) newTransition.getLabellingState();
	}


	public void complete(SCharacter sChar, ManagedSet<SAState> collapseDest,
			ManagedSet<SAState> dest, PrincipalJustification justif, ManagedSet<SATransitionO1> T) {
		assert(this.getStateOrder() == 1) : "can only complete order-1 states";
		Managers.saManager.addTransitionO1((SAState) this, sChar, collapseDest, dest, justif, T);
		return;
	}

	public abstract byte getStateOrder();




}
