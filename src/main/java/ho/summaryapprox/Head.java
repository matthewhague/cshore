/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.summaryapprox;

import java.util.LinkedList;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import ho.managers.Managers;
import ho.util.ImmutableSet;
import ho.util.Pair;
import ho.structures.cpds.CPushRule;
import ho.structures.cpds.EvalNonTermRule;
import ho.structures.cpds.CollapseRule;
import ho.structures.cpds.ControlState;
import ho.structures.cpds.Cpds;
import ho.structures.cpds.DestructiveRule;
import ho.structures.cpds.PopRule;
import ho.structures.cpds.PushRule;
import ho.structures.cpds.RewLinkRule;
import ho.structures.cpds.RewRule;
import ho.structures.cpds.GetOrder0ArgRule;
import ho.structures.cpds.GetArgRule;
import ho.structures.cpds.AltRule;
import ho.structures.cpds.StackHead;
import ho.structures.cpds.Rule;
import ho.structures.cpds.SCharacter;

/**
 *	Class for representing a node in the approximate control-flow graph. Contains
 *  the control-state and top stack character, which is its defining feature. Also
 *  maintains information about possible stack characters below this head and at
 *  the target of the collapse link.
 *
 *
 */
class Head {
	static Logger logger = Logger.getLogger(Head.class);

	private HeadManager manager; // Manager to use for heads.

	/*
	 *  Maximum order of the CPDS being approximated.
	 */
	private byte order;

    /*
     * hash code
     */
    private int hashCode;

	/*
	 *	Defining features... top stack character and control-state of all stacks
	 *  over-approximated by this hear.
	 */

	private SCharacter top;
	private ControlState cs;

	private byte collapseOrder; // Should be uniquely determined by top, but currently
										// it is not extractable from top... but we make this uniqueness
										// assumption nonetheles...
										// order of the link (pointing to order-(collapseOrder - 1) stack)


	/*
	 * Flag set to true when visited on backwards reachability computation from error state.
	 */

	private boolean visited = false;

	/*
	 * Keep track of context in which top-most order-i component of stack might have been
	 * created. (Reverse `summary' edges).
	 *
	 */


	private ImmutableSet<StackBelowApprox>[] origins; // origins[i] should contain StackBelowApprox of
													//order-i describing
													// where to find the result of a pop_j for j <= i on
													// top_{i+1}
													// stack. origins[0] describes result of collapse.
													// Thus array has length order + 1 (max index [order])
													// THese will be supplied to summary edges.





	private Map<Head, ImmutableSet<StackBelowApprox>>[] holesToBeFilledFromSummary;
								// When we create an order-k summary edge to this from a head h,
								// we put at index [k] in image of h under map,
								// the contexts a_n----a_k+1 (with hole at order-k)
								// that we want filled from h.origins[k].






	/*
	 *
	 * This is akin to `summary edges'.
	 *
	 * Summaries[i] contains those heads h that wish to be informed about any new elements
	 * in origins[i] because they share the `top_i stack' with this. Currently origins[0]
	 * not used, but previously have thought about using to update links on rewrites... but
	 * rewrite origins are propagated anyway via rules...
	 *
	 */

	private ImmutableSet<Head>[] summaries;


	/*
	 * Keeps track of possible *incoming* rules to this head.
	 */

	private ImmutableSet<Rule> inRules;

	private ImmutableSet<Head> backEdges; // Heads that might lead into this head via a transition.


	/*
	 * Keeps track of outgoing operations... necessary to generate
	 * new heads when become away of new incoming summary.
	 */
	boolean guaranteedError = false; // set to true if always-possible operation goes to error state
									// in which case disregard all other operations.

	private ImmutableSet<Rule>[] popRules; // pop rules *from* this head with index + 1 being order of pop
									// (so order-i pops contained in popRules[i-1])
	private ImmutableSet<Rule> collapseRules; // Collapse rules *from* this head, with order assumed to
										// be equal to collapse Order.
	private ImmutableSet<Rule> rewRules;
	private ImmutableSet<Rule>[] rewLinkRules;
	private ImmutableSet<Rule>[] cPushRules;
	private ImmutableSet<Rule> evalNonTermRules;
	private ImmutableSet<Rule>[] pushRules;
    private ImmutableSet<Rule>[] getArgRules;
    private ImmutableSet<Rule> getOrder0ArgRules;
    private ImmutableSet<AltRule> altRules;






	/**
	 * @param manager		HeadManager to use
	 * @param order			Order of Cpds being approximated
	 * @param cs			Control-state of configuration to represent
	 * @param top			Top stack symbol of configuration to represent
	 * @param collapseOrder	Order of collapse link (should depend on top... need to adjust code to
	 *												make derivable directly from top.).
	 */
	@SuppressWarnings("unchecked")
	Head(HeadManager manager, byte order,  ControlState cs, SCharacter top, byte collapseOrder) {
		this.manager = manager;
		this.order = order;
		this.top = top;
		this.cs = cs;
		this.collapseOrder = collapseOrder;

		this.inRules = ImmutableSet.<Rule>makeEmpty();
		this.backEdges = ImmutableSet.<Head>makeEmpty();

		this.origins = (ImmutableSet<StackBelowApprox>[]) new ImmutableSet[order + 1];
		this.holesToBeFilledFromSummary = new HashMap[order + 1];

		this.summaries = (ImmutableSet<Head>[]) new ImmutableSet[order + 1];

		this.popRules = new ImmutableSet[order];
		this.collapseRules = ImmutableSet.<Rule>makeEmpty();
		this.rewRules = ImmutableSet.<Rule>makeEmpty();
		this.rewLinkRules = new ImmutableSet[order];
		this.cPushRules = new ImmutableSet[order];
		this.evalNonTermRules = ImmutableSet.<Rule>makeEmpty();
		this.pushRules = new ImmutableSet[order];
        this.getArgRules = new ImmutableSet[order];
        this.getOrder0ArgRules = ImmutableSet.<Rule>makeEmpty();
        this.altRules = ImmutableSet.<AltRule>makeEmpty();


		for(byte i = 0; i < order; i++) {
			this.popRules[i] = ImmutableSet.<Rule>makeEmpty();
			this.rewLinkRules[i] = ImmutableSet.<Rule>makeEmpty();
			this.cPushRules[i] = ImmutableSet.<Rule>makeEmpty();
			this.pushRules[i] = ImmutableSet.<Rule>makeEmpty();
			this.getArgRules[i] = ImmutableSet.<Rule>makeEmpty();
		}

		for(byte i = 0; i <= order; i++) {
			this.summaries[i] = ImmutableSet.<Head>makeEmpty();
			this.origins[i] = ImmutableSet.<StackBelowApprox>makeEmpty();
			this.holesToBeFilledFromSummary[i] = new HashMap<Head, ImmutableSet<StackBelowApprox>>();

		}

        this.hashCode = computeHashCode();
	}


	// Resets all of rule sets to be empty.
	private void emptyRuleSets() {
		this.collapseRules = ImmutableSet.<Rule>makeEmpty();

		this.rewRules = ImmutableSet.<Rule>makeEmpty();
		this.evalNonTermRules = ImmutableSet.<Rule>makeEmpty();
        this.getOrder0ArgRules = ImmutableSet.<Rule>makeEmpty();

		for(byte i = 0; i < order; i++) {
			this.popRules[i] = ImmutableSet.<Rule>makeEmpty();
			this.rewLinkRules[i] = ImmutableSet.<Rule>makeEmpty();
			this.cPushRules[i] = ImmutableSet.<Rule>makeEmpty();
			this.pushRules[i] = ImmutableSet.<Rule>makeEmpty();
			this.getArgRules[i] = ImmutableSet.<Rule>makeEmpty();
		}
	}



	/**
	 *
	 * Makes a head "initial" by adding empty StackBelowApprox to it.
	 * This should trigger updates and start generating the approximate reachability graph.
	 *
	 */
	void makeInitial() {
		this.addOrigin(manager.getEmptyStackBelowApprox());
	}




	/*
	 *
	 * Methods for adding rules *from* this head. Ensures that if any rule
	 * with guaranteed-to-be-possible stack operation goes to error, then
	 * this is only rule coming from here.
	 *
	 */


	void addFromRule(PushRule rule) {
		if(!this.guaranteedError) {
			if(rule.getControl2() == Managers.csManager.makeControl(Cpds.error_state)) {
				this.guaranteedError = true;
				this.emptyRuleSets();
			}
			this.pushRules[rule.getOrder() -1]
					= this.pushRules[rule.getOrder() -1].add(rule);
		}
	}

	void addFromRule(CPushRule rule) {
		if(!this.guaranteedError) {
			if(rule.getControl2() == Managers.csManager.makeControl(Cpds.error_state)) {
				this.guaranteedError = true;
				this.emptyRuleSets();
			}
			this.cPushRules[rule.getOrder() -1]
					= this.cPushRules[rule.getOrder() -1].add(rule);
		}
	}

	void addFromRule(EvalNonTermRule rule) {
		if(!this.guaranteedError) {
			if(rule.getControl2() == Managers.csManager.makeControl(Cpds.error_state)) {
				this.guaranteedError = true;
				this.emptyRuleSets();
			}
			this.evalNonTermRules
					= this.evalNonTermRules.add(rule);
		}
	}



	void addFromRule(RewRule rule) {
		if(!this.guaranteedError) {
			if(rule.getControl2() == Managers.csManager.makeControl(Cpds.error_state)) {
				this.guaranteedError = true;
				this.emptyRuleSets();
			}
			this.rewRules = this.rewRules.add(rule);
		}
	}

	// Note that RewLinkRule not guaranteed to be usable as cannot be used if nothing below.
	void addFromRule(RewLinkRule rule) {
		if(!this.guaranteedError) {
			this.rewLinkRules[rule.getOrder() - 1]
					= this.rewLinkRules[rule.getOrder() - 1].add(rule);
		}
	}

	// Note that GetArgRule not guaranteed to be usable as cannot be used if nothing below.
	void addFromRule(GetArgRule rule) {
		if(!this.guaranteedError) {
			this.getArgRules[rule.getOrder() - 1]
					= this.getArgRules[rule.getOrder() - 1].add(rule);
		}
	}

	void addFromRule(GetOrder0ArgRule rule) {
		if(!this.guaranteedError) {
			if(rule.getControl2() == Managers.csManager.makeControl(Cpds.error_state)) {
				this.guaranteedError = true;
				this.emptyRuleSets();
			}
			this.getOrder0ArgRules = this.getOrder0ArgRules.add(rule);
		}
	}


	void addFromRule(PopRule rule) {
		if(!this.guaranteedError) {
			this.popRules[rule.getOrder() - 1]
					= this.popRules[rule.getOrder() - 1].add(rule);
		}
	}

	void addFromRule(CollapseRule rule) {
		if(!this.guaranteedError) {
			this.collapseRules = this.collapseRules.add(rule);
		}
	}

	void addFromRule(AltRule rule) {
		if(!this.guaranteedError) {
            this.altRules = this.altRules.add(rule);
		}
	}


	/*
	 *	Methods for updating heads reachable via rules starting at this head
	 *  in the light of a newOrigin describing new possible stack contents at this head.
	 */

	void makePops(StackBelowApprox newOrigin, byte order) {
		Head popTargetStackSource = newOrigin.getBelowAtOrder(order);

		if(popTargetStackSource != null) {
			// The newOrigin makes such a pop possible.
			StackBelowApprox socket = newOrigin.makeHole(order);

			for(Rule popRule : this.popRules[order -1 ]) {
				assert(popRule.getOrder() == order);
				Head popTarget = manager.getAddHead(popRule.getControl2(), popTargetStackSource.getTop(),
											popTargetStackSource.getCollapseOrder());
				popTarget.incomingDestruction(popTargetStackSource, socket, order);
				popTarget.addInRule(popRule);
				popTarget.addInEdgeFrom(this);
			}
		}
	}

	/**
	 *
	 * @param head		head to which there is a back-edge (from which there is an edge into this head).
	 */
	private void addInEdgeFrom(Head head) {
		this.backEdges = this.backEdges.add(head);

	}


	void makeCollapses(StackBelowApprox newOrigin){
		Head colTargetStackSource = newOrigin.getBelowAtOrder((byte) 0);

		if(colTargetStackSource != null) {
			// The newOrigin makes such a collapse possible.
			StackBelowApprox socket = newOrigin.makeHole(this.collapseOrder);

			for(Rule colRule : this.collapseRules) {
				assert(colRule.getOrder() == this.collapseOrder);
				Head colTarget = manager.getAddHead(colRule.getControl2(), colTargetStackSource.getTop(),
											colTargetStackSource.getCollapseOrder());
				colTarget.incomingDestruction(colTargetStackSource, socket, this.collapseOrder);

				colTarget.addInRule(colRule);
				colTarget.addInEdgeFrom(this);
			}
		}
	}

	void makePushes(StackBelowApprox newOrigin, byte order) {
		for(Rule pushRule : this.pushRules[order - 1]) {
			assert(pushRule.getOrder() == order);
			Head pushTarget = manager.getAddHead(pushRule.getControl2(), this.top, this.collapseOrder);
			pushTarget.addOrigin(newOrigin.changeBelowApprox(order, this));

			pushTarget.addInRule(pushRule);
			pushTarget.addInEdgeFrom(this);
		}
	}

	void makeCPushes(StackBelowApprox newOrigin, byte order) {
		for(Rule cPushRule : this.cPushRules[order-1]) {
			assert(cPushRule.getOrder() == order);
			Head cPushTarget = manager.getAddHead(cPushRule.getControl2(),
						((CPushRule) cPushRule).getPushCharacter(), order);

			StackBelowApprox originToSend
				= newOrigin.changeBelowApprox((byte) 0, newOrigin.getBelowAtOrder(order));
																			// Add target of link
																			// as specified by newOrigin.
			originToSend = originToSend.changeBelowApprox((byte) 1, this);
									// set this head to be the result of a pop_1.


			cPushTarget.addOrigin(originToSend);

			cPushTarget.addInRule(cPushRule);
			cPushTarget.addInEdgeFrom(this);
		}
	}

	void makeEvalNonTerms(StackBelowApprox newOrigin) {
		for(Rule evalNonTermRule: this.evalNonTermRules) {
			Head evalNonTermTarget
                = manager.getAddHead(evalNonTermRule.getControl2(),
						             ((EvalNonTermRule) evalNonTermRule).getPushCharacter(),
                                     (byte)1);

			StackBelowApprox originToSend
				= newOrigin.changeBelowApprox((byte) 0, newOrigin.getBelowAtOrder((byte)1));
																			// Add target of link
																			// as specified by newOrigin.
			originToSend = originToSend.changeBelowApprox((byte) 1, this);
									// set this head to be the result of a pop_1.


			evalNonTermTarget.addOrigin(originToSend);

			evalNonTermTarget.addInRule(evalNonTermRule);
			evalNonTermTarget.addInEdgeFrom(this);
		}
	}


	void makeRewLinks(StackBelowApprox newOrigin, byte order) {
		for(Rule rewLinkRule : this.rewLinkRules[order-1]) {
			assert(rewLinkRule.getOrder() == order);
			Head rewLinkTarget = manager.getAddHead(rewLinkRule.getControl2(),
						((RewLinkRule) rewLinkRule).getRewCharacter(), order);

			StackBelowApprox originToSend
				= newOrigin.changeBelowApprox((byte) 0, newOrigin.getBelowAtOrder(order));
																			// Add target of link
																			// as specified by newOrigin.


			rewLinkTarget.addOrigin(originToSend);

			rewLinkTarget.addInRule(rewLinkRule);
			rewLinkTarget.addInEdgeFrom(this);
		}
	}

	void makeGetArgs(StackBelowApprox newOrigin, byte order) {
		for(Rule getArgRule : this.getArgRules[order-1]) {
			assert(getArgRule.getOrder() == order);
			Head getArgTarget = manager.getAddHead(getArgRule.getControl2(),
						((GetArgRule) getArgRule).getRewCharacter(), order);

			StackBelowApprox originToSend = manager.getEmptyStackBelowApprox();
            originToSend = originToSend.changeBelowApprox((byte)0,
                                                          newOrigin.getBelowAtOrder(order));
            originToSend = originToSend.changeBelowApprox((byte)1,
                                                          newOrigin.getBelowAtOrder((byte)1));

			getArgTarget.addOrigin(originToSend);

			getArgTarget.addInRule(getArgRule);
			getArgTarget.addInEdgeFrom(this);
		}
	}


	void makeRew(StackBelowApprox newOrigin) {
		for(Rule rewRule : this.rewRules) {
			Head rewTarget = manager.getAddHead(rewRule.getControl2(),
						((RewRule) rewRule).getRewCharacter(), this.collapseOrder);
			//TODO:				//WARNING: Actually this violates unicity of collapse-order
							// in CPDS produced by HORS (but doesn't matter as no collapse ever performed
							// from state to which rewritten)... but we should change CPDS semantics..



			rewTarget.addOrigin(newOrigin); // Stuff below stack doesn't change... (although
						//TODO:		// if we eventually change link situation, then we should change order-0
									// belowStack to null.

			rewTarget.addInRule(rewRule);
			rewTarget.addInEdgeFrom(this);
		}
	}

    void makeGetOrder0Arg(StackBelowApprox newOrigin) {
		for(Rule getOrder0ArgRule : this.getOrder0ArgRules) {
			Head getOrder0ArgTarget = manager.getAddHead(getOrder0ArgRule.getControl2(),
						((GetOrder0ArgRule) getOrder0ArgRule).getRewCharacter(), this.collapseOrder);
			//TODO:				//WARNING: Actually this violates unicity of collapse-order
							// in CPDS produced by HORS (but doesn't matter as no collapse ever performed
							// from state to which rewritten)... but we should change CPDS semantics..



			getOrder0ArgTarget.addOrigin(newOrigin); // Stuff below stack doesn't change... (although
						//TODO:		// if we eventually change link situation, then we should change order-0
									// belowStack to null.

			getOrder0ArgTarget.addInRule(getOrder0ArgRule);
			getOrder0ArgTarget.addInEdgeFrom(this);
		}
	}

    void makeAlt(StackBelowApprox newOrigin) {
		for(AltRule altRule : this.altRules) {
            for (StackHead h : altRule.getDestinations()) {
                Head altTarget = manager.getAddHead(h.getControl(),
                                                    h.getCharacter(),
                                                    this.collapseOrder);
                //TODO:				//WARNING: Actually this violates unicity of collapse-order
                                // in CPDS produced by HORS (but doesn't matter as no collapse ever performed
                                // from state to which rewritten)... but we should change CPDS semantics..


                altTarget.addOrigin(newOrigin); // Stuff below stack doesn't change... (although
                            //TODO:		// if we eventually change link situation, then we should change order-0
                                        // belowStack to null.

                altTarget.addInRule(altRule);
                altTarget.addInEdgeFrom(this);
            }
		}
	}


	/**
	 * Called when collapse_k or pop_k operation is performed leading into this head.
	 *
	 * @param summarySource			Where the order-order StackBelowApprox to form the new stack description
	 *								should come from (and where summary edge should come from to perform
	 *								future updates).
	 * @param socket				StackBelowApprox with hole at order-order (a_n--... ---a_order+1---null)
	 *								into which origins[order] should be plugged. If order==n, then just null
	 * @param order					Order of pop or collapse being performed that leads to this.
	 */
	private void incomingDestruction(Head summarySource, StackBelowApprox socket, byte order) {
		assert(order == this.order && socket == null || order < this.order && socket!= null);

		if(this.order == order) {
			if(!summarySource.summaries[order].contains(this)) {
				// No sockets to plug into as outer-most level. We have checked not already summary
				// edge, so update as appropriate.
				summarySource.addSummaryEdgeTo(this, order);
				for(StackBelowApprox fromSummaryOrigin : summarySource.getOrigins(order))
					this.addOrigin(fromSummaryOrigin);
			}
		}
		else {

			ImmutableSet<StackBelowApprox> relevantHoles
				= this.holesToBeFilledFromSummary[order].get(summarySource);

			if(relevantHoles == null){
				relevantHoles = ImmutableSet.<StackBelowApprox>makeEmpty();
				this.holesToBeFilledFromSummary[order].put(summarySource, relevantHoles);
				summarySource.addSummaryEdgeTo(this, order);
			}

			if(!relevantHoles.contains(socket)) {
				this.holesToBeFilledFromSummary[order].put(summarySource,
                                                           relevantHoles.add(socket));
				for(StackBelowApprox plug : summarySource.getOrigins(order))
					this.addOrigin(socket.splice(plug));

			}
		}
	}

	/**
	 * Called by a summarySource on target of summary edge when new StackBelowApprox of given order
	 * is created.
	 *
	 * @param summarySource		The caller (source of summary edge)
	 * @param order				Order of the summary edge (and the new StackBelowApprox)
	 * @param plug				The new StackBelowApprox to be "plugged into" sockets of target.
	 */
	private void updateFromSummary(Head summarySource, byte order, StackBelowApprox plug) {
		assert(plug.getOrder() == order);

		if(order == this.order) {
			this.addOrigin(plug); // At outer-most level newOrigin is just a complete origin to add.
		} else {
            ImmutableSet<StackBelowApprox> sockets
                = this.holesToBeFilledFromSummary[order].get(summarySource);
			for(StackBelowApprox socket : sockets)
				// shouldn't be null-pointer exception in above as would only have summary
				// edge if summarySource has something in image.
				this.addOrigin(socket.splice(plug));
        }
	}




	/**
	 *
	 * @param target		Target of summary edge.
	 * @param order		Order of summary edge
	 */
	private void addSummaryEdgeTo(Head target, byte order) {
			this.summaries[order] = this.summaries[order].add(target);
	}


	/**
	 * Adds an origin, updating all relevant sets and updating summaries and refreshing
	 * rules. // TODO: Find a way to be more efficient in way that rules are refreshed.
	 *
	 * Note that in HORS CPDA  we have only one out-going push/pop/collapse operation
	 * and so may as well rely on it checking whether it needs to be supplied with newOrigin
	 * rather than prejudging that... however if multiple outgoing rules, to avoid multiple checks
	 * might want some sort of check here (e.g. by storing sockets previously used).
	 *
	 * @param newOrigin	new origin of order-order (outer-most)
	 */
	private void processOrigin(StackBelowApprox newOrigin) {
		assert(newOrigin.getOrder() == this.order);
		boolean updateSuccessors = false; // Set to true if know that new.

		//Memoisation in StackBelowApprox should make this not too bad...
		for(byte i = order; i >= 0; i--) {
			StackBelowApprox prefix = newOrigin.getStrictPrefix((byte)(i +1)); //strict prefix so add 1.
			if(!this.origins[i].contains(prefix)) {
				updateSuccessors = true;
				this.origins[i] = this.origins[i].add(prefix);
				// Update summaries:
				for(Head summaryTarget : this.summaries[i])
					summaryTarget.updateFromSummary(this, i, prefix);
			}
		}

		if(updateSuccessors) {
			this.makeCollapses(newOrigin);
			for(byte i = 1; i <= order; i++) {
				this.makePops(newOrigin, i);
				this.makePushes(newOrigin, i);
				this.makeCPushes(newOrigin, i);
				this.makeRewLinks(newOrigin, i);
				this.makeGetArgs(newOrigin, i);
			}
            this.makeEvalNonTerms(newOrigin);
			this.makeRew(newOrigin);
            this.makeGetOrder0Arg(newOrigin);
            this.makeAlt(newOrigin);
		}

	}

    static private LinkedList<Pair<Head,StackBelowApprox>> originWorklist
        = new LinkedList<Pair<Head,StackBelowApprox>>();
    static boolean processingOriginWorklist = false;
    void addOrigin(StackBelowApprox origin) {
        if (!processingOriginWorklist) {
            processingOriginWorklist = true;
            this.processOrigin(origin);
            while (!originWorklist.isEmpty()) {
                Pair<Head, StackBelowApprox> headOrigin = originWorklist.pop();
                headOrigin.first().processOrigin(headOrigin.second());
            }
            processingOriginWorklist = false;
        } else {
            originWorklist.push(new Pair<Head, StackBelowApprox>(this, origin));
        }
    }


	private final ImmutableSet<StackBelowApprox> getOrigins(byte order) {
		return this.origins[order];
	}

	/**
	 *
	 * @param rule	Rule to record as an incoming rule to this node.
	 */
	private void addInRule(Rule rule) {
		this.inRules = this.inRules.add(rule);

	}



	/**
	 *	Set visited flag to true (to mark when visited during backwards reachability).
	 */
	private void markAsVisited(){
		this.visited = true;
	}

	/**
	 *
	 * @return	The status of the "visited flag" (set to true when visited during backwards
	 *			reachability).
	 */
	private boolean isVisited() {
		return this.visited;
	}


	/**
	 * Do backwards reachability in constructed graph from this node
	 * along backEdges. For each destructive rule should also update set of
	 * stack characters representing stack characters that might see after performing
	 * whilst remaining on path to an error state.
	 *
	 * @return	HashSet of rules that may have been used to reach error state.
	 *
	 */
	ImmutableSet<Rule> getRestrictedRules() {
		this.markAsVisited();

		// Update annotation of rules coming in here.
		for(Rule rule : this.inRules)
			if(rule instanceof DestructiveRule)
				((DestructiveRule) rule).addAfterSChar(this.top);


		ImmutableSet<Rule> result = this.inRules;



		for(Head backEdge : this.backEdges)
			if (!backEdge.isVisited()) {
				result = result.union(backEdge.getRestrictedRules());
			}
		return result;
	}

	SCharacter getTop() {
		return this.top;
	}

	byte getCollapseOrder() {
		return this.collapseOrder;
	}



	private int computeHashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cs == null) ? 0 : cs.hashCode());
		result = prime * result + ((top == null) ? 0 : top.hashCode());
		return result;
	}

    @Override
    public int hashCode() {
        return hashCode;
    }


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Head other = (Head) obj;
		if (cs == null) {
			if (other.cs != null)
				return false;
		} else if (!cs.equals(other.cs))
			return false;
		if (top == null) {
			if (other.top != null)
				return false;
		} else if (!top.equals(other.top))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Head [top=" + top + ", cs=" + cs + ", inRules=" + inRules
				+  "]";
	}



}
