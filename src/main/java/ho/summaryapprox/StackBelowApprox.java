/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.summaryapprox;

import java.util.Stack;

import ho.structures.cpds.SCharacter;
import ho.util.HashMemoiser1Arg;
import ho.util.Memoiser1Arg;
import ho.util.Pair;
import ho.util.Triple;

/**
 * Class used to specify possible orders below.
 *
 * Forms natural chain induced by "next" (a_n---...---a_k---...---a_0)
 *
 */
class StackBelowApprox {
	private byte order;			// Order of stack being considered.


	private Head belowAtOrder; // Head which may have stack whose top_{order} order-(order -1) stack is
							// possibly the order-(order -1) stack below top-most order-(order -1)
							// stack on present stack.
							// If order = 0, then head which may have annotation.



	private StackBelowApprox next; // StackBelow approx for order -1 (or null if order = 0).
									// Can also be null when this is considered to have a "hole"
									// in it (a "context").


	private HeadManager manager; // Manager to use.

	private StackBelowApprox[] holeMemoiser; // Memoises creation of holes at each order.
	private StackBelowApprox[] prefixMemoiser; //Memoises the prefixes at each order.

	private Head[] belowAtOrderMemoiser;

	private Memoiser1Arg<StackBelowApprox, StackBelowApprox> spliceMemoiser
				= new HashMemoiser1Arg<StackBelowApprox, StackBelowApprox>();
					// For memoising splicing.

	private Memoiser1Arg<Head, StackBelowApprox>[] changeBelowApproxMemoiser;
				// Memoiser for changeBelowApprox... first argument (a byte)
				// is index of array.


    private int hashCode;

	/**
	 *
	 * @param manager		HeadManager to manage unicity
	 * @param order				Order of stack being considered (order-0 for atomic element).
	 * @param belowAtOrder    Head which may have stack whose top_{order} order-(order -1) stack is
							possibly the order-(order -1) stack below top-most order-(order -1)stack on
							present stack.
							 If order = 0, then head which may have annotation.
	 *
	 * @param next			StackBelow approx for order -1 (or null if order = 0).
	 */
	@SuppressWarnings("unchecked")
	StackBelowApprox(HeadManager manager, byte order, Head belowAtOrder, StackBelowApprox next) {
		assert(order > 0 || (order == 0 && next == null)) : "Can only have null belowAtOrder at order-0" +
				", at order-0 next must be null";
		assert(next == null || next.getOrder() == order -1) : "Next's order must be order below if not null.";

		this.manager = manager;
		this.order = order; this.belowAtOrder = belowAtOrder; this.next = next;


		holeMemoiser = new StackBelowApprox[order + 1];
		prefixMemoiser = new StackBelowApprox[order + 2]; // To easily allow to take prefix of whole thing
						// without going out-of-bounds on array.

		belowAtOrderMemoiser = new Head[order + 1];


		changeBelowApproxMemoiser =  (Memoiser1Arg<Head, StackBelowApprox>[]) new Memoiser1Arg[order +1];

		for(byte i = 0 ; i <= this.order ; i++)
			changeBelowApproxMemoiser[i] = new HashMemoiser1Arg<Head, StackBelowApprox>();

        this.hashCode = computeHashCode();
	}

    private int computeHashCode() {
        return order +
               (belowAtOrder == null ? 0 : belowAtOrder.hashCode()) +
               (next == null ? 0 : next.hashCode());
    }

    public int hashCode() {
        return hashCode;
    }

	/**
	 *
	 * @return	Order of stack being described (atomic element order-0)
	 */
	byte getOrder() {
		return order;
	}

	/**
	 *
	 * @return	Head containing possible order-(this.getOrder() -1) stacks immediately below
	 *			top-most this.(getOrder() -1) stack. Null if no stack below.
	 */
	Head getBelowAtOrder() {
		return belowAtOrder;
	}

	/**
	 *
	 * @return	Next StackBelowApprox considering stack of order immediately below or null
	 *			if order==0. Returns null if is a "context" (has a hole at order below).
	 */
	StackBelowApprox getNext() {
		return next;
	}


	/**
	 * Splices together a StackBelowApprox of order <= this.getOrder().
	 * Suppose b_k'---b_k'-1 --- b_0 is chain induced by next fields for this StackBelowApprox
	 * @param toInsert	StackBelowApprox to Insert  a_k --- a_k-1 --- a_0
	 *					(where the a_i represent the chain witnessed by next, describing
	 *					order_i stack immediately below).
	 * @return			b_k'---b_k'-1----b_k+1---a_k---a_k-1----a_0
	 *					In particular if k' = k, then we return toInsert.
	 *
	 */
	StackBelowApprox splice(StackBelowApprox toInsert) {
		assert(this.order >= toInsert.getOrder());

		StackBelowApprox result = spliceMemoiser.get(toInsert);

		if(result == null)	{
			if(toInsert.getOrder() == this.order)
				result =  toInsert;
			else if(toInsert.getOrder() == this.order -1)
				result = manager.getAddStackBelowApprox(this.order, this.belowAtOrder, toInsert); // Avoid NullPointerException when splicing into a hole.
			else
				result = manager.getAddStackBelowApprox(this.order, this.belowAtOrder, this.next.splice(toInsert));

			spliceMemoiser.put(toInsert, result);
		}

		return result;
	}

	/**
	 *
	 * @param order				Order to change (must be <= this.getOrder())
	 * @param newBelowApprox	belowApprox (Head) to change at given order.
	 * @return					StackBelowApprox where belowApprox is changed at given order.
	 *							(In particular when order == 0 this is the `link')
	 *							SummaryReverseLookup stays same as summaries do not care
	 *							about top character.
	 */
	StackBelowApprox changeBelowApprox(byte order, Head newBelowApprox) {
		assert( order <= this.order && order >= 0);

		StackBelowApprox result = changeBelowApproxMemoiser[order].get(newBelowApprox);

		if(result == null){
			if(this.order == order)
				result = manager.getAddStackBelowApprox(this.order, newBelowApprox, this.next);
			else
				result = this.splice(this.next.changeBelowApprox(order, newBelowApprox));

			changeBelowApproxMemoiser[order].put(newBelowApprox, result);
		}
		return result;
	}




	/**
	 *
	 * @param holeOrder		order k at which to create the hole.
	 * @return				When applied to a_n---a_n-1----a_k---a_k-1 --- a_k-2 ----.... (possibly down to a_0)
	 *						returns a_n---a_n-1---a_k+1
	 *
	 *						Returns null when k = order.
	 */
	StackBelowApprox makeHole(Byte holeOrder) {
		if(holeOrder == this.order)
			return null;

		StackBelowApprox result = holeMemoiser[holeOrder];

		if(result == null) {
			if(holeOrder == this.order - 1)
				result = manager.getAddStackBelowApprox(this.order, this.belowAtOrder,
						null);
			else
				result = manager.getAddStackBelowApprox(this.order, this.belowAtOrder,
						this.next.makeHole(holeOrder));

			holeMemoiser[holeOrder] = result;
		}

		return result;

	}

	/**
	 *
	 * @param holeOrder		order k below which to take prefix.
	 * @return				When applied to a_n---a_n-1----a_k---a_k-1 --- a_k-2 ----a_l (possibly down to a_0)
	 *						returns a_k-1, a_k-2----a_l
	 *
	 *						Returns null when k = l. (Only memoise usefully when l=0 in this case
	 *						since cannot distinguish between uninitialised memoiser position and null return).
	 *						But this should be fine as only expect to call in cases when l == 0.
	 *
	 *						We allow k = n+1, in which case just return the whole thing.
	 *
	 */
	StackBelowApprox getStrictPrefix(Byte strictPrefixOrder) {
		if(strictPrefixOrder == 0)
			return null;


		StackBelowApprox result = prefixMemoiser[strictPrefixOrder - 1];

		if(result == null) {
			if(strictPrefixOrder == this.order  + 1)
				result =  this;
			else
				result =  next.getStrictPrefix(strictPrefixOrder);

			prefixMemoiser[strictPrefixOrder -1 ] = result;
		}

		return result;


	}




	/**
	 *
	 * @param order		order at which to look up
	 * @return			belowAtOrder at component of order-order
	 */
	Head getBelowAtOrder(byte order) {
		Head result = belowAtOrderMemoiser[order];
		if(result == null) {
			if(this.order == order)
				result = this.belowAtOrder;
			else
				result = next.getBelowAtOrder(order);

			belowAtOrderMemoiser[order] = result;
		}
		return result;
	}







}
