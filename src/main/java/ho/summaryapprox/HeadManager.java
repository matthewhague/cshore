/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.summaryapprox;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.apache.log4j.Logger;

import ho.managers.Managers;
import ho.structures.cpds.CPushRule;
import ho.structures.cpds.EvalNonTermRule;
import ho.structures.cpds.CollapseRule;
import ho.structures.cpds.ControlState;
import ho.structures.cpds.Cpds;
import ho.structures.cpds.PopRule;
import ho.structures.cpds.PushRule;
import ho.structures.cpds.GetOrder0ArgRule;
import ho.structures.cpds.GetArgRule;
import ho.structures.cpds.RewLinkRule;
import ho.structures.cpds.RewRule;
import ho.structures.cpds.AltRule;
import ho.structures.cpds.Rule;
import ho.structures.cpds.SCharacter;
import ho.util.DoubleMap;
import ho.util.HashSetManager;
import ho.util.ManagedSet;
import ho.util.SetManager;
import ho.util.Triple;
import ho.util.TripleMap;
import ho.util.ImmutableSet;

public class HeadManager {
    static Logger logger = Logger.getLogger(HeadManager.class);

	SetManager<Head> hSetManager = new HashSetManager<Head>(); // Set manager to use for heads.
														// Should also enable recursion
														// as sets don't change so can iterate over them
														// without `adding elements' causing iteration to fail.

	SetManager<Rule> rSetManager = new HashSetManager<Rule>();

	SetManager<StackBelowApprox> sbaSetManager = new HashSetManager<StackBelowApprox>();




	byte order; // The order of CPDS being dealt with.

	Cpds origCpds; // The original Cpds to work with.

	List<Head> initialHeads = new LinkedList<Head>(); // Heads corresponding to initial configurations.
	List<Head> errorHeads = new LinkedList<Head>(); // Keep track of heads with error state.


	private DoubleMap<ControlState, SCharacter, Head> headUniquifier
	= new DoubleMap<ControlState, SCharacter, Head>(); // Maintain uniqueness of
																		// each head.
										// assuming collapse order uniquely determined by top character.

	private DoubleMap<Head, StackBelowApprox, StackBelowApprox>[] stackBelowApproxUniquifier;
					// Maintain uniqueness of stacks below. (Index i for
	//stackBe...ier[i] is order of stackBelowApproxUniquifier
					// first argument is "belowAtOrder" and second argument is "next")

	private StackBelowApprox emptyStackBelowApprox; // Stack-belowApprox with nulls everywhere on belowStacks.

	@SuppressWarnings("unchecked")
	public HeadManager(Cpds cpds) {
		this.origCpds = cpds;
		this.order = cpds.getOrder();
		this.stackBelowApproxUniquifier = (DoubleMap<Head, StackBelowApprox, StackBelowApprox>[] )
												new DoubleMap[this.order + 1];
		for(byte i = 0; i <= this.order ; i++)
			this.stackBelowApproxUniquifier[i] = new DoubleMap<Head, StackBelowApprox,
							 StackBelowApprox>();

		this.emptyStackBelowApprox = this.getEmptyStackBelowApprox(this.order);
	}


	/**
	 * Renders a particular head initial... no check is made to see whether this has already been
	 * made initial. In doing this, graph construction should be initiated.
	 *
	 * @param cs		Initial Control-state
	 * @param sc		Initial stack character (WARNING: Assumed to have collapse-order 1).
	 */
	public void addInitialHead(ControlState cs, SCharacter sc) {
		Head initHead = this.getAddHead(cs, sc, (byte) 1);
		initialHeads.add(initHead);
		initHead.makeInitial();
	}

	/**
	 *
	 * @return		The Cpds rules that may be used to get from an initial state to an error state
	 *				on basis of constructed approximate reachability graph.
	 *
	 */
	public ImmutableSet<Rule> getMaybeErrorRules() {
		ImmutableSet<Rule> result = ImmutableSet.<Rule>makeEmpty();
		for(Head errorHead : this.errorHeads) {
            ImmutableSet<Rule> s = errorHead.getRestrictedRules();
			result = result.union(s);
        }
		return result;
	}

	/**
	 *
	 * @return	A set of stack characters that the approx reachability graph indicates
	 * might accompany a reachable configuration with qerror as control-state.
	 */
	public Set<SCharacter> getErrorChars() {
		Set<SCharacter> result = new HashSet<SCharacter>();
		for(Head head : errorHeads)
			result.add(head.getTop());

		return result;
	}

	/**
	 * Creates new head if necessary, otherwise returns existing one (enforces unicity).
	 *
	 * @param cs				Control-state of head
	 * @param top				Stack character of head
	 * @param collapseOrder		Collapse order of Head (should be uniquely determined by cs... messy...)
	 * @return					Head meeting given specification
	 */
	Head getAddHead(ControlState cs, SCharacter top, byte collapseOrder) {
		Head result = headUniquifier.get(cs, top);
		if(result == null) {
			result = new Head(this, order, cs, top, collapseOrder);
			headUniquifier.put(cs, top, result);

			if(cs == Managers.csManager.makeControl(Cpds.error_state))
				this.errorHeads.add(result);

			// Add the rules.
			for(PopRule rule : this.origCpds.getPopRulesFwd(cs, top))
				result.addFromRule(rule);

			for(CollapseRule rule : this.origCpds.getCollapseRulesFwd(cs, top))
				result.addFromRule(rule);

			for(RewRule rule : this.origCpds.getRewRulesFwd(cs, top))
				result.addFromRule(rule);

			for(GetOrder0ArgRule rule : this.origCpds.getGetOrder0ArgRulesFwd(cs, top))
				result.addFromRule(rule);

			for(RewLinkRule rule : this.origCpds.getRewLinkRulesFwd(cs, top))
				result.addFromRule(rule);

			for(GetArgRule rule : this.origCpds.getGetArgRulesFwd(cs, top))
				result.addFromRule(rule);

			for(CPushRule rule : this.origCpds.getCPushRulesFwd(cs, top))
				result.addFromRule(rule);

            for(EvalNonTermRule rule : this.origCpds.getEvalNonTermRulesFwd(cs, top))
				result.addFromRule(rule);

			for(PushRule rule : this.origCpds.getPushRulesFwd(cs, top))
				result.addFromRule(rule);

            for(AltRule rule : this.origCpds.getAltRulesFwd(cs, top))
                result.addFromRule(rule);
		}



		return result;
	}

	/**
	 * Create new StackBelowApprox, ensures unicity.
	 *
	 * @param order				Order of stack that talking about (order-0 is atomic element).
	 * @param belowAtOrder		Head that contains stacks whose top-most order-(order -1) stacks might be
	 *							stack order-(order -1) stack immediately below top-most order-(order -1) stack
	 *							in stack described by this StackBelowApprox. Null if no stack below at this order.
	 *							At order-0 describes annotation (so should have order-linkOrder) or null
	 *							if no link.
	 * @param next				If order > 0, then should be order-(order - 1) StackBelowApprox talking about
	 *							order-(order-1) stack immediately below top-most order-(order -1) stack.
	 *							If order = 0 then null.
	 *
	 *
	 * @return					Canonicalised StackBelowApprox defined by parameters above.
	 */
	StackBelowApprox getAddStackBelowApprox(byte order, Head belowAtOrder,
												 StackBelowApprox next) {
		StackBelowApprox result = stackBelowApproxUniquifier[order].get(belowAtOrder,
							 next);
		if(result == null) {
			result = new StackBelowApprox(this, order,belowAtOrder, next);
			stackBelowApproxUniquifier[order].put(belowAtOrder, next, result);
		}
		return result;
	}

	/**
	 * @return	StackBelowApprox with nulls in all belowStacks representing
	 *			case where no pops of any order are possible.
	 */
	StackBelowApprox getEmptyStackBelowApprox() {
		return this.emptyStackBelowApprox;
	}

	private StackBelowApprox getEmptyStackBelowApprox(byte order) {
		if(order == 0)
			return this.getAddStackBelowApprox((byte) 0, null, null);
		else
			return this.getAddStackBelowApprox(order, null, getEmptyStackBelowApprox((byte)(order - 1)));
	}

	@Override
	public String toString() {
		return java.util.Arrays.toString(this.headUniquifier.values().toArray());
	}



}
