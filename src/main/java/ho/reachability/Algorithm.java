/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.reachability;

import java.util.Collection;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.LinkedList;

import java.lang.Iterable;

import org.apache.log4j.Logger;

import ho.structures.cpds.*;

import ho.structures.sa.*;

import ho.util.SetManager;
import ho.util.ManagedSet;

import ho.witness.Witness;
import ho.witness.PrincipalJustification;

import ho.fake.FakeManager;

import ho.lazystates.LazyStateManager;
import ho.lazystates.LazyState;
import ho.lazystates.LazyStateO1;

import ho.managers.Managers;

/*
 * The class responsible for actually running the algorithm on a given Cpds.
 */
public class Algorithm {

    static Logger logger = Logger.getLogger(Algorithm.class);

    static Cpds cpds;
    static SAManager saManager = Managers.saManager;
    static ControlStateManager csManager = Managers.csManager;
    static SCharacterManager scManager = Managers.scManager;
    static SetManager<SAState> sManager = Managers.sManager;
    static FakeManager fakeManager = Managers.fakeManager;
    static LazyStateManager lazyStateManager
        = Managers.lazyStateManager;

    /*
     * Runs the saturation algorithm on the CPDS.  Use isReachable or getSA to
     * retrieve the results
     *
     * @param a cpds
     */
    public static void check(Cpds cpds_in) {
        cpds = cpds_in;
        // sanity check
        ControlState qerror = csManager.makeControl(Cpds.error_state);
        if (!cpds.getControls().contains(qerror)) {
            logger.warn("Cpds does not contain control state '" +
                        qerror +
                        "'.  It is trivially not reachable.");
        } else {
            initialise();
            saturate();
        }
    }

    /**
     * returns the stack automaton built so far (inefficient since it copies
     * internal data structures to a currently clunky (for testing only) SA
     * implementationi)
     *
     * @return the SA built so far
     */
    public static SA getSA() {
        return saManager.getSA();
    }


    /**
     * resets saManager and adds new transitions for error state
     */
    private static void initialise() {
        Managers.algorithmReset();
        saManager.reset(cpds.getOrder());
        fakeManager.setOrder(cpds.getOrder());
        addInitialTransitions();
    }

    /**
     * Does the checking -- builds the automaton and checks for reachable error
     * state
     *
     */
    private static void saturate() {

        mainLoop();
    }

    /**
     * Assumes automaton already built/saturated.
     *
     * @param p the control state
     * @param a the character
     * @return true iff the initial config <p, [...[a]...]> is accepted by the
     *         saturated automaton
     */
    public static boolean reaches(ControlState p, SCharacter a) {
        return saManager.accepts(p, a);
    }

    /**
     * Assumes automaton already built/saturated.
     *
     * @param c a configuration
     * @return true iff the config is accepted by the saturated automaton
     */
    public static boolean reaches(CpdsConfig c) {
        return saManager.accepts(c);
    }



    /**
     * Assumes automaton already built/saturated.
     *
     * @param p the control state
     * @param a the character
     * @return a witness trace from <p, [..[a]..]> to error state or null if no
     *         such path
     */
    public static Witness getCounterExample(ControlState p, SCharacter a) {
        return saManager.getCounterExample(p, a);
    }


    /**
     * Runs simple CEGAR algorithm that limits the size of statesets in
     * transitions, starting with 0.  I.e. instead of adding q --> Q, add q -->
     * 0 (when size is 0).  Generate a witness, if it fails, increase the size
     * until no witness or real witness.
     *
     * @param cpds the cpds to check
     * @param q init control state
     * @param a init stack char
     * @return null if no run from <q, a> to error, a witness if there is one
     */
    public static Witness simpleCegarCheck(Cpds cpds,
                                           ControlState p,
                                           SCharacter a) {
        int bound = 0;
        Witness trace = null;
        boolean done = false;

        while (!done) {
            saManager.setStatesetBound(bound);
            check(cpds);
            logger.debug("ss bound: " + saManager.getStatesetBound());
            if (reaches(p, a)) {
                trace = getCounterExample(p, a);
                if (!isValidTrace(p, a, trace)) {
                    bound++;
                    trace = null;
                } else {
                    done = true;
                }
            } else {
                done = true;
            }
        }

        return trace;
    }

    private static boolean isValidTrace(ControlState p,
                                        SCharacter a,
                                        Witness trace) {
        //logger.debug("Valid: " + trace);
        CpdsConfig conf = new CpdsConfig(p, a, cpds.getOrder());
        if (conf.applyRules(trace.iterator())) {
            ControlState qerror = csManager.makeControl(Cpds.error_state);
            return csManager.equal(conf.getControlState(),
                                   qerror);
        } else {
            return false;
        }
    }


    /**
     * Adds transitions corresponding to accepting any stack from the error
     * state (the state we're interested in reaching.
     *
     * Only adds those transitions corresponding to stack-characters identified as
     * being possibly associated with reachable error state of CPDS.
     *
     */
    private static void addInitialTransitions() {
        // add q -- q' --> 0 up to order-1
        ControlState qerror = csManager.makeControl(Cpds.error_state);
        // for each stack character, add q -- a, 0 --> 0
        for (SCharacter a : cpds.getErrorSChars()) {
            addEmptyTransition(qerror, a);
        }
    }

    /**
     * Adds a transition p --- a, 0 ---> 0, ..., 0
     *
     * @param p the control state
     * @param a the character
     */
    private static void addEmptyTransition(ControlState p, SCharacter a) {
        SAState q = saManager.getAddOuterState(p);

        ManagedSet<SAState> e = sManager.makeEmptySet();
        for (byte order = cpds.getOrder(); order > 1; order--) {
            SATransition t = saManager.getAddTransition(q, e);
            q = t.getLabellingState();
        }

        saManager.addTransitionO1(q, a, e, e);
    }

    /**
     * Adds a transition p --- a, 0 ---> 0, ..., 0 with justification j
     *
     * @param p the control state
     * @param a the stack character
     * @param j the principal justification
     */
    private static void addEmptyTransition(ControlState p,
                                           SCharacter a,
                                           PrincipalJustification j) {
        SAState q = saManager.getAddOuterState(p);

        ManagedSet<SAState> e = sManager.makeEmptySet();
        for (byte order = cpds.getOrder(); order > 1; order--) {
            SATransition t = saManager.getAddTransition(q, e);
            q = t.getLabellingState();
        }

        saManager.addTransitionO1(q, a, e, e, j);
    }


    /**
     * Deal with order-N pops
     *
     * @param  targetSCharacter		Only process pop rules that may lead to this stack character on top
     *								in some error trace.
     * @param pprime				Only process pop rules with this control-state on right-hand-side.
     */
    private static void doOrdNPops(SCharacter targetSCharacter, ControlState pprime) {
        byte n = cpds.getOrder();
        ManagedSet<SAState> e = sManager.makeEmptySet();

        if (n > 1) {

                SAState spprime = saManager.getAddOuterState(pprime);
                ManagedSet<SAState> qpprime = sManager.makeSingleton(spprime);

                Collection<PopRule> pops = cpds.getPopRules(pprime, n);
                for (PopRule r : pops) {
	if(r.afterSCharsContains(targetSCharacter)) {
	                    // need to add p -- a, 0 --> (0, ..., 0, {p'})
	                    SAState qp = saManager.getAddOuterState(r.getControl1());

	                    // add (p) ----> (p')
	                    SATransition tnew = saManager.getAddTransition(qp, qpprime);

	                    // finally transitions to empty set for the remainder
	                    SAState q = tnew.getLabellingState();
	                    for (byte k = (byte)(n - 1); k > 1; k--) {
	                        tnew = saManager.getAddTransition(q, e);
	                        q = tnew.getLabellingState();
	                    }

	                    // finally, q -- a, 0 --> 0
                        PrincipalJustification j = new PrincipalJustification(r);
	                    saManager.addTransitionO1(q, r.getCharacter(), e, e, j);
	}
                }

        } else {

                SAState spprime = saManager.getAddOuterState(pprime);
                ManagedSet<SAState> qpprime = sManager.makeSingleton(spprime);

                Collection<PopRule> pops = cpds.getPopRules(pprime, n);
                for (PopRule r : pops) {
	if(r.afterSCharsContains(targetSCharacter)) {
	                    // need to add p -- a, 0 --> {p'}
	                    SAState qp = saManager.getAddOuterState(r.getControl1());
	                    saManager.addTransitionO1(qp,
                                                  r.getCharacter(),
                                                  e,
                                                  qpprime,
                                                  new PrincipalJustification(r));
	}
                }
            }

    }

    /**
     * Deal with order-N collapses
     *
     * @param targetSChar	We only add collapses that might still reach a qerror configuration
     *						when they result in a targetSChar on top of the stack.
     *
     * @param pprime		The control-state on the RHS of the collapse rules in which we are interested.
     */
    private static void doOrdNCollapses(SCharacter targetSChar, ControlState pprime) {
        byte n = cpds.getOrder();
        ManagedSet<SAState> e = sManager.makeEmptySet();


            SAState spprime = saManager.getOuterState(pprime);
            ManagedSet<SAState> qpprime = sManager.makeSingleton(spprime);

            Collection<CollapseRule> collapses = cpds.getCollapseRules(pprime, n);
            for (CollapseRule r : collapses) {
		if(r.afterSCharsContains(targetSChar)) {
		/*
		 * If we are in here then this collapse rule might lead to the stack character
		 * supplied in parameter to this method, so we are good to go.
		 *
		 */

	                // need to add p -- a, {p'} --> (0, ..., 0)
	                SAState qp = saManager.getAddOuterState(r.getControl1());

	                SAState q = qp;
	                // transitions to empty set for most
	                for (byte k = n; k > 1; k--) {
	                    SATransition tnew = saManager.getAddTransition(q, e);
	                    q = tnew.getLabellingState();
	                }

	                // finally, q -- a, 0 --> 0
                    PrincipalJustification j = new PrincipalJustification(r);
	                saManager.addTransitionO1(q, r.getCharacter(), qpprime, e, j);
	            }
            }

    }


    /**
     * Main worklist loop of the algorithm
     */
    private static void mainLoop() {
        while (saManager.hasNewTransition() || saManager.hasNewTransitionO1()) {
            if (saManager.hasNewTransitionO1()) {
                SATransitionO1 t = saManager.getNewTransitionO1();
                processTransitionO1(t);
            } else {
                SATransition t = saManager.getNewTransition();
                processTransition(t);
            }
            //logger.debug(saManager.shortStats());
        }
    }

    /**
     * The main loop part dealing with order-k transitions.
     *
     * @param t the transition to process
     */
    private static void processTransition(SATransition t) {
        processTransitionPushes(t);
        fakeManager.updateFake(t);
    }

    /**
     * The loop for dealing with processing a transition wrt push rules
     */
    private static void processTransitionPushes(SATransition t) {
        // note: we're making the assumption here that we don't get null in
        // return (to avoid a null test every time)
        // (i think this is satisfied by the kind of transitions we add to Tnew)
        SAState source = t.getSource();
        ControlState pprime = source.getControlState();
        byte order = t.getTransitionOrder();
        ManagedSet<SAState> qk = t.getDest();

        Collection<PushRule> pushes = cpds.getPushRules(pprime, order);

        for (PushRule r : pushes) {
            LazyState sourcep = lazyStateManager.getAddLazyState(source,
                                                                 r.getControl1());
            SAState labelq = t.getLabellingState();

            fakeManager.addFake(sourcep,
                                r.getCharacter(),
                                labelq,
                                qk,
                                new PrincipalJustification(r, t));
        }
    }

    /**
     * The loop for dealing with processing a transition wrt pop rules
     *
     * Take into account restrictions imposed by stack characters that might be
     * on top of stack after performing rule. (Why we take an SATransitionO1 as
     * this has stack character associated with it).
     *
     */
    private static void processTransitionPops(SATransitionO1 t) {
        // note: we're making the assumption here that we don't get null in
        // return (to avoid a null test every time) (i think this is satisfied
        // by the kind of transitions we add to Tnew)
        SCharacter destChar = t.getLabellingCharacter();


        ControlState pprime = t.getSource().getControlState();

        ManagedSet<SAState> e = sManager.makeEmptySet();

        SATransition nextTran = t.getSource().getTransition();

        for(byte order = 1; order < cpds.getOrder(); order++) {
	SAState source = nextTran.getLabellingState();
	        Collection<PopRule> pops = cpds.getPopRules(pprime, order);

	        if (order > 1) {
	            for (PopRule r : pops) {
		            if(r.afterSCharsContains(destChar)) {
		                // for source = (p', Qk+1, ..., Qn)
		                // need to add p -- a, 0 --> (0, ..., 0, {source}, Qk+1, ..., Qn)

		                // getAdding (p, Qk+1, ..., Qn) creates transitions from Qn to Qk+1
		                SAState sourcep = saManager.switchControlStateAdd(source,
		                                                                  r.getControl1());

		                // then add (p, Qk+1, ..., Qn) ----> (p', Qk+1, ..., Qn)
		                ManagedSet<SAState> dest = sManager.makeSingleton(source);
		                SATransition tnew = saManager.getAddTransition(sourcep, dest);

		                SAState q = tnew.getLabellingState();
		                // finally transitions to empty set for the remainder
		                for (byte k = (byte)(order - 1); k > 1; k--) {
		                    tnew = saManager.getAddTransition(q, e);
		                    q = tnew.getLabellingState();
		                }

		                // finally, q -- a, 0 --> 0
                        PrincipalJustification j = new PrincipalJustification(r);
		                saManager.addTransitionO1(q, r.getCharacter(), e, e, j);
		           }
	            }
	        } else {
	            for (PopRule r : pops) {
		            if(r.afterSCharsContains(destChar)) {
		                SAState sourcep = saManager.switchControlStateAdd(source,
		                                                                  r.getControl1());

		                // then add (p, Qk+1, ..., Qn) ----> (p', Qk+1, ..., Qn)
		                ManagedSet<SAState> dest = sManager.makeSingleton(source);
		                saManager.addTransitionO1(sourcep,
                                                  r.getCharacter(),
                                                  e,
                                                  dest,
                                                  new PrincipalJustification(r));
		            }
	            }
	        }
	        nextTran = nextTran.getSource().getTransition();
        }
        doOrdNPops(destChar, pprime);
    }


    /** The loop for dealing with processing a transition wrt collapse rules
     *
     * Takes O1 transition and then deals with collapses at all orders, taking
     * into account information about stack characters that the rule might lead
     * to on an error path.
     *
     * This is why take an O1 transition as stack character of O1-transition
     * represents stack character that would be seen after performing the
     * collapse.
     *
     */
    private static void processTransitionCollapses(SATransitionO1 t) {
        // Note that we assume that there are no order-1 collapses.

        // note: we're making the assumption here that we don't get null in
        // return (to avoid a null test every time)
        // (i think this is satisfied by the kind of transitions we add to Tnew)

        // The stack character that will
        // be seen after the collapse being considered.
        SCharacter colTargetSChar = t.getLabellingCharacter();

        //Control-state associated with this transition.
	ControlState pprime = t.getSource().getControlState();

        // This is the order-2 transition leading into the order-1
        // transition (and we know that this order-2 transition will lead to
        // character colTargetSChar)
        SATransition nextTran = t.getSource().getTransition();

	for(byte order = 2; order < cpds.getOrder(); order++) {
            // Get transition of next order.
		nextTran = nextTran.getSource().getTransition();
			// This will be null if
			// we reach outer-level of loop
			// but this is OK as loop should terminate
			// before this point.


	        SAState source = nextTran.getLabellingState();

	        ManagedSet<SAState> e = sManager.makeEmptySet();

            Collection<CollapseRule> collapses = cpds.getCollapseRules(pprime,
            order); for (CollapseRule r : collapses) {
            if(r.afterSCharsContains(colTargetSChar)) {
			/*
                     * If we get in here, then stack character read by t might
                     * indeed appear after performing r on a reachable
                     * configuration, such that after the application of r the
                     * resulting config. still stands a chance of reaching
                     * qerror
			 */


		            // for source = (p', Qk+1, ..., Qn)  need to add
                    // p -- a, { source } --> (0, ..., 0, Qk+1, ..., Qn)

                    // getAdding (p, Qk+1, ..., Qn) creates transitions from Qn
                    // to Qk+1
                    SAState sourcep
                        = saManager.switchControlStateAdd(source,
		                                                  r.getControl1());

		            SAState q = sourcep;
		            // finally transitions to empty set for the remainder
		            for (byte k = order; k > 1; k--) {
		                SATransition tnew = saManager.getAddTransition(q, e);
		                q = tnew.getLabellingState();
		            }

		            // finally, q -- a, {source} --> 0
                    PrincipalJustification j = new PrincipalJustification(r);
		            ManagedSet<SAState> dest = sManager.makeSingleton(source);
		            saManager.addTransitionO1(q, r.getCharacter(), dest, e, j);
		}
	        }


	}

	/*
	 * We finish by processing order-N collapses (where N is order of the CPDS).
	 */
	doOrdNCollapses(colTargetSChar, pprime);

    }


    /**
     * The main loop part dealing with order-1 transitions.
     *
     * @param t the transition to process
     */
    private static void processTransitionO1(SATransitionO1 t) {
	processTransitionPops(t);
	processTransitionCollapses(t);
        processTransitionPushesO1(t);
        processTransitionRewsO1(t);
        processTransitionRewLinksO1(t);
        processTransitionGetArgsO1(t);
        processTransitionGetOrder0ArgsO1(t);
        processTransitionEvalNonTermsO1(t);
        processTransitionAltO1(t);
        fakeManager.updateFakeO1(t);
    }


    /**
     * The loop for dealing with new order-1 transitions wrt push(k)(b)
     *
     * @param t the new order-1 transition
     */
    private static void processTransitionPushesO1(SATransitionO1 t) {
        // note: we're making the assumption here that we don't get null in
        // return (to avoid a null test every time)
        // (i think this is satisfied by the kind of transitions we add to Tnew)

        SAState source = t.getSource();
        ControlState pprime = source.getControlState();
        SCharacter b = t.getLabellingCharacter();
        ManagedSet<SAState> q1 = t.getDest();


        // note: this code allows push(b)(1), but essentially ignores that
        // there is a link (this is ok because we disallow collapse 1 and
        // order-1 collapse links in stack automata)

        ManagedSet<SAState> cdest = t.getCollapseDest();
        if (cdest.isEmpty()) {
            for (byte k = 1; k <= cpds.getOrder(); ++k) {
                Collection<CPushRule> pushes = cpds.getCPushRules(pprime, b, k);
                for (CPushRule r : pushes) {
                    LazyStateO1 sourcep =
                        lazyStateManager.getAddLazyStateO1(source,
                                                           r.getControl1(),
                                                           k,
                                                           cdest);
                    fakeManager.addFakeO1(sourcep,
                                          r.getCharacter(),
                                          q1,
                                          new PrincipalJustification(r, t));
                }
            }
        } else {
            byte k = cdest.choose().getStateOrder();
            Collection<CPushRule> pushes = cpds.getCPushRules(pprime, b, k);
            for (CPushRule r : pushes) {
	            LazyStateO1 sourcep =
                    lazyStateManager.getAddLazyStateO1(source,
                                                       r.getControl1(),
                                                       k,
                                                       cdest);
                fakeManager.addFakeO1(sourcep,
                                      r.getCharacter(),
                                      q1,
                                      new PrincipalJustification(r, t));
            }
        }
    }

    /**
     * The loop for dealing with new order-1 transitions wrt eval-nt(b)
     *
     * @param t the new order-1 transition
     */
    private static void processTransitionEvalNonTermsO1(SATransitionO1 t) {
        // note: we're making the assumption here that we don't get null in
        // return (to avoid a null test every time)
        // (i think this is satisfied by the kind of transitions we add to Tnew)

        SAState source = t.getSource();
        ControlState pprime = source.getControlState();
        SCharacter b = t.getLabellingCharacter();
        ManagedSet<SAState> q1 = t.getDest();


        // note: this code allows push(b)(1), but essentially ignores that
        // there is a link (this is ok because we disallow collapse 1 and
        // order-1 collapse links in stack automata)

        ManagedSet<SAState> cdest = t.getCollapseDest();
        if (cdest.isEmpty()) {
            Collection<EvalNonTermRule> evalNTs
                = cpds.getEvalNonTermRules(pprime, b);
            for (EvalNonTermRule r : evalNTs) {
                LazyStateO1 sourcep =
                    lazyStateManager.getAddLazyStateO1(source,
                                                       r.getControl1(),
                                                       (byte)1,
                                                       cdest);
                fakeManager.addFakeO1(sourcep,
                                      r.getCharacter(),
                                      q1,
                                      new PrincipalJustification(r, t));
            }
        }
    }



    /**
     * The loop for dealing with new order-1 transitions wrt rew(b)
     *
     * @param t the new order-1 transition
     */
    private static void processTransitionRewsO1(SATransitionO1 t) {
        // note: we're making the assumption here that we don't get null in
        // return (to avoid a null test every time)
        // (i think this is satisfied by the kind of transitions we add to Tnew)
        SAState source = t.getSource();
        ControlState pprime = source.getControlState();
        SCharacter b = t.getLabellingCharacter();

        Collection<RewRule> rews = cpds.getRewRules(pprime, b);
        for (RewRule r : rews) {
            // for t = (p', Q2, ..., Qn) --b, Qbr--> (Q1, ..., Qn)
            // need to add (p, Q2, ..., Qn) -- a, Qbr --> (Q1, ..., Qn)
            SAState q = saManager.switchControlStateAdd(source,
                                                        r.getControl1());

            saManager.addTransitionO1(q,
                                      r.getCharacter(),
                                      t.getCollapseDest(),
                                      t.getDest(),
                                      new PrincipalJustification(r, t));
        }
    }



    /**
     * The loop for dealing with new order-1 transitions wrt rew(b)(k)
     *
     * @param t the new order-1 transition
     */
    private static void processTransitionRewLinksO1(SATransitionO1 t) {
        // note: we're making the assumption here that we don't get null in
        // return (to avoid a null test every time)
        // (i think this is satisfied by the kind of transitions we add to Tnew)
        SAState source = t.getSource();
        ControlState pprime = source.getControlState();
        SCharacter b = t.getLabellingCharacter();
        ManagedSet<SAState> q1 = t.getDest();
        ManagedSet<SAState> e = sManager.makeEmptySet();

        // whenever we have p' --- b, Qbr ---> Q1, ..., Qn
        // with Qbr of order-k
        // we add p --- a, { } ---> Q1, ..., Qk + Qbr, ..., Qn

        ManagedSet<SAState> cdest = t.getCollapseDest();
        if (cdest.isEmpty()) {
            // could be any order
            for (byte k = 1; k <= cpds.getOrder(); ++k) {
                Collection<RewLinkRule> rewLinks = cpds.getRewLinkRules(pprime, b, k);
                for (RewLinkRule r : rewLinks) {
                    // no need to union cos empty set
                    SAState q = saManager.switchControlStateAdd(
                                              source,
                                              r.getControl1()
                                          );

                    saManager.addTransitionO1(q,
                                              r.getCharacter(),
                                              e,
                                              t.getDest(),
                                              new PrincipalJustification(r, t));
                }
            }
        } else {
            byte k = cdest.choose().getStateOrder();
            Collection<RewLinkRule> rewLinks = cpds.getRewLinkRules(pprime, b, k);
            if (k > 1) {
                for (RewLinkRule r : rewLinks) {
                    SAState q = saManager.switchControlStateUnionOrdkAdd(
                                              source,
                                              r.getControl1(),
                                              cdest,
                                              k
                                          );

                    saManager.addTransitionO1(q,
                                              r.getCharacter(),
                                              e,
                                              t.getDest(),
                                              new PrincipalJustification(r, t));
                }
            } else {
                for (RewLinkRule r : rewLinks) {
                    SAState q = saManager.switchControlStateAdd(
                                              source,
                                              r.getControl1()
                                          );

                    ManagedSet<SAState> dest = sManager.union(t.getDest(),
                                                              cdest);

                    saManager.addTransitionO1(q,
                                              r.getCharacter(),
                                              e,
                                              dest,
                                              new PrincipalJustification(r, t));
                }
            }
        }
    }

    /**
     * The loop for dealing with new order-1 transitions wrt getarg(b)(k)
     *
     * @param t the new order-1 transition
     */
    private static void processTransitionGetArgsO1(SATransitionO1 t) {
        // note: we're making the assumption here that we don't get null in
        // return (to avoid a null test every time)
        // (i think this is satisfied by the kind of transitions we add to Tnew)
        SAState source = t.getSource();
        ControlState pprime = source.getControlState();
        SCharacter b = t.getLabellingCharacter();
        ManagedSet<SAState> q1 = t.getDest();
        ManagedSet<SAState> e = sManager.makeEmptySet();

        // whenever we have p' --- b, Qbr ---> Q1, ..., Qn
        // with Qbr of order-k
        // we add p --- a, 0 ---> Q1, 0, ..., Qbr, ..., 0

        ManagedSet<SAState> cdest = t.getCollapseDest();
        if (cdest.isEmpty()) {
            // could be any order
            for (byte k = 1; k <= cpds.getOrder(); ++k) {
                Collection<GetArgRule> getArgs = cpds.getGetArgRules(pprime, b, k);
                for (GetArgRule r : getArgs) {
                    addGetArgTransition(r.getControl1(),
                                        r.getCharacter(),
                                        t.getDest(),
                                        cdest,
                                        k,
                                        new PrincipalJustification(r, t));
                }
            }
        } else {
            byte k = cdest.choose().getStateOrder();
            Collection<GetArgRule> getArgs = cpds.getGetArgRules(pprime, b, k);
            for (GetArgRule r : getArgs) {
                addGetArgTransition(r.getControl1(),
                                    r.getCharacter(),
                                    t.getDest(),
                                    cdest,
                                    k,
                                    new PrincipalJustification(r, t));
            }
        }
    }

    /**
     * Adds a transition p --- a, 0 ---> dest1,0,...,0,destk,0,...,0 with justification j
     *
     * @param p the control state
     * @param a the stack character
     * @param dest1 the order-1 dest
     * @param destk the order-k dest
     * @param k the order
     * @param j the principal justification
     */
    private static void addGetArgTransition(ControlState p,
                                            SCharacter a,
                                            ManagedSet<SAState> dest1,
                                            ManagedSet<SAState> destk,
                                            byte k,
                                            PrincipalJustification j) {
        SAState q = saManager.getAddOuterState(p);
        ManagedSet<SAState> e = sManager.makeEmptySet();

        if (k > 1) {
            SATransition t;
            for (byte order = cpds.getOrder(); order > k; order--) {
                t = saManager.getAddTransition(q, e);
                q = t.getLabellingState();
            }

            t = saManager.getAddTransition(q, destk);
            q = t.getLabellingState();

            for (byte order = (byte)(k-1); order > 1; order--) {
                t = saManager.getAddTransition(q, e);
                q = t.getLabellingState();
            }

            saManager.addTransitionO1(q, a, e, dest1, j);
        } else {
            for (byte order = cpds.getOrder(); order > 1; order--) {
                SATransition t = saManager.getAddTransition(q, e);
                q = t.getLabellingState();
            }

            ManagedSet<SAState> dest = sManager.union(dest1,
                                                      destk);

            saManager.addTransitionO1(q, a, e, dest, j);
        }
    }


    /**
     * The loop for dealing with new order-1 transitions wrt getorder0arg(b)
     *
     * @param t the new order-1 transition
     */
    private static void processTransitionGetOrder0ArgsO1(SATransitionO1 t) {
        // note: we're making the assumption here that we don't get null in
        // return (to avoid a null test every time)
        // (i think this is satisfied by the kind of transitions we add to Tnew)
        SAState source = t.getSource();
        ControlState pprime = source.getControlState();
        SCharacter b = t.getLabellingCharacter();
        ManagedSet<SAState> e = sManager.makeEmptySet();

        Collection<GetOrder0ArgRule> getOrder0Args = cpds.getGetOrder0ArgRules(pprime, b);
        for (GetOrder0ArgRule r : getOrder0Args) {
            // need to add p -- a, 0 --> (0, ..., 0)
            PrincipalJustification j = new PrincipalJustification(r, t);
            addGetArgTransition(r.getControl1(),
                                r.getCharacter(),
                                t.getDest(),
                                e,
                                (byte)1,
                                j);
        }
    }



    private static void processTransitionAltO1(SATransitionO1 t) {
        ControlState qi = t.getSource().getControlState();
        SCharacter ai = t.getLabellingCharacter();
        for (AltRule r : cpds.getAltRules(qi, ai)) {
            processAltRuleAgainstTransition(r, t, qi, ai);
        }
    }


    /**
     * adds all transitions implied by an alt rule and new transition t
     *
     * @param r the alt rule
     * @param t the new order-1 transition
     * @param qi the control state of the transition
     * @param ai the character of the transition
     */
    private static void processAltRuleAgainstTransition(AltRule r,
                                                        SATransitionO1 t,
                                                        ControlState qi,
                                                        SCharacter ai) {
        AltImageSet imageSet = getAltImageSetFrom(r, t, qi, ai);
        for (AltImage image : imageSet) {
            addAltImageTransition(r.getControl1(),
                                  r.getCharacter(),
                                  image,
                                  r);
        }
    }


    private static void addAltImageTransition(ControlState p,
                                              SCharacter a,
                                              AltImage i,
                                              Rule r) {
        SAState q = saManager.getAddOuterState(p);
        for (byte k = cpds.getOrder(); k > (byte)1; k--) {
            ManagedSet<SAState> dest = sManager.makeSet(i.getDest(k));
            SATransition t = saManager.getAddTransition(q, dest);
            q = t.getLabellingState();
        }
        ManagedSet<SAState> dest = sManager.makeSet(i.getDest((byte)1));
        ManagedSet<SAState> colDest = sManager.makeSet(i.getCollapseDest());
        saManager.addTransitionO1(q, a, colDest, dest,
                                  new PrincipalJustification(r));
    }


    /**
     * gets image set from an alt rule using always transition t
     *
     * @param r the alt rule
     * @param t the new order-1 transition
     * @param qi the control state of the transition
     * @param ai the character of the transition
     */
    private static AltImageSet getAltImageSetFrom(AltRule r,
                                                  SATransitionO1 t,
                                                  ControlState qi,
                                                  SCharacter ai) {
        byte order = cpds.getOrder();
        AltImage initImage = new AltImage(t, order);
        AltImageSet imageSet = new AltImageSet(order);
        imageSet.add(initImage);

        for (StackHead h : r.getDestinations()) {
            ControlState qj = h.getControl();
            SCharacter aj = h.getCharacter();
            // don't reprocess t
            if (!csManager.equal(qi, qj) ||
                !scManager.equal(ai, aj)) {
                AltImageSet nextImage = new AltImageSet(order);
                extendAltImageSet(qj, aj, imageSet, nextImage);
                imageSet = nextImage;
            }
        }

        return imageSet;
    }


    /**
     * Extend an alternating image set with all transitions from q, a
     *
     * i.e. for a set of images, take all transitions
     *
     *     q --- a, Qcol ---> Q1, ..., Qn
     *
     * and union with all images in set
     *
     * @param q the control state
     * @param a the stack character
     * @param oldIS the original image set to extend.
     * @param newIS the new image set being built
     */
    private static void extendAltImageSet(ControlState q,
                                          SCharacter a,
                                          AltImageSet oldIS,
                                          AltImageSet newIS) {
        for (SATransitionO1 t : saManager.oldTranO1HeadIterator(q, a)) {
            for (AltImage i : oldIS) {
                newIS.add(new AltImage(i, t));
            }
        }
    }

}
