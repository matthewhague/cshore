/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.reachability;

import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ho.structures.cpds.*;

import ho.structures.sa.*;
import ho.structures.sa.SAManager.IFullTransitionAcceptor;
import ho.structures.sa.SAManager.IOrderStateAcceptor;
import ho.structures.sa.SAManager.ISimpleTransitionAcceptor;
import ho.structures.sa.SAManager.ISimpleTransitionO1Acceptor;
import ho.structures.sa.SAManager.SimpleEnumeration;
import ho.util.SetManager;
import ho.util.ManagedSet;
import ho.util.Flag;

import ho.witness.Witness;

import ho.managers.Managers;

/*
 * The class responsible for actually running the algorithm on a given Cpds.
 */
public class NaiveAlgorithm {

    static Logger logger = Logger.getLogger(Algorithm.class);

    static Cpds cpds;
    static SAManager saManager = Managers.saManager;
    static ControlStateManager csManager = Managers.csManager;
    static SCharacterManager scManager = Managers.scManager;
    static SetManager<SAState> sManager = Managers.sManager;

    /*
     * Runs the saturation algorithm on the CPDS.  Use isReachable or getSA to
     * retrieve the results
     *
     * @param a cpds
     */
    public static void check(Cpds cpds_in) {
        logger.debug("Running NaiveAlgorithm.");

        cpds = cpds_in;
        // sanity check
        ControlState qerror = csManager.makeControl(Cpds.error_state);
        if (!cpds.getControls().contains(qerror)) {
            logger.warn("Cpds does not contain control state '" +
                        qerror +
                        "'.  It is trivially not reachable.");
        } else {
            initialise();
            saturate();
        }
    }

    /**
     * returns the stack automaton built so far (inefficient since it copies
     * internal data structures to a currently clunky (for testing only) SA
     * implementation)
     *
     * @return the SA built so far
     */
    public static SA getSA() {
        return saManager.getSA();
    }


    /**
     * resets saManager and adds new transitions for error state
     */
    private static void initialise() {
        Managers.algorithmReset();
        saManager.reset(cpds.getOrder());
        saManager.setTrackNew(false);
        addInitialTransitions();
    }

    /**
     * Does the checking -- builds the automaton and checks for reachable error
     * state
     *
     */
    private static void saturate() {
        mainLoop();
    }

    /**
     * Assumes automaton already built/saturated.
     *
     * @param p the control state
     * @param a the character
     * @return true iff the initial config <p, [...[a]...]> is accepted by the
     *         saturated automaton
     */
    public static boolean reaches(ControlState p, SCharacter a) {
        return saManager.accepts(p, a);
    }

    /**
     * Assumes automaton already built/saturated.
     *
     * @param c a configuration
     * @return true iff the config is accepted by the saturated automaton
     */
    public static boolean reaches(CpdsConfig c) {
        return saManager.accepts(c);
    }



    /**
     * Assumes automaton already built/saturated.
     *
     * @param p the control state
     * @param a the character
     * @return a witness trace from <p, [..[a]..]> to error state or null if no
     *         such path
     */
    public static Witness getCounterExample(ControlState p, SCharacter a) {
        return saManager.getCounterExample(p, a);
    }


    /**
     * Adds transitions corresponding to accepting any stack from the error
     * state (the state we're interested in reaching.
     *
     * Only adds those transitions corresponding to stack-characters identified as
     * being possibly associated with reachable error state of CPDS.
     *
     */
    private static void addInitialTransitions() {
        ControlState qerror = csManager.makeControl(Cpds.error_state);
        for (SCharacter a : cpds.getErrorSChars()) {
            SAState q = saManager.getAddOuterState(qerror);
            saManager.addEmptyTransition(q, a);
        }
    }


    /**
     * Main worklist loop of the algorithm
     */
    private static void mainLoop() {
        boolean updated = true;
        while (updated) {
            updated = false;
            for (PopRule r : cpds.getPopRules()) {
                updated |= doPopRule(r);
            }
            for (CollapseRule r : cpds.getCollapseRules()) {
                updated |= doCollapseRule(r);
            }
            for (PushRule r : cpds.getPushRules()) {
                updated |= doPushRule(r);
            }
            for (RewRule r : cpds.getRewRules()) {
                updated |= doRewRule(r);
            }
            for (RewLinkRule r : cpds.getRewLinkRules()) {
                updated |= doRewLinkRule(r);
            }
            for (AltRule r : cpds.getAltRules()) {
                updated |= doAltRule(r);
            }
            for (EvalNonTermRule r : cpds.getEvalNonTermRules()) {
                updated |= doEvalNonTermRule(r);
            }
            for (GetArgRule r : cpds.getGetArgRules()) {
                updated |= doGetArgRule(r);
            }
            for (GetOrder0ArgRule r : cpds.getGetOrder0ArgRules()) {
                updated |= doGetOrder0ArgRule(r);
            }
            for (CPushRule r : cpds.getCPushRules()) {
                updated |= doCPushRule(r);
            }
        }
    }

    /**
     * @return true if a change was made to stack automaton
     */
    private static boolean doPopRule(PopRule r) {
        // p -- pop(k) --> p'
        // find all qp' -- qk --> Qk+1,...,Qn
        // add qp -- a, 0 --> 0,...,0,{qk},Qk+1,...,Qn

        byte k = r.getOrder();
        SAState qnext = saManager.getAddOuterState(r.getControl2());
        final SCharacter a = r.getCharacter();
        final Flag updated = new Flag();
        saManager.iterateOrderState(qnext, k, new IOrderStateAcceptor() {
            public void accept(SAState qk) {
                SAState qksrc
                    = saManager.switchControlStateAdd(qk, r.getControl1());
                ManagedSet<SAState> qkset
                    = sManager.makeSingleton(qk);
                ManagedSet<SAState> e = sManager.makeEmptySet();
                if (k == 1) {
                    updated.orWith(saManager.addTransitionO1(qksrc, a, e, qkset));
                } else {
                    SATransition t = saManager.getAddTransition(qksrc, qkset);
                    updated.orWith(saManager.addEmptyTransition(t.getLabellingState(), a));
                }
            }
        });
        return updated.value();
    }

    /**
     * @return true if a change was made to stack automaton
     */
    private static boolean doCollapseRule(CollapseRule r) {
        // p -- collapse(k) --> p'
        // find all qp' -- qk --> Qk+1,...,Qn
        // add qp -- a, {qk} --> 0,...,0,Qk+1,...,Qn

        byte k = r.getOrder();
        SAState qnext = saManager.getAddOuterState(r.getControl2());
        final SCharacter a = r.getCharacter();
        final Flag updated = new Flag();

        saManager.iterateOrderState(qnext, k, new IOrderStateAcceptor() {
            public void accept(SAState qk) {
                SAState qksrc
                    = saManager.switchControlStateAdd(qk, r.getControl1());
                ManagedSet<SAState> qkset
                    = sManager.makeSingleton(qk);
                updated.orWith(saManager.addEmptyTransition(qksrc, a, qkset));
            }
        });
        return updated.value();
    }

    /**
     * @return true if a change was made to stack automaton
     */
    private static boolean doPushRule(PushRule r) {
        // For p -- push(k) --> p'
        // and qp' -- a, Qbr --> Q1, ..., Qn
        // and Qk -- a, Qbr' --> Q1', ..., Qk'
        // add qp -- a, Qbr + Qbr' --> Q1'+Q1,...,Qk-1'+Qk-1,Qk',Qk+1,...,Qn
        //
        // find qp' -- qk --> Qk+1, ..., Qn
        // then
        //      qk -- qk-1 --> Qk -- Qk-1' --> Qk'
        // then
        //      {qk-1} + Qk-1' -- a, Qbr --> Q1,...,Qk-1
        // note e.g. Qbr is the union of Qbr and Qbr' above, then add
        //      qp -- a, Qbr --> Q1,...,Qk-1,Qk',Qk+1,...,Qn

        // Apologies for nasty nesting!  (You might want to full screen...)

        final byte k = r.getOrder();
        SAState qnext = saManager.getAddOuterState(r.getControl2());
        final SCharacter a = r.getCharacter();
        final Flag updated = new Flag();

        saManager.iterateOrderState(qnext, k, new IOrderStateAcceptor() {
            public void accept(SAState qk) {
                final SAState qksrc
                    = saManager.switchControlStateAdd(qk, r.getControl1());
                if (k == 1) {
                    logger.error("TODO: k=1 for push handling.");
                } else {
                    saManager.iterateSimple(qk,
                                            new ISimpleTransitionAcceptor() {
                        public void accept(SATransition t) {
                            ManagedSet<SAState> Qk = t.getDest();
                            for (SimpleEnumeration e :
                                    saManager.enumerateSimple(Qk)) {
                                ManagedSet<SAState> Qkprime = e.getDest();
                                ManagedSet<SAState> Qkminus1
                                    = sManager.add(e.getLabel(), t.getLabellingState());

                                List<ManagedSet<SAState>> Qlist
                                    = new ArrayList<ManagedSet<SAState>>(cpds.getOrder() - k);
                                Qlist.add(Qkprime);

                                saManager.iterateFullInner(Qkminus1,
                                                           (byte)(k - 1),
                                                           a,
                                                           Qlist,
                                                           new IFullTransitionAcceptor() {
                                    public void accept(ManagedSet<SAState> Qbr,
                                                       List<ManagedSet<SAState>> fullQlist) {
                                        boolean t_is_new
                                            = saManager.addTransition(qksrc,
                                                                      a,
                                                                      Qbr,
                                                                      fullQlist);
                                        updated.orWith(t_is_new);
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });

        return updated.value();
    }

    /**
     * @return true if a change was made to stack automaton
     */
    private static boolean doRewRule(RewRule r) {
        // For p -- a, rew(b) --> p'
        // and qp' -- b, Qbr --> Q1, ..., Qn
        // add qp' -- a, Qbr --> Q1, ..., Qn

        SAState qnext = saManager.getAddOuterState(r.getControl2());
        final ControlState p = r.getControl1();
        final SCharacter a = r.getCharacter();
        final SCharacter b = r.getRewCharacter();
        final Flag updated = new Flag();

        saManager.iterateSimpleO1(qnext,
                                  b,
                                  new ISimpleTransitionO1Acceptor () {
            public void accept(SATransitionO1 t) {
                SAState q1src
                    = saManager.switchControlStateAdd(t.getSource(), p);
                boolean t_is_new
                    = saManager.addTransitionO1(q1src,
                                                a,
                                                t.getCollapseDest(),
                                                t.getDest());
                updated.orWith(t_is_new);
            }
        });

        return updated.value();
    }

    /**
     * @return true if a change was made to stack automaton
     */
    private static boolean doRewLinkRule(RewLinkRule r) {
        // For p -- a, rew(b) --> p'
        // and qp' -- b, Qbr --> Q1, ..., Qn
        // add qp' -- a, 0 --> Q1, ..., Qk + Qbr, ..., Qn
        //
        SAState qnext = saManager.getAddOuterState(r.getControl2());
        final ControlState p = r.getControl1();
        final SCharacter a = r.getCharacter();
        final SCharacter b = r.getRewCharacter();
        final Flag updated = new Flag();
        final ManagedSet<SAState> e = sManager.makeEmptySet();
        final byte k =  r.getOrder();

        if (k > 1) {
            saManager.iterateSimpleO1(qnext,
                                      b,
                                      new ISimpleTransitionO1Acceptor () {
                public void accept(SATransitionO1 t) {
                    SAState q = t.getSource();
                    ManagedSet<SAState> colDest = t.getCollapseDest();

                    if (!colDest.isEmpty() &&
                        colDest.choose().getStateOrder() != k)
                        return;

                    SAState q1src
                        = saManager.switchControlStateUnionOrdkAdd(q,
                                                                   p,
                                                                   colDest,
                                                                   k);
                    boolean t_is_new
                        = saManager.addTransitionO1(q1src, a, e, t.getDest());
                    updated.orWith(t_is_new);
                }
            });
        } else {
            saManager.iterateSimpleO1(qnext,
                                      b,
                                      new ISimpleTransitionO1Acceptor () {
                public void accept(SATransitionO1 t) {
                    SAState q = t.getSource();
                    ManagedSet<SAState> colDest = t.getCollapseDest();

                    if (!colDest.isEmpty() &&
                        colDest.choose().getStateOrder() != k)
                        return;

                    SAState q1src
                        = saManager.switchControlStateAdd(q, p);

                    ManagedSet<SAState> Q1 = sManager.union(colDest,
                                                            t.getDest());

                    boolean t_is_new
                        = saManager.addTransitionO1(q1src, a, e, Q1);
                    updated.orWith(t_is_new);
                }
            });

        }

        return updated.value();
    }

    /**
     * @return true if a change was made to stack automaton
     */
    private static boolean doAltRule(AltRule r) {
        AltImageSet ais = getAltImageSetFrom(r);
        boolean updated = false;
        for (AltImage ai : ais) {
            updated |= addFromAltImage(r.getControl1(),
                                       r.getCharacter(),
                                       ai);
        }
        return updated;
    }

    /**
     * Constructs and adds a new transition q -- a, Qbr --> Q1, ..., Qn from an
     * AltImage
     *
     * @param p the source control state
     * @param a the character label
     * @param ai the AltImage
     * @return true if transition is new
     */
    public static boolean addFromAltImage(ControlState p,
                                          SCharacter a,
                                          AltImage ai) {
        // TODO: add justification rule as argument
        SAState q = saManager.getAddOuterState(p);
        for (byte k = cpds.getOrder(); k > (byte)1; k--) {
            ManagedSet<SAState> dest = sManager.makeSet(ai.getDest(k));
            SATransition t = saManager.getAddTransition(q, dest);
            q = t.getLabellingState();
        }
        ManagedSet<SAState> dest = sManager.makeSet(ai.getDest((byte)1));
        ManagedSet<SAState> colDest = sManager.makeSet(ai.getCollapseDest());
        return saManager.addTransitionO1(q, a, colDest, dest);
    }




    /**
     * gets image set from an alt rule using always transition t
     *
     * @param r the alt rule
     */
    private static AltImageSet getAltImageSetFrom(AltRule r) {
        byte order = cpds.getOrder();
        AltImage initImage = new AltImage(order);
        AltImageSet imageSet = new AltImageSet(order);
        imageSet.add(initImage);

        for (StackHead h : r.getDestinations()) {
            ControlState qj = h.getControl();
            SCharacter aj = h.getCharacter();
            AltImageSet nextImage = new AltImageSet(order);
            extendAltImageSet(qj, aj, imageSet, nextImage);
            imageSet = nextImage;
        }

        return imageSet;
    }


    /**
     * Extend an alternating image set with all transitions from q, a
     *
     * i.e. for a set of images, take all transitions
     *
     *     q --- a, Qcol ---> Q1, ..., Qn
     *
     * and union with all images in set
     *
     * @param q the control state
     * @param a the stack character
     * @param oldIS the original image set to extend.
     * @param newIS the new image set being built
     */
    private static void extendAltImageSet(ControlState p,
                                          SCharacter a,
                                          AltImageSet oldIS,
                                          AltImageSet newIS) {
        SAState qp = saManager.getAddOuterState(p);
        saManager.iterateSimpleO1(qp, a, new ISimpleTransitionO1Acceptor() {
            public void accept(SATransitionO1 t) {
                for (AltImage i : oldIS) {
                    newIS.add(new AltImage(i, t));
                }
            }
        });
    }


    /**
     * @return true if a change was made to stack automaton
     */
    private static boolean doEvalNonTermRule(EvalNonTermRule r) {
        // this is essentially a p -- a, cpush(b, 1) --> p' rule
        // so for all p' -- b, 0 --> Q1 -- a, Qbr --> Q1'
        // add p -- a, Qbr --> Q1'

        final Flag updated = new Flag();
        final ControlState p1 = r.getControl1();
        final SCharacter a = r.getCharacter();
        final SCharacter b = r.getPushCharacter();

        SAState qpprime = saManager.getAddOuterState(r.getControl2());
        saManager.iterateSimpleO1(qpprime,
                                  b,
                                  new ISimpleTransitionO1Acceptor() {
            public void accept(SATransitionO1 t) {
                SAState new_src = null;

                if (t.getCollapseDest().isEmpty()) {
                    Collection<SimpleEnumeration> es
                        = saManager.enumerateSimpleO1(t.getDest(), a);

                    for (SimpleEnumeration e : es) {
                        if (new_src == null)
                            new_src =
                                saManager.switchControlStateAdd(t.getSource(),
                                                                p1);
                        boolean t_is_new
                            = saManager.addTransitionO1(new_src,
                                                        a,
                                                        e.getLabel(),
                                                        e.getDest());

                        updated.orWith(t_is_new);
                    }
                }
            }
        });

        return updated.value();
    }

    /**
     * @return true if a change was made to stack automaton
     */
    private static boolean doGetArgRule(GetArgRule r) {
        // for p -- a, getarg(b) --> p'
        // whenever we have p' --- b, Qbr ---> Q1, ..., Qn
        // with Qbr of order-k
        // we add p --- a, 0 ---> Q1, 0, ..., Qbr, ..., 0

        final Flag updated = new Flag();
        final byte k = r.getOrder();
        final SAState q1 = saManager.getAddOuterState(r.getControl1());
        final SAState q2 = saManager.getAddOuterState(r.getControl2());
        final ManagedSet<SAState> e =  sManager.makeEmptySet();
        final SCharacter a = r.getCharacter();
        final SCharacter b = r.getRewCharacter();

        saManager.iterateSimpleO1(q2,
                                  b,
                                  new ISimpleTransitionO1Acceptor() {

            public void accept(SATransitionO1 t) {
                ManagedSet<SAState> colDest = t.getCollapseDest();
                ManagedSet<SAState> dest = t.getDest();

                if (!colDest.isEmpty() &&
                    colDest.choose().getStateOrder() != k)
                    return;

                boolean is_new = false;

                if (k > 1) {
                    SATransition tnew;
                    SAState q = q1;
                    for (byte order = cpds.getOrder(); order > k; order--) {
                        tnew = saManager.getAddTransition(q, e);
                        q = tnew.getLabellingState();
                    }

                    tnew = saManager.getAddTransition(q, colDest);
                    q = tnew.getLabellingState();

                    for (byte order = (byte)(k-1); order > 1; order--) {
                        tnew = saManager.getAddTransition(q, e);
                        q = tnew.getLabellingState();
                    }

                    is_new = saManager.addTransitionO1(q, a, e, dest);
                } else {
                    SAState q = q1;
                    for (byte order = cpds.getOrder(); order > 1; order--) {
                        SATransition tnew = saManager.getAddTransition(q, e);
                        q = tnew.getLabellingState();
                    }

                    ManagedSet<SAState> new_dest = sManager.union(dest,
                                                                  colDest);

                    is_new = saManager.addTransitionO1(q, a, e, new_dest);
                }

                updated.orWith(is_new);
            }
        });

        return updated.value();
    }

    /**
     * @return true if a change was made to stack automaton
     */
    private static boolean doGetOrder0ArgRule(GetOrder0ArgRule r) {
        // for p -- a, getorder0(b) --> p'
        // for each p' -- b, Qbr --> Q1, ..., Qn
        // add p -- a, 0 --> 0, ..., 0

        final SAState q1 = saManager.getAddOuterState(r.getControl1());
        final SAState q2 = saManager.getAddOuterState(r.getControl2());
        final SCharacter a = r.getCharacter();
        final SCharacter b = r.getRewCharacter();

        final Flag updated = new Flag();

        saManager.iterateSimpleO1(q2,
                                  b,
                                  new ISimpleTransitionO1Acceptor () {
            public void accept(SATransitionO1 t) {
                boolean t_is_new
                    = saManager.addEmptyTransition(q1, a);
                updated.orWith(t_is_new);
            }
        });

        return updated.value();
    }

    /**
     * @return true if a change was made to stack automaton
     */
    private static boolean doCPushRule(CPushRule r) {
        // for p -- a, cpush(b, 1) --> p' rule
        // for all p' -- b, Qbr --> Q1 -- a, Qbr' --> Q1'
        // add p -- a, Qbr' --> Q1', Q2, ..., Qk + Qbr, ..., Qn

        final Flag updated = new Flag();
        final ControlState p1 = r.getControl1();
        final SCharacter a = r.getCharacter();
        final SCharacter b = r.getPushCharacter();
        final byte k = r.getOrder();

        SAState qpprime = saManager.getAddOuterState(r.getControl2());
        saManager.iterateSimpleO1(qpprime,
                                  b,
                                  new ISimpleTransitionO1Acceptor() {
            public void accept(SATransitionO1 t) {
                SAState new_src = null;
                ManagedSet<SAState> cdest = t.getCollapseDest();


                if (cdest.isEmpty() || cdest.choose().getStateOrder() == k) {
                    Collection<SimpleEnumeration> es
                        = saManager.enumerateSimpleO1(t.getDest(), a);

                    for (SimpleEnumeration e : es) {
                        // I'm concerned here that we're not treating order-1
                        // correctly -- i.e. should union with Q1 -- or is it
                        // fine because cpush(1) is never collapsed?
                        if (new_src == null)
                            new_src =
                                saManager.switchControlStateUnionOrdkAdd(t.getSource(),
                                                                         p1,
                                                                         cdest,
                                                                         k);
                        boolean t_is_new
                            = saManager.addTransitionO1(new_src,
                                                        a,
                                                        e.getLabel(),
                                                        e.getDest());

                        updated.orWith(t_is_new);
                    }
                }
            }
        });

        return updated.value();
    }



}
