/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.reachability;

import java.io.Reader;

import org.apache.log4j.Logger;

import ho.main.Main;

import ho.parsers.CpdsParser;

import ho.reachability.Algorithm;
import ho.structures.cpds.Cpds;
import ho.structures.cpds.ControlState;
import ho.structures.cpds.SCharacter;

import ho.witness.Witness;

import ho.managers.Managers;

public class CpdsProcessor {
    static Logger logger = Logger.getLogger(CpdsProcessor.class);

    public CpdsProcessor(Reader in) {
	    CpdsParser parser = new CpdsParser();
	    Cpds cpds = parser.getCpds(in);
	    cpds.trivialiseConstraints();
	    System.out.println("Read:\n\n" + cpds.toString());

        if (cpds.hasInitConfig()) {
            ControlState p = cpds.getInitControlState();
            SCharacter a = cpds.getInitCharacter();
            Witness witness = null;
            if (Main.getNaiveSaturation()) {
                NaiveAlgorithm.check(cpds);
                if (NaiveAlgorithm.reaches(p, a)) {
                    logger.debug("reaches");
                    witness = NaiveAlgorithm.getCounterExample(p, a);
                } else {
                    logger.debug("does not reach");
                }
            } else if (Main.getSimpleCegar()) {
                witness = Algorithm.simpleCegarCheck(cpds, p, a);
            } else {
	            Algorithm.check(cpds);
                if (Algorithm.reaches(p, a))
                    witness = Algorithm.getCounterExample(p, a);
            }
            System.out.println("Reaches error: " + (witness != null));
            if (witness != null) {
                System.out.println("Witness:");
                System.out.println(witness);
            }
        } else {
	        Algorithm.check(cpds);
	        Managers.saManager.printAutomaton();
        }
    }
}
