/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class NaiveUnsafeMultiMap<K, V> implements UnsafeMultiMap<K, V> {

	private Set<V> emptySet = new HashSet<V>();
	private Map<K, Set<V>> map = new HashMap<K, Set<V>>();
	private Set<V> imageSet = new HashSet<V>();

	public Collection<V> get(K key) {
		Set<V> returnSet = map.get(key);
		if (map.get(key) == null)
			returnSet = emptySet;
		return returnSet;
	}

	public boolean hasInImage(V v) {
		return imageSet.contains(v);
	}

	public Iterable<V> put(K key, V v) {
		Set<V> original = map.get(key);
		Set<V> setToAddTo;

		if (original == null) {
			setToAddTo = new HashSet<V>();
			map.put(key, setToAddTo);
		}
		else
			setToAddTo = original;

		setToAddTo.add(v);
		imageSet.add(v);

		return original;
	}

    public boolean put_has(K key, V v) {
		Set<V> original = map.get(key);
		Set<V> setToAddTo;

		if (original == null) {
			setToAddTo = new HashSet<V>();
			map.put(key, setToAddTo);
		}
		else
			setToAddTo = original;

		imageSet.add(v);
		return setToAddTo.add(v);
	}

	protected Set<V> getSet(K key) {
		return map.get(key);
	}

	public boolean hasTuple(K k, V v) {
        Set<V> s = map.get(k);
        if (s != null) {
            return s.contains(v);
        } else
            return false;
	}

    public void clear() {
        map.clear();
        imageSet.clear();
    }

    public int size() {
        int size = 0;
        for (Set<V> s : map.values())
            size += s.size();
        return size;
    }

    public Set<K> keySet() {
        return map.keySet();
    }

    public Collection<V> values() {
        return imageSet;
    }

    public Set<Map.Entry<K, Set<V>>> entrySet() {
        return map.entrySet();
    }
}
