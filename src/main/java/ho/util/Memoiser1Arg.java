/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.util;


/**
 * An object to use for memoisation of single argument methods.  Essentially
 * provides a map from T to R, but may, for example, only store the last n
 * mappings placed (to act as a cache of calls, rather than a full map).
 *
 * Note, this does not provide proper memoisation, as in it's not "memo(f)" for
 * some function f.  This is because i figured it would be more efficient for
 * each method to handle it's own memoisation, rather than creating an object to
 * carry a callback (since java can't pass functions properly).
 *
 * So we have to do:
 *
 * f(x) =
 *    y = memo.get(x)
 *    if (v == null)
 *      y = <computation>
 *      memo.put(x, y)
 *    return y
 *
 */
public interface Memoiser1Arg<T, R> {

    /**
     * @param arg the argument to the method
     * @return the cached value associated with the arg, null if not found
     */
    public R get(T arg);

    /**
     * @param arg the argument to the method
     * @param val the value to associate with the argument
     * @return the previous value if one existed, null otherwise
     */
    public R put(T arg, R val);

}
