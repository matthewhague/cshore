/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;

import java.lang.Iterable;

/**
 * A map indexed by three keys rather than one.  The same as having a map from a
 * triple (K1, K2, K3) to V.
 */
public class TripleMap<K1, K2, K3, V> {
    Map<K1, Map<K2, Map<K3, V>>> map = new HashMap<K1, Map<K2, Map<K3, V>>>();

    public TripleMap() { }

    /**
     * @param k1 the first index
     * @param k2 the second index
     * @param k3 the third index
     * @return the value at the index (k1, k2), null if none
     */
    public V get(K1 k1, K2 k2, K3 k3) {
        V v = null;
        Map<K2, Map<K3, V>> map2 = map.get(k1);
        if (map2 != null) {
            Map<K3, V> map3 = map2.get(k2);
            if (map3 != null) {
                v = map3.get(k3);
            }
        }
        return v;
    }

    /**
     * @param k1 the first index
     * @param k2 the second index
     * @param k3 the third index
     * @param v the value to put at the index (k1, k2)
     * @return the previous value, null if none
     */
    public V put(K1 k1, K2 k2, K3 k3, V v) {
        V oldV = null;
        Map<K2, Map<K3, V>> map2 = map.get(k1);
        if (map2 != null) {
            Map<K3, V> map3 = map2.get(k2);
            if (map3 != null) {
                oldV = map3.put(k3, v);
            } else {
                map3 = new HashMap<K3, V>();
                map3.put(k3, v);
                map2.put(k2, map3);
            }
        } else {
            Map<K3, V> map3 = new HashMap<K3, V>();
            map3.put(k3, v);
            map2 = new HashMap<K2, Map<K3, V>>();
            map2.put(k2, map3);
            map.put(k1, map2);
        }
        return oldV;
    }

    /**
     * @return a collection of the values V appearing in the map
     */
    public Collection<V> values() {
        Collection<V> values = new HashSet<V>();
        for (Map<K2, Map<K3, V>> map2 : map.values()) {
            for (Map<K3, V> map3 : map2.values()) {
                values.addAll(map3.values());
            }
        }
        return values;
    }

    /**
     * @return the number of (key, key)->value mappings in the map
     */
    public int size() {
        int size = 0;
        for (Map<K2, Map<K3, V>> map2 : map.values()) {
            for (Map<K3, V> map3 : map2.values()) {
                size += map3.size();
            }
        }
        return size;
    }

    /**
     * clears maps
     */
    public void clear() {
        map.clear();
    }

    /**
     * Iterates over all values associated to k1 and k2
     */
    public Iterable<V> get(K1 k1, K2 k2) {
        Map<K2, Map<K3, V>> map2 = map.get(k1);
        if (map2 != null) {
            Map<K3, V> map3 = map2.get(k2);
            if (map3 != null) {
                return new OuterIterator<V>(map3.entrySet());
            }
        }
        return null;
    }


    private class OuterIterator<V> implements Iterator<V>, Iterable<V> {
        Iterator<Map.Entry<K3, V>> i;

        OuterIterator(Iterable<Map.Entry<K3, V>> i) {
            this.i = i.iterator();
        }

        OuterIterator(Iterator<Map.Entry<K3, V>> i) {
            this.i = i;
        }

        public boolean hasNext() {
            return i.hasNext();
        }

        public V next() {
            Map.Entry<K3, V> next = i.next();
            if (i != null) {
                return next.getValue();
            } else {
                return null;
            }
        }

        public void remove() {
            i.remove();
        }

        public Iterator<V> iterator() {
            return this;
        }
    }
}
