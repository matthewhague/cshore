/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */





package ho.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import org.apache.log4j.Logger;

import java.util.Iterator;

/**
 * Implementation of QuadMap using nesting of hashmaps.
 *
 * QuadMapTest seems to suggest this is the more reliable quadmap.
 */
public class QuadMap<K1, K2, K3, K4, V>  {

    static Logger logger = Logger.getLogger(QuadMap.class);

    public interface K3Iterator<K3, K4, V> extends Iterator<K3> {
        /**
         * May null pointer error if you don't call next first.
         *
         * @return the set of K4 keys attached to the current K3
         */
        public Set<K4> getK4s();

        /**
         * @return the map entries of K4 to V
         */
        public Set<Map.Entry<K4, V>> getK4Entries();
    }

    private class NestedK3Iterator<K3, K4, V>
        implements K3Iterator<K3, K4, V> {

        private Iterator<Map.Entry<K3, Map<K4, V>>> i;
        private Map.Entry<K3, Map<K4, V>> current = null;

        public NestedK3Iterator(Iterator<Map.Entry<K3, Map<K4, V>>> i) {
            this.i = i;
        }

        public final boolean hasNext() {
            return i.hasNext();
        }

        public final K3 next() {
            current  = i.next();
            return current.getKey();
        }

        public final Set<K4> getK4s() {
            return current.getValue().keySet();
        }

        public final void remove() {
            i.remove();
        }

        public final Set<Map.Entry<K4, V>> getK4Entries() {
            return current.getValue().entrySet();
        }
    }

    Map<K1, Map<K2, Map<K3, Map<K4, V>>>> map =
        new HashMap<K1, Map<K2, Map<K3, Map<K4, V>>>>();

    public QuadMap() { }

    public V get(K1 k1, K2 k2, K3 k3, K4 k4) {
        V v = null;
        Map<K2, Map<K3, Map<K4, V>>> map2 = map.get(k1);
        if (map2 != null) {
            Map<K3, Map<K4, V>> map3 = map2.get(k2);
            if (map3 != null) {
                Map<K4, V> map4 = map3.get(k3);
                if (map4 != null) {
                    v = map4.get(k4);
                }
            }
        }
        return v;
    }

    public V put(K1 k1, K2 k2, K3 k3, K4 k4, V v) {
        V oldV = null;
        Map<K2, Map<K3, Map<K4, V>>> map2 = map.get(k1);
        if (map2 != null) {
            Map<K3, Map<K4, V>> map3 = map2.get(k2);
            if (map3 != null) {
                Map<K4, V> map4 = map3.get(k3);
                if (map4 != null) {
                    oldV = map4.put(k4, v);
                } else {
                    map4 = new HashMap<K4, V>();
                    map4.put(k4, v);
                    map3.put(k3, map4);
                }
            } else {
                Map<K4, V> map4 = new HashMap<K4, V>();
                map4.put(k4, v);
                map3 = new HashMap<K3, Map<K4, V>>();
                map3.put(k3, map4);
                map2.put(k2, map3);
            }
        } else {
            Map<K4, V> map4 = new HashMap<K4, V>();
            map4.put(k4, v);
            Map<K3, Map<K4, V>> map3 = new HashMap<K3, Map<K4, V>>();
            map3.put(k3, map4);
            map2 = new HashMap<K2, Map<K3, Map<K4, V>>>();
            map2.put(k2, map3);
            map.put(k1, map2);
        }
        return oldV;
    }

    public Collection<V> values() {
        Collection<V> values = new HashSet<V>();
        for (Map<K2, Map<K3, Map<K4, V>>> map2 : map.values()) {
            for (Map<K3, Map<K4, V>> map3 : map2.values()) {
                for (Map<K4, V> map4 : map3.values()) {
                    values.addAll(map4.values());
                }
            }
        }
        return values;
    }

    public int size() {
        int size = 0;
        for (Map<K2, Map<K3, Map<K4, V>>> map2 : map.values()) {
            for (Map<K3, Map<K4, V>> map3 : map2.values()) {
                for (Map<K4, V> map4 : map3.values()) {
                    size += map4.size();
                }
            }
        }
        return size;
    }

    public void clear() {
        map.clear();
    }


    public final K3Iterator<K3, K4, V> keyIterator(K1 k1, K2 k2) {
        Map<K2, Map<K3, Map<K4, V>>> map2 = map.get(k1);
        if (map2 != null) {
            Map<K3, Map<K4, V>> map3 = map2.get(k2);
            if (map3 != null)
                return new NestedK3Iterator<K3, K4, V>
                               (map3.entrySet().iterator());
        }
        return null;
    }

    /**
     * @return an iterable over all V associated with k1, k2
     */
    public final Iterable<V> valuesIterator(K1 k1, K2 k2) {
        return new Iterable<V>() {
            public Iterator<V> iterator() {
                return new K1K2VIterator(k1, k2);
            }
        };
    }

    public Set<K2> keySet(K1 k1) {
        Map<K2, Map<K3, Map<K4, V>>> map2 = map.get(k1);
        if (map2 == null)
            return null;
        else
            return map2.keySet();
    }

    private class K1K2VIterator implements Iterator<V> {
        // strategy: next always contains next or is null if done, findNext does
        // hard work.

        Iterator<Map.Entry<K3, Map<K4, V>>> outer;
        Iterator<Map.Entry<K4, V>> inner;
        V next = null;

        public K1K2VIterator(K1 k1, K2 k2) {
            Map<K2, Map<K3, Map<K4, V>>> map2 = map.get(k1);
            if (map2 != null) {
                Map<K3, Map<K4, V>> map3 = map2.get(k2);
                if (map3 != null) {
                    outer = map3.entrySet().iterator();
                    findNext();
                }
            }
        }

        public boolean hasNext() {
            return (next != null);
        }

        public V next() {
            V res = next;
            findNext();
            return res;
        }

        private void findNext() {
            if (inner == null || !inner.hasNext()) {
                if (outer.hasNext()) {
                    inner = outer.next().getValue().entrySet().iterator();
                    findNext();
                } else {
                    next = null;
                }
            } else {
                next = inner.next().getValue();
            }
        }
    }
}
