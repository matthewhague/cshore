/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.util;

import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;

/**
 * A memoiser for methods taking <K1, K2, K3, byte> and returning V -- assuming
 * byte to only take small values.
 */
public class Memoiser4ArgByte<K1, K2, K3, V> {

    private ArrayList<TripleMap<K1, K2, K3, V>> array
        = new ArrayList<TripleMap<K1, K2, K3, V>>();


    public Memoiser4ArgByte() { }

    /**
     * @param k1 the first arg
     * @param k2 the second arg
     * @param k3 the third arg
     * @param b the byte
     * @return the value V of f(k1, k2, k3, b) or null if not memoised
     */
    public final V get(K1 k1, K2 k2, K3 k3, byte b) {
        if (array.size() <= b) {
            return null;
        } else {
            return array.get(b).get(k1, k2, k3);
        }
    }


    /**
     * Add an entry to memoise
     *
     * @param k1 the first argument
     * @param k2 the second argument
     * @param k3 the third argument
     * @param b the byte argument
     * @param v the v to add
     */
    public final void put(K1 k1, K2 k2, K3 k3, byte b, V v) {
        ensureSize(b);
        array.get(b).put(k1, k2, k3, v);
    }

    /**
     * @param size assure we have maps for indexes up to size-1
     */
    private final void ensureSize(byte b) {
        int curSize = array.size();
        if (curSize <= b) {
            array.ensureCapacity(b + 1);
            for (int i = curSize; i <= b; i++) {
                TripleMap<K1, K2, K3, V> m
                    = new TripleMap<K1, K2, K3, V>();
                array.add(i, m);
            }
        }
    }


}

