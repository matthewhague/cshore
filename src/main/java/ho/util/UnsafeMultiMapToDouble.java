/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util;

/**
 * Analagous comments to UnsafeMapToTriple apply (will copy, paste and modify appropriately
 * here once `right' definition has been found.
 *
 */
public interface UnsafeMultiMapToDouble<K, V1, V2>  {

	public Iterable<Pair<V1, V2>> get( K key );

	public Iterable<Pair<V1, V2>>  put( K key, V1 v1, V2 v2);

	public boolean hasInImage( V1 v1, V2 v2);

	/**
	 *
	 * @param k key
	 * @param v1 item 1 in pair
	 * @param v2 item 2 in pair
	 * @return true if k maps to set containing (v1, v2), false otherwise
	 */
	public boolean hasTuple(K k, V1 v1, V2 v2);

}
