/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
* Provides a map from elements to *sets*. It is barebones and intended
* for use where entries are very frequently added and iterated through.
* `Unsafe' refers to assumptions allowing iterators to avoid allocation and
* providing maximal flexibility with implementation (see comments below).
*
*
* NOTE: Maybe should rename UnsafeMultiMapToTriple to reflect fact image consists
* of sets
*/

public interface UnsafeMultiMap<K, V> {


		/**
		 *
		 * @param key	Key for which to look up sets
		 * @return		An Iterable that allows one to go through sets mapped to by key.
		 *				It is assumed that these sets will never be modified and moreover
		 *				will not be retained when the next item in iterator is requested
		 *				(allowing same triple to be repeatedly used by the iterator rather
		 *				than allocating one on each iteration).
		 *
		 *				Should never return null, but may return an Iterable giving an empty
		 *				Iterator.
		 *
		 */
		public Collection<V> get( K key );


		/**
		 *
		 * @param key	Key for item to be added
		 * @param v		Item to be added to set associated with key
		 * @return		The previous set associate with key if key already
		 *				existed, otherwise returns null (if key not already
		 *				there).
		 */
		public Iterable<V>  put( K key, V v);

		/**
		 *
		 * @param key	Key for item to be added
		 * @param v		Item to be added to set associated with key
		 * @return		boolean true if v was already mapped to key[
		 */
		public boolean put_has(K key, V v);


		/**
		 * Remark: Matt's experiments suggest that it is worth keeping
		 * a separate table with the image. (This was originally done
		 * when managing Targets, but it will crop up in a few places
		 * and so it seems sensible to abstract this decision into
		 * a single place).
		 *
		 *
		 * @param v
		 * @return	True iff v belongs to a set in the image
		 *			of map.
		 */
		public boolean hasInImage( V v );

		public boolean hasTuple(K k, V v);

        public void clear();

        public int size();

        /**
         * @return the key set
         */
        public Set<K> keySet();

        /**
         * @return the value set
         */
        public Collection<V> values();

        /**
         * @return the entry set key to set of values
         */
        public Set<Map.Entry<K, Set<V>>> entrySet();
}
