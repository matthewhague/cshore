/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util;

public class Quin<A, B, C, D, E> {



			private A a;
			private B b;
			private C c;
			private D d;
			private E e;

			int hashCode;

			public void set( A a, B b, C c, D d, E e) {
				this.a = a; this.b = b; this.c = c; this.d = d; this.e = e;
				this.hashCode = (a == null ? 0 : a.hashCode()) +
									(b == null ? 0 : b.hashCode()) +
									(c == null ? 0 : c.hashCode()) +
									(d== null ? 0 : d.hashCode()) + (e== null ? 0 : e.hashCode());
			}

			public A first() {
				return a;
			}

			public B second() {
				return b;
			}

			public C third() {
				return c;
			}

			public D fourth() {
				return d;
		}

			public E fifth() {
				return e;
		}

			@SuppressWarnings("unchecked")
			@Override
			public final boolean equals(Object o) {
	            if (o instanceof Quin) {
	                Quin q = (Quin)o;
	                return ((q.first() == null && this.first() == null) ||
	                         this.first() != null && this.first().equals(q.first())) &&
	                       ((q.second() == null && this.second() == null) ||
	                         this.second() != null && this.second().equals(q.second())) &&
	                       ((q.third() == null && this.third() == null) ||
	                         this.third() != null && this.third().equals(q.third())) &&
	                       ((q.fourth() == null && this.fourth() == null) ||
	                        this.fourth() != null && this.fourth().equals(q.fourth())) &&
		                       ((q.fifth() == null && this.fifth() == null) ||
			                        this.fifth() != null && this.fifth().equals(q.fifth()));
	            } else
	                return false;
			}

			@Override
			public final int hashCode() {
				return hashCode;

			}
	}

