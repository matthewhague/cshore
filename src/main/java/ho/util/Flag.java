
package ho.util;

public class Flag {
    private boolean value = false;
    public Flag() { }
    public void orWith(boolean v) { value |= v; }
    public boolean value() { return value; }
}


