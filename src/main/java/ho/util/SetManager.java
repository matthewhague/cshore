/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.util;

import java.util.Map;
import java.util.HashMap;

import java.util.Set;
import java.util.HashSet;


/**
 * The interface for a set manager, which creates and provides manipulation
 * methods for ManagedSet objects (with the hope that this can be faster than
 * standard Java set manipulation (using memoisation, unique representations,
 * and so on).
 */
public interface SetManager<T> {

    /**
     * @return the empty set
     */
    public ManagedSet<T> makeEmptySet();

    /**
     * @param s a set of objects to construct a managed set out of
     * @return the managed set representation of s
     */
    public ManagedSet<T> makeSet(Set<T> s);

    /**
     * @param e the element to make a singleton set from
     * @return the managed set representation of {e}
     */
    public ManagedSet<T> makeSingleton(T e);

    /**
     * @param s1 first part of the union
     * @param s2 second part of the union
     * @return a managed set representation of s1 union s2
     */
    public ManagedSet<T> union(ManagedSet<T> s1, ManagedSet<T> s2);

    /**
     * @param s the set to remove an element from
     * @param e the element to remove from the set
     * @return a managed set representation of s - {e}
     */
    public ManagedSet<T> subtract(ManagedSet<T> s, T e);

    /**
     * @param s the set to add an element to
     * @param e the element to add
     * @return a managed set representation of s + {e}
     */
    public ManagedSet<T> add(ManagedSet<T> s, T e);

    /**
     * @param s1 the first part of the equality test
     * @param s2 the second part of the equality test
     * @return true if s1 and s2 are equal, false otherwise
     */
    public boolean equal(ManagedSet<T> s1, ManagedSet<T> s2);

    /**
     * @param s1 the first part of the subset test
     * @param s2 the second part of the subset test
     * @return true if s1 subseteq s2, false otherwise
     */
    public boolean subset(ManagedSet<T> s1, ManagedSet<T> s2);


    /**
     * Resets the manager
     */
    public void reset();

}
