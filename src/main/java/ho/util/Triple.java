/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util;

public class Triple<A, B, C> {
	private A a;
	private B b;
	private C c;

    public Triple() { }

    public Triple(A a, B b, C c) {
        set(a, b, c);
    }

	private int hashCode;

	public void set( A a, B b, C c) {
		this.a = a; this.b = b; this.c = c;
		this.hashCode = (a == null ? 0 : a.hashCode()) +
						(b == null ? 0 : b.hashCode()) +
						(c == null ? 0 : c.hashCode());
	}

	public A first() {
		return a;
	}

	public B second() {
		return b;
	}

	public C third() {
		return c;
	}

	@SuppressWarnings("unchecked")
	@Override
	public final boolean equals(Object o) {
		if (o instanceof Triple) {
            Triple t = (Triple)o;
            return ((t.first() == null && this.first() == null) ||
                     this.first() != null && this.first().equals(t.first())) &&
                   ((t.second() == null && this.second() == null) ||
                     this.second() != null && this.second().equals(t.second())) &&
                   ((t.third() == null && this.third() == null) ||
                     this.third() != null && this.third().equals(t.third()));
		} else
			return false;
	}

	@Override
	public int hashCode() {
		return hashCode;

	}
}
