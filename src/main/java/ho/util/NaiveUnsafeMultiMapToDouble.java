/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class NaiveUnsafeMultiMapToDouble<K, V1, V2> implements
		UnsafeMultiMapToDouble<K, V1, V2> {

	NaiveUnsafeMultiMap<K, Pair<V1, V2>> map = new NaiveUnsafeMultiMap<K, Pair<V1, V2>>();

	Pair<V1, V2> disposablePair = new Pair<V1, V2>();

	public final Iterable<Pair<V1, V2>> get(K key) {
		return map.get(key);
	}

	public final boolean hasInImage(V1 v1, V2 v2) {
		disposablePair.set(v1, v2);
		return map.hasInImage(disposablePair);
	}

	public final Iterable<Pair<V1, V2>> put(K key, V1 v1, V2 v2) {
		if (!hasTuple(key, v1, v2)) {
			Pair<V1, V2> newPair = new Pair<V1, V2>();
			newPair.set(v1, v2);
			return map.put(key, newPair);
		}
		else
			return map.get(key);
	}

	public final boolean hasTuple(K k, V1 v1, V2 v2) {
        disposablePair.set(v1, v2);
		return map.hasTuple(k, disposablePair);
	}

}
