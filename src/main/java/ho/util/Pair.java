/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util;

public class Pair<A, B> {
	private A a;
	private B b;
	private int hashCode;

    public Pair() { }

    public Pair(A a, B b) {
        set(a, b);
    }

	public void set( A a, B b) {
		this.a = a; this.b = b;
		hashCode = (a == null ? 0 : a.hashCode()) +
				(b == null ? 0 : b.hashCode());
	}

	public A first() {
		return a;
	}

	public B second() {
		return b;
	}

	@SuppressWarnings("unchecked")
	@Override
	public final boolean equals(Object o) {
		if ( o!=null && o.getClass() == Pair.class
				&& ((Pair) o).first().getClass() == this.first().getClass()
					&& ((Pair) o).second().getClass() == this.second().getClass())
				return this.first().equals(((Pair<A, B>) o).first())
					&& this.second().equals(((Pair<A, B>) o).second());
		else
			return false;

	}

	@Override
	public int hashCode() {
		return hashCode;

	}

}
