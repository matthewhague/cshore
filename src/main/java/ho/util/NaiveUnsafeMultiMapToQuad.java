/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class NaiveUnsafeMultiMapToQuad<K, V1, V2, V3, V4> implements
		UnsafeMultiMapToQuad<K, V1, V2, V3, V4> {

	NaiveUnsafeMultiMap<K, Quad<V1, V2, V3, V4>> map = new NaiveUnsafeMultiMap<K, Quad<V1, V2, V3, V4>>();

	Quad<V1, V2, V3, V4> disposableQuad = new Quad<V1, V2, V3, V4>();

	public Iterable<Quad<V1, V2, V3, V4>> get(K key) {
		return map.get(key);
	}

	public boolean hasInImage(V1 v1, V2 v2, V3 v3, V4 v4) {
		disposableQuad.set(v1, v2, v3, v4);
		return map.hasInImage(disposableQuad);
	}

	public Iterable<Quad<V1, V2, V3, V4>> put(K key, V1 v1, V2 v2, V3 v3, V4 v4) {
		disposableQuad.set(v1, v2, v3, v4);
		if (map.getSet(key) == null || !map.getSet(key).contains(disposableQuad)) {
			Quad<V1, V2, V3, V4> newQuad = new Quad<V1, V2, V3, V4>();
			newQuad.set(v1, v2, v3, v4);
			return map.put(key, newQuad);
		}
		else
			return map.get(key);
	}

    public int size() {
        return map.size();
    }
}
