/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.util;

import java.util.Iterator;
import java.lang.Iterable;

/**
 * For iterating over an iterator of iterables as one big iterator
 */
public class MultiIterator<V> implements Iterator<V>, Iterable<V> {
    Iterator<? extends Iterable<V>> i;
    Iterator<V> curI;

    /**
     * @return a multi iterator to iterate over all values V
     */
    public MultiIterator(Iterable<? extends Iterable<V>> i) {
        this.i = i.iterator();
    }


    /**
     * @return a multi iterator to iterate over all values V
     */
    public MultiIterator(Iterator<? extends Iterable<V>> i) {
        this.i = i;
    }

    public boolean hasNext() {
        ensureInnerIterator();
        if (curI != null)
            return curI.hasNext();
        else
            return false;
    }

    public V next() {
        ensureInnerIterator();
        if (curI != null) {
            return curI.next();
        } else {
            return null;
        }
    }

    public void remove() {
        if (curI != null)
            curI.remove();
    }

    public Iterator<V> iterator() {
        return this;
    }

    /**
     * Find next non-empty iterator and store in curI, curI will be null if not found.
     *
     * If curI hasNext when this is called, then curI remains unchanged
     */
    private void ensureInnerIterator() {
        while ((curI == null || !curI.hasNext())
                && i.hasNext()) {
            curI = i.next().iterator();
        }
    }
}
