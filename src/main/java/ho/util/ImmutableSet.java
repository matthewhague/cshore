/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.util;

import java.lang.Iterable;
import java.util.Iterator;

import scala.collection.JavaConversions;
import scala.collection.GenTraversableOnce;
import scala.collection.immutable.HashSet;


/**
 * Wrapper class for an immutable set implementation of our choice...
 */
public class ImmutableSet<T> implements Iterable<T> {

    private HashSet<T> set = new HashSet<T>();

    private ImmutableSet() {
        this.set = new HashSet<T>();
    }

    private ImmutableSet(HashSet<T> set) {
        this.set = set;
    }


    public static final <T> ImmutableSet<T> makeEmpty() {
        return new ImmutableSet<T>();
    }

    public final ImmutableSet<T> add(T e) {
        return new ImmutableSet<T>(set.$plus(e));
    }

	public final ImmutableSet<T> union(ImmutableSet<T> s2) {
        ImmutableSet<T> result = new ImmutableSet<T>(set);
        for (T e : s2) {
            result.set = result.set.$plus(e);
        }
        return result;
    }

    public final Iterator<T> iterator() {
        return JavaConversions.asJavaIterator(set.iterator());
    }

    public final boolean contains(T e) {
        return set.contains(e);
    }
}


