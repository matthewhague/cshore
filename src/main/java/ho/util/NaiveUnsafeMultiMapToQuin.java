/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class NaiveUnsafeMultiMapToQuin<K, V1, V2, V3, V4, V5> implements
		UnsafeMultiMapToQuin<K, V1, V2, V3, V4, V5> {

	NaiveUnsafeMultiMap<K, Quin<V1, V2, V3, V4, V5>> map = new NaiveUnsafeMultiMap<K, Quin<V1, V2, V3, V4, V5>>();

	Quin<V1, V2, V3, V4, V5> disposableQuin = new Quin<V1, V2, V3, V4, V5>();

	public Iterable<Quin<V1, V2, V3, V4, V5>> get(K key) {
		return map.get(key);
	}

	public boolean hasInImage(V1 v1, V2 v2, V3 v3, V4 v4, V5 v5) {
		disposableQuin.set(v1, v2, v3, v4, v5);
		return map.hasInImage(disposableQuin);
	}

	public Iterable<Quin<V1, V2, V3, V4, V5>> put(K key, V1 v1, V2 v2, V3 v3, V4 v4, V5 v5) {
		disposableQuin.set(v1, v2, v3, v4, v5);
		if (map.getSet(key) == null || !map.getSet(key).contains(disposableQuin)) {
			Quin<V1, V2, V3, V4, V5> newQuin = new Quin<V1, V2, V3, V4, V5>();
			newQuin.set(v1, v2, v3, v4, v5);
			return map.put(key, newQuin);
		}
		else
			return map.get(key);
	}

}
