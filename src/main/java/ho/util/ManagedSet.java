/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */




package ho.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Iterator;

import java.lang.StringBuilder;

import ho.managers.Managers;


// The main use of this set is to override the hashing function -- since the
// point of managed set is to be able to compare by reference, so there's no
// need to hash the contents of the set.
//
// We could also override modifying methods to throw runtimeexceptions to force
// immutability.  Though that would cause problems in set manager...

/** A set managed by a SetManager implementation -- the main assumption is that
 * for any set, there will only be one object (the SetManager will ensure this),
 * hence we can compare equality quickly, and use the system hashcode rather
 * than a hash of each element in the set.
 *
 * These objects should only be manipulated using SetManager methods.
 *
 * They are also considered immutable -- the pacakage level protection on
 * state-change methods is intended to mean that they shouldn't be used by
 * anything other than SetManager.
 */
public class ManagedSet<T> implements Iterable<T> {

    protected Set<T> s;

    /**
     * @param c the initial elements to put in the set
     */
    ManagedSet(Collection<? extends T> c) {
        s = new HashSet<T>(c);
    }

    ManagedSet() {
        s = new HashSet<T>();
    }

    public final int hashCode() {
        return System.identityHashCode(this);
    }

    public final boolean equals(Object o) {
        if (o.getClass() == ManagedSet.class)
            return (this == o);
        else
            return s.equals(o);
    }

    /**
     * @return the number of elements in the set
     */
    public final int size() {
        return s.size();
    }

    /**
     * @param o an object to test containment of
     * @return true if o is in the set, false otherwise
     */
    public final boolean contains(Object o) {
        return s.contains(o);
    }

    /**
     * @return true if the set is empty, false otherwise
     */
    public final boolean isEmpty() {
        return s.isEmpty();
    }

    /**
     * @return any element if non-empty, null otherwise
     */
    public final T choose() {
        if (s.isEmpty())
            return null;
        else
            return s.iterator().next();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("_{ ");
        for (T e : s) {
            sb.append(e);
            sb.append(" ");
        }
        sb.append("}");
        return sb.toString();
    }


    public final void add(T e) {
        s.add(e);
    }

    public final void addAll(Set<? extends T> c) {
        s.addAll(c);
    }

    public final void remove(Object o) {
        s.remove(o);
    }

    /**
     * Note: only to be called by SetManager
     *
     * @return the underlying java Set object
     */
    public final Set<T> getSet() {
        return s;
    }

    /**
     * Note: only to be called by SetManager (because iterators can allow
     * updates
     *
     * @return an iterator over the set
     */
    public final Iterator<T> iterator() {
        return s.iterator();
    }

}
