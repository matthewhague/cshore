/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.util;

import java.util.Map;
import java.util.HashMap;

import java.util.Set;
import java.util.HashSet;


/**
 * Implementation of SetManager based on Hash maps for finding previously
 * allocated sets, and using memoisation to speed up manipulation.
 */
public class HashSetManager<T> implements SetManager<T> {

    // keep a map for singletons to avoid cost of building a singleton set then
    // looking it up (i don't think we'll use makeSet directly, in fact...)
    Map<Set<T>, ManagedSet<T>> universeEntry = new HashMap<Set<T>, ManagedSet<T>>();
    Map<T, ManagedSet<T>> singletons = new HashMap<T, ManagedSet<T>>();
    ManagedSet<T> emptySet;


    // memoisation
    Memoiser2Arg<ManagedSet<T>, ManagedSet<T>, ManagedSet<T>> unionMemo
        = new Memoiser2Arg<ManagedSet<T>, ManagedSet<T>, ManagedSet<T>>();
    Memoiser2Arg<ManagedSet<T>, T, ManagedSet<T>> subtractMemo
        = new Memoiser2Arg<ManagedSet<T>, T, ManagedSet<T>>();
    Memoiser2Arg<ManagedSet<T>, T, ManagedSet<T>> addMemo
        = new Memoiser2Arg<ManagedSet<T>, T, ManagedSet<T>>();
    Memoiser2Arg<ManagedSet<T>, ManagedSet<T>, Boolean> subsetMemo
        = new Memoiser2Arg<ManagedSet<T>, ManagedSet<T>, Boolean>();



    public HashSetManager() {
        emptySet = makeSet(new HashSet<T>());
    }



    public final ManagedSet<T> getEmptySet() {
	    return emptySet;
    }

    public final ManagedSet<T> makeSet(Set<T> s) {
        if (s.size() == 1) {
            return makeSingleton(s.iterator().next());
        } else {
            ManagedSet<T> ms = universeEntry.get(s);
            if (ms == null) {
                ms = new ManagedSet<T>(s);
                universeEntry.put(s, ms);
            }
            return ms;
        }
    }

    public final ManagedSet<T> makeEmptySet() {
        return emptySet;
    }

    public final ManagedSet<T> makeSingleton(T e) {
        ManagedSet<T> ms = singletons.get(e);
        if (ms == null) {
            ms = new ManagedSet<T>();
            ms.add(e);
            singletons.put(e, ms);
        }
        return ms;
    }

    public final ManagedSet<T> union(ManagedSet<T> s1, ManagedSet<T> s2) {
        ManagedSet<T> s12 = unionMemo.get(s1, s2);
        if (s12 == null) {
            s12 = new ManagedSet<T>(s1.getSet());
            s12.addAll(s2.getSet());
            s12 = addIfNew(s12);
            unionMemo.put(s1, s2, s12);
        }
        return s12;
    }

    public final ManagedSet<T> subtract(ManagedSet<T> s, T e) {
        ManagedSet<T> sminus = subtractMemo.get(s, e);
        if (sminus == null) {
            sminus = s;
            if (s.contains(e)) {
                sminus = new ManagedSet<T>(s.getSet());
                sminus.remove(e);
                // note, makeSet will catch if we've made a singleton.
                sminus = addIfNew(sminus);
            }
            subtractMemo.put(s, e, sminus);
        }
        return sminus;
    }

    public ManagedSet<T> add(ManagedSet<T> s, T e) {
        ManagedSet<T> sadd = addMemo.get(s, e);
        if (sadd == null) {
            sadd = s;
            if (!s.contains(e)) {
                sadd = new ManagedSet<T>(s.getSet());
                sadd.add(e);
                sadd = addIfNew(sadd);
            }
            addMemo.put(s, e, sadd);
        }
        return sadd;
    }


    public final boolean equal(ManagedSet<T> s1, ManagedSet<T> s2) {
        return (s1 == s2);
    }

    public boolean subset(ManagedSet<T> s1, ManagedSet<T> s2) {
        Boolean b = subsetMemo.get(s1, s2);
        if (b == null) {
            b = s2.getSet().containsAll(s1.getSet());
            subsetMemo.put(s1, s2, b);
        }
        return b;
    }



    // non-public
    //


    /**
     * @param s set to add to maps
     * @return either s, if it is new, or the managed set representation of s
     * that already exists
     */
    protected final ManagedSet<T> addIfNew(ManagedSet<T> s) {
        if (s.size() == 1) {
            T e = s.iterator().next();
            ManagedSet<T> ms = singletons.get(e);
            if (ms == null) {
                singletons.put(e, s);
                ms = s;
            }
            return ms;
        } else {
            Set<T> innerSet = s.getSet();
            ManagedSet<T> ms = universeEntry.get(innerSet);
            if (ms == null) {
                universeEntry.put(innerSet, s);
                ms = s;
            }
            return ms;
        }
    }


    public final void reset() {
        universeEntry = new HashMap<Set<T>, ManagedSet<T>>();
        singletons = new HashMap<T, ManagedSet<T>>();

        unionMemo = new Memoiser2Arg<ManagedSet<T>,
                                     ManagedSet<T>,
                                     ManagedSet<T>>();
        subtractMemo = new Memoiser2Arg<ManagedSet<T>,
                                        T,
                                        ManagedSet<T>>();
        addMemo = new Memoiser2Arg<ManagedSet<T>,
                                   T,
                                   ManagedSet<T>>();

        emptySet = makeSet(new HashSet<T>());
    }

}
