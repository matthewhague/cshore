/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class NaiveUnsafeMultiMapToTriple<K, V1, V2, V3> implements
		UnsafeMultiMapToTriple<K, V1, V2, V3> {

	NaiveUnsafeMultiMap<K, Triple<V1, V2, V3>> map = new NaiveUnsafeMultiMap<K, Triple<V1, V2, V3>>();

	Triple<V1, V2, V3> disposableTriple = new Triple<V1, V2, V3>();

	public final Iterable<Triple<V1, V2, V3>> get(K key) {
		return map.get(key);
	}

	public final boolean hasInImage(V1 v1, V2 v2, V3 v3) {
		disposableTriple.set(v1, v2, v3);
		return map.hasInImage(disposableTriple);
	}

	public final Iterable<Triple<V1, V2, V3>> put(K key, V1 v1, V2 v2, V3 v3) {
		if (!hasTuple(key, v1, v2, v3)) {
			Triple<V1, V2, V3> newTriple = new Triple<V1, V2, V3>();
			newTriple.set(v1, v2, v3);
			return map.put(key, newTriple);
		}
		else
			return map.get(key);
	}


	public final boolean hasTuple(K k, V1 v1, V2 v2, V3 v3) {
		disposableTriple.set(v1,  v2, v3);
        return map.hasTuple(k, disposableTriple);
	}

    public Set<K> keySet() {
        return map.keySet();
    }

}
