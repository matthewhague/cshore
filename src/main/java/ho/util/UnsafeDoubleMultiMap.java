/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util;

import java.util.Collection;

/**
 * Provides mapping from doubles to *sets* of elements.
 *
 */
public interface UnsafeDoubleMultiMap <K1, K2, V>  {

	public Collection<V> get(K1 key1, K2 key2 );

	public Iterable<V> put(K1 key1, K2 key2, V v);

    public boolean put_has(K1 key1, K2 key2, V v);

	public boolean hasInImage( V v);

	public boolean hasTuple(K1 k1, K2 k2, V v);

    /**
     * @param k1 the K1 key
     * @param k2 the K2 key
     * @param v the v to remove
     */
    public void remove(K1 k1, K2 k2, V v);

    /**
     * @return the number of items in the image
     */
    public int size();

    /**
     * @return the image
     */
    public Collection<V> values();

    /**
     * @param k1 K1 index
     * @return the set of K2 keys associated to k1
     */
    public Collection<K2> keySet(K1 k1);


    /**
     * @return teh set of K1 keys
     */
    public Collection<K1> keySet();

    /**
     * @return the number of pairs <k1, k2> with mappings
     */
    public int numKeyPairs();
}
