/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util;

import java.util.HashMap;
import java.util.Map;



/**
 * Factory for creating multi-maps.
 *
 */
public class MapFactory{

	public static <K1, K2, V> DoubleMap<K1, K2, V> newDoubleMap() {
		return new DoubleMap<K1, K2, V>();
	}

	public static final <K, V> Map<K, V> newMap() {
		return new HashMap<K, V>();
	}

	public static final <K1, K2, K3, K4, V> QuadMap<K1, K2, K3, K4, V> newQuadMap() {
		return new QuadMap<K1, K2, K3, K4, V>();
	}

	public static final <K1, K2, K3, V> TripleMap<K1, K2, K3, V> newTripleMap() {
		return new TripleMap<K1, K2, K3, V>();
	}

	public static <K, V> UnsafeMultiMap<K, V>  newUnsafeMultiMap() {
		return new NaiveUnsafeMultiMap<K, V>();
	}


	public static <K, V1, V2> UnsafeMultiMapToDouble<K, V1, V2>  newUnsafeMapToDouble() {
		return new NaiveUnsafeMultiMapToDouble<K, V1, V2>();
	}

	public static final <K1, K2, K3, K4, V> UnsafeMultiMapToQuad<K1, K2, K3, K4, V> newUnsafeMapToQuad() {
		return new NaiveUnsafeMultiMapToQuad<K1, K2, K3, K4, V>();
	}

	public static <K, V1, V2, V3, V4, V5> UnsafeMultiMapToQuin<K, V1, V2, V3, V4, V5>  newUnsafeMapToQuin() {
		return new NaiveUnsafeMultiMapToQuin<K, V1, V2, V3, V4, V5>();
	}


	public static final <K1, K2, K3, V> UnsafeMultiMapToTriple<K1, K2, K3, V> newUnsafeMapToTriple() {
		return new NaiveUnsafeMultiMapToTriple<K1, K2, K3, V>();
	}

	public static final <K1, K2, V> UnsafeDoubleMultiMap<K1, K2, V> newUnsafeDoubleMultiMap() {
		return new NaiveUnsafeDoubleMultiMap<K1, K2, V>();
	}

}
