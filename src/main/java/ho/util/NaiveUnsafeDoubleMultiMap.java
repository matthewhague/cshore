/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

public class NaiveUnsafeDoubleMultiMap<K1, K2, V> implements
		UnsafeDoubleMultiMap<K1, K2, V> {

    static Logger logger = Logger.getLogger(NaiveUnsafeDoubleMultiMap.class);

	private DoubleMap<K1, K2, Set<V>> map = new DoubleMap<K1, K2, Set<V>>();
	private Set<V> imageUnion = new HashSet<V>();
	private Set<V> emptySet = new HashSet<V>();

	private Set<Triple<K1, K2, V>> tuples = new HashSet<Triple<K1, K2 , V>>();
	private Triple<K1, K2, V> disposableTriple = new Triple<K1, K2, V>();


	public Collection<V> get(K1 key1, K2 key2) {
		Set<V> returnSet = map.get(key1, key2);;
		if (returnSet == null)
			returnSet = emptySet;
		return returnSet;
	}

	public boolean hasInImage(V v) {
		return imageUnion.contains(v);
	}

	public Iterable<V> put(K1 key1, K2 key2, V v) {
		Set<V> lookup = map.get(key1, key2);
		Set<V> setMappedTo;

		if (lookup == null) {
			setMappedTo = new HashSet<V>();
			map.put(key1, key2, setMappedTo);
		}
		else
			setMappedTo = lookup;

		if(setMappedTo.add(v)){
			Triple<K1, K2, V> newTriple = new Triple<K1, K2, V>();
			newTriple.set(key1, key2, v);
			tuples.add(newTriple);
		}

		imageUnion.add(v);

		return lookup;

	}


    public boolean put_has(K1 key1, K2 key2, V v) {
		Set<V> lookup = map.get(key1, key2);
		Set<V> setMappedTo;

        logger.debug("lookup " + key1 + ", " + key2);
        logger.debug("found: " + lookup);

        for (K1 k : keySet()) {
            logger.debug(key1 + " = " + k);
            logger.debug(k.equals(key1));
        }

		if (lookup == null) {
			setMappedTo = new HashSet<V>();
			map.put(key1, key2, setMappedTo);
		}
		else
			setMappedTo = lookup;

        imageUnion.add(v);

		if(setMappedTo.add(v)){
			Triple<K1, K2, V> newTriple = new Triple<K1, K2, V>();
			newTriple.set(key1, key2, v);
			tuples.add(newTriple);
            return true;
		} else {
            return false;
        }
	}



	public boolean hasTuple(K1 k1, K2 k2, V v) {
		disposableTriple.set(k1, k2, v);
		return tuples.contains(disposableTriple);
	}


    public void remove(K1 k1, K2 k2, V v) {
        disposableTriple.set(k1, k2, v);
        tuples.remove(disposableTriple);
		Set<V> vset = map.get(k1, k2);;
		if (vset != null)
            vset.remove(v);
    }

    public int size() {
        return imageUnion.size();
    }

    public Collection<V> values() {
        return imageUnion;
    }

    public Collection<K2> keySet(K1 k1) {
        return map.keySet(k1);
    }

    public Collection<K1> keySet() {
        return map.keySet();
    }

    public int numKeyPairs() {
        int n = 0;
        for (K1 k1 : keySet())
            n += keySet(k1).size();
        return n;
    }
}
