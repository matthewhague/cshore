/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.util;


/**
 * An object to use for memoisation of two argument methods.  Essentially
 * provides a map from T1, T2 to R, but may, for example, only store the last n
 * mappings placed (to act as a cache of calls, rather than a full map).
 *
 * Note, this does not provide proper memoisation, as in it's not "memo(f)" for
 * some function f.  This is because i figured it would be more efficient for
 * each method to handle it's own memoisation, rather than creating an object to
 * carry a callback (since java can't pass functions properly).
 *
 * So we have to do:
 *
 * f(x, y) =
 *    z = memo.get(x, y)
 *    if (z == null)
 *      z = <computation>
 *      memo.put(x, y, z)
 *    return z
 *
 */
public class Memoiser2Arg<T1, T2, R> {

    // obviously, we'd probably want to allow hash parameters to be passed
    // through, but can do that later...
    DoubleMap<T1, T2, R> mem = new DoubleMap<T1, T2, R>();

    public Memoiser2Arg() { }


    /**
     * @param arg1 the first argument to the method
     * @param arg2 the second argument to the method
     * @return the cached value associated with arg1 and arg2, null if not found
     */
    public final R get(T1 arg1, T2 arg2) {
        return mem.get(arg1, arg2);
    }

    /**
     * @param arg1 the first argument to the method
     * @param arg2 the second argument to the method
     * @param val the value to associate with the arguments
     * @return the previous value if one existed, null otherwise
     */
    public final R put(T1 arg1, T2 arg2, R val) {
        return mem.put(arg1, arg2, val);
    }



}




