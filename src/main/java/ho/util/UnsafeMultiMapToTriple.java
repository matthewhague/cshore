/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util;

import java.util.Set;

/**
 * Provides a map from elements to *sets* of triples. It is barebones and intended
 * for use where entries are very frequently added and iterated through.
 * `Unsafe' refers to assumptions allowing iterators to avoid allocation and
 * providing maximal flexibility with implementation (see comments below).
 *
 *
 * NOTE: Maybe should rename UnsafeMultiMapToTriple to reflect fact image consists
 * of sets
 */
public interface UnsafeMultiMapToTriple<K, V1, V2, V3>  {

	/**
	 *
	 * @param key	Key for which to look up triples
	 * @return		An Iterable that allows one to go through truples mapped to by key.
	 *				It is assumed that these triples will never be modified and moreover
	 *				will not be retained when the next item in iterator is requested
	 *				(allowing same triple to be repeatedly used by the iterator rather
	 *				than allocating one on each iteration).
	 *
	 *				Should never return null, but may return an Iterable giving an empty
	 *				Iterator.
	 *
	 */
	public Iterable<Triple<V1, V2, V3>> get( K key );


	/**
	 *
	 * @param key	Key for item to be added
	 * @param v1	first item of triple to associate with key
	 * @param v2	second item of triple to associate with key
	 * @param v3	third item of triple to associate with key
	 * @return		The previous triple associate with key if key already
	 *				existed, otherwise returns null (if key not already
	 *				there). Same assumption about this triple applies
	 *				as before
	 */
	public Iterable<Triple<V1, V2, V3>>  put( K key, V1 v1, V2 v2, V3 v3);

	/**
	 * Remark: Matt's experiments suggest that it is worth keeping
	 * a separate table with the image. (This was originally done
	 * when managing Targets, but it will crop up in a few places
	 * and so it seems sensible to abstract this decision into
	 * a single place).
	 *
	 *
	 * @param v1
	 * @param v2
	 * @param v3
	 * @return	True iff triple formed from arguments belongs to a set in the image
	 *			of map.
	 */
	public boolean hasInImage( V1 v1, V2 v2, V3 v3 );

	/**
	 *
	 * @param k
	 * @param v1
	 * @param v2
	 * @param v3
	 * @return  true if k maps to a set containing (v1, v2, v3), otherwise false.
	 */
	public boolean hasTuple(K k, V1 v1, V2 v2, V3 v3);

    /**
     * @return the key set
     */
    public Set<K> keySet();

}

