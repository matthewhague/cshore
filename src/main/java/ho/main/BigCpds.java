/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.main;

import java.util.Random;
import java.util.HashSet;

import ho.structures.cpds.*;

public class BigCpds {

    int NUM_CONTROLS = 60*60;
    int NUM_STACK_CHARS = 100;
    int NUM_RULES = 300300;

    int MAX_ORDER = 2;

    static ControlStateManager csManager = ho.managers.Managers.csManager;
    static SCharacterManager scManager = ho.managers.Managers.scManager;

    Random r = new Random();

    public void run() {
        long start = System.currentTimeMillis();

        Cpds cpds = new Cpds();
        HashSet<Rule> rs = new HashSet<Rule>();
        for (int i = 0; i < NUM_RULES; ++i) {
            cpds.addRule(randRule());
            rs.add(randRule());
        }

        long end = System.currentTimeMillis();

        System.out.println("Adding " + cpds.getNumRules() +
                           " and " + rs.size() + " took " +
                           ((end - start) / 1000.0) +
                           " seconds.");
    }


    private ControlState randControl() {
        return csManager.makeControl("q" + r.nextInt(NUM_CONTROLS));
    }

    private SCharacter randCharacter() {
        return scManager.makeCharacter("a" + r.nextInt(NUM_STACK_CHARS));
    }

    private byte randOrder() {
        return (byte)(r.nextInt(MAX_ORDER) + 1);
    }

    private Rule randRule() {
        ControlState q1 = randControl();
        ControlState q2 = randControl();
        SCharacter a = randCharacter();
        byte k = randOrder();
        Rule rule;

        switch (r.nextInt(6)) {
            case 0:
                rule = new PushRule(q1, a, q2, k);
                break;
            case 1:
                rule = new PopRule(q1, a, q2, k);
                break;
            case 2:
                rule = new CPushRule(q1, a, q2, randCharacter(), k);
                break;
            case 3:
                rule = new RewLinkRule(q1, a, q2, randCharacter(), k);
                break;
            case 4:
                rule = new RewRule(q1, a, q2, randCharacter());
                break;
            case 5:
            default:
                rule = new CollapseRule(q1, a, q2, (byte)2);
                break;
        }

        return rule;
    }


}
