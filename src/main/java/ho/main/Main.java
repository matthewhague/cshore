/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.main;

import java.io.FileReader;

import org.apache.log4j.Logger;

import java.util.regex.Pattern;

import ho.hors.HorsProcessor;

import ho.reachability.CpdsProcessor;

public class Main {

    static Logger logger = Logger.getLogger(Main.class);

    private static boolean useOrder0ForwardsAnalysis = false;
    private static boolean useSCPDS = false;
    private static String filename = null;
    private static boolean simpleCegar = false;
    private static boolean naiveSaturation = false;

    public static boolean getUseSCPDS() { return useSCPDS; }
    public static boolean getUseOrder0ForwardsAnalysis() {
        return useOrder0ForwardsAnalysis;
    }
    public static boolean getSimpleCegar() { return simpleCegar; }
    public static boolean getNaiveSaturation() { return naiveSaturation; }

    public static void main(String[] args) throws Exception {
        logger.warn("HO: Model-Checker");
        logger.info("HO: Model-Checker");
        logger.debug("HO: Model-Checker (debug logger says hi)");

        readArgs(args);

        if (filename != null) {
            FileReader reader = new FileReader(filename);

            boolean isHors = Pattern.matches(".*\\.hors[c]?", filename);
            boolean isHrs = Pattern.matches(".*\\.hrs[c]?", filename);
            boolean isCpds = Pattern.matches(".*\\.cpds", filename);

            if(isHors){
                System.out.println(HorsProcessor.processFromFile(reader));
            } else if (isHrs) {
                System.out.println(HorsProcessor.processHrsFromFile(reader));
            } else if (isCpds) {
                new CpdsProcessor(reader);
            } else {
               System.err.println("Unrecognised file extension for " + filename);
               System.err.println("Known types:");
               System.err.println("    .hors: higher order recursion scheme");
               System.err.println("    .horsc: higher order recursion scheme with cases");
               System.err.println("    .hrs: higher order recursion scheme in Kobayashi syntax");
               System.err.println("    .cpds: collapsible pushdown system");
            }
        } else {
            System.err.println("Specify a file to read on the command line.");
        }
    }


    private static void readArgs(String[] args) {
        for (int i = 0; i < args.length; ++i) {
            if ("--help".equals(args[i]) || "-h".equals(args[i])) {
                System.out.println("Specify a filename on the command line, plus any of the following options:");
                System.out.println("    --help | -h : display this message.");
                System.out.println("    --streamlined | -s : use streamlined CPDS.");
                System.out.println("    --order-0-forwards-analysis | -0fa : use order-0 forwards analysis preprocessing step.");
                System.out.println("    --simple-cegar | -sc : use simple CEGAR algorithm that increases a bound on size of statesets in the automaton.");
            } else if ("--streamlined".equals(args[i]) ||
                       "-s".equals(args[i])) {
                useSCPDS = true;
            } else if ("--order-0-fowards-analysis".equals(args[i]) ||
                       "-0fa".equals(args[i])) {
                useOrder0ForwardsAnalysis = true;
            } else if ("--simple-cegar".equals(args[i]) ||
                       "-sc".equals(args[i])) {
                simpleCegar = true;
            } else if ("--naive-saturation".equals(args[i]) ||
                       "-ns".equals(args[i])) {
                naiveSaturation = true;
            } else {// assume filename
                filename = args[i];
            }
        }
    }

}
