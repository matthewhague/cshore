/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.parsers

import scala.util.parsing.combinator._
import ho.hors._
import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.collection.mutable.Map
import java.io.Reader
//import scala.util.parsing.combinator.Parsers.Parser


object HSymbs{
  val ID = """[a-zA-Z]([a-zA-Z0-9_'-])*"""r //good for variables/terminals/non-terminals (should exclude
          // keywords from here!)
    val typeIDInt = """[0-9]+"""r //when just a number k 
                                  //denotes type 0 => 0 =>0...=> 0 with k arguments

    val nont = "nont" // declare nonterminal
    val tml = "tml"  // declare terminal
    
    
    val whiteSpace = """(\s|//.*|(?m)\(\*(\*(?!\))|[^*])*\*\))+"""r //includes comments (* ... *) and // style 
     
    val initnt = "initnt" // declare initial non-terminals (makes sense to have more than one
              // if testing for reachable from several states)
    val failnt = "failnt" // declare failure non-terminals
    //val separator = " " // separator for list (when listing failure non-terminals but useful to have
                          // as space for when doing RHS of transitions etc.
    
    val state = "state" // declare states of automaton reading tree
      
    val initst = "initst" //declare initial states
    val failst = "failst" //declare fail states

    val domain = "domain"
    val horsCase = "case"
      
    val endMarker = ";"  
      
    val tranArrow = "->"
    val typeArrow = "=>"
    val typeDec = ":"
    val eq = "="      
    val nondet = "|"
    val obracket = "("
    val cbracket = ")"
    val comma = ","
}

object HorsParser extends RegexParsers 
                     with TypeParser 
                     with RuleParser 
                     with ListParser 
                     with PropAutParser {
    
    protected override val whiteSpace = HSymbs.whiteSpace
    
    def apply ( inJava : Reader ) : ParseResult[Hors] = {
      val hors = new Hors
      
      val failNT = hors.failNonTerms
      val initNT = hors.initNonTerms
      val failSt = hors.failPropAutStates
      val initSt = hors.initPropAutStates
      
      implicit val propTrans = hors.propAutRules
      implicit val rules = hors.rules
      implicit val domains = hors.domains
      
      implicit val stringToState = new HashMap[String, PropAutState] 
      implicit val stringToAtom = new HashMap[String, Term]
      
      val statement = (stateDec | 
                       nonTermDeclaration | 
                       tmlDeclaration | 
                       rule |
                       failNonTerms(failNT) | 
                       initNonTerms(initNT) | 
                       failStates(failSt) | 
                       initStates(initSt) | 
                       transition |
                       domainDec)
                             
        parseAll(statement.*, inJava) match {
        case Success(_, next) => Success(hors, next)
        case Failure(msg, next) => Failure(msg, next)
      }
    }


    def domainDec(implicit stringToAtom : Map[String, Term],
                           domains : Map[String, Domain]) : Parser[Unit]
        = new Parser[Unit] {
            def apply (in : Input) = {
                (HSymbs.domain ~ 
                 HSymbs.ID ~ 
                 list(HSymbs.ID) ~ 
                 HSymbs.endMarker)(in) match {
                    case Success(_ ~ id ~ ids ~ _, next) => {
                        domains.get(id) match {
                            case Some(_) => 
                                Failure("Domain " + id + " declared twice!",
                                        next)
                            case None => {
                                var values : List[String] = Nil
                                var duplicates = new HashSet[String]
                                for (v <- ids) {
                                    if (values.contains(v))
                                        duplicates += v
                                    else
                                        values = values ::: List(v)
                                }
                                if (duplicates.isEmpty) {
                                    domains.put(id, new Domain(id, values))
                                    val t = new DomainType(id)
                                    for (v <- values) 
                                        stringToAtom.put(v, new DomConst(v, t))
                                    Success((), next)
                                } else {
                                    Failure("Domain " +
                                            id + 
                                            " contains duplicates " +
                                            duplicates.mkString(", "), next)
                                }
                            }
                        }
                    }
                    case Failure(msg, next) => Failure(msg, next)
                }
            }
        }

    def nonTermDeclaration(implicit stringToAtom : Map[String, Term],
                                    domains : Map[String, Domain]) : Parser[Unit] =
        new Parser[Unit] {
            def apply(in : Input) = 
                (HSymbs.nont ~ 
                 HSymbs.ID ~ 
                 HSymbs.typeDec ~ 
                 typ ~ 
                 HSymbs.endMarker)(in) match  {
                    case Success(_ ~ id ~ _ ~ t ~_, next)  => 
                        if(stringToAtom.keySet.contains(id))
                            Failure("Duplicate introduction of identifier: " + id, next)
                        else {
                            stringToAtom.put(id, NonTerm(id, t))
                            Success((), next)
                        }
                    case Failure(msg, next) => Failure(msg, next)
                }
        }
    
    def tmlDeclaration(implicit stringToAtom : Map[String, Term],
                                domains : Map[String, Domain]) : Parser[Unit] =
        new Parser[Unit] {
            def apply(in : Input) = 
                (HSymbs.tml ~ 
                 HSymbs.ID ~ 
                 HSymbs.typeDec ~ 
                 typ ~ 
                 HSymbs.endMarker)(in) match  {
                    case Success(_ ~ id ~ _ ~ t ~ _, next)  => 
                        if(stringToAtom.keySet.contains(id))
                            Failure("Duplicate introduction of identifier: " + id, next)
                        else {
                            if(t.getOrder > 1)
                                Failure("terminal: " + id + "declared with type of order > 1: " + t, next)
                            else {
                                stringToAtom.put(id, Terminal(id, t))
                                Success((), next)
                            }
                        }
                    case Failure(msg, next) => Failure(msg, next)
                }
        }
    
    def initNonTerms(initNonTerms : HashSet[NonTerm])
                    (implicit stringToAtom : Map[String, Term]) : Parser[Unit] =
        new Parser[Unit] {
            def apply( in : Input) = {
                (HSymbs.initnt ~ 
                 list(nonTermAtom) ~ 
                 HSymbs.endMarker)(in) match {
                    case Success(_ ~ nonTerms ~ _ , next) => {
                        val invalidInitials = new HashSet[NonTerm]
                        for(nonTerm <- nonTerms){
                            if(nonTerm.getOrder != 0)
                                invalidInitials.add(nonTerm)
                            else
                                {initNonTerms.add(nonTerm)}
                        }
                        if(invalidInitials.isEmpty)
                            Success((), next)
                        else
                            Failure("Cannot have non-terminals with nonzero order as initial: " + 
                                    invalidInitials,
                                    next)
                    }
                    case Failure(msg, next) => Failure(msg, next)  
                }
            }
        }
    
    def failNonTerms(failNonTerms : HashSet[NonTerm])(implicit stringToAtom : Map[String, Term]) 
        : Parser[Unit] =
            HSymbs.failnt ~ list(nonTermAtom) ~ HSymbs.endMarker ^^
            {case _ ~ nonTerms ~ _ => for(nonTerm <- nonTerms)yield failNonTerms.add(nonTerm)}
    
}

trait PropAutParser extends RegexParsers with ListParser with TermParser {
    def stateDec(implicit stringToState : Map[String, PropAutState]) : Parser[Unit]
            = new Parser[Unit] {
                def apply (in : Input) = {
                      (HSymbs.state ~  list(HSymbs.ID) ~ HSymbs.endMarker)(in) match
                      {case Success( _ ~ ids ~ _, next) => 
                        {
                            val alreadyDeclared = new HashSet[String]
                            for (id <- ids) {if(stringToState.keySet.contains(id)) alreadyDeclared.add(id);
                                                stringToState.put(id, PropAutState(id))}
                            if(alreadyDeclared.isEmpty) 
                                Success((), next)
                            else
                                Failure("Duplicate declaration of prop. aut. states: " +
                                            alreadyDeclared.toString, next)
                        }
                      case Failure(msg, next) => Failure(msg, next)
                     }
                }
      
    }
    
    def state(implicit stringToState : Map[String, PropAutState]) : Parser[PropAutState] 
            = new Parser[PropAutState] {
                def apply (in : Input) = {
                    HSymbs.ID(in) match {
                      case Success(id, next) => 
                            stringToState.get(id) match {
                              case Some(q) => Success(q, next)
                              case None => Failure("Prop. aut state undeclared: " + id, next)
                            }    
                      
                      case Failure(msg, next) => Failure(msg, next)
                                
                    }
                }
    }
    
    def failStates(failSt : HashSet[PropAutState])(implicit stringToState : Map[String, PropAutState]) 
        : Parser[Unit] =
            HSymbs.failst ~ list(state) ~ HSymbs.endMarker ^^
            {case _ ~ states ~ _ => for(state <- states)yield failSt.add(state)}
    
    def initStates(initSt : HashSet[PropAutState])(implicit stringToState : Map[String, PropAutState]) 
        : Parser[Unit] =
            HSymbs.initst ~ list(state) ~ HSymbs.endMarker ^^
            {case _ ~ states ~ _ => for(state <- states)yield initSt.add(state)}
    
    def transition(implicit stringToState : Map[String, PropAutState], 
        propTrans : Map[(PropAutState, Terminal, Int), PropAutState],
        stringToTerm : Map[String, Term]) : Parser[Unit] = 
              new Parser[Unit] {
                def apply (in : Input) = {
                    (state ~ HSymbs.tranArrow ~ tmlAtom ~ list(state) ~ HSymbs.endMarker)(in) match {
                      case Success(state ~ _ ~ tml ~ nextStates ~ _, next) => 
                        if(propTrans.keySet.contains((state, tml, 1)))
                          Failure("Transition from: " + state + " on terminal: " + tml +
                              " already defined.", next)
                        else {
                            if(nextStates.length != tml.getArity)
                            { 
                              Failure("Transition from: " + state + "on terminal: " + tml +
                                  " with arity: " + tml.getArity + " has incorrect number of child" +
                                          "states: " + nextStates.length, next)
                            }
                            else     {
                                var count = 1
                                for(nextState <- nextStates)
                                    {propTrans.put((state, tml, count), nextState); count = count + 1}
                                Success((), next)
                            }
                            
                        }
                      case Failure(msg, next) => Failure(msg, next)
                    }  
                }
    }
              
    
    
            
}

trait TypeParser extends RegexParsers {
    // This expects positive byte (should be enforced by regular expressions when parsing)
    // (Treating as Int rather than Byte due to type inference issue).
    private def getO1Type( b : Int ) : Type = b match { 
      case 0 => new Ground
      case _ => new Arrow(new Ground, getO1Type( b -1 ))
    }
    
    private def O1Parser : Parser[Type] = 
        HSymbs.typeIDInt ^^
            { case b => getO1Type(java.lang.Integer.parseInt(b)) }
    
    
    private def DomParser(implicit domains : Map[String, Domain]) : Parser[Type] = 
        new Parser[Type] {
            def apply(in : Input) = {
                (HSymbs.ID)(in) match {
                    case Success(d, next) =>
                        domains.get(d) match {
                            case Some(dom) => Success(new DomainType(dom), next)
                            case None => 
                                Failure("Domain " + d + 
                                        " not declared before use.",
                                        next)
                        }
                    case Failure(msg, next) => Failure(msg, next)
                }
            }
        }
    
            
    private def assocUnit(implicit domains : Map[String, Domain]) : Parser[Type] = (
        O1Parser 
    |   DomParser
    | (HSymbs.obracket ~ typ ~ HSymbs.cbracket) ^^ {case _ ~ t ~ _ => t} 
    )
    
    def typ(implicit domains : Map[String, Domain]) : Parser[Type] = (
        (assocUnit ~ HSymbs.typeArrow ~ typ) ^^ {
            case arg ~ _ ~ ret => new Arrow(arg, ret)
        } 
    | assocUnit)
    
     
}


trait TermParser extends RegexParsers with ListParser{
   
   def variable(nonTerm : NonTerm, vars : Map[String, (Int, Type)]) : Parser[Term] = 
           new Parser[Term] {
                   def apply (in : Input) = {
                       {
                           HSymbs.ID(in) match {
                             case Success(result, next) => 
                               vars.get(result) match {
                                   case Some((i, t)) => Success(Var(nonTerm, i, result, t), next) 
                                   case None => Failure("id is not variable (this message " +
                                           "should never be printed, if it is we have" +
                                           "a bug in the parser)", next)
                               }
                             case Failure(msg, next) => Failure(msg, next)
                   }
                           }
                       } 
                   }

   def nonVarAtom(implicit stringToAtom : Map[String, Term]) : Parser[Term] =
           new Parser[Term] {
                   def apply (in : Input) = {
                       HSymbs.ID(in) match
                       {
                         case Success(result, next) =>
                           stringToAtom.get(result) match {
                             case Some(t) => Success(t, next)
                             case None => Failure("Unknown identifier: " + result, next)
                           }
                         case Failure(msg, next) => Failure(msg, next)
                       }
                   }
               }
   
   def nonTermAtom(implicit stringToAtom : Map[String, Term]) : Parser[NonTerm] = 
     new Parser[NonTerm] {
       def apply (in : Input) = {
           nonVarAtom(stringToAtom)(in) match {
               case Success(NonTerm(id, t), next) => Success(NonTerm(id, t), next)
               case Success(otherAtom, next) => Failure("Expected non-terminal but got something else: " + 
                   otherAtom.toString, next)
               case Failure(msg, next) => Failure(msg, next)
           }
       }
   }
   
   def tmlAtom(implicit stringToAtom : Map[String, Term]) : Parser[Terminal] = 
     new Parser[Terminal] {
       def apply (in : Input) = {
           nonVarAtom(stringToAtom)(in) match {
               case Success(Terminal(id, t), next) => Success(Terminal(id, t), next)
               case Success(otherAtom, next) => Failure("Expected terminal but got something else: " + 
                   otherAtom.toString, next)
               case Failure(msg, next) => Failure(msg, next)
           }
       }
   }

   def domConstAtom(implicit stringToAtom : Map[String, Term]) : Parser[DomConst] = 
     new Parser[DomConst] {
       def apply (in : Input) = {
         nonVarAtom(stringToAtom)(in) match {
           case Success(DomConst(id, t), next) => 
             Success(DomConst(id, t), next)
           case Success(otherAtom, next) => 
             Failure("Expected domain constant but got something else: " + 
                     otherAtom.toString, 
                     next)
           case Failure(msg, next) => Failure(msg, next)
         }
      }
   }

    def caseStmt (nonTerm : NonTerm, vars : Map[String, (Int, Type)])
                 (implicit stringToAtom : Map[String, Term],
                           domains : Map[String, Domain]) : Parser[Term] = 
        new Parser[Term] {
            def apply (in : Input) = {                 
                (HSymbs.horsCase ~ 
                 HSymbs.obracket ~ 
                 term(nonTerm, vars) ~
                 HSymbs.comma ~ 
                 cslist(term(nonTerm, vars)) ~
                 HSymbs.cbracket)(in) match  {
                    case Success(_ ~ _ ~ cond ~ _ ~ branches ~ _, 
                                 next) => 
                        constructCase(cond, branches) match {
                            case GoodCase(c) => Success(c, next)
                            case BadCase(msg) => Failure(msg, next)

                        }
                    case Failure(msg, next) => Failure(msg, next)
                }
            }
        }

    def constructCase(cond : Term, branches : List[Term]) : CaseAttempt = {
        cond.typ match {
            case DomainType(dom) => {
                if (dom.getValues.size == branches.size) {
                    branches match {
                        case bh::bt  => { 
                            var msg = ""
                            for (b <- bt) {
                                if (bh.typ != b.typ) {
                                    msg = "Inconsistent branch type in case statement:" +
                                          bh + " vs. " + b
                                }
                            }
                            if (msg == "")
                                GoodCase(new CaseTerm(cond, 
                                                      branches,
                                                      bh.typ))
                            else
                                BadCase(msg)
                              
                        }
                        case Nil => BadCase("Case statement has no branches.")
                    }
                } else {
                    BadCase("Case statement has too few branches.")
                }
            }

            case _ => BadCase("Case statement conditional must be of domain type: " + 
                              cond)
        }
    }

  
   
    def assocUnit(nonTerm : NonTerm, vars : Map[String,  (Int, Type)])
                 (implicit stringtoAtom : Map[String, Term],
                           domains : Map[String, Domain]) : Parser[Term] = (
        variable(nonTerm, vars) 
    |   nonVarAtom 
    |   caseStmt(nonTerm, vars)
    |   (HSymbs.obracket ~ term(nonTerm, vars) ~ HSymbs.cbracket) 
            ^^ {case _ ~ t ~ _ => t}
    )
   
  
   val makeApp : (Term, List[Term]) => AppAttempt = (fun : Term, args : List[Term]) => {
                          args match {
                               case Nil => GoodApp(fun)
                               case arg :: rest => fun.applyTo(arg) match {
                                 case GoodApp(t) => makeApp(t, rest) 
                                 case BadApp(msg) => BadApp(msg)
                               }
                             }
                     }
   
   
   
   //This parser doesn't handle non-determinism operator.
   def simpleTerm (nonTerm : NonTerm, vars : Map[String,  (Int, Type)])
                  (implicit stringToAtom : Map[String, Term],
                            domains : Map[String, Domain]) : Parser[Term] = 
           new Parser[Term] {
               def apply(in : Input) = {
                 (assocUnit(nonTerm, vars) ~ list(assocUnit(nonTerm, vars)))(in) match {
                   case Success(fun ~ argList, next) => { 
                     makeApp(fun, argList) match {
                       case GoodApp(t) => Success(t, next)
                       case BadApp(msg) => Failure(msg, next)
                     }    
                    
                   }
                   case Failure(msg, next) => Failure(msg, next)
                 }
               }
   }
  
    
    def nonDetParser (nonTerm : NonTerm, vars : Map[String,  (Int, Type)])
                     (implicit stringToAtom : Map[String, Term],
                               domains : Map[String, Domain]) : Parser[Term] =    
                 new Parser[Term] {
                def apply(in : Input) = {
                  
                  (simpleTerm(nonTerm, vars) ~ HSymbs.nondet ~ term(nonTerm, vars))(in) match {
                    case Success(t1 ~ _ ~ t2, next) => {
                      
                      if(t1.typ == t2.typ)
                        Success(NonDet(t1, t2, t1.typ), next)
                       else
                         Failure("Different types for terms in non-deterministic choice: " +
                                  t1.toString + " : " + t1.typ.toString + " and " +
                                  t2.toString + " : " + t2.typ.toString, next)
                    }
                      case Failure(msg, next) => Failure(msg, next)
                  }
                   
                  } 
                }
    
    
    /**
    * @param nonTerm    The non-terminal on LHS of rule of which parsed term is RHS
    * @param vars    Map from variable names to position in non-terminal on LHS and type
    * @return     Parser result including parsed simple term
    */
   def term (nonTerm : NonTerm, vars : Map[String,  (Int, Type)])
            (implicit stringToAtom : Map[String, Term],
                      domains : Map[String, Domain]) : Parser[Term] =
            // NOTE: if simpleTerm comes first, then parsing non-det terms fails
            //       if nondetparser comes first then parsing simple terms is
            //       prohibitively slow on large examples.  Since we don't use
            //       non-det parser for our benchmarks, i'm plumping for this
            //       order 
            simpleTerm(nonTerm, vars) | nonDetParser(nonTerm, vars)
                   
}

trait RuleParser extends RegexParsers with TermParser {
    
  def ruleLHS(implicit stringToAtom : Map[String, Term]) : Parser[(NonTerm, Map[String, (Int, Type)])] =
     new Parser[(NonTerm, Map[String, (Int, Type)])] {
       def apply(in : Input) = {nonVarAtom(stringToAtom)(in) match {
                   case Success(NonTerm(a,t), next) => 
                     
                       variableList(NonTerm(a, t), 1, t, new HashMap)(next) match {
                         case Success(vars, next) => Success((NonTerm(a, t), vars), next)
                         case Failure(msg, next) => Failure(msg, next)
                       }
                       
                     
                     
                   case Success(_, next) => 
                     Failure("LHS of rule does not begin with previously declared non-terminal.", next)
                   case Failure(msg, next) => 
                     Failure(msg, next)
                 }
             }
         }
  
  def variableList(nonTerm : NonTerm, pos : Int, remainingType : Type, vars : Map[String, (Int, Type)]) 
       : Parser[Map[String, (Int, Type)]] =
             new Parser[Map[String, (Int, Type)]] {
                 def apply (in : Input) = {
                   (HSymbs.ID | HSymbs.eq)(in) match {
                     case Success(HSymbs.eq, next) => remainingType match {
                       case DomainType(_) => Success(vars, next)
                       case Ground() => Success(vars, next)
                       case Arrow(arg, ret) => Failure("Too few variables in LHS " +
                                                           "of definition of: " + nonTerm, next)
                     }
                     case Success(id, next) => remainingType match {
                       
                       case Arrow(arg, ret) => if(vars.keySet.contains(id))
                                                       Failure("Duplicate variable id: " + id +
                                                               "in definition of non-terminal: " + nonTerm, next)
                                                 else {
                                                   vars.put(id, (pos, arg))
                                                   variableList(nonTerm, pos + 1, ret, vars)(next)
                                                 }
                       case _ => Failure("Too many variables in LHS of definition of: " + nonTerm, next)
                      
                     }
                     case Failure(msg, next) => Failure(msg, next)
                   }
                 }
               
             }
        
      def rule(implicit rules : Map[NonTerm, Term],  
                        stringToAtom : Map[String, Term],
                        domains : Map[String, Domain]) : Parser[Unit] =
     // Note that ruleLHS will parse the HSymbs.eq symbol marks end of variable list)
           new Parser[Unit] { 
                   def apply( in : Input) = {
                       ruleLHS(stringToAtom)(in) match 
                                 {
                                     case Success((nonTerm, vars), next) => 
                                       {//println("vars: " + vars.mkString("\n"))
                                         (term(nonTerm, vars)(stringToAtom, domains) ~ HSymbs.endMarker)(next)} 
                                         match {
                                           case Success(term ~ _, next) => 
                                             { 
                                               if(rules.keySet.contains(nonTerm))
                                                 Failure("Duplicate definition of: " + nonTerm, next)
                                               else {
                                                   if(term.getOrder == 0.toByte) {
                                                       rules.put(nonTerm, term)
                                                       Success((), next)
                                               
                                                   }
                                                   else {
                                                       Failure("RHS of definition of: " + nonTerm +
                                                           " does not have type of order-0, it has type: " +
                                                           nonTerm.typ.toString, next)
                                                  
                                                   }
                                               }
                                             }
                                           case Failure(msg, next) => Failure(msg, next)
                                         }
                                     case Failure(msg, next) => Failure(msg, next)
                                 }
                   }
   }
  
}
