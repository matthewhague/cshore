/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.parsers

import scala.util.parsing.combinator._;	

import scala.collection.JavaConversions.asJavaCollection;

import java.util.Set;
import java.util.HashSet;
import java.io.Reader;
import java.lang.Exception

import ho.managers.Managers;

import ho.structures.cpds._;

object CSymbs {
    // a character guaranteed not to appear in user input IDs of control states,
    // stack characters, &c.
    def nonIDChar : Char = '#'
}

class CpdsParseException(msg : String) extends Exception(msg) {
}

class CpdsParser extends RegexParsers with ListParser {
    val csManager = Managers.csManager;
    val scManager = Managers.scManager;

    //includes comments (* ... *) and // style 
    protected override val whiteSpace 
        = """(\s|//.*|(?m)\(\*(\*(?!\))|[^*])*\*\))+"""r 

    val ID = """[a-zA-Z]([a-zA-Z0-9_'-])*"""r

    val NUM = """[0-9][0-9]*"""r

    def ord : Parser[Byte] = 
        NUM ^^ { 
            case b => val v = java.lang.Byte.parseByte(b)
                      if (v == 0) 
                          throw new CpdsParseException("Order-0 operation encountered.")
                      else 
                          v
        }

    def highord : Parser[Byte] = 
        NUM ^^ { 
            case b => val v = java.lang.Byte.parseByte(b)
                      if (v <= 1) 
                          throw new CpdsParseException("Order-" + b +
                                                       " found when order > 1 required.")
                      else
                          v
        }


    def height: Parser[Int] = 
        NUM ^^ { case b => java.lang.Integer.parseInt(b) }


    def control : Parser[ControlState] = (
      ID ^^ { case c => csManager.makeControl(c) }
    )

    def schar : Parser[SCharacter] = (
      ID ^^ { case sc => scManager.makeCharacter(sc) }
    )

    def cpds : Parser[Cpds] = (
        rule ~ cpds ^^ { case rule ~ cpds => cpds.addRule(rule); cpds }
    | "initcs" ~ control ~ cpds
        ^^ { case _ ~ c ~ cpds => cpds.setInitControlState(c); cpds }
    | "initsc" ~ schar ~ cpds
        ^^ { case _ ~ c ~ cpds => cpds.setInitCharacter(c); cpds }
    | "" ^^^ new Cpds
    ) 



    
    def rule : Parser[Rule] = (
        "(" ~ control ~ schar ~ "push(" ~ highord ~ ")" ~ control ~ ")" 
            ^^ { case _ ~ q1 ~ a ~ _ ~ k ~ _ ~ q2 ~ _ 
                     => new PushRule(q1, a, q2, k) }

    |   "(" ~ control ~ schar ~ "pop(" ~ ord ~ ")" ~ control ~ ")" 
            ^^ { case _ ~ q1 ~ a ~ _ ~ k ~ _ ~ q2 ~ _ 
                     => new PopRule(q1, a, q2, k) }

    |   "(" ~ control ~ schar ~ "collapse(" ~ highord ~ ")" ~ control ~ ")" 
            ^^ { case _ ~ q1 ~ a ~ _ ~ k ~ _ ~ q2 ~ _ 
                     => new CollapseRule(q1, a, q2, k) }

    |   "(" ~ control ~ schar ~ "rew(" ~ schar ~ ")" ~ control ~ ")" 
            ^^ { case _ ~ q1 ~ a ~ _ ~ b ~ _ ~ q2 ~ _ 
                     => new RewRule(q1, a, q2, b) }

    |   "(" ~ control ~ schar ~ "push(" ~ schar ~ ")" ~ "(" ~ highord ~ ")" ~ control ~ ")" 
            ^^ { case _ ~ q1 ~ a ~ _ ~ b ~ _ ~ _ ~ k ~ _ ~ q2 ~ _ 
                     => new CPushRule(q1, a, q2, b, k) }

    |   "(" ~ control ~ schar ~ "push(" ~ schar ~ ")" ~ control ~ ")" 
            ^^ { case _ ~ q1 ~ a ~ _ ~ b ~ _ ~ q2 ~ _ 
                     => new CPushRule(q1, a, q2, b, 1.toByte) }

    |   "(" ~ control ~ schar ~ "rew(" ~ schar ~ ")" ~ "(" ~ ord ~ ")" ~ control ~ ")" 
            ^^ { case _ ~ q1 ~ a ~ _ ~ b ~ _ ~ _ ~ k ~ _ ~ q2 ~ _ 
                     => new RewLinkRule(q1, a, q2, b, k) }
    |   "(" ~ control ~ schar ~ headset ~ ")"
            ^^ { case _ ~ q1 ~ a ~ hs ~ _
                    => new AltRule(q1, a, hs) }
    )

    def headset : Parser[Set[StackHead]] = (
        "{" ~ headsetend
            ^^ { case _ ~ hs => hs }
    )

    def headsetend : Parser[Set[StackHead]] = (
        head ~ headsetend
            ^^ { case h ~ hs => hs.add(h); hs }
    |   head ~ "}"
            ^^ { case h ~ _ => var hs = new HashSet[StackHead]
                               hs.add(h)
                               hs }
    )

    def head : Parser[StackHead] = (
        "(" ~ control ~ schar ~ ")"
            ^^ { case _ ~ q ~ a ~ _ => new StackHead(q, a) }
    )

    def stackend : Parser[CpdsStack] = (
     "](" ~ ord ~ ")" ~ stackend
        ^^ { case _ ~ k ~ _ ~ s => 
                if (!s.pushEmpty(k))
                    throw new CpdsParseException("Cannot add empty " + 
                                                 k + 
                                                 "-stack to " + 
                                                 s)
                s }
    | "](" ~ ord ~ ")" ^^ { case _ ~ k ~ _ => new CpdsStack(k) }
    | schar ~ stackend 
        ^^ { case a ~ s =>
                if (!s.pushCharLink(a, 1, 0))
                    throw new CpdsParseException("Cannot add " +
                                                 a + " " +
                                                 "to top of " +
                                                 s)
                s }
    | "(" ~ schar ~ ord ~ height ~ ")" ~ stackend
        ^^ { case _ ~ a ~ k ~ h ~ _ ~ s => 
                if (!s.pushCharLink(a, k, h))
                    throw new CpdsParseException("Cannot add (" + 
                                                 a + " " +
                                                 k + " " +
                                                 h + ") " +
                                                 "to top of " +
                                                 s)
                s }
    | "[" ~ stackend 
        ^^ { case _ ~ s => s }
    )

    def stack : Parser[CpdsStack] = (
      "[" ~ stackend
        ^^ { case _ ~ s => s }
    )

    
    def config : Parser[CpdsConfig] = (
      "<" ~ control ~ stack ~ ">"
        ^^ { case _ ~ c ~ s ~ _ => new CpdsConfig(c, s, s.getOrder()) }
    )


    def controlpair : Parser[Tuple2[ControlState, ControlState]] = (
        "(" ~ control ~ control ~ ")"
            ^^ { case _ ~ q1 ~ q2 ~ _ => new Pair(q1, q2) }
    )


    def getCpds(reader: Reader) = {
        val parsed = parseAll(cpds, reader)
        parsed match {
            case Failure(msg, where) => {
                println("parser failure: " + where + " : " + msg)
                throw new CpdsParseException("CPDS Parser failure: " + 
                                             where + " : " + msg)
            }
            case _ => {
                parsed.get
            }
        }
    }

    def getRule(reader: Reader) = {
        val parsed = parseAll(rule, reader)
        parsed match {
            case Failure(msg, where) => {
                println("parser failure: " + where + " : " + msg)
                throw new CpdsParseException("CPDS Parser failure: " + 
                                             where + " : " + msg)
            }
            case _ => {
                parsed.get
            }
        }
    }

    def getConfig(reader: Reader) = {
        val parsed = parseAll(config, reader)
        parsed match {
            case Failure(msg, where) => {
                throw new CpdsParseException("SAParser failure: " + 
                                             where + " : " + msg)
            }
            case _ => {
                parsed.get
            }
        }
    }

    def getStack(reader: Reader) = {
        val parsed = parseAll(stack, reader)
        parsed match {
            case Failure(msg, where) => {
                throw new CpdsParseException("SAParser failure: " + 
                                             where + " : " + msg)
            }
            case _ => {
                parsed.get
            }
        }
    }



}
