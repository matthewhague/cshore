/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



// We're copy-pasting HorsParser rather than trying to re-use code (which would
// be super messy i think)


package ho.parsers

import scala.util.parsing.combinator._
import scala.util.parsing.input.CharArrayReader
import ho.hors._
import scala.collection.mutable.LinkedHashMap
import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.collection.mutable.Map
import java.io.Reader
//import scala.util.parsing.combinator.Parsers.Parser


object HrsSymbs{
    val ID = """[a-zA-Z]([a-zA-Z0-9_'-])*"""r //good for variables/terminals/non-terminals (should exclude
          // keywords from here!)
    val whiteSpace = """(\s|//.*|(?m)/\*(\*(?!/)|[^*])*\*/)+""".r //includes comments (* ... *) and // style 
     
    val RHSSTRING = """[\sa-zA-Z0-9_'-(),]*"""r 
    
    val endMarker = "."  
    val tranArrow = "->"
    val ruleDef = """->|="""r      
    val obracket = "("
    val cbracket = ")"
    val comma = ","

    val domain = "domain"
    val horsCase = "case"

    val beginHors = "%BEGING"
    val endHors = "%ENDG"
    val beginAut = "%BEGINA"
    val endAut = "%ENDA"
}

class HrsParseException(msg : String) extends Exception(msg) {
}

/**
 * Do a first pass parse of a HORS where we do not interpret the RHS (just keep
 * a string).  On the second pass we turn the RHS to UntypedTerms.  We need to
 * do this in two passes so that we can know what's a non-term and what's a term
 * (non-terms will have a rule).
 */
class FirstPassHrs {
    val initNonTerms = new HashSet[UTNonTerm]

    val idToNonTerm = new HashMap[String, UTNonTerm]
    val nonTermToArgs = new HashMap[UTNonTerm, LinkedHashMap[String, UTVar]]
    val nonTermToArgsList = new HashMap[UTNonTerm, List[UTVar]]
    val nonTermToRHS = new HashMap[UTNonTerm, String]
    var domains : List[Domain] = Nil

    def addNonTermVar(id : String,
                      nt : UTNonTerm,
                      args : LinkedHashMap[String, UTVar],
                      rhs : String) = {
        if (idToNonTerm.keySet.contains(id)) {
            throw new HrsParseException("Tried to add two copied of non-terminal " + id)
        } else {
            idToNonTerm.put(id, nt)
            nonTermToArgs.put(nt, args)
            nonTermToRHS.put(nt, rhs)
        }
    }

    def addDomain(id : String, vals : List[String]) {
        domains = new Domain(id, vals) :: domains
    }

    def getNonTerm(id : String) : Option[UTNonTerm] = idToNonTerm.get(id)

    def getArgs(nt : UTNonTerm) : Option[LinkedHashMap[String, UTVar]] = {
        nonTermToArgs.get(nt)
    }

    def getRHS(nt : UTNonTerm) : Option[String] = nonTermToRHS.get(nt)

}


object HrsParser extends RegexParsers 
                    with HrsPropAutParser
                    with ListParser {
    
    protected override val whiteSpace = HrsSymbs.whiteSpace
    
    def apply(inJava : Reader) : ParseResult[Hrs] = {
        implicit val hrs = new Hrs
        implicit val stringToState = new HashMap[String, PropAutState] 
        implicit val stringToAtom = new HashMap[String, UntypedTerm]

        parseAll(hrsDef ~ autDef, inJava) match {
            case Success(_, next) => {
                // add terminals to hrs
                for (atom <- stringToAtom.values) {
                    atom match {
                        case UTTerminal(id) => hrs.terminals.add(UTTerminal(id))
                        case _ => ()
                    }
                }
                Success(hrs, next)
            }
            case Failure(msg, next) => Failure(msg, next)
        }
    }




    def hrsDef(implicit hrs : Hrs,
                        stringToAtom : Map[String, UntypedTerm],
                        stringToState : Map[String, PropAutState]) : Parser[Unit] = (
        firstPassHors 
            ^^ { case fpHors => {
                    doSecondPassOnHrs(fpHors)
                 }
               }
    )



    def firstPassHors : Parser[FirstPassHrs] = (
        rep(domDef) ~ HrsSymbs.beginHors ~ fpHorsRule ~ fpHorsBody ~ HrsSymbs.endHors
            ^^ { case domList ~ _ ~ ((id, nt, vars, rhs)) ~ fpHors ~ _ => {
                    fpHors.initNonTerms.add(nt)
                    fpHors.addNonTermVar(id, nt, vars, rhs)
                    for ((domId, domVals) <- domList)
                        fpHors.addDomain(domId, domVals)
                    fpHors
                 } 
               }
    |   HrsSymbs.beginHors ~ HrsSymbs.endHors
            ^^ { case _ ~ _ => new FirstPassHrs }
    )

    def domDef : Parser[(String, List[String])] = (
        HrsSymbs.domain ~ HrsSymbs.ID ~ rep(HrsSymbs.ID) ~ HrsSymbs.endMarker
            ^^ { case _ ~ id ~ ids ~ _ => {
                    var values : List[String] = Nil
                    var duplicates = new HashSet[String]
                    for (v <- ids) {
                        if (values.contains(v))
                            duplicates += v
                        else
                            values = values ::: List(v)
                    } 
                    if (duplicates.isEmpty) 
                        (id, values)
                    else 
                        throw new HrsParseException("Domain " +
                                                    id + 
                                                    " contains duplicates " +
                                                    duplicates.mkString(", "))
                 }
               }
    )

    def fpHorsBody : Parser[FirstPassHrs] = (
        rep(fpHorsRule)
            ^^ { case ruleList => {
                    val fpHors = new FirstPassHrs
                    for ((id, nt, vars, rhs) <- ruleList)
                        fpHors.addNonTermVar(id, nt, vars, rhs)
                    fpHors
                 }
               }
    )

    def fpHorsRule : Parser[(String, 
                             UTNonTerm, 
                             LinkedHashMap[String, UTVar],
                             String)] = (
        HrsSymbs.ID ~ 
        list(HrsSymbs.ID) ~ 
        HrsSymbs.ruleDef ~ 
        HrsSymbs.RHSSTRING ~
        HrsSymbs.endMarker 
            ^^ { case id ~ argList ~ _ ~ rhs ~ _ => {
                    val nt = new UTNonTerm(id)
                    val args = new LinkedHashMap[String, UTVar]
                    for ((arg, i) <- argList.zipWithIndex) {
                        args.put(arg, new UTVar(nt, i+1, arg))   
                    }
                    (id, UTNonTerm(id), args, rhs)
                 }
               }
    )

    def doSecondPassOnHrs(fpHors : FirstPassHrs) 
                         (implicit hrs : Hrs,
                                   stringToAtom : Map[String, UntypedTerm],
                                   stringToState : Map[String, PropAutState]) = {
        // domains
        for (dom <- fpHors.domains) {
            // TODO: add to string to atom all of the values
            hrs.addDomain(dom)
            for (v <- dom.getValues)
                stringToAtom.put(v, new UTDomConst(v))
        }
        // init non-terms
        for (nt <- fpHors.initNonTerms)
          hrs.initNonTerms += nt
        // put non terminals in stringToAtom
        for ((nt, _) <- fpHors.nonTermToRHS) {
            stringToAtom.put(nt.id, nt)
        }
        // get rules
        for ((nt, stringRHS) <- fpHors.nonTermToRHS) {
            val args = fpHors.nonTermToArgs.get(nt) match {
                           case Some(a) => a
                           case None => new LinkedHashMap[String, UTVar]
                       }
            val (_, argsList) = args.toList.unzip
            val rhsReader = new CharArrayReader(stringRHS.toCharArray)
            val parsedRHS = parseAll(term(nt, args), rhsReader)
            val rhs = parsedRHS match {
                          case Failure(msg, where) => {
                              throw new HrsParseException("Hrs Parser failure: " + 
                                                          where + " : " + msg)
                          }
                          case _ =>  parsedRHS.get
                      }
            hrs.rules.put(nt, (argsList, rhs))
        }
    }


}

trait HrsPropAutParser extends RegexParsers 
                          with HrsTermParser
                          with ListParser {

    def autDef(implicit hrs : Hrs,
                        stringToAtom : Map[String, UntypedTerm],
                        stringToState : Map[String, PropAutState]) : Parser[Unit] = (
        HrsSymbs.beginAut ~ transition ~ autBody ~ HrsSymbs.endAut
            ^^ { case _ ~ ((s, a, ss)) ~ _ ~ _ => {
                    addTransition(s, a, ss)
                    val _ = hrs.initPropAutStates.add(s)
                 }
               }
    |   HrsSymbs.beginAut ~ HrsSymbs.endAut
            ^^ { case _ ~ _ => () }
    )

    def autBody(implicit hrs : Hrs,
                         stringToAtom : Map[String, UntypedTerm],
                         stringToState : Map[String, PropAutState]) : Parser[Unit] = (
        transition ~ autBody 
            ^^ { case ((s, a, ss)) ~ _ => addTransition(s, a, ss) }
    |   "" ^^^ ()
    )

    def transition(implicit stringToState : Map[String, PropAutState],
                            stringToAtom : Map[String, UntypedTerm],
                            hrs : Hrs) : Parser[(PropAutState,
                                                 UTTerminal,
                                                 List[PropAutState])] = (
        state ~ 
        tmlAtom ~ 
        HrsSymbs.tranArrow ~ 
        list(state) ~ 
        HrsSymbs.endMarker 
            ^^ { case state ~ tml ~ _ ~ dests ~ _ => (state, tml, dests) }
    )

    def addTransition(state : PropAutState,
                      tml : UTTerminal,
                      dests : List[PropAutState])
                     (implicit hrs : Hrs) = {
        if(hrs.propAutRules.keySet.contains((state, tml))) {
            throw new HrsParseException("Transition from: " + 
                                        state + 
                                        " on terminal: " + 
                                        tml +
                                        " already defined.")
        } else {
            val _ = hrs.propAutRules.put((state, tml), dests)
        }
    }

    def state(implicit stringToState : Map[String, 
                                           PropAutState]) : Parser[PropAutState] = (
        HSymbs.ID ^^ { case id => {
                            getState(id)                            
                       }
                     }
    )


    def getState(id : String)
                (implicit stringToState : Map[String, PropAutState]) = {
        stringToState.get(id) match {
            case Some(s) => s
            case None => { 
                val s = PropAutState(id)
                stringToState.put(id, s)
                s
            }
        }
    }

}



trait HrsTermParser extends RegexParsers 
                       with ListParser {
   
    def variable(nonTerm : UTNonTerm, vars : Map[String, UTVar]) : Parser[UntypedTerm] = 
        new Parser[UntypedTerm] {
            def apply (in : Input) = {
                HrsSymbs.ID(in) match {
                    case Success(result, next) => 
                        vars.get(result) match {
                            case Some(v) => Success(v, next) 
                            case None => Failure("id is not variable (this message " +
                                                 "should never be printed, if it is we have" +
                                                 "a bug in the parser)", next)
                        }
                    case Failure(msg, next) => Failure(msg, next)
                }
            }
        } 


    def nonVarAtom(implicit stringToAtom : Map[String, UntypedTerm]) : Parser[UntypedTerm] = (
        HrsSymbs.ID 
            ^^ { case id => {
                    implicit val args = new LinkedHashMap[String, UTVar]
                    getAtom(id)
                 }
               }
    )

    def atom(implicit stringToAtom : Map[String, UntypedTerm],
                      args : Map[String, UTVar]) : Parser[UntypedTerm] = (
        HrsSymbs.ID 
            ^^ { case id => getAtom(id) }
    )

    def getAtom(id : String)
               (implicit stringToAtom : Map[String, UntypedTerm],
                         args : Map[String, UTVar]) : UntypedTerm = {
        args.get(id) match {
            case Some(atm) => atm
            case None => {
                stringToAtom.get(id) match {
                    case Some(atm) => atm
                    case None => {
                        // it's a terminal we haven't seen before
                        val tml = UTTerminal(id)
                        stringToAtom.put(id, tml)
                        tml
                    }
                }
            }
        }
    }
   
   def nonTermAtom(implicit stringToAtom : Map[String, UntypedTerm]) : Parser[UTNonTerm] = 
     new Parser[UTNonTerm] {
       def apply (in : Input) = {
           nonVarAtom(stringToAtom)(in) match {
               case Success(UTNonTerm(id), next) => Success(UTNonTerm(id), next)
               case Success(otherAtom, next) => Failure("Expected non-terminal but got something else: " + 
                   otherAtom.toString, next)
               case Failure(msg, next) => Failure(msg, next)
           }
       }
   }
   
   def tmlAtom(implicit stringToAtom : Map[String, UntypedTerm]) : Parser[UTTerminal] = 
     new Parser[UTTerminal] {
       def apply (in : Input) = {
           nonVarAtom(stringToAtom)(in) match {
               case Success(UTTerminal(id), next) => Success(UTTerminal(id), next)
               case Success(otherAtom, next) => Failure("Expected terminal but got something else: " + 
                   otherAtom.toString, next)
               case Failure(msg, next) => Failure(msg, next)
           }
       }
   }


   def caseStmt(nonTerm : UTNonTerm, vars : Map[String, UTVar])
               (implicit stringToAtom : Map[String, UntypedTerm]) : Parser[UTCaseTerm] = 
        new Parser[UTCaseTerm] {
            def apply (in : Input) = {                 
                (HrsSymbs.horsCase ~ 
                 HrsSymbs.obracket ~ 
                 term(nonTerm, vars) ~
                 HrsSymbs.comma ~ 
                 cslist(term(nonTerm, vars)) ~
                 HrsSymbs.cbracket)(in) match  {
                    case Success(_ ~ _ ~ cond ~ _ ~ branches ~ _, 
                                 next) => 
                        Success(new UTCaseTerm(cond, branches), next)
                    case Failure(msg, next) => Failure(msg, next)
                }
            }
        }

 
    def assocUnit(nonTerm : UTNonTerm, vars : Map[String, UTVar])
                 (implicit stringtoAtom : Map[String, UntypedTerm]) : Parser[UntypedTerm] = (
        // check case first else it determines "case" is a terminal"...
        caseStmt(nonTerm, vars)
    |   variable(nonTerm, vars) 
    |   nonVarAtom 
    |   (HrsSymbs.obracket ~ term(nonTerm, vars) ~ HrsSymbs.cbracket) 
            ^^ {case _ ~ t ~ _ => t}
    )
   
  
    def makeApp(fun : UntypedTerm, args : List[UntypedTerm]) : UntypedTerm = {
        args match {
            case Nil => fun
            case arg :: rest => makeApp(fun.applyTo(arg), rest)
        }
    } 
   
   
    //This parser doesn't handle non-determinism operator.
    def term(nonTerm : UTNonTerm, vars : Map[String,  UTVar])
           (implicit stringToAtom : Map[String, UntypedTerm]) : Parser[UntypedTerm] = (
        (assocUnit(nonTerm, vars) ~ list(assocUnit(nonTerm, vars)))
            ^^ { case fun ~ argList => makeApp(fun, argList) }
    ) 
    
                  
}

