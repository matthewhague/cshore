/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



import scala.collection.JavaConversions._

import scala.util.parsing.combinator._	

import java.io.Reader
import java.lang.Exception

import ho.util.ManagedSet

import ho.managers.Managers

import ho.structures.cpds.ControlState
import ho.structures.cpds.SCharacter

import ho.structures.sa._


package ho.parsers {
    class SAParseException(msg : String) extends Exception(msg) {
    }

    class SAParser extends RegexParsers {

        val csManager = Managers.csManager
        val scManager = Managers.scManager
        val sManager = Managers.sManager
        val saManager = Managers.saManager
    
        val ID = """[a-zA-Z]([a-zA-Z0-9_'-])*"""r

        def control : Parser[ControlState] = (
          ID ^^ { case c => csManager.makeControl(c) }
        )

        def schar : Parser[SCharacter] = (
          ID ^^ { case sc => scManager.makeCharacter(sc) }
        )

        def sastate : Parser[SAState] = (
          control ^^ { case qc => saManager.getAddOuterState(qc) }
        | "(" ~ sastate ~ "," ~ sastateset ~ ")" 
            ^^ { case _ ~ q ~ _ ~ qs ~ _ => 
                     saManager.getAddTransition(q, qs).getLabellingState() }
        )

        def sastatesetend : Parser[ManagedSet[SAState]] = (
          "}" ^^ { case _ => sManager.makeEmptySet() }
        | sastate ~ "}" ^^ { case q ~ _ => sManager.makeSingleton(q) }
        | sastate ~ sastatesetend 
            ^^ { case q ~ qs => var sq = sManager.makeSingleton(q)
                                         sManager.union(sq, qs) }
        )

        def sastateset : Parser[ManagedSet[SAState]] = (
          "{" ~ sastatesetend ^^ { case _ ~ s => s }
        )

        def checkTransition(q : SAState, qs : ManagedSet[SAState]) {
            val k = q.getStateOrder()
            for (q2 <- qs) {
                if (q2.getStateOrder() != k)
                    throw new SAParseException("In transition from " + 
                                               q + 
                                               " to " +
                                               qs + 
                                               " the order of state " +
                                               q2 + 
                                               " is " +
                                               q2.getStateOrder() +
                                               " while " +
                                               q +
                                               " is order " +
                                               q.getStateOrder())
            }
        }

        def transition : Parser[SATransition] = (
          sastate ~ "------>" ~ sastateset 
                ^^ { case q ~ _ ~ qs => checkTransition(q, qs)
                                        saManager.getAddTransition(q, qs) }
        )

        def checkTransition(q : SAState, 
                            qsbr : ManagedSet[SAState], 
                            qs : ManagedSet[SAState]) {
            if (q.getStateOrder() != 1) 
                throw new SAParseException("Cannot build an order-1 transition " +
                                           "with source state " + 
                                           q + 
                                           " (not an order-1 state, but order " +
                                           q.getStateOrder() +
                                           ")")    
            for (q2 <- qs) {
                if (q2.getStateOrder() != 1)
                    throw new SAParseException("In transition from " + 
                                               q + 
                                               " to " +
                                               qs + 
                                               " the order of state " +
                                               q2 + 
                                               " is " +
                                               q2.getStateOrder() +
                                               " not order-1")
            }

            if (!qsbr.isEmpty()) {
                val qeg = qsbr.choose()
                val k = qeg.getStateOrder()
                for (q2 <- qsbr) {
                    if (q2.getStateOrder() != k)
                        throw new SAParseException("In transition from " + 
                                                   q + 
                                                   " to " +
                                                   qs + 
                                                   " the order of state " +
                                                   q2 + 
                                                   " is " +
                                                   q2.getStateOrder() +
                                                   " while " +
                                                   qeg +
                                                   " is order " +
                                                   qeg.getStateOrder())
                }
            }
        }


        def transitionO1 : Parser[SATransitionO1] = (
          sastate ~ "---" ~ schar ~ "," ~ sastateset ~ "--->" ~ sastateset 
            ^^ { case q ~ _ ~ a ~ _ ~ qsbr ~ _ ~ qs => 
                     checkTransition(q, qsbr, qs)
                     saManager.getAddTransitionO1(q, a, qsbr, qs) }
        )


        def sa : Parser[SA] = (
          transition ~ sa
                ^^ { case t ~ sa => sa.addTransition(t); sa }
        | transitionO1 ~ sa
                ^^ { case t ~ sa => sa.addTransitionO1(t); sa }
        | "" ^^^ new SA 
        ) 


        def getSA(reader: Reader) = {
            val parsed = parseAll(sa, reader)
            parsed match {
                case Failure(msg, where) => {
                    throw new SAParseException("SAParser failure: " + 
                                               where + " : " + msg)
                }
                case _ => {
                    parsed.get
                }
            }
        }

    }
}
