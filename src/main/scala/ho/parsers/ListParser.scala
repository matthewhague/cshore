/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.parsers

import scala.util.parsing.combinator._;	


trait ListParser extends RegexParsers {
    /** 
     * @param parsev  a parser for objects of type V
     * @return     It will consume as much of input as possible
     *             creating a list of Vs until parser for V fails
     */
    def genlist[V](parsev : Parser[V], sep : String) : Parser[List[V]] = 
        new Parser[List[V]] {
            def apply(in : Input) = {
                (parsev ~ sep ~ genlist(parsev, sep))(in) match {
                    case Success(v ~ _ ~ tail, next) => Success(v :: tail, next)
                    case Failure(_, next) => {
                        (parsev)(in) match {
                            case Success(v, next) => Success(List(v), next)
                            case Failure(_, _) => Success(Nil, in)
                        }
                    }
                }
            }
        }

    def list[V](parsev : Parser[V]) : Parser[List[V]] = genlist(parsev, "")

    def cslist[V](parsev : Parser[V]) : Parser[List[V]] = 
        genlist(parsev, ",")

}

