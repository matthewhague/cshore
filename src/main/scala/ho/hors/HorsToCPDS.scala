/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.hors

import ho.main.Main;
import ho.structures.cpds.Cpds
import scala.collection.JavaConversions._
import scala.collection.mutable.Set
import ho.structures.cpds.Rule
import ho.structures.cpds.RewRule
import ho.structures.cpds.CPushRule
import ho.structures.cpds.EvalNonTermRule
import ho.structures.cpds.PopRule
import ho.structures.cpds.CollapseRule
import ho.structures.cpds.PushRule
import ho.structures.cpds.GetArgRule
import ho.structures.cpds.GetOrder0ArgRule
import ho.structures.cpds.RewRule
import ho.structures.cpds.RewLinkRule
import scala.collection.mutable.HashSet
import ho.structures.cpds.ControlStateHors
import ho.structures.cpds.SCharacterTerm

/**
 * Apply to a Hors (which includes a property automaton) to get an equivalent CPDS.
 * Explores from the starting symbol forwards and in this way avoids some subterms
 * that are not subterms of any reachable term. This could be improved a little
 * by a binding analysis (which could probably be integrated with this construction
 * fairly striaghtforwardly.
 * UPDATE: Actually, in the event I don't think much at all, if anything, wil
 * be stripped due to findBindings exploring all subterms. However, it would
 * be worth looking into such restrictions.
 * 
 * `Position' being looked up will be > 0 if it is an argument position and = 0 if we
 * are due to evaluate the head symbol.
 * 
 */

object HorsToCPDS {

    var alreadyProcessed : Set[(PropAutState, Term)] 
        = new HashSet[(PropAutState, Term)]
    var alreadyFindBindings : Set[Term] 
        = new HashSet[Term]

    var cpds : Cpds = new Cpds
    var hors : Hors = new Hors
    var horsOrder : Byte = 0 : Byte
    var propAutStates : Set[PropAutState]
        = new HashSet[PropAutState]

    /**
     * @param horsIn    Hors to translate to a CPDS
     * @return    The resulting CPDS (takes into account property automaton in hors)
     */
    def apply(horsIn : Hors) : Cpds = {
      cpds = new Cpds()
      horsOrder = horsIn.getOrder : Byte
      alreadyProcessed.clear
      alreadyFindBindings.clear
      propAutStates.clear
      hors = horsIn 
      for (s <- hors.getPropAutStates)
        propAutStates += s
     
      for(initNonTerm <- hors.initNonTerms; initState <- hors.initPropAutStates)
        processTerm(initState, initNonTerm)
      
      setUpErrorStates(cpds, hors)
       
      cpds
    }

    /**
     * Sets up the cpds control-state/stack element combinations that should
     * have a transition to the fixed error state of a CPDS due to them
     * being failing property automaton states/ failing non-terminals.
     *
     * Note: fail property automaton states are dealt with in terminalChild,
     * except if initial prop aut state is also a fail
     */
    private def setUpErrorStates(cpds : Cpds, hors : Hors) : Unit = {
        // Keep track of rules separately when adding to CPDS to avoid ConcurrentModification exception
      // caused by adding to CPDS whilst using iterators.
    val rules = new HashSet[RewRule]()

    for (initState <- hors.initPropAutStates) {
        if (hors.failPropAutStates.contains(initState)) {
            for (initNonTerm <- hors.initNonTerms) {
                val cs = ho.managers.Managers.csManager.makeControl(initState, 0, false)
                val sourceChar = ho.managers.Managers.scManager.makeCharacter(initNonTerm)

                rules += new RewRule(cs, sourceChar, 
                                     ho.managers.Managers.csManager.makeControl(Cpds.error_state),
                                     sourceChar, ToError())
            }
        }
    }

    for (sChar <- cpds.getCharacters) {
        sChar match {
            case sc : SCharacterTerm => 
                sc.getTerm.getHead match {
                    case nt : NonTerm => 
                        if(hors.failNonTerms.contains(nt)) {
                            for (controlState <- cpds.getControls) {
                                controlState match {
                                    case cs : ControlStateHors =>
                                        if (cs.getPosition == 0) {
                                            //cs.getPosition == 0 is good as do not need to pollute
                                            // CPDS with transitions to error from configurations
                                            // representing in-middle-of-finding-variable binding.
                                            rules += new RewRule(controlState, 
                                                                 sChar,
                                                                 ho.managers.Managers.csManager.makeControl(Cpds.error_state),
                                                                 sChar, ToError())
                                        }

                                    case _ => ()
                                }
                            }
                        }
                    case _ => ()
                }
            case _ => ()
        }
    }

    for(rule <- rules)cpds.addRewRule(rule)
    
    }
    
    /**
     * Since CPDS is stateful, making this of type Unit. (Could do a `less mutable' version
     * in a monadic setting... hopefully implicits clean up state passing a bit... not sure
     * whether this is good Scala etiquette or not). Corresponding elimiation of some unreachable
     * property automaton states.
     * 
     * @param  cpds      Cpds to be modified
     * @param     hors    Hors being translated
     * so inefficient, so better to compute only once and pass as argument)
     * @propAutState                property automaton state to process in combination with...
     * @param     term                 Hors term to process (add corresponding bits to Cpds)
     * 
     */
    var termWorklist : java.util.HashSet[(PropAutState, Term)] 
        = new java.util.HashSet[(PropAutState, Term)]
    var recursingTerms : Boolean = false

    private def processTerm(propAutState : PropAutState, 
                            term : Term) : Unit = {
        if (recursingTerms) {
            termWorklist.add((propAutState, term))
        } else {
            recursingTerms = true
            processTermBody(propAutState, term)
            while (!termWorklist.isEmpty) {
                val i : java.util.Iterator[(PropAutState, Term)] 
                    = termWorklist.iterator
                var (p, t) = i.next
                i.remove
                processTermBody(p, t)
            }
            recursingTerms = false
        }
    }


    var timebind : Long = 0
    var timerest : Long = 0
    private def processTermBody(propAutState : PropAutState, 
                                term : Term) : Unit = {
        if(!alreadyProcessed.contains((propAutState, term))) {
            alreadyProcessed += ((propAutState, term))
            
            val head = term.getHead
            val argList = term.getArgsOfHead 
            
            // Add all transitions corresponding to looking up value to which a variable
            // is bound by looking at subterm of term
            findBindings(term, argList)
          
            head match {
              case nt : NonTerm =>
                if(head.getOrder == 0) {
                  // term must equal head and so no arguments, so can just rewrite
                  hors.getRhs(nt) match {
                      case Some(t) => {evalOrd0NonTerm(propAutState, term, t) 
                                      processTerm(propAutState, t) }
                      
                      case None => () // No rule corresponding to this non-terminal exists
                  } 
                  }
                  else {
                  hors.getRhs(nt) match {
                    // Non-terminal have arguments push-one (no link needed as all 
                    // parameters will be present)
                      case Some(t) => { evalHONonTerm(propAutState, term, t)
                                          processTerm(propAutState, t) }
                      case None => () // No rule corresponding to this non-terminal exists
                  } 
              }
              
              case tml : Terminal => 
                // Nothing to do for order-0 terminal as dead end.
                if (tml.getOrder > 0) {
                    for(pos <- 1 to tml.getArity){
                      hors.getNextState(propAutState, tml, pos) match {
                        case Some(nextState) => {
                             terminalChild(propAutState, term, pos, nextState)
                             processTerm(nextState, term)}
                        case None => () // No property automaton transition
                      }
                    }
                 }
              
              case v : Var =>
                if(v.getOrder == 0)
                  callOrd0Variable(propAutState, v, term, v.pos)
                else
                  callHOVariable(propAutState, v, term, v.pos) 
                
              case nonDet : NonDet => {
                evalNonDet(propAutState, nonDet, nonDet.choice1, nonDet.choice2)
                processTerm(propAutState, nonDet.choice1)
                processTerm(propAutState, nonDet.choice2) 
              }
              case dum : Dummy => () //Nothing to do if a dummy symbol
              
              case app : App => {assert(false,  "App should never be head symbol"); null} // One
              // could create a NonAppTerm type to avoid this case, but when I tried things
              // elsewhere seemed to start looking messier...

              case _ => 
                throw new Exception("HorsToCPDS.scala can't handle " + head)
               
            }
          }
    }
    
    /**
     * Creates all of the CPDS rules for finding a particular argument of the head
     * of a term. Will recursively call processTerm with new subterms
     * explored.
     * 
     * @param     term    The term whose head's argument we want to create rules to find. 
     * @param     argList The list of arguments that are syntactic arguments of the head of term
     *                     (could be derived from term itself, but passed to avoid computing
     *                         multiple times). List will have left-most as first argument
     * 
     * Note that term.typ.getArity will be the number of arguments of the head of term
     * that are not syntactic arguments and that must be found via a collapse.
     * 
     */
    var timeargs : Long = 0
    var timelooks : Long = 0
    var count = 0
    var lookscount = 0
    private def findBindings(term : Term, 
                             argList : List[Term]) : Unit = {
      // Stack character corresponding to the term being considered:
      val sourceSChar = ho.managers.Managers.scManager.makeCharacter(term)
      if(!alreadyFindBindings.contains(term)) {
        alreadyFindBindings += term
   
        for(propAutState <- propAutStates) {
        //First process syntactic arguments
          var pos = 0 
          val argsstart = System.currentTimeMillis
          for(arg <- argList) {
            pos = pos + 1
            val sourceState = ho.managers.Managers.csManager.makeControl(propAutState, pos, false)
            val targetState = ho.managers.Managers.csManager.makeControl(propAutState, 0, false)
            
            val targetSChar = ho.managers.Managers.scManager.makeCharacter(arg)
            
            val rule = 
              if(Main.getUseSCPDS()) {
                    if(arg.getOrder == 0)
                        new GetOrder0ArgRule(sourceState, sourceSChar, targetState, targetSChar, FindBinding())
                    else
                        new GetArgRule(sourceState, sourceSChar, targetState, targetSChar, 
                                        (horsOrder - arg.getOrder + 1).toByte, FindBinding())
                  
              } else {
                    if(arg.getOrder == 0)
                        // Then we are finding binding of an order-0 variable so no need to attach a link
                        new RewRule(sourceState, sourceSChar, targetState, targetSChar, FindBinding())
                    else
                        // We are looking for binding of a higher-order variable and so we need to
                        // attach a link to stack that would have been copied. Order of variable
                      // must be equal to order of arg.
                        new RewLinkRule(sourceState, sourceSChar, targetState, targetSChar, 
                                        (horsOrder - arg.getOrder + 1).toByte, FindBinding())
              }
            cpds.addRule(rule)
            // Remember to process the new argument:
            processTerm(propAutState, arg)
          }
          timeargs = timeargs + (System.currentTimeMillis - argsstart)
        
          val looksstart = System.currentTimeMillis
          // Now process arguments that are not syntactic arguments.
          if(term.getArity > 0) {
            for(i <- (pos + 1) to term.getHead.getArity) {
              lookscount = lookscount + 1
              val sourceState = ho.managers.Managers.csManager.makeControl(propAutState, i, false)
              // (i - pos) is the offset from left-most syntactic argument to position of 
              // this new argument to be looked up.
              val targetState = ho.managers.Managers.csManager.makeControl(propAutState, 
                  (i - pos), false)
              
              
              // Note that order of link on term will be horsOrder - term.getOrder + 1
              val rule = new CollapseRule(sourceState, 
                            sourceSChar,
                            targetState,
                            (horsOrder - term.getOrder +1).toByte, FindBinding())
              //("Adding Rule: " + rule)
              cpds.addCollapseRule(rule)
            }
          }
          timelooks = timelooks + (System.currentTimeMillis - looksstart)
          count = count + 1
          if (count%1000 == 0)
            println("looks " + (timelooks / 1000.0) + 
                    " aut " + (timeargs / 1000.0) + 
                    " looks count " + lookscount)
        }
      }
    }
    
    /**
     * When evaluating a order-0 non-terminal we can just rewrite it to its RHS (the
     * property automaton state remains same as have not emitted any terminal symbol).
     * We keep the `needToPopOne' flag false (as no need to pop one before continuing)
     */
    private def evalOrd0NonTerm(propAutState : PropAutState, 
                                sourceTerm : Term, 
                                targetTerm : Term) : Unit = {
        val cs = ho.managers.Managers.csManager.makeControl(propAutState, 0, false)
        val sourceChar = ho.managers.Managers.scManager.makeCharacter(sourceTerm)
        val targetChar = ho.managers.Managers.scManager.makeCharacter(targetTerm)
        
        val newRule = new RewRule(cs, sourceChar, cs, targetChar, Rewrite())
        cpds.addRewRule(newRule)
        
        targetTerm.getHead match {
          case nt : NonTerm => 
            if(hors.failNonTerms.contains(nt)) {
                val errorcs = ho.managers.Managers.csManager.makeControl(Cpds.error_state)
                val erule = new RewRule(cs, sourceChar, errorcs, targetChar, ToError())
                cpds.addRewRule(erule)
            }
          case _ => ()
        }
    }
    
    /**
     * Similar to above except this time we need to do a push-one (without any link being created)
     */
    private def evalHONonTerm(propAutState : PropAutState, 
                              sourceTerm : Term, 
                              targetTerm : Term) : Unit = {
        val cs = ho.managers.Managers.csManager.makeControl(propAutState, 0, false)
        val sourceChar = ho.managers.Managers.scManager.makeCharacter(sourceTerm)
        val targetChar = ho.managers.Managers.scManager.makeCharacter(targetTerm)
        
        if (Main.getUseSCPDS()) {
            val r = new EvalNonTermRule(cs, sourceChar, cs, targetChar, Rewrite())
            cpds.addEvalNonTermRule(r);
        } else {
            val r = new CPushRule(cs, sourceChar, cs, targetChar, 1, Rewrite())
            cpds.addCPushRule(r)
        }
        
        targetTerm.getHead match {
          case nt : NonTerm => 
            if(hors.failNonTerms.contains(nt)) {
                val errorcs = ho.managers.Managers.csManager.makeControl(Cpds.error_state)
                val erule = new RewRule(cs, sourceChar, errorcs, targetChar, ToError())
                cpds.addRewRule(erule)
            }
          case _ => ()
        }
    }
    
    /**
     * @param source    property automaton state that beginning with
     * @param term        Term that is headed by a terminal symbol
     * @param target    The next property automaton state when decending down pos child
     * @param pos    The child that is being explored (used to annotated transition)
     */
    private def terminalChild(source : PropAutState, 
                              term : Term, 
                              pos : Int, 
                              target : PropAutState) : Unit = {
        val sourcecs = ho.managers.Managers.csManager.makeControl(source, 0, false)
        val schar = ho.managers.Managers.scManager.makeCharacter(term)
         val targetcs = ho.managers.Managers.csManager.makeControl(target, pos, false)
         
         val rule = new RewRule(sourcecs, schar, targetcs, schar, TerminalChild(pos))
        cpds.addRewRule(rule)

        if(hors.failPropAutStates.contains(target)) {
            val errorcs = ho.managers.Managers.csManager.makeControl(Cpds.error_state)
            val erule = new RewRule(sourcecs, schar, errorcs, schar, ToError())
            cpds.addRewRule(erule)
        }
    }
    
   
    /**
     * @param propAutState     The property automaton state that should be maintained
     * @param head     The head of term (which should be a variable of order > 0)
     * @param term     The term whose head is a higher-order variable to be called
     * @param varParameterPos    The position of the formal parameter of a non-terminal to which this variable
     *                             is bound.
     */
    private def callHOVariable(propAutState : PropAutState, 
                               head : Var, 
                               term : Term, 
                               varParameterPos : Int) : Unit = {
        //Top symbol remains same throughout both transitions that we create:
        val sChar = ho.managers.Managers.scManager.makeCharacter(term)
        
        val sourcecs = ho.managers.Managers.csManager.makeControl(propAutState, 0, false)
        
        // True flag below indicates we've just done higher-order push and are about
        // to start popping to find what variable bound to.
        val middlecs = ho.managers.Managers.csManager.makeControl(propAutState, 0, true)
        val targetcs = ho.managers.Managers.csManager.makeControl(propAutState, varParameterPos, false)
        
        val varOrder = head.getOrder //Order of the variable
        
        
        val rule1 = new PushRule(sourcecs, sChar, middlecs, (horsOrder - varOrder + 1).toByte, 
                                FindBinding())
        val rule2 = new PopRule(middlecs, sChar, targetcs, 1.toByte, 
                                FindBinding()) 
        
        cpds.addPushRule(rule1)
        cpds.addPopRule(rule2)    

    }
    
    /**
     * Expects a term with order-0 variable as its head.
     */
    private def callOrd0Variable(propAutState : PropAutState, 
                                 head : Var, 
                                 term : Term, 
                                 varParameterPos : Int) : Unit = {
        //Top symbol remains same throughout both transitions that we create:
        val sChar = ho.managers.Managers.scManager.makeCharacter(term)
        
        val sourcecs = ho.managers.Managers.csManager.makeControl(propAutState, 0, false)
        
        val targetcs = ho.managers.Managers.csManager.makeControl(propAutState, varParameterPos, false)

        val rule = new PopRule(sourcecs, sChar, targetcs, 1.toByte, FindBinding()) 

        cpds.addPopRule(rule)    
    }
    
    /**
     * Adds CPDS rule for non-deterministic choice 
     * @param propAutState    the property automaton state to maintain
     * @param term    The term constituting non-deterministic choice.
     * @param     leftChoice     The left-hand choice 
     * @param    rightChoice     The right-hand choice
     */
    private def evalNonDet(propAutState : PropAutState, 
                           term : NonDet, 
                           leftChoice : Term, 
                           rightChoice : Term) : Unit = {
        val cs = ho.managers.Managers.csManager.makeControl(propAutState, 0, false)
        val sourceChar = ho.managers.Managers.scManager.makeCharacter(term)
        val targetCharLeft = ho.managers.Managers.scManager.makeCharacter(leftChoice)
        val targetCharRight = ho.managers.Managers.scManager.makeCharacter(rightChoice)
        
        val newRuleLeft = new RewRule(cs, sourceChar, cs, targetCharLeft, LeftNonDetChoice())
        val newRuleRight = new RewRule(cs, sourceChar, cs, targetCharRight, RightNonDetChoice())
        
        cpds.addRewRule(newRuleLeft)
        
        
        cpds.addRewRule(newRuleRight)
        
        
        leftChoice.getHead match {
          case nt : NonTerm => 
            if(hors.failNonTerms.contains(nt)) {
                val errorcs = ho.managers.Managers.csManager.makeControl(Cpds.error_state)
                val erule = new RewRule(cs, sourceChar, errorcs, targetCharLeft, ToError())
                cpds.addRewRule(erule)
            }
          case _ => ()
        }
        
        rightChoice.getHead match {
          case nt : NonTerm => 
            if(hors.failNonTerms.contains(nt)) {
                val errorcs = ho.managers.Managers.csManager.makeControl(Cpds.error_state)                
                val erule = new RewRule(cs, sourceChar, errorcs, targetCharRight, ToError())
                cpds.addRewRule(erule)
            }
          case _ => ()
        }
        
        
    }
    
    
    
    

}
