/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.hors
import java.io.Reader
import java.util.HashSet
import ho.parsers.HorsParser
import ho.parsers.HrsParser
import ho.reachability.Algorithm
import ho.reachability.NaiveAlgorithm
import ho.structures.cpds.Cpds
import ho.structures.cpds.StackHead
import ho.managers.Managers
import ho.summaryapprox.HeadManager
import ho.util.ManagedSet
import ho.structures.cpds.Rule
import scala.collection.JavaConversions._
import ho.minimisers.CpdsRuleCompression
import ho.minimisers.Order0RuleElimination
import ho.minimisers.CoarseOrder0RuleElimination
import ho.witness.Witness
import ho.util.ImmutableSet
import ho.main.Main;

object HorsProcessor {

    def processHrsFromFile(in : Reader) = {
        val parsed = HrsParser(in)
        parsed.toString
        parsed match {
            case HrsParser.Success(hrs, _) => checkHrsToString(hrs)
            case HrsParser.Failure(msg, _) => "Parse error: " + parsed.toString
        }
    }

    def checkHrsToString(hrs : Hrs) : String = {
        val hors = HrsToHors(hrs)
	    "The Hrs to process is:\n" + hrs.toString + "\n\n" + "====== \n" + 
	    "Result of running algorithm: \n" + "======= \n" +
	    	checkHorsToString(hors)
    }

	def processFromFile(in : Reader) = {


      println("parsing")
	  val parsed = HorsParser(in) 
      println("parsed")
	  
	  parsed match {
	  case HorsParser.Success(hors, _) => 
	    "The Hors to process is:\n" + hors.toString + "\n\n" + "====== \n" + 
	    "Result of running algorithm: \n" + "======= \n" +
	    	checkHorsToString(hors)
	    	
	  case HorsParser.Failure(msg, _) => "Parse error: " + parsed.toString
	  }
	}
	
	/**
	 * @param Hors to model-check
	 * @return A Pair (cpds, reachables) where cpds is the constructed cpds and
	 * 				reachables is a list of pairs (initState, initNonTerm) of 
	 * 			initial prop aut. state and initial non-term from which a fail configuration is reachable.
	 */
	def checkHors(hors : Hors) : (Cpds, List[(PropAutState, NonTerm, Witness)]) = {
	    println("Constructing Cpds...")
	    val startTimeMakeCpds = System.currentTimeMillis
		var cpds : Cpds = (new ho.structures.cpds.HorsToCPDS()).translate(hors)
        // making cpds not included in reportable time
		val runTimeMakeCpds = System.currentTimeMillis() - startTimeMakeCpds;
		println("Cpds made from parsed Hors in " + 
                (runTimeMakeCpds / 1000.0) + 
                "s.")
		/*
		println("Running Rule Compression")
		val startTimeCompress = System.currentTimeMillis();
		val initHeads = new HashSet[StackHead]()
		for(initnt <- hors.initNonTerms; 
			initst <- hors.initPropAutStates) {
			val q = Managers.csManager.makeControl(initst, 0, false)
			val a = Managers.scManager.makeCharacter(initnt)
			initHeads.add(new StackHead(q, a))
		}
		(new CpdsRuleCompression()).minimise(cpds, initHeads)
		

		val runTimeCompress = System.currentTimeMillis() - startTimeCompress;
		println("Cpds rule compression took : " + (runTimeCompress / 1000.0) + "s.")
		println("Compressed Cpds has " + cpds.getNumRules + " rules.")
		*/

        val startReportedTime = System.currentTimeMillis();

        if (Main.getUseOrder0ForwardsAnalysis()) {
            println("Performing order-0 forwards analysis...")
            val startOrder0FATime = System.currentTimeMillis()
            val initNumRules = cpds.getNumRules
    		val initHeads = new HashSet[StackHead]()
    		for(initnt <- hors.initNonTerms; 
			    initst <- hors.initPropAutStates) {
                val q = Managers.csManager.makeControl(initst, 0, false)
                val a = Managers.scManager.makeCharacter(initnt)
			    initHeads.add(new StackHead(q, a))
		    }
            cpds = (new CoarseOrder0RuleElimination()).minimise(cpds, initHeads)
            val order0FATime = System.currentTimeMillis() - startOrder0FATime
            println("Order-0 forwards analysis took : " + 
                    (order0FATime / 1000.0) + 
                    "s")
            println("Cpds before had " + 
                    initNumRules + 
                    " and after has " +
                    cpds.getNumRules +
                    " rules.")
        }

		println("Constructing approximate reachability graph");
		val startTimeApprox = System.currentTimeMillis();
		val approxGraph = new HeadManager(cpds); // Construct approx. reachability graph
		// Supply initial configurations.
		for(initNonTerm <- hors.initNonTerms; initState <- hors.initPropAutStates)
		  approxGraph.addInitialHead(Managers.csManager.makeControl(initState, 0, false), 
				  							Managers.scManager.makeCharacter(initNonTerm))
		
		val runTimeApprox = System.currentTimeMillis() - startTimeApprox;
		println("Approximate reachability graph constructed in : " + (runTimeApprox / 1000.0) + "s.")
		// println("Heads over-approximation: " + approxGraph)
		
		println("Extracting pruned Cpds")
		val startTimePrune = System.currentTimeMillis();
		val ruleSet : ImmutableSet[Rule] = approxGraph.getMaybeErrorRules
		val prunedCpds = new Cpds
		for(rule <- ruleSet)
		  prunedCpds.addRule(rule)
		  
		// Set the annotation on the CPDS indicating which stack characters might accompany a configuration
		// with error control-state
		prunedCpds.setErrorSChars(approxGraph.getErrorChars)
		  
		val runTimePrune = System.currentTimeMillis() - startTimePrune;
		println("Pruned Cpds Constructed in : " + (runTimePrune / 1000.0) + "s.")
		println("Pruned Cpds has " + prunedCpds.getNumRules + " rules. Original Cpds had: " + cpds.getNumRules)

        var reachables : List[(PropAutState, NonTerm, Witness)] = Nil
        var reportableTime : Long = 0

        if (Main.getNaiveSaturation()) {
            // TODO: refactor to avoid copy paste!
            println("Running Naive Cpds Backwards Reachability Algorithm...")
            val startTimeAlg = System.currentTimeMillis();
            NaiveAlgorithm.check(prunedCpds)
            val runTimeAlg = System.currentTimeMillis() - startTimeAlg;
            println("Naive algorithm Complete in " + (runTimeAlg / 1000.0) + "s.")

            val startTimeWitnesses = System.currentTimeMillis();
            reachables = {
                for(initnt <- hors.initNonTerms; 
                   initst <- hors.initPropAutStates;
                   val p = Managers.csManager.makeControl(initst, 0, false);
                   val a = Managers.scManager.makeCharacter(initnt);
                   if(NaiveAlgorithm.reaches(p, a))) 
                   yield (initst, 
                          initnt, 
                          NaiveAlgorithm.getCounterExample(p, a))
            }.toList

            val finishedTime = System.currentTimeMillis()
            val runTimeWitnesses = finishedTime - startTimeWitnesses
            println("Generated witnesses in " + (runTimeWitnesses / 1000.0) + "s.")
            reportableTime = finishedTime - startReportedTime
        } else if (Main.getSimpleCegar()) {
            for(initnt <- hors.initNonTerms; 
                initst <- hors.initPropAutStates;
                val p = Managers.csManager.makeControl(initst, 0, false);
                val a = Managers.scManager.makeCharacter(initnt)) {

                println("Running simple CEGAR algorithm for " + 
                        p.toString + 
                        " and " +
                        a.toString)
                val startTimeAlg = System.currentTimeMillis();
                val witness = Algorithm.simpleCegarCheck(prunedCpds, p, a)
                if (witness != null)
                    reachables = ((initst, initnt, witness))::reachables

                val finishedTime = System.currentTimeMillis()
                reportableTime = finishedTime - startReportedTime
                val runTimeTotal = reportableTime + runTimeMakeCpds
            }
        } else {
            println("Running Cpds Backwards Reachability Algorithm...")
            val startTimeAlg = System.currentTimeMillis();
            Algorithm.check(prunedCpds)
            val runTimeAlg = System.currentTimeMillis() - startTimeAlg;
            println("Algorithm Complete in " + (runTimeAlg / 1000.0) + "s.")

            val startTimeWitnesses = System.currentTimeMillis();
            reachables = 
              {for(initnt <- hors.initNonTerms; 
                   initst <- hors.initPropAutStates;
                   val p = Managers.csManager.makeControl(initst, 0, false);
                   val a = Managers.scManager.makeCharacter(initnt);
                   if(Algorithm.reaches(p, a))) 
                   yield (initst, initnt, Algorithm.getCounterExample(p, a))}.toList
    //	             yield (initst, initnt, null)}.toList

            val finishedTime = System.currentTimeMillis()
            val runTimeWitnesses = finishedTime - startTimeWitnesses
            println("Generated witnesses in " + (runTimeWitnesses / 1000.0) + "s.")
            reportableTime = finishedTime - startReportedTime
        }

        val runTimeTotal = reportableTime + runTimeMakeCpds
        println("Total Reportable Time: " + (reportableTime / 1000.0) + "s.")
        println("Total Time with Translation: " + (runTimeTotal / 1000.0) +
        "s.")

        (prunedCpds, reachables)
	}
	
	/**
	 * Does check using checkHors method then produces a string describing the result
	 */
	def checkHorsToString(hors : Hors) : String = {
		val (cpds, failingInits) = checkHors(hors)
			
		val reachables : List[String] = 
			for((initst, initnt, ceg) <- failingInits)
				yield "Can fail from initial property state: " + initst +
				      "\n starting with initial non-terminal: " + initnt + 
                      "\n via witness " + ceg + "\n"
					  	

		//"========= \n The constructed CPDS is: \n ======= \n " + cpds.toString + "\n ========= \n" +
		"Cpds stats:\n" + cpds.cpdsStats() + "\n" +
		"The result of the reachability check is:" + "\n ========= \n" +
		(
			if(reachables.isEmpty)
				"**** We cannot fail! ****"
			else
				reachables.mkString("\n")
		) + "\n" +
        Managers.saManager.automatonStats()
//		  "\n ==========\n Stack automaton constructed: \n ==========\n" +
//		  Managers.saManager.getSA().toString


		
	}
	
	/**
	 * @param hors Hors to check
	 * @param depth	Depth to which one should search the evaluation tree
	 * @return A list of reductions witnessing reachability of fail configuration
	 * 			from some initial configuration where fail configuration occurs
	 * 				at most depth deep in the evaluation tree. Search for failing states
	 * along a particular branch stops when either depth is reached or a fail state is found
	 * (so only oen fail state on any given branch)
	 */
	def checkHorsBruteForce(hors : Hors, depth : Int) : List[Reduction] = 
		{for(initState <- (hors.initPropAutStates); initTerm <- (hors.initNonTerms))
		  yield checkHorsBruteForce(hors, HorsEvaluator(hors, initState, initTerm),
				  					 depth, Nil)}.toList flatten
	
	
	
	private def checkHorsBruteForce(hors : Hors, evalTree : EvalTree, depth : Int, redSoFar : Reduction) 
		: List[Reduction] = {
		if (depth > 0) {
			evalTree match {
				case NonDetNode(state, term, left, right) => 
				  if(hors.failPropAutStates.contains(state))
				    List((state, term, FailDir()) :: redSoFar)
				    // Note that head will not be non-terminal so cannot have failed due to non-terminal.
				  else {
				    checkHorsBruteForce(hors, left, depth - 1, (state, term, NonDetLeftDir()) :: redSoFar) :::
				    checkHorsBruteForce(hors, right, depth - 1, (state, term, NonDetRightDir()) :: redSoFar) 
				  }
				  
				
				case TerminalNode(state, term, evTs) => 
				  if(hors.failPropAutStates.contains(state))
				    List((state, term, FailDir()) :: redSoFar)
				  else
					{for((evT, i) <- evTs.zipWithIndex)
						yield 
							checkHorsBruteForce(hors, evT, depth - 1, 
							    (state, term, BranchDir(i)) :: redSoFar)} flatten
				  				
				  
				case NonTermNode(state, term, evT) =>
				  if(hors.failPropAutStates.contains(state) || 
				      ((term getHead) match {case nt : NonTerm => hors.failNonTerms.contains(nt)
				      						case _ => false}))
					  List((state, term, FailDir()) :: redSoFar)
				  else
				    checkHorsBruteForce(hors, evT, depth - 1, (state, term, RewriteDir()) :: redSoFar)
				 
				case DeadEnd() => Nil //Found a leaf without finding an error.
				case _ => assert(false, "Shouldn't happen"); null //*** Put this in to supress
					// a spurious compiler warning that seems to suggest unmatched cases 
				// for evalTree, listing cases from Term. Perhaps
				// bug in the compiler?***
				
			}
		}
		else
		  Nil // Reached depth without finding error along current reduction
	}
}
