/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


// Translates a Hrs to Hors, mainly means type inference.

package ho.hors

import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet


/** 
 * TODO:
 *      1) Typing of terminals (will be erased by typeenv scoping! -- implement
 *      global meta variable environment a la haskell references?)
 *      2) Convert to HORS -- actually tricky cos you need to type from the
 *      ground up (or re-use chris's parsing code somehow)
 *      3) Circularity check in meta variables
 */



class TypeInferenceException(msg : String) extends Exception(msg) {
}


object HrsToHors {
    // need a dummy terminal for arity zero terminals
    // c becomes c(dummy) so that c can be read in hors
    def dummyTml = Terminal("_dummy", Ground())
    def failState = PropAutState("_fail")

    // Global index of next unique meta variable name
    var metaCount = 0
    // lookup of meta variables
    val metas = new HashMap[MetaVariable, Type]   

    def newMetaTyVar() : MetaVariable = {
        val mv = new MetaVariable("m" + metaCount.toString)
        metaCount += 1
        mv
    }

    class TypeEnv(env : scala.collection.immutable.HashMap[UntypedTerm, Type] 
                      = new scala.collection.immutable.HashMap[UntypedTerm, Type]) {
        var curEnv = env

        def lookup(t : UntypedTerm) : Type = {
            curEnv.get(t) match {
                case Some(ty) => ty
                case None => 
                    throw new TypeInferenceException("Tried to lookup " +
                                                     t.toString + 
                                                     " which is not bound.")
            }
        }

        def isPresent(t : UntypedTerm) : Boolean = curEnv.contains(t)

        def optLookup(t : UntypedTerm) : Option[Type] = curEnv.get(t)

        // add binding, creating a new env rather than overwriting current
        def extendEnv(t : UntypedTerm, ty : Type) : TypeEnv
            = new TypeEnv(curEnv + ((t, ty)))

        // add binding to current without creating a new copy
        def writeTy(t : UntypedTerm, ty : Type) : Unit = {
            curEnv = curEnv + ((t, ty))
        }

        def lookup(t : MetaVariable) : Type = {
            metas.get(t) match {
                case Some(ty) => ty
                case None => 
                    throw new TypeInferenceException("Tried to lookup " +
                                                     t.toString + 
                                                     " meta variable which is not bound.")
            }
        }

        def optLookup(t : MetaVariable) : Option[Type] = metas.get(t)

        // add binding to current without creating a new copy
        def writeTy(t : MetaVariable, ty : Type) : Unit = {
            metas.put(t, ty)
        }

    
        // not sure if we need this, so return empty for now
        def getEnvTypes : HashSet[Type] = new HashSet[Type]

        def unify(t1 : Type, optT2 : Option[Type]) : Type = optT2 match {
            case None => t1
            case Some(t2) => { doUnify(t1, t2); t1 }
        }

        def doUnify(t1 : Type, t2 : Type) : Unit = (t1, t2) match {
            case (MetaVariable(id1), MetaVariable(id2)) if (id1 == id2) => ()
            case (MetaVariable(id1), _) => unifyVar(MetaVariable(id1), t2)
            case (_, MetaVariable(id2)) => unifyVar(MetaVariable(id2), t1)
            case (Arrow(f1, a1), Arrow(f2, a2)) => {
                doUnify(f1, f2)
                doUnify(a1, a2)
            }
            case (Ground(), Ground()) => ()
            case (DomainType(dom1), DomainType(dom2)) => {
                if (dom1 != dom2)
                    throw new TypeInferenceException("Can't unify " + 
                                                     dom1 + 
                                                     " and " + 
                                                     dom2)
            }
            case _ => throw new TypeInferenceException("Error in type inference!")
        }

        def unifyFun(t : Type) : (Type, Type) = t match {
            case Arrow(funTy, argTy) => (funTy, argTy)
            case _ => {
                val argTy = newMetaTyVar()
                val resTy = newMetaTyVar()
                doUnify(t, (Arrow(argTy, resTy)))
                (argTy, resTy)
            }
        }

        def unifyVar(mv : MetaVariable, t : Type) = {
            optLookup(mv) match {
                case Some(vt) => doUnify(vt, t)
                case None => unifyUnboundVar(mv, t)
            }
        }

        def unifyUnboundVar(mv1 : MetaVariable, t : Type) = t match {
            case MetaVariable(mv2) => {
                // we know they're not the same variable cos unify would catch
                optLookup(MetaVariable(mv2)) match {
                    case Some(t2) => doUnify(mv1, t2)
                    case None => writeTy(mv1, t)
                }
            }
            case _ => {
                // TODO: check for circularity
                writeTy(mv1, t)
            }
        }

        def zonk(t : Type) : Type = {
            t match {
                case Arrow(funTy, argTy) => {
                    val funTyZ = zonk(funTy)
                    val argTyZ = zonk(argTy)
                    Arrow(funTyZ, argTyZ)
                }
                case Ground() => Ground()
                case DomainType(_) => t
                case MetaVariable(mv) => {
                    optLookup(MetaVariable(mv)) match {
                        case Some(ty) => zonk(ty)
                        case None => Ground() // unbound metas go to ground
                    }
                }
                case _ => throw new TypeInferenceException("Cannot zonk " +
                                                           t.toString)
            }
        }

        val zonkUp = (zonk(_)).compose((lookup(_:UntypedTerm)))
    }

    def makeInitTypeEnv(hrs : Hrs) : TypeEnv = {
        var env = new TypeEnv()
        for ((nt, _) <- hrs.rules) {
            val varTy = newMetaTyVar()
            env.writeTy(nt, varTy)
        }
        for (t <- hrs.terminals) {
            val varTy = newMetaTyVar()
            env.writeTy(t, varTy)
        }
        for ((_, d) <- hrs.domains) {
            for (v <- d.getValues) {
                if (env.isPresent(UTDomConst(v)))
                    throw new TypeInferenceException("Ambiguous domain value " +
                                                     v)
                env.writeTy(UTDomConst(v), DomainType(d))
            }
        }
        env
    }

    def makeTmlType(arity : Int) : Type = arity match {
        case 0 => Ground()
        case n => Arrow(Ground(), makeTmlType(n - 1))
    }

    def apply(hrs : Hrs) = {
        val hors = new Hors
        // then convert to hors
        val env = inferTypes(hrs)
        translateRules(hrs, hors, env)
        translateInitNonTerms(hrs, hors, env)
        translateAut(hrs, hors, env)
        translateDoms(hrs, hors)
        hors
    }

    def inferTypes(hrs : Hrs) : TypeEnv = {
        // initalise env
        val env = makeInitTypeEnv(hrs)
        // first infer all types
        for ((nt, (args, rhs)) <- hrs.rules) {
            val ty = inferType(nt, args, rhs, env)
            val v = env.lookup(nt)
            env.doUnify(v, ty)
        }
        // the property automaton can teach us more about the terminals
        for (((_, t), args) <- hrs.propAutRules) {
            val ty = makeTmlType(args.length)
            val v = env.lookup(t)
            env.doUnify(v, ty)
        }
        env
    }

    def inferType(nt : UTNonTerm, 
                  args : List[UTVar], 
                  rhs : UntypedTerm, 
                  env : TypeEnv) : Type = {
        args match {
            // case Nil => env.zonk(inferType(rhs, env))
            // zonking above conflicts with doms (since it will assume unbound
            // types are ground not a possible domain value, so don't zonk (it
            // seems to work for now)
            //
            // TODO: check this is ok to do...
            case Nil => inferType(rhs, env)
            case vbl :: rest => {
                val varTy = newMetaTyVar()
                val newEnv = env.extendEnv(vbl, varTy)
                val bodyTy = inferType(nt, rest, rhs, newEnv)
                makeArr(varTy, bodyTy)
            }
        }
    }

    def inferType(t : UntypedTerm, env : TypeEnv) : Type 
        = checkType(t, None, env)

    /**
     * @param t the term to type
     * @param expTy optional expected type
     * @param env the type environment 
     * @return the type of t
     */
    def checkType(t : UntypedTerm, 
                  expTy : Option[Type],
                  env : TypeEnv) : Type = t match {
        case UTNonTerm(_) => {
            var ntTy = env.lookup(t)
            env.unify(ntTy, expTy)
        }
        case UTTerminal(_) => {
            var tmlTy = env.lookup(t)
            env.unify(tmlTy, expTy)
        }
        case UTVar(_,_,_) => {
            val varTy = env.lookup(t)
            env.unify(varTy, expTy)
        }
        case UTDomConst(_) => {
            val domTy = env.lookup(t)
            env.unify(domTy, expTy)
        }
        case UTApp(fun, arg) => {
            val funTy = inferType(fun, env)
            val (argTy, resTy) = env.unifyFun(funTy)
            checkType(arg, Some(argTy), env)
            env.unify(resTy, expTy)
        }
        case UTCaseTerm(cond, cases) => {
            val condTy = inferType(cond, env)
            cases match {
                case Nil => 
                    throw new TypeInferenceException(t + " has no cases.")
                case alt :: alts => {
                    var caseTy = inferType(alt, env)
                    caseTy = env.unify(caseTy, expTy)
                    for (alt <- alts) {
                        var altTy = inferType(alt, env)
                        altTy = env.unify(altTy, expTy)
                        caseTy = env.unify(altTy, Some(caseTy))
                    }
                    caseTy
                }
            }
        }
        case _ => 
            throw new TypeInferenceException("Unhandled Hrs Term " + 
                                             t.toString +
                                             " in hrs to tors translation.")
    }


    def getMetaTyVars(types : HashSet[Type]) : HashSet[MetaVariable] = {
        // TODO, maybe not needed
        new HashSet[MetaVariable]
    }

    def makeArr(t1 : Type, t2 : Type) : Type = Arrow(t1, t2)


    def translateRules(hrs : Hrs, hors : Hors, env : TypeEnv) = {
        for ((nt, (args, rhs)) <- hrs.rules) {
            val (horsNT, horsRHS) = translateRule(nt, rhs, args, env)
            hors.rules.put(horsNT, horsRHS)
        }
    }

    def translateRule(nt : UTNonTerm,
                      rhs : UntypedTerm,
                      args : List[UTVar],
                      env : TypeEnv) : (NonTerm, Term) = {
        val ty = env.zonkUp(nt)
        val varTypes = new HashMap[UTVar, Type]
        getVarTypes(args, ty, varTypes)
        val horsNT = hrsNonTermToHorsTerm(nt, env)
        val horsRHS = hrsTermToHorsTerm(rhs, varTypes, env)
        (horsNT, horsRHS)
    }

    def getVarTypes(args : List[UTVar],
                    ty : Type,
                    varTypes : HashMap[UTVar, Type]) : Unit =  (args, ty) match {
        case (Nil, Ground()) => () // ended well
        case (Nil, DomainType(_)) => () // also ended well
        case (vbl :: rest, Arrow(vblTy, restTy)) => {
            varTypes.put(vbl, vblTy)
            getVarTypes(rest, restTy, varTypes)
        }
        case _ => {
            throw new TypeInferenceException("Error extracting types for " +
                                             args.mkString(" ") + 
                                             "variables from type " +
                                             ty.toString)
        }
    }

    def hrsNonTermToHorsTerm(t : UTNonTerm, env : TypeEnv) : NonTerm = t match {
        case UTNonTerm(id) => NonTerm(id, env.zonkUp(t))
    }


    def hrsTermToHorsTerm(t : UntypedTerm, 
                          varTypes : HashMap[UTVar, Type],
                          env : TypeEnv) : Term = t match {
        case UTNonTerm(id) => 
            hrsNonTermToHorsTerm(UTNonTerm(id), env)

        case UTTerminal(id) => 
            hrsTmlToHorsTml(UTTerminal(id), env)

        case UTDomConst(dom) => 
            hrsDomConstToHorsDomConst(UTDomConst(dom), env)

        case UTVar(nt, pos, id) => 
            hrsVarToHorsVar(UTVar(nt, pos, id), varTypes, env)

        case UTCaseTerm(cond, alts) => 
            hrsCaseToHorsCase(UTCaseTerm(cond, alts), varTypes, env) 
        
        case UTApp(fun, arg) => 
            hrsAppToHorsApp(UTApp(fun, arg), varTypes, env)  

        case _ => {
            throw new TypeInferenceException("hrsTermtoHorsTerm cannot translate " +
                                             t.toString) 
        }
    }

    def hrsTmlToHorsTml(t : UTTerminal, env : TypeEnv) = {
        env.zonkUp(t) match {
            case Ground() => {
                App(Terminal(t.id, Arrow(Ground(), Ground())), dummyTml, Ground())
            }
            case ty => Terminal(t.id, ty)
        }
    }

    def hrsDomConstToHorsDomConst(t : UTDomConst, env : TypeEnv) = {
        val ty = env.zonkUp(t)
        ty match {
            case DomainType(_) => {
                DomConst(t.id, ty)
            }
            case _ => throw new TypeInferenceException("Infered non dom type " +
                                                       ty + 
                                                       " for domain const " +
                                                       t)
        }
    }

    def hrsVarToHorsVar(t : UTVar, 
                        varTypes : HashMap[UTVar, Type],
                        env : TypeEnv) = {
        varTypes.get(t) match {
            case Some(ty) => {
                val horsNT = hrsNonTermToHorsTerm(t.nonTerm, env)
                Var(horsNT, t.pos, t.id, ty)
            }
            case None => {
                throw new TypeInferenceException("hrsTermToHorsTerm " +
                                                 "does not know type of " +
                                                 t.toString)
            }
        }

    }


    def hrsCaseToHorsCase(t : UTCaseTerm, 
                          varTypes : HashMap[UTVar, Type],
                          env : TypeEnv) = {
        val horsCond = hrsTermToHorsTerm(t.cond, varTypes, env)

        horsCond.getType match {
            case DomainType(dom) => {
                if (dom.getValues.size != t.cases.size) 
                    throw new TypeInferenceException("Number of branches in " +
                                                     t + 
                                                     " does not match dom " +
                                                     dom +
                                                     " inferred as type for " +
                                                     t.cond)
            } 
            case _ => 
                throw new TypeInferenceException("Inferred non-domain type " +
                                                 horsCond.getType +
                                                 " for case condition in " +
                                                 t)
        }

        val horsCases = t.cases.map ((c : UntypedTerm) => {
            hrsTermToHorsTerm(c, varTypes, env)
        })
        CaseTerm(horsCond, horsCases, horsCases.head.getType)    
    }



    def hrsAppToHorsApp(t : UTApp, 
                        varTypes : HashMap[UTVar, Type],
                        env : TypeEnv) = {
        val horsFun = hrsTermToHorsTerm(t.fun, varTypes, env)
        val horsArg = hrsTermToHorsTerm(t.arg, varTypes, env)
        horsFun.typ match {
            case Arrow(argTyp, retTyp) if (argTyp == horsArg.typ) => {
                App(horsFun, horsArg, retTyp)
            }
            case _ => {
                throw new TypeInferenceException("hrsTermtoHorsTerm " +
                                                 " cannot translate " +
                                                 t.toString + 
                                                 " of a type mismatch " +
                                                 "(this is a bug!).") 
            }
        }
    }



    def translateInitNonTerms(hrs : Hrs, hors : Hors, env : TypeEnv) {
        for (nt <- hrs.initNonTerms) {
            hors.initNonTerms.add(hrsNonTermToHorsTerm(nt, env))
        }
    }

    def translateAut(hrs : Hrs, hors : Hors, env : TypeEnv) = {
        for (q <- hrs.getPropAutStates) {
            for (a <- hrs.terminals) {
                addTransitions(q, a, hrs, hors, env)
            }
        }
        // should only really be one i guess
        for (q <- hrs.initPropAutStates) {
            hors.initPropAutStates.add(q)
        }
        hors.failPropAutStates.add(failState)
    }

    def hrsTmlToHorsTmlAut(t : UTTerminal, env : TypeEnv) : Terminal = {
        env.zonkUp(t) match {
            case Ground() => Terminal(t.id, Arrow(Ground(), Ground()))
            case ty => Terminal(t.id, ty)
        }
    }

    def addTransitions(q : PropAutState,
                       a : UTTerminal,
                       hrs : Hrs,
                       hors : Hors,
                       env : TypeEnv) {
        hrs.propAutRules.get((q, a)) match {
            case Some(Nil) => {
                // nothing to add, this would be immediate acceptance in a hrs
                // hence there will be no run to _fail
            }
            case Some(dests) => {
                val horsA = hrsTmlToHorsTmlAut(a, env)
                for ((dest, i) <- dests.zipWithIndex) {
                    hors.propAutRules.put((q, horsA, i+1), dest)
                }
            }
            case None => {
                val horsA = hrsTmlToHorsTmlAut(a, env)
                env.zonkUp(a).getArity match {
                    case 0 => hors.propAutRules.put((q, horsA, 1), failState)
                    case n => {
                        for (i <- 1 to n) {
                            hors.propAutRules.put((q, horsA, i), failState)
                        }
                    }
                }
            }
        }

    }

    def translateDoms(hrs : Hrs, hors : Hors) = {
        for (dom <- hrs.getDomains)
            hors.addDomain(dom)

    }
}
