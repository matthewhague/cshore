/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.hors

import scala.collection.immutable.HashMap
import ho.util.LazyWrapper

object HorsEvaluator {
	
	def apply(hors : Hors, state : PropAutState, term : Term) : EvalTree =
	  evalTreeForHors(hors, state, term)
	 
	def apply(hors : Hors, path : HorsPath) : Reduction = 
	  evalHorsForPath(hors : Hors, path : HorsPath)
	  
	
	
    
	/**
	 * Compute lazy evaluation tree for a hors from a given propAutState and term.
	 * 
	 */
	def evalTreeForHors( hors : Hors, state : PropAutState, term : Term) : EvalTree = {
		if (hors.failPropAutStates.contains(state))
		    Error(state, term)
		else {
			val head = term getHead
			val args = term getArgsOfHead
	
			head match {
				case NonDet(left, right, _) 
					=> NonDetNode(state, term, LazyWrapper(evalTreeForHors(hors, state, quickApp(left, args))), 
								LazyWrapper(evalTreeForHors(hors, state, quickApp(right, args))))
	  	
					
	  
				case tml : Terminal 
					=> {
						  val children : List[LazyWrapper[EvalTree]] = 
						    for((arg, i) <- args.zipWithIndex) 
						    	yield { 
						    		hors.getNextState(state, tml, i + 1) match {
						    			case None => LazyWrapper(DeadEnd()) : LazyWrapper[EvalTree]
						    			case Some(nextState) 
						    				=> LazyWrapper(evalTreeForHors(hors, nextState, arg))
						    		}
						    	}
					 	 TerminalNode(state, term, children)
					}
				
				case nonTerm : NonTerm 
					=> if (hors.failNonTerms.contains(nonTerm))
							Error(state, term)
					  else {
						  hors.getRhs(nonTerm) match {
						  case None => DeadEnd() // No rule exists for non-terminal so cannot evaluate
							  			// further
						  case Some(rhs) => {
						     val bindingMap : HashMap[Int, Term] = 
							  (args.zipWithIndex).foldLeft(HashMap[Int, Term]()){case (m, (t, i))
						       	=> m + ((i+1) -> t)}
						     val newTerm = rhs resolveBindings(bindingMap)
							  NonTermNode(state, term, LazyWrapper(evalTreeForHors(hors, state, newTerm))) 
						  }
					  }
				  }
				
				case dum : Dummy => DeadEnd()
				  
				case vari : Var
					=> DeadEnd() // Term is not open and cannot evaluate if unbound variable 
						// at head. 
			
			
				  
				
				case _ => assert(false, "Should not have an App for a head"); null
			}
		}
	}
	
	
	
    /**
     * @param 	hors	Hors to reduce
     * @param	path	specification of how it should be reduced
     * @param 	return	list of (PropAutState, Term, Direction) pairs indicating
     * 					the reduction that results. Reduction will terminate
     * 					as soon as a direction in the path is reached that cannot be fired.
     */
	def evalHorsForPath(hors : Hors, path : HorsPath) : Reduction = {
	   val evalTree = evalTreeForHors(hors, path._1, path._2)
	   evalHorsForPath(evalTree, path._3, Nil) reverse
	}
	
	//Warning: Note that list returned from this function needs to be reversed to get correct Path
	private def evalHorsForPath(evalTree : EvalTree, dirs : List[Direction], redSoFar : Reduction) 
		: Reduction = {
			dirs match {
			  case Nil => redSoFar
			  case dir :: nextDirs => {
			    evalTree match {
			      case NonDetNode(state, term, left, right) =>
			        dir match {
			          case NonDetLeftDir() =>
			            evalHorsForPath(left, nextDirs, (state, term, NonDetLeftDir()) :: redSoFar)
			          case NonDetRightDir() =>
			            evalHorsForPath(right, nextDirs, (state, term, NonDetLeftDir()) :: redSoFar)
			          case _ =>  redSoFar
			        }
			      case TerminalNode(state, term, children) =>
			        dir match {
			          case BranchDir(i) =>
			            	if(i > children.length)
			            	  redSoFar
			            	else
			            	  evalHorsForPath(children(i-1), nextDirs, (state, term, BranchDir(i)) :: redSoFar)
			          case _ => redSoFar
			        }
			      case NonTermNode(state, term, child) =>
			        dir match {
			          case RewriteDir() => 
			            evalHorsForPath(child, dirs, (state, term, RewriteDir()) :: redSoFar)
			          case _ => redSoFar
			        }
			      case DeadEnd() => redSoFar
			      case Error(state, term) => 
			        dir match {
			          case FailDir() =>
			            (state, term, FailDir()) :: redSoFar
			          case _ => redSoFar
			        }
			        
			    }
			  }
			    
			}
	  
	}
	
	
	private def quickApp(head : Term, args : List[Term]) = 
	  head.applyTo(args) match {
	  	case GoodApp(result) => result
	  	case BadApp(result) => assert(false, "We were assuming that this application would succeed"); null
	}
		  		
	
}



/**
 * Case class for resolving direction choices during scheme evaluation
 */
sealed abstract class Direction

case class NonDetLeftDir extends Direction
case class NonDetRightDir extends Direction
case class BranchDir(i : Int) extends Direction
case class RewriteDir extends Direction
case class FailDir extends Direction // when searching for failure nodes used to indicate final failing
							// node in the reduction.


/**
 * Type of evaluation trees of a Hors. A list of directions may represent
 * a path in a given evaluation tree (and the list of directions is what
 * (when it is implemented) would be extracted as witness from Cpds algorithm).
 * 
 * Nodes are labelled with propAutState and term at this particular point during evaluation.
 *  
 */

sealed abstract class EvalTree

case class NonDetNode(state : PropAutState, term : Term, 
			left : LazyWrapper[EvalTree], right : LazyWrapper[EvalTree])
				extends EvalTree
case class TerminalNode(state: PropAutState, term : Term, children : List[LazyWrapper[EvalTree]]) 
		extends EvalTree
case class NonTermNode(state : PropAutState, term : Term, child : LazyWrapper[EvalTree]) extends EvalTree
case class DeadEnd extends EvalTree // Child of a NonTermNode when it is not possible
									// to evaluate a non-terminal due to lack of rule
									// or if unbound variable at head position, or if blocked
									// due to lack of property automaton transition 
case class Error(state : PropAutState, term : Term) extends EvalTree // Used in place of a 
									//node that would be an error state
 									// (due to either non-terminal or property automaton;
									// the parameters contain the would-be term/ property automaton
									// state.










