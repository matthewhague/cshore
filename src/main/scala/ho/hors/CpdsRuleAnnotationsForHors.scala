/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.hors

import ho.structures.cpds.RuleAnnotation


/**
 * Used to annotate CPDS rules to indicate what they correspond to
 * in terms of HORS evaluation. Can also be used to specify
 * a particular sequence of HORS operations used to evaluate a particular
 * path in a particular element of the set of value trees it generates.
 * 
 */
abstract class HorsEvaluationSpecification extends RuleAnnotation 

/**
 * The first three indicate when CPDS rules correspond to choosing one of two
 * options for non-deterministic choice, and picking the child of a terminal
 * (determinacy of property automaton means this uniquely determines the 
 * corresponding action by the scheme.
 */
case class LeftNonDetChoice extends RuleAnnotation
case class RightNonDetChoice extends RuleAnnotation
case class TerminalChild(child : Int) extends RuleAnnotation

/**
 * The next indicates when a CPDS rule corresponds to rewriting to the
 * RHS of a Hors rule.
 */
case class Rewrite extends RuleAnnotation

/**
 * The next is used when the rule is proceeding in the search for 
 * term to which some variable is bound.
 */
case class FindBinding extends RuleAnnotation

/**
 * The next is used when transitioning to an error state
 */
case class ToError extends RuleAnnotation
