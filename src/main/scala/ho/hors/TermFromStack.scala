/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.hors

import ho.structures.cpds.CpdsStack
import ho.structures.cpds.SCharacterTerm
import ho.managers.Managers
import scala.collection.immutable.HashSet
import scala.collection.immutable.HashMap

final class UndefinedLabel(msg : String) extends DummyLabel {
  override def toString : String = "Undefined: " + msg
}

object TermFromStack {
    // Extract the term represented by a CPDS stack
    // assuming that the stack is made up of an appropriate
    // alphabet. If an inappropriate stack character
    // or empty stack is reached, then  a Dummy labelled
    // with an UndefinedLabel is used. 
    // We assume that the type of term represented by stack 
  // is order-0... if it is not we will get some UndefinedLabel Dummy terms
  // in place of the missing arguments.
	def apply (stack : CpdsStack) : Term = extractTerm(stack, 0, 0)
	
	// Extract term with given arity represented by a stack when pos = 0, when
	// pos > 0 extract term that is pos th argument of top element of stack
	// THe arity is necessary to be able to know how many `excess' arguments
	// should be collected (as a higher order term on the stack 
	// might or might not yet have had its parameters bound)
	private def extractTerm (stack : CpdsStack, pos : Int, arity : Int) : Term = {
	   if(stack.getTop1 == null)  Dummy(new UndefinedLabel("Hit empty"), Ground()) //Type here doesn't really
	   //matter as types of dummies are changed as necessary during applications.
	   else {
	   stack.getTop1() match {
	     case schar : SCharacterTerm =>
	       	
	        
	       
	        if(pos > 0) {
	        	 // We are searching for the pos th argument
	             // of the top symbol.
	        	 val head = schar.getTerm.getHead
	        	 val args = schar.getTerm.getArgsOfHead
	        	 if(pos <= args.length) {
	        	   // We can mutate the stack safely here...
	        	   stack.applyRew(Managers.scManager.makeCharacter(args(pos -1)))
	        	   extractTerm(stack, 0, arity)
	        	 }
	        	 else {
	        	   // Again we can safely mutate the stack
	        	   if(stack.getOrder == schar.getTerm.getOrder)
	        	     stack.applyPop(1.toByte)
	        	   else 
	        	     stack.applyCollapse((stack.getOrder - schar.getTerm.getOrder + 1).toByte)
	        	   
	        	   extractTerm(stack, pos - args.length, arity)
	        	 }
	        	 	
	        }
	        else {
	          
	          
	            // First collect non-syntactic arguments of top element.
	        val excessArgs : List[Term]= 
	          if(schar.getTerm.getArity == arity)
	            Nil
	          else if(schar.getTerm.getArity < arity)
	            List(Dummy(new UndefinedLabel("Term: " + schar.getTerm + "insufficient arity, less than: " + arity),
	                Ground())) //Again type of Dummy doesn't matter as will be changed when applied...
	          else 
	        	  for(i <- (1 to (schar.getTerm.getArity - arity)).toList)
	        	    yield extractTerm(new CpdsStack(stack), i, 
	        	    			schar.getTerm.getArgTypes(i - 1).getArity)
	        
	        //Now resolve bindings of variables in syntactic term actually on top of stack:
		     	val varIndices = getVarIndices(schar.getTerm)
		     	val bindings : Map[Int, Term] = 
		     	  (for((i, ar) <- varIndices)
		     	    yield { val nextStack = (new CpdsStack(stack))
		     	    		nextStack.applyPop(1) // Searching for binding below as syntacitic argument.
		     		  		i -> extractTerm(nextStack, i, ar)}) toMap
		     	    
		     	schar.getTerm.resolveBindings(bindings).applyToForce(excessArgs)
		     	
	        }
	     case _ => Dummy(new UndefinedLabel("Alien stack char."), Ground()) // Not a stack character that can use.
	   }  
	   }
	}
	
	
	
	// Returns set of variable indices used in term together with arity of its type (index, arity).
	private def getVarIndices(term : Term) : Set[(Int, Int)] = 
	  term match {
	  	case App(left, right, _) => getVarIndices(left) ++ getVarIndices(right)
	  	case NonDet(left, right, _) => getVarIndices(left) ++ getVarIndices(right)
	  	case Var(_, i, _, typ) => HashSet((i, typ.getArity)) 
	  	case _ => HashSet()
	  	
	}
	
}
