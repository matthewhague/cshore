/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.hors

import scala.collection.mutable.HashMap
import scalaz.NewType
import scala.collection.mutable.HashSet
import scala.collection.immutable.IndexedSeq

sealed class Hors {
  val rules = new HashMap[NonTerm, Term] 
  val failNonTerms = new HashSet[NonTerm]
  val initNonTerms = new HashSet[NonTerm]
  val domains = new HashMap[String,Domain]
  
  // The follwoing maps (q, g, i) onto state q' that automaton should go to 
  // when reading terminal g in state q and going down ith child:
  val propAutRules = new HashMap[(PropAutState, Terminal, Int), PropAutState]
  val failPropAutStates = new HashSet[PropAutState]
  val initPropAutStates = new HashSet[PropAutState]
  
  
  
  /** 
   * @param 	nonTerm 	A non-terminal
   * @return 				The RHS of the rule (enclosed in Some(_)) if there is a rule
   * 						for nonTerm, otherwise None
   */
  def getRhs(nonTerm : NonTerm) : Option[Term] = rules.get(nonTerm)

  /**
   * @param name the name of the domain
   * @return the set of domain values
   */
  def getDomain(name : String) : Option[Domain] =
    domains.get(name)

  /**
   * @param dom the domain to add to the hrs
   */
  def addDomain(dom : Domain) = domains.put(dom.getName, dom)


  /**
   * @return iterator over all domains
   */
  def getDomains : Iterator[Domain] =
    domains.values.iterator
  
  def getPropAutStates : Iterator[PropAutState] = {
//		  (initPropAutStates.iterator ++ propAutRules.valuesIterator)
		  var retSet = HashSet[PropAutState]()
		  for(x <- (initPropAutStates.iterator ++ propAutRules.valuesIterator)) retSet = retSet + x
		  retSet.iterator // ugly way to ensure unicity (non-unicity was causing duplicate CPDS rules to be 
		  // created)
  }
  
  
  // Look up what property automaton takes a particular state to when reading a terminal symbol
  // (if a rule for that property automaton exists, otherwise return None
  def getNextState(propAutState : PropAutState, tml : Terminal, pos : Int) : Option[PropAutState] 
		  = propAutRules.get((propAutState, tml, pos))
		  

		 /**
		  * Get the order of the recursion scheme. This is horrible and rubbishy encapsulation
		  * is responsible. Must hide rules map etc. and have order computed on fly
		  * as non-terminals added. Would mean adjusting parser code. Yuk!
		  * 
		  * Also assume only called once hors is complete (to memoise order).
		  */
		  private var orderIsMemoed : Boolean = false
		  private var memoOrder : Byte = 0
  def getOrder : Byte = {
    if (!orderIsMemoed) {
	    var order = 0
	    for(nonTerm <- rules.keySet) if(nonTerm.getOrder > order) order = nonTerm.getOrder
	    memoOrder = order.toByte
	    orderIsMemoed = true
	    
	}
    memoOrder
  }
  
  
  
  
  override def toString = {
    "===========================\n" +
    "Non-Terminals are: \n" +
    rules.keys.map({nonTerm => {nonTerm.toString + " : " + nonTerm.typ.toString}}).mkString("\n") + "\n" +
    "=========================== \n" +
    "Initial Non-Terminals are: \n" +
    initNonTerms.mkString(", ") + "\n" + 
    "=========================== \n" +
    "Failing Non-Terminals are: \n" +
    failNonTerms.mkString(", ") + "\n" + 
    "===========================\n" +
    "HORS Rules are: \n" +
    rules.map({case (lhs, rhs) => {lhs.toString + "  =  " + rhs.toString}}).mkString("\n") + "\n" + 
    "===========================\n" +
    "Initial Prop. Aut. State are: \n" +
    initPropAutStates.mkString(", ") + "\n" +
    "===========================\n" +
    "Failing Prop. Aut. State are: \n" +
    failPropAutStates.mkString(", ") + "\n" +
    "===========================\n" +
    "Prop. Aut. Rules are: \n" +
    propAutRules.map({case ((state, tml, child), nextState) => 
      state.toString + " --[" + tml.toString + ", " + child.toString + "]-->" + nextState.toString}).
      	mkString("\n")
     //TODO: should make 
    								//more readable by putting all successor states in same rule.
  }
    
}


abstract sealed class Type {
  val getOrder : Byte = this match {
    case DomainType(_) => 0
    case Ground() => 0
    case Arrow(arg, ret) => scala.math.max((1 + arg.getOrder), ret.getOrder).toByte
    case MetaVariable(_) => 0
  }
  
  val getArity : Int = this match {
    case DomainType(_) => 0
    case Ground() => 0
    case Arrow(arg, ret) => 1 + ret.getArity
    case MetaVariable(_) => 0
  }
  
  val getArgTypes : List[Type] = (this match {
    case DomainType(_) => Nil
    case Ground() => Nil
    case Arrow(arg, ret) => arg :: ret.getArgTypes 
    case MetaVariable(_) => Nil
  }) reverse
  
  override def toString : String = this match {
      case DomainType(domain) => domain.getName
      case Ground() => "0"
      case Arrow(Ground(), ret) => "0 => " + ret.toString()
      case Arrow(arg, ret) => "(" + arg.toString() + ") =>" + ret.toString()
      case MetaVariable(id) => "<" + id + ">"
    }


  override def equals(o : Any) : Boolean = 
    (this, o) match {
        case (DomainType(d1), DomainType(d2)) => (d1 == d2)
        case (Ground(), Ground()) => true
        case (Arrow(a1, r1), Arrow(a2, r2)) => (a1 == a2) && (r1 == r2)
        case (MetaVariable(id1), MetaVariable(id2)) => (id1 == id2)
        case _ => false
    }
}
case class DomainType(domain : Domain) extends Type {
    def getDomain : Domain = domain
}
case class Ground extends Type
case class Arrow(arg : Type, ret : Type) extends Type

// While infering types we need metavariables for as yet undetermined types
case class MetaVariable(id : String) extends Type



abstract sealed class AppAttempt
case class GoodApp(app : Term) extends AppAttempt
case class BadApp(errorMsg : String) extends AppAttempt with DummyLabel //DummyLabel
							//means can be used to label a Dummy when we want
 						// a placeholder for badly typed subterms in a term.

abstract sealed class CaseAttempt
case class GoodCase(app : Term) extends CaseAttempt
case class BadCase(errorMsg : String) extends CaseAttempt

object Term {
  
  /**
   * Attempt to create a new term by applying list of arguments
   * to head.
   */
  def apply (head : Term, args : List[Term]) : AppAttempt = 
    args match {
    case Nil => GoodApp(head)
    case arg :: nextArgs 
    	=> head.applyTo(arg) match {
    	  case GoodApp(app) => Term(app, nextArgs)
    	  case badApp : BadApp => badApp
    	}
  	}

   
  /**
   * Generates a term with variables replaced by terms to which they are bound
   * as specified by bindings map. The Int specifies the `position' of the variable
   * to be substituted (as in position on non-terminal on LHS of a rule).
   * 
   * Uses applyToForce so that e.g. when constructing from a Cpds stack
   * we generate a term even if arrangement of stack disrespects typing, with 
   * suitable labelling of bad typing.
   */
  def resolveBindings(openTerm : Term, bindings : Map[Int, Term]) : Term = 
    openTerm match {
    	case App(u, v, typ) => resolveBindings(u, bindings).applyToForce(resolveBindings(v, bindings))
    	case nt : NonTerm => nt
    	case tml : Terminal => tml
    	case NonDet(left, right, typ) => NonDet(resolveBindings(left, bindings), 
    										resolveBindings(right, bindings), typ)
    	case Var(_, i, _, _) => bindings.get(i) match {
    	  										case Some(boundTerm) 
    	  												=> boundTerm 
    	  										case None => openTerm
    	  									
    										}
        case CaseTerm(cond, branches, typ) => {
            val condRes = resolveBindings(cond, bindings)
            var branchesRes = Nil
            def doBranch(b : Term) = resolveBindings(b, bindings)
            val branchRes = branches.map(doBranch)
            CaseTerm(condRes, branchRes, typ)

        }
        case dc : DomConst => dc
    	case dmy : Dummy => dmy
  }
}

abstract sealed class Term( val typ : Type ) {
  val getOrder : Byte = typ.getOrder
  val getArity : Int = typ.getArity
  val getArgTypes : List[Type] = typ.getArgTypes
  val getType : Type = typ
  
  def resolveBindings(bindings : Map[Int, Term]) : Term 
  	= Term.resolveBindings(this, bindings)
  
  /**
   *  @return largest subterm in function position that is not an application
   *  			(will determine next rewrite step in left-most reduction)
   */
  val getHead : Term = 
    
    this match {
    case App(fun, arg, _) => fun getHead
    case _ => this
  }
  
  /**
   * @return List of arguments of the head term (in sense above)
   * with first position in sequence being left-most 
   * 			and final position being right-most.
   */
  val getArgsOfHead : List[Term] = getArgsOfHead(List()) 
  
  private def getArgsOfHead(argsSoFar : List[Term]) : List[Term] = this match {
    case App(fun, arg, _) =>  fun getArgsOfHead(arg :: argsSoFar)
    case _ => argsSoFar 
  }
  
  // Attempts to apply argument to this term, returning GoodApp(result) if possible with types
  // and BadApp with error message otherwise.
  def applyTo(arg : Term) = 
    this.typ match {
    	case Arrow(fun, ret) =>  if(fun == arg.typ) GoodApp(App(this, arg, ret)) 
    								else BadApp(arg + " has type: " + arg.typ + " expected: " + fun
    								    + " in app to: " + this)
    	case _ => BadApp("Cannot apply term: " + this + " with non-arrow type: " + this.typ + " to: " + arg)
  }
  
  // Attempts to apply a list of arguments.
  def applyTo(args : List[Term]) : AppAttempt = 
    args match {
    	case next :: remainder => 
    	  this.applyTo(next) match {
    	    case GoodApp(result) => result.applyTo(remainder)
    	    case badApp : BadApp => badApp
    	  }
    	case Nil => GoodApp(this)
  }
  
  // The following methods are similar to above, except that they do not `fail'
  // if there is a type error. Instead they place a Dummy of a suitable type
  // instead of the argument. This is useful in e.g. extracting terms
  // from an arbitrary CPDS stack where things might not be well typed.
  // If the term does not have an arrow type, a Dummy is returned of ground type.
  // If argument is a dummy and we are apply argument to term of arrow type,
  // then dummy's type is always `changed' so as to type-check
  // so as to avoid wrapping in another a BadApp dummy.
  def applyToForce(arg : Term) : Term = 
    this.typ match {
    	case Arrow(fun, ret) =>
    	  arg match {
    	    case Dummy(label, typ) => App(this, Dummy(label, fun), ret)
    	    case _ =>
	    	  if(fun == arg.typ) App(this, arg, ret) 
	    								else App(this, Dummy(BadApp(arg + " has type: " + arg.typ + " expected: " + fun
	    								    + " in app to: " + this), fun), ret)
    	  }
    	case _ => Dummy(BadApp("Cannot apply term: " + this + " with non-arrow type: " + this.typ + " to: " + arg),
    						Ground())
  }
  
  def applyToForce(args : List[Term]) : Term =
    args match {
    	case next :: remainder => 
    	  this.applyToForce(next).applyToForce(remainder)
    	case Nil => this
  }
  
  override def toString : String = this match {
    case CaseTerm(cond, branches, _) => "case(" + 
                                        cond + "," +
                                        branches.mkString(",") +
                                        ")"
    case NonTerm(id, _) => id
    case Terminal(id, _) => id
    case DomConst(id, _) => id
    case Var(nonterm, pos, id, _) => id + "<" + nonterm + ", " + pos + ">"
    case NonDet(choice1, choice2, _) => "(" + choice1 + " | " + choice2 + ")"
    case App(fun, App(fun1, arg1, t), _) => fun + "(" + App(fun1, arg1, t) + ")"
    case App(fun, atomArg, _) => fun + " " + atomArg.toString
    case Dummy(label, t) => "<*" + label.toString + "*>"
  }
}

case class CaseTerm (cond : Term, 
                     cases : List[Term], 
                     override val typ : Type) extends Term(typ : Type) {
    val ourHashCode = {
        var hc = cond.hashCode
        // don't know a good scala way of doing this
        for (t <- cases)
            hc += t.hashCode
        hc
    }

    def getCond : Term = cond
    def getCases : List[Term] = cases

    override def hashCode : Int = ourHashCode
}

case class DomConst (id : String, override val typ : Type) extends Term(typ : Type) {
    val ourHashCode = id.hashCode + typ.hashCode
    override def hashCode : Int = ourHashCode
}

case class NonTerm (id : String, override val typ : Type) extends Term(typ : Type) {
    val ourHashCode = id.hashCode + typ.hashCode
    override def hashCode : Int = ourHashCode
}

case class Terminal (id : String, override val typ : Type) extends Term(typ : Type) {
    val ourHashCode = id.hashCode + typ.hashCode
    override def hashCode : Int = ourHashCode
}


case class Var (nonTerm : NonTerm, pos : Int, id : String, override val typ : Type) extends Term(typ : Type) {
    val ourHashCode = nonTerm.hashCode + id.hashCode + typ.hashCode + pos
    override def hashCode : Int = ourHashCode
}
// refer to varibles by position in defnition
// of given non-terminal symbol---keep id attached for convenience as well for error reporting

case class App (fun : Term, arg : Term, override val typ : Type) extends Term(typ : Type) {
    val ourHashCode = fun.hashCode + arg.hashCode + typ.hashCode
    override def hashCode : Int = ourHashCode
}

case class NonDet(choice1 : Term, choice2 : Term, override val typ : Type) extends Term(typ : Type) {
    val ourHashCode = choice1.hashCode + choice2.hashCode + typ.hashCode
    override def hashCode : Int = ourHashCode
}


// This is a special case of a non-terminal that should never be given 
case class Dummy(label : DummyLabel, override val typ : Type) extends Term(typ) {
    val ourHashCode = label.hashCode + typ.hashCode
    override def hashCode : Int = ourHashCode
}
trait DummyLabel //Trait to indicate a possible `label' for a dummy.


case class PropAutState(id : String) {
  override def toString = id

  val ourHashCode = id.hashCode
  override def hashCode : Int = ourHashCode
}


