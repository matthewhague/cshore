/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.hors

import ho.structures.cpds.CpdsAnnotatedRunTree
import ho.structures.cpds.CARTreeNode
import ho.structures.cpds.CpdsConfig
import ho.util.LazyWrapper
import ho.structures.cpds.RuleAnnotation
import ho.structures.cpds.ControlStateString
import ho.structures.cpds.Cpds
import ho.structures.cpds.ControlStateHors

object HorsTreeFromCpdsTree {
    // Take a CpdsAnnotatedRunTree and translate it into an EvalTree of a Hors.
    // If Cpds resulted from translation from a Hors, this should be an evaluation
    // tree of the Hors itself. Note that if Cpds is mal-formed the tree generated
    // by this won't necessarily be a valid evaluation tree for any Hors. There
    // are some checks placed, however, as necessary to create a well-formed 
    // Hors evaluation tree. For example the RuleAnnotations of children
    // of a node must be compatible as these determine the type of Node in 
    // Hors evalution tree of the parent. Existence of a RuleAnnotation
    // of children of a node that is a ToError annotation determines wheter
    // a node is turned into a dead end Error node in the EvalTree.
    // If unable to process subtree as a hors evaluation tree, it is replaced 
    // with a DeadEnd
  
	def apply ( cpdsTree : CpdsAnnotatedRunTree ) : EvalTree = 
			getEvalTree(cpdsTree)
	
	private def getEvalTree( cpdsTree : CpdsAnnotatedRunTree) : EvalTree =
	  cpdsTree match {
	  	case CARTreeNode(config : CpdsConfig, 
				children : List[(RuleAnnotation, LazyWrapper[CpdsAnnotatedRunTree])]) 
					=> {
						  
						  
						 config.getControlState match {
							 case csH : ControlStateHors => { 
							   //See whether children contain a transition to error (base decision
							  // on annotations)
							  val isErrorNode = 
							    children.map(
							        {case (ToError(), _)=> true; case _ => false }).fold(false)(_ || _) 
							        
								  val term = TermFromStack(config.getStack)
								  val state = csH.getPropAutState
								  
								  if(isErrorNode)
								    // Since child will transition to error state,
								    // current node should be wrapped up in an Error and be a leaf
								    // of the EvalTree
								    Error(csH.getPropAutState, term)
							     else {
							    	 //sort children so that terminals in order and that any child
							       // that is not a terminalChild is to left of terminal children.
									  val sortedChildren = children sortWith(
											  	{case ((TerminalChild(i), _), (TerminalChild(j), _)) 
										  				=> i < j
									  				 case ((TerminalChild(_), _), _) => false
									  				case _ => true })
									  
									  sortedChildren match {
									    case List((LeftNonDetChoice(), tl), (RightNonDetChoice(), tr) )
									    	=> NonDetNode(state, term, 
									    				LazyWrapper(getEvalTree(tl)), 
									    				LazyWrapper(getEvalTree(tr)))
									    case List((RightNonDetChoice(), tr), (LeftNonDetChoice(), tl) )
									    	=> NonDetNode(state, term, 
									    				LazyWrapper(getEvalTree(tl)), 
									    				LazyWrapper(getEvalTree(tr)))
									    case List((Rewrite(), t))
									    	=> NonTermNode(state, term, LazyWrapper(getEvalTree(t)))
									   
									     
									    
									    case List((FindBinding(), t)) => getEvalTree(t) // no counter part
									    	//to searching for binding in Hors, which is done immediately.
									    
									    
									    
									    // If TerminalChild is on left, then all must be TerminalChildren
									    // by sorting, moreover they must be in order. (In partifcular in event 
									    // that there are repetitions of an index we will exceed arity
									    // of terminal).
									    case (TerminalChild(i), t ) :: _ =>
									      TerminalNode(state, term, 
									          sortedChildren.map({ case (_, ti) => LazyWrapper(getEvalTree(ti))}))
									    
									    case List((TerminalChild(i), t)) =>
									      TerminalNode(state, term, 
									          sortedChildren.map({ case (_, ti) => LazyWrapper(getEvalTree(ti))}))
									          
									    
									    case _ => DeadEnd() // As we don't know how to convert to hors EvalTree
									      
									  }
							    	 	
							
									}
							
								}
							 case _ => DeadEnd() //Error states should have been handled in advance
							           // so anything else cannot be interpreted as a Hors, so finish here.
							  }
						}
				  
	}
	  
}
