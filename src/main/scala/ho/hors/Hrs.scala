/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.hors

import scala.collection.mutable.HashMap
import scalaz.NewType
import scala.collection.mutable.HashSet
import scala.collection.immutable.IndexedSeq


/**
 * Ultimatedly we should make a untyped terms a subclass of typed terms, but
 * that's a job for later...
 */


sealed class Hrs {
  val rules = new HashMap[UTNonTerm, (List[UTVar], UntypedTerm)] 
  val initNonTerms = new HashSet[UTNonTerm]
  val domains = new HashMap[String,Domain]
  val terminals = new HashSet[UTTerminal]

  val initPropAutStates = new HashSet[PropAutState]
  val propAutRules = new HashMap[(PropAutState, UTTerminal), List[PropAutState]]

  /** 
   * @param 	nonTerm 	A non-terminal
   * @return 				The RHS of the rule (enclosed in Some(_)) if there is a rule
   * 						for nonTerm, otherwise None
   */
  def getRule(nonTerm : UTNonTerm) : Option[(List[UTVar], UntypedTerm)] 
    = rules.get(nonTerm)

  /**
   * @param dom the domain to add to the hrs
   */
  def addDomain(dom : Domain) = domains.put(dom.getName, dom)

  /**
   * @param name the name of the domain
   * @return the set of domain values
   */
  def getDomain(name : String) : Option[Domain] =
    domains.get(name)

  /**
   * @return iterator over all domains
   */
  def getDomains : Iterator[Domain] =
    domains.values.iterator

  def getPropAutStates : Iterator[PropAutState] = {
      var retSet = HashSet[PropAutState]()
      for (q <- initPropAutStates)
        retSet.add(q)
      for (qs <- propAutRules.values;
           q <- qs)
        retSet.add(q)      
      retSet.iterator 
  }
 

  
  override def toString = {
    "===========================\n" +
    "Non-Terminals are: \n" +
    rules.keys.map({nonTerm => {nonTerm.toString}}).mkString(" ") + "\n" +
    "=========================== \n" +
    "Initial Non-Terminals are: \n" +
    initNonTerms.mkString(" ") + "\n" + 
   "===========================\n" +
    "Terminals are: \n" +
    terminals.map({terminal => {terminal.toString}}).mkString(" ") + "\n" +
    "=========================== \n" +
    "HRS Rules are: \n" +
    rules.map({case (lhs, (args, rhs)) => {
                  lhs.toString + " " +
                  args.mkString(" ") + "  =  " +
                  rhs.toString
              }}).mkString("\n") + "\n" +
    "=========================== \n" +
    "Initial Prop. Aut. States are: \n" +
    initPropAutStates.mkString(", ") + "\n" + 
    "=========================== \n" +
    "Prop. Aut. Rules are: \n" +
    propAutRules.map({
        case ((state, tml), nextStates) => 
            state.toString + " " + 
            tml.toString + " ->  " + 
            nextStates.mkString(" ") + "."
    }).mkString("\n") 
  }
}

object UntypedTerm {
  
  /**
   * Attempt to create a new term by applying list of arguments
   * to head.
   */
  def apply (head : UntypedTerm, args : List[UntypedTerm]) : UntypedTerm = 
    args match {
    case Nil => head
    case arg :: nextArgs => UntypedTerm(head.applyTo(arg), nextArgs)
  	}
}

abstract sealed class UntypedTerm() {
  /**
   *  @return largest subterm in function position that is not an application
   *  			(will determine next rewrite step in left-most reduction)
   */
  val getHead : UntypedTerm = 
    this match {
    case UTApp(fun, arg) => fun getHead
    case _ => this
  }
  
  /**
   * @return List of arguments of the head term (in sense above)
   * with first position in sequence being left-most 
   * 			and final position being right-most.
   */
  val getArgsOfHead : List[UntypedTerm] = getArgsOfHead(List()) 
  
  private def getArgsOfHead(argsSoFar : List[UntypedTerm]) : List[UntypedTerm] = 
    this match {
      case UTApp(fun, arg) =>  fun getArgsOfHead(arg :: argsSoFar)
      case _ => argsSoFar 
    }
  
  def applyTo(arg : UntypedTerm) = UTApp(this, arg) 
  
  def applyTo(args : List[UntypedTerm]) : UntypedTerm = 
    args match {
    	case next :: remainder => 
    	  this.applyTo(next).applyTo(remainder)
        case Nil => this
    }
  
  override def toString : String = this match {
    case UTCaseTerm(cond, branches) => "case(" + 
                                       cond + "," +
                                       branches.mkString(",") +
                                       ")"
    case UTNonTerm(id) => id
    case UTTerminal(id) => id
    case UTDomConst(id) => id
    case UTVar(nonterm, pos, id) => id + "<" + nonterm + ", " + pos + ">"
    case UTApp(fun, UTApp(fun1, arg1)) => fun + "(" + UTApp(fun1, arg1) + ")"
    case UTApp(fun, atomArg) => fun + " " + atomArg.toString
  }
}

case class UTCaseTerm (cond : UntypedTerm, 
                       cases : List[UntypedTerm]) extends UntypedTerm() {
    val ourHashCode = {
        var hc = cond.hashCode
        // don't know a good scala way of doing this
        for (t <- cases)
            hc += t.hashCode
        hc
    }

    def getCond : UntypedTerm = cond
    def getCases : List[UntypedTerm] = cases

    override def hashCode : Int = ourHashCode
}

case class UTDomConst (id : String) extends UntypedTerm() {
    val ourHashCode = id.hashCode 
    override def hashCode : Int = ourHashCode
}

case class UTNonTerm (id : String) extends UntypedTerm() {
    val ourHashCode = id.hashCode 
    override def hashCode : Int = ourHashCode
}

case class UTTerminal (id : String) extends UntypedTerm() {
    val ourHashCode = id.hashCode 
    override def hashCode : Int = ourHashCode
}


case class UTVar (nonTerm : UTNonTerm, 
                  pos : Int, 
                  id : String) extends UntypedTerm() {
    val ourHashCode = nonTerm.hashCode + id.hashCode + pos
    override def hashCode : Int = ourHashCode
}
// refer to varibles by position in defnition
// of given non-terminal symbol---keep id attached for convenience as well for error reporting

case class UTApp (fun : UntypedTerm, arg : UntypedTerm) extends UntypedTerm() {
    val ourHashCode = fun.hashCode + arg.hashCode 
    override def hashCode : Int = ourHashCode
}



