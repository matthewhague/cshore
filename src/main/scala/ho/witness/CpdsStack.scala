/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.witness

import ho.structures.cpds.Rule
import ho.structures.cpds.PushRule
import ho.structures.cpds.CPushRule
import ho.structures.cpds.EvalNonTermRule
import ho.structures.cpds.PopRule
import ho.structures.cpds.CollapseRule
import ho.structures.cpds.RewRule
import ho.structures.cpds.RewLinkRule
import ho.structures.cpds.GetArgRule
import ho.structures.cpds.GetOrder0ArgRule

/**
 * An implementation of a generic CPDS stack (stack alphabet A). (I think should be slightly more efficient
 * than deep cloning as Java version currently uses... this tries to leave responsibility
 * for persistence (and avoidance of unnecessary copying) to Scala built-in datastructures). 
 * 
 * We follow semantics whereby a stack must always contain at least one character (and pop fails
 * if it tries to destroy). 
 */




sealed abstract class CpdsStack[A]
	

object CpdsStackOps{
	private	case class O1Stack[A](val contents : List[(A, Byte, Int)]) extends CpdsStack[A]
			// (A, Byte, Int) is (stack-character, order of link, target of link (measured from bottom of stack).
			// We forbid order-1 collapses and as convention set Byte to 0 whenever Int is 1 (Int being 1 
				// can be viewed as "no link"). LHS of contents is top of stack.
	
	private case class HOStack[A]( order : Byte, height : Int, 
									val contents : List[CpdsStack[A]]) extends CpdsStack[A]
			// order is order of stack, height is height of stack (used when creating links and memoises
			// length of list) and contents is list representing stack contents. LHS is top of stack.
	
	
	/** 
	 * Creates a stack of particular order with single symbol 
	 */
	def createSingleElementStack[A](  order : Byte,  symbol : A) : CpdsStack[A] = {
	  if(order == 1)
	    new O1Stack[A](List((symbol, 1.toByte, 0.toByte)))
	  else 
	    new HOStack[A](order, 1.toByte, List(createSingleElementStack[A]((order - 1).toByte, symbol)))
	}
	
	/**
	 * Returns top stack character on stack.
	 */
	def top[A](s : CpdsStack[A]) : A = s match {
	  case O1Stack((topchar, linkord, linktarg) :: rest) => topchar
	  case HOStack(_, _, top_s :: rest) => top(top_s)
      case _ => throw new Exception("CpdsStack.linkDetails can't handle " + s)
	  // Should never have another pattern as stacks should always be non-empty. 
	}
	
	private def linkDetails[A](s : CpdsStack[A]) : (Byte, Int) = s match {
	  case O1Stack((_, linkord, linktarg) :: rest) => (linkord, linktarg)
	  case HOStack(_, _, top_s :: rest) => linkDetails(top_s)
      case _ => throw new Exception("CpdsStack.linkDetails can't handle " + s)
	  // Should never have another pattern as stacks should always be non-empty.
	} 
	
	
	/**
	 * Do a pop operation. Correctness assumes order is valid for the stack
	 * and that a pop is possible (otherwise will throw pattern match exception.
	 * We could wrap in an Option to cover case when impossible pop attempted
	 * but it is more efficient just to guarantee that will never happen).
	 */
	def pop[A](order : Byte)(s : CpdsStack[A]) : CpdsStack[A]  = s match {
		case O1Stack(_ :: rest) => O1Stack(rest) 
        case O1Stack(Nil) => 
          throw new Exception("CpdsStack.pop can't handle empty o1 stack.")
		case HOStack(sorder, height, top :: rest) =>
		  if(sorder > order)
			  HOStack(sorder, height, pop(order)(top) :: rest)
		  else
		     HOStack(sorder, height - 1, rest)
        case HOStack(_, _, Nil) => 
          throw new Exception("CpdsStack.pop can't handle empty ho stack.")
	}
	
	
	
	/**
	 * Assumes stack > order 1 and link is > 1 (we disallow collapses on order-1 links).
	 * Could do Option, but as with Pop, more efficient not to and assume will only be
	 * called when possible to do the collapse.
	 */
	def collapse[A](s : CpdsStack[A]) : CpdsStack[A] = {
		val (ord, target) = linkDetails(s)
		collapsePreHelper[A](ord, target)(s)
	}

	// Navigates to top stack of order of link (where collapse will occur) then calls collapseHelper
	// to actually do the collapse
	private def collapsePreHelper[A](ord : Byte, target : Int)(s : CpdsStack[A]) : CpdsStack[A] = {
	  
	  
	  s match {
	  	case HOStack(sorder, height, top :: rest) =>
	  	  if(sorder > ord)
	  		  	HOStack(sorder, height, collapsePreHelper(ord, target)(top) :: rest)
	  	  else
	  	    collapseHelper[A](target)(s)
        case HOStack(_, _, Nil) =>
          throw new Exception("CpdsStack.collapsePreHelper can't do empty ho stack.")
        case O1Stack(_) =>
          throw new Exception("CpdsStack.collapsePreHelper can't do o1 stack.")
	  }
	}
	
	// Assumes stack of order of link has been passed---removes stacks until targetHeight is reached.
	private def collapseHelper[A](targetHeight : Int)(s : CpdsStack[A]) : CpdsStack[A] = 
			s match {
	  case HOStack(sorder, height, top :: rest) =>
	   	if(height > targetHeight)
	   	  collapseHelper(targetHeight)(HOStack(sorder, height - 1, rest))
	   	else
	   	  HOStack(sorder, height, top :: rest)
      case HOStack(_, _, Nil) =>
        throw new Exception("CpdsStack.collapseHelper can't do empty ho stack.")
      case O1Stack(_) =>
        throw new Exception("CpdsStack.collapseHelper can't do o1 stack.")
	}
	
	
    def evalNT[A](schar : A)(s : CpdsStack[A]) : CpdsStack[A] = 
	  s match {
	  	case HOStack(order, height, top :: rest) =>
	  	    HOStack(order, height, evalNT[A](schar)(top) :: rest)
        case HOStack(_, _, Nil) =>
            throw new Exception("CpdsStack.evalNT can't handle empty ho stack.")
        case O1Stack(curcontents) =>
            O1Stack((schar, 1.toByte, 0) :: curcontents)
	  	    
	}
	
	def cpush[A](schar : A, linkOrder : Byte)(s : CpdsStack[A]) : CpdsStack[A] = 
	  s match {
	  	case HOStack(order, height, top :: rest) =>
	  	  if(order == linkOrder)
	  	    HOStack(order, height, (cpushInner[A](schar, linkOrder, height - 1)(top)) :: rest)
	  	  else
	  	    HOStack(order, height, cpush[A](schar, linkOrder)(top) :: rest)
        case HOStack(_, _, Nil) =>
            throw new Exception("CpdsStack.cpush can't do empty ho stack.")
        case O1Stack(curcontents) =>
            O1Stack((schar, linkOrder, 0) :: curcontents)
	  	    
	}
	
	// For doing a cpush once inside stack of order for which wish to create a link.
	private def cpushInner[A](schar : A, linkOrder : Byte, linkTarget : Int)(s : CpdsStack[A]) : CpdsStack[A] =
	  s match {
	  		case O1Stack(curcontents) => O1Stack((schar, linkOrder, linkTarget) :: curcontents)
	  		case HOStack(order, height, top :: rest) => HOStack(order, height, 
	  		    cpushInner[A](schar, linkOrder, linkTarget)(top) :: rest)
            case HOStack(_, _, Nil) =>
                throw new Exception("CpdsStack.cpushInner can't handle empty ho stack.")
	}


	
	
    def rew[A](schar : A)(s : CpdsStack[A]) : CpdsStack[A] = 
	  s match {
        case O1Stack((_, linkord, linktarg) :: rest) =>
            O1Stack((schar, linkord, linktarg) :: rest)
        case O1Stack(Nil) =>
            throw new Exception("CpdsStack.rew can't do emtpy o1 stack.")
	  	case HOStack(order, height, top :: rest) =>
	  	    HOStack(order, height, rew[A](schar)(top) :: rest)
        case HOStack(_, _, Nil) =>
            throw new Exception("CpdsStack.rew can't do emtpy ho stack.")
	}

    
    // this just does the same as rew, but maybe we want to change it to forget
    // all stack information except top char (as it is all that is required)
    def getOrder0Arg[A](schar : A)(s : CpdsStack[A]) : CpdsStack[A] = 
	  s match {
        case O1Stack((_, linkord, linktarg) :: rest) =>
            O1Stack((schar, linkord, linktarg) :: rest)
        case O1Stack(Nil) =>
            throw new Exception("Cpds.getOrder0Arg can't handle emtpy o1 stack.")
	  	case HOStack(order, height, top :: rest) =>
	  	    HOStack(order, height, getOrder0Arg[A](schar)(top) :: rest)
        case HOStack(_, _, Nil) =>
            throw new Exception("Cpds.getOrder0Arg can't handle emtpy ho stack.")
	}


	def rewLink[A](schar : A, linkOrder : Byte)(s : CpdsStack[A]) : CpdsStack[A] = 
	  s match { 
        case HOStack(order, height, top :: rest) =>
	  	  if(order == linkOrder)
	  	    HOStack(order, height, (rewLinkInner[A](schar, linkOrder, height - 1)(top)) :: rest)
	  	  else
	  	    HOStack(order, height, rewLink[A](schar, linkOrder)(top) :: rest)
        case HOStack(_, _, Nil) =>
          throw new Exception("CpdsStack.rewLink can't handle emtpy ho stack.")
	    case O1Stack(_) =>
          throw new Exception("CpdsStack.rewLink can't handle o1 stack.")
  	    
	}

	// For doing a rewLink once inside stack of order for which wish to create a link.
	private def rewLinkInner[A](schar : A, linkOrder : Byte, linkTarget : Int)(s : CpdsStack[A]) : CpdsStack[A] =
	  s match {
	  		case O1Stack((_, _, _) :: rest) => 
                O1Stack((schar, linkOrder, linkTarget) :: rest)
            case O1Stack(Nil) =>
                throw new Exception("CpdsStack.rewLinkInner hit emtpy o1 stack.")
	  		case HOStack(order, height, top :: rest) => 
                HOStack(order, 
                        height, 
                        rewLinkInner[A](schar, linkOrder, linkTarget)(top) ::
                        rest)
            case HOStack(_, _, Nil) =>
                throw new Exception("CpdsStack.rewLinkInner hit emtpy ho stack.")
	}



    // This is just the same as rew link, but we may want to change it to a
    // getarg that may forget irrelvant parts of the stack later (esp. for
    // counter example generation i suppose)
	def getArg[A](schar : A, linkOrder : Byte)(s : CpdsStack[A]) : CpdsStack[A] = 
	  s match {
	  	case HOStack(order, height, top :: rest) =>
	  	  if(order == linkOrder)
	  	    HOStack(order, height, (getArgInner[A](schar, linkOrder, height - 1)(top)) :: rest)
	  	  else
	  	    HOStack(order, height, getArg[A](schar, linkOrder)(top) :: rest)
          case HOStack(_, _, Nil) =>
            throw new Exception("CpdsStack.getArg can't handle emtpy HOStack.")
	    case O1Stack(_) => 
          throw new Exception("CpdsStack.getArg can't handle O1Stack.")
	}

    // For doing a rewLink once inside stack of order for which wish to create a link.
	private def getArgInner[A](schar : A, linkOrder : Byte, linkTarget : Int)(s : CpdsStack[A]) : CpdsStack[A] =
	  s match {
	  		case O1Stack((_, _, _) :: rest) => 
                O1Stack((schar, linkOrder, linkTarget) :: rest)
	  		case O1Stack(Nil) => 
                 throw new Exception("CpdsStack.getArgInner can't handle Nil.")
	  		case HOStack(order, height, top :: rest) => 
                HOStack(order, 
                        height, 
                        getArgInner[A](schar, linkOrder, linkTarget)(top) ::
                        rest)
            case HOStack(_, _, Nil) => 
                throw new Exception("CpdsStack.getArgInner can't handle Nil.")
                
	}


	
	/**
	 * Do higher-order Push operation of given order, assumed to be > 1 (and assumed to be valid for the stack)
	 */
	def push[A](order : Byte)(s : CpdsStack[A]) : CpdsStack[A] =  s match {
	  case HOStack(sorder, height, top :: rest) =>
	      if(sorder > order)
	        new HOStack(sorder, height, ((push(order)(top)) :: rest))
	      else
	        new HOStack(sorder, height + 1, top :: (top :: rest))
      case HOStack(_, _, Nil) =>
          throw new Exception("CpdsStack.push can't push on an empty HOStack.")
      case O1Stack(_) =>  
          throw new Exception("CpdsStack.push can't push on an O1Stack.")
	  }

}
