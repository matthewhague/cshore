/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.structures.cpds
import ho.util.LazyWrapper
import scala.collection.JavaConversions._

object CpdsEvaluator {
	
	def apply(cpds : Cpds, initState : ControlState, initSymb : SCharacter) : CpdsAnnotatedRunTree = 
	{
	  
	  
	  var rules : List[Rule] = List()
	  for (rule <- cpds.getRules) rules = rule :: rules  //Yuk... but seem to have problems otherwise...
	  
	  CpdsEvaluator(rules, new CpdsConfig(initState, initSymb, cpds.getOrder))
	}
    def apply(cpds : Cpds, initConfig : CpdsConfig) : CpdsAnnotatedRunTree = 
    		CpdsEvaluator(cpds.getRules().toList, initConfig)
    
	def apply(rules : List[Rule], config : CpdsConfig) : CpdsAnnotatedRunTree =
		evalTreeForCpds(rules, config)
	
	/**
	 * Compute (lazy) run tree with annotations for a particular list of rules from a given
	 * configuration
	 * 
	 * We supply list of rules rather than Cpds as Cpds currently does not make it convenient
	 * to extract set of all rules. Indeed for efficient extraction one would need
	 * to index rules differently, and since I think this may only be used for testing
	 * don't want to pollute Cpds with having to maintain another index. So we'll
	 * be a bit inefficient with a list here.
	 * 
	 */
	def evalTreeForCpds (rules : List[Rule], config : CpdsConfig) : CpdsAnnotatedRunTree = {
			
			val tryRules = (rule : Rule) => {
				val deepClonedConfig = new CpdsConfig(config)
				if (deepClonedConfig.applyRule(rule))
				  List((rule.getAnnotation(), (LazyWrapper(evalTreeForCpds(rules, deepClonedConfig)))))
				else
				  Nil
				
			}
			
			CARTreeNode(config, rules.flatMap(tryRules)) 
			
	}
}
