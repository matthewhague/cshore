/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.fake.target
import org.scalacheck.Properties
import org.scalacheck.Prop._
import org.scalacheck.Gen
import ho.structures.sa.StateTranGenerator
import ho.structures.sa.SATransition
import scala.collection.mutable.HashSet
import ho.util.ManagedSet
import ho.structures.sa.SAState
import scala.collection.Iterator
import scala.util.Random

// Need function that can create a new (empty) TargetProcessor using a specific TargetCompleteAdder
// and SATransGetter
abstract class TargetProcessorSpecification ( targProcGen : ((TargetCompleteAdder, SATransGetter) => TargetProcessor)  )
	extends Properties("TargetProcessor") with StateTranGenerator 
	{
		println("In TargetProcessor property")
		
		type Target = (ManagedSet[SAState], ManagedSet[SAState], ManagedSet[SAState])
		

		val targProc = targProcGen (MockTargetCompleteAdder, MockSATransGetter)
		
		// Compute (inefficiently but hopefully correctly) possible targets 
		//that could be completed 
		// by a given collection of transitions (of the same order)
		private def completableTargets(transitions : Set[SATransition]) = {
			val ctargets = new HashSet[Target]
			
			for (transition <- transitions) yield ctargets.add((setman.makeSingleton(transition.getSource),
															   setman.makeSingleton(transition.getLabellingState),
															   transition.getDest))
		    
		    // Compute fixpoint (inefficiently) to form all possible targets that could be completed
			// we should not have any targets with empty LHS 
			def fixpoint : Unit = {
				var changed = false
				for{ (s1, s2, s3) <- ctargets; (s1b, s2b, s3b) <- ctargets }
					yield (changed = changed & ctargets.add((
																setman.union(s1, s1b),
																setman.union(s2, s2b),
																setman.union(s3, s3b)
															)))
				if(changed) fixpoint 
					else return () 
				
			} 
			ctargets
		}	
		
		property("OuterStateTargets") = {
				println("In OuterStateTargets property")
				forAll { ( transSet : Set[SATransitionOuter] ) => {
				  		saman.reset(3)
				  		MockTargetCompleteAdder.reset
				  		MockSATransGetter.reset
				  		
				  		// Oh dear... need to explicitly unbox via a map...
				  		val posTargets = completableTargets(transSet map (x => x.value))
				  		
				  		// Get possible LHSs of targets
				  		val posTargetLHSs = (for(posTarget <- posTargets) yield posTarget._1).iterator
				  		
				  		  
				  		// This set will contain a subset of targets' LHSs that we
				  		// know could be completed by the chosen set of transitions
				  		val chosenTargetLHSs = new HashSet[ManagedSet[SAState]]
				  		
				  		
				  		for ( trans <- transSet )
				  			yield {
				  				// should be robust with respect to ordering of being informed/adding to
				  				// old states
				  				if (Random.nextInt(2)==1)
				  					{MockSATransGetter.add(trans)
				  					targProc.update(trans)}
				  				else
				  					{targProc.update(trans)
				  					MockSATransGetter.add(trans)}
				  			
				  					
				  				// Pick around a third of targets
				  				if(Random.nextInt(3) == 1 && posTargetLHSs.hasNext) {
				  					val nextLHS = posTargetLHSs.next
				  					targProc.addTarget(nextLHS)
				  					chosenTargetLHSs.add(nextLHS)
				  				}
				  				else (!posTargetLHSs.hasNext || {posTargetLHSs.next ; true})
				  				  // Should be at least as many pos target LHSs
				  													 // as transitions
				  				  
				  		  	}
				  		
				  		val targetsWithChosenLHS = for {LHS <- chosenTargetLHSs 
				  										target <- posTargets 
				  										if (target._1 == LHS)} yield target
				  		
				  		// targetsWithChosenLHS should be precisely the targets reported
				  		// by the TargetProcessor
				  		(targetsWithChosenLHS == MockTargetCompleteAdder.completeTargs)
				  	
					}
				} }
}
