/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.fake.target

import ho.structures.cpds.SCharacter
import ho.structures.sa.SAState
import ho.structures.sa.SATransitionO1
import ho.structures.sa.SATransition
import ho.util.NaiveUnsafeMultiMap
import ho.util.NaiveUnsafeDoubleMultiMap
import java.lang.Iterable

object MockSATransGetter extends SATransGetter {
  
	private var transitions = new NaiveUnsafeMultiMap[SAState, SATransition]
	private var transitionsO1 = new NaiveUnsafeDoubleMultiMap[SAState,
                                                              SCharacter, 
                                                              SATransitionO1]
  
	def oldTranIterator ( q : SAState ) : Iterable[SATransition] 
			= transitions.get(q)
	
	def oldTranO1Iterator ( q : SAState, a : SCharacter ) : Iterable[SATransitionO1]
			= transitionsO1.get(q, a)
	
			// Ugh... need to add some reset methods to NaiveUnsafeMultiMap etc...
	def reset : Unit = {transitions = new NaiveUnsafeMultiMap[SAState, SATransition]; 
						transitionsO1 = new NaiveUnsafeDoubleMultiMap[SAState, 
                                                                      SCharacter,
                                                                      SATransitionO1]}
	
	def add(tran : SATransition) = transitions.put(tran.getSource, tran)
	def add(tran : SATransitionO1) = transitionsO1.put(tran.getSource, 
                                                       tran.getLabellingCharacter,
                                                       tran)
}
