/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.fake.target

import ho.util.ManagedSet
import ho.structures.sa.SAState
import ho.structures.sa.SATransitionO1
import ho.structures.cpds.SCharacter
import scala.collection.mutable.HashSet

object MockTargetCompleteAdder 
	extends  TargetCompleteAdder {
  
	private val _completeTargs = new HashSet[(ManagedSet[SAState], ManagedSet[SAState], ManagedSet[SAState])]
	private val _completeTargsO1 = new HashSet[(SCharacter, 
                                                ManagedSet[SAState], 
												ManagedSet[SAState],
                                                ManagedSet[SAState],
                                                ManagedSet[SATransitionO1])]
		
	override def addTargetComplete( initLHS : ManagedSet[SAState], labellingStates : ManagedSet[SAState], 
	     dest : ManagedSet[SAState]) : Unit 
	     	= _completeTargs.add((initLHS, labellingStates, dest)) 
	
	override def addTargetCompleteO1( sChar : SCharacter, initLHS : ManagedSet[SAState], 
	    labellingStates : ManagedSet[SAState],  dest : ManagedSet[SAState],
        justif : ManagedSet[SATransitionO1]) : Unit 
	    	= _completeTargsO1.add((sChar, initLHS, labellingStates, dest, justif))
	
	def completeTargs = _completeTargs
	def completeTargsO1 = _completeTargsO1
	
	def reset : Unit = { _completeTargs.clear; _completeTargsO1.clear }
	    	    
}
