/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



import java.io.StringReader

import java.util.HashSet

import scala.collection._

import org.scalacheck.Properties
import org.scalacheck.Prop._
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Gen

import org.scalacheck.util.Buildable

import ho.structures.cpds._

import ho.minimisers.CpdsRuleCompression

import ho.reachability.Algorithm

import ho.structures.sa._

import ho.managers.Managers

import ho.parsers.CpdsParser;
import ho.parsers.SAParser;


package ho.reachability {

    object NaiveAlgorithmSpecification
        extends BaseAlgorithmSpecification("NaiveAlgorithm") {

        def doTest(sCpds : String, sSa : String) = {
            try {
                reset()
                val cpds = cpdsParser.getCpds(new StringReader(sCpds))
                cpds.trivialiseConstraints
                NaiveAlgorithm.check(cpds)
                var sa = Algorithm.getSA()
                val sagood = saParser.getSA(new StringReader(sSa))
                val result = (sagood == sa)
                if (!result) {
                    println("\nMismatch!")
                    println("For cpds:\n")
                    println(cpds)
                    println("Algorithm returns:\n")
                    println(sa)
                    println("Test expected:\n")
                    println(sagood)
                }
                result
            } 
            catch {
                case e : Throwable => println("Error in AlgorithmSpecification test: " + e)
                                              e.printStackTrace()
                                              false 
            }
        }

        def doReachTest(cpdsS : String, reaches : Boolean) = {
            try {
                reset()
                val endControl = csman.makeControl(Cpds.error_state)

                var reader = new StringReader(cpdsS)
                var cpds = cpdsParser.getCpds(reader)
                
                var p = csman.makeControl(initControl)
                var a = scman.makeCharacter(initCharacter)
                var conf = new CpdsConfig(p, a, cpds.getOrder())

                // println("Test:")
                // println(cpdsS)
                // println("Reachable: " + reaches)
                // println("")

                var protectedHeads = new HashSet[StackHead]()
                protectedHeads.add(new StackHead(p, a))
                (new CpdsRuleCompression()).minimise(cpds, protectedHeads)
                
                cpds.trivialiseConstraints

                NaiveAlgorithm.check(cpds)
                var result = Algorithm.reaches(p, a)
                var passed = true
                if (result != reaches) {
                    println("Wrong result " + result)
                    passed = false
                } else if (reaches) {
                    // TODO: activate when witness generation works

                    // test counter example generation
                    //val witness = Algorithm.getCounterExample(p, a)
                    //passed = conf.applyRules(witness.iterator())
                    //if (passed) {
                    //    passed = csman.equal(conf.getControlState(), 
                    //                         endControl)
                    //}
                    //if (!passed) {
                    //    println("Bad witness:")
                    //    println(witness)
                    //}
                }
                if (!passed) {
                    println("Cpds:")
                    println(cpdsS)
                    println("Produced SA:")
                    println(Algorithm.getSA())
                }
                passed
            } catch {
                case e : Throwable => println("Error with:")
                                      println(cpdsS)
                                      e.printStackTrace()
                                      false
            }
        }
    }
}
