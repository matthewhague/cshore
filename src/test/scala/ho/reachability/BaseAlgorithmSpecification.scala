/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



import java.io.StringReader

import java.util.HashSet

import scala.collection._

import org.scalacheck.Properties
import org.scalacheck.Prop._
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Gen

import org.scalacheck.util.Buildable

import ho.structures.cpds._

import ho.minimisers.CpdsRuleCompression

import ho.reachability.Algorithm

import ho.structures.sa._

import ho.managers.Managers

import ho.parsers.CpdsParser;
import ho.parsers.SAParser;


package ho.reachability {

    abstract class BaseAlgorithmSpecification(title : String)
        extends Properties(title) {

        var saman = Managers.saManager
        var csman = Managers.csManager
        var scman = Managers.scManager
        var sman = Managers.sManager

        var cpdsParser = new CpdsParser
        var saParser = new SAParser

        val initControl   = "as_q0"
        val initCharacter = "as_a0"


        def reset() = { 
            Managers.reset()
        }

        def doTest(sCpds : String, sSa : String) : Boolean

        def doReachTest(cpdsS : String, reaches : Boolean) : Boolean



        property("Algorithm: build an exponential size stack --- reachable") = {
            val initRule = "(" + initControl + " " + 
                                 initCharacter + " " +
                                 "rew(a0_0) " +
                                 "qup" + ")"
            doReachTest(initRule + "\n" + 
                        """(qup a0_0 push(a0_1) qup)
                           (qup a1_0 push(a0_1) qup)
                           (qup a0_1 push(a0_2) qup)
                           (qup a1_1 push(a0_2) qup)
                           
                           (qup a0_2 push(2) qpa2_1)
                           (qpa2_1 a0_2 rew(a1_2) qpa2_2)
                           (qpa2_2 a1_2 push(2) qdown)
                           
                           (qdown a1_2 pop(1) qdown)
                           (qdown a0_1 rew(a1_1) qup)
                           (qdown a1_1 pop(1) qdown)
                           (qdown a0_0 rew(a1_0) qup)
                           (qdown a1_0 rew(a1_0) qdone0)
                           
                           (qdone0 a1_0 pop(2) qdone1)
                           (qdone1 a1_2 pop(2) qdone2)
                           (qdone2 a0_2 pop(2) qdone3)
                           (qdone3 a1_2 pop(2) qdone4)
                           (qdone4 a0_2 pop(2) qdone5)
                           (qdone5 a1_2 pop(2) qdone6)
                           (qdone6 a0_2 pop(2) qdone7)
                           (qdone7 a1_2 rew(a1_2) error)""", true)
        }

        property("Algorithm: build an exponential size stack --- unreachable") = {
            val initRule = "(" + initControl + " " + 
                                 initCharacter + " " +
                                 "rew(a0_0) " +
                                 "qup" + ")"
            doReachTest(initRule + "\n" + 
                        """(qup a0_0 push(a0_1) qup)
                           (qup a1_0 push(a0_1) qup)
                           (qup a0_1 push(a0_2) qup)
                           (qup a1_1 push(a0_2) qup)
                           
                           (qup a0_2 push(2) qpa2_1)
                           (qpa2_1 a0_2 rew(a1_2) qpa2_2)
                           (qpa2_2 a1_2 push(2) qdown)
                           
                           (qdown a1_2 pop(1) qdown)
                           (qdown a0_1 rew(a1_1) qup)
                           (qdown a1_1 pop(1) qdown)
                           (qdown a0_0 rew(a1_0) qup)
                           (qdown a1_0 rew(a1_0) qdone0)
                           
                           (qdone0 a1_0 pop(2) qdone1)
                           (qdone1 a1_2 pop(2) qdone2)
                           (qdone2 a0_2 pop(2) qdone3)
                           (qdone3 a1_2 pop(2) qdone4)
                           (qdone4 a0_2 pop(2) qdone5)
                           (qdone5 a1_2 pop(2) qdone6)
                           (qdone6 a0_2 pop(2) qdone7)
                           (qdone7 a1_2 pop(2) qdone8)
                           (qdone8 a0_2 pop(2) qdone9)
                           (qdone9 a1_2 rew(a1_2) error)""", false)
        }



        /////////////////////////////////////////////////////////////////////////
        // Random Tests

        val MAX_ORDER = 5
        val MAX_RUN_LENGTH = 10


        // a data structure for representing branching runs
        // reachable says whether the current node is valid -- that is reachable
        // and has a top1 character
        sealed abstract class RunTree
        case class RunStraight(l : RunTree, 
                               rl : Rule) extends RunTree
        case class RunBranch(l : RunTree, 
                             r : RunTree, 
                             rl : Rule,
                             rr : Rule) extends RunTree
        case class RunLeaf(valid : Boolean) extends RunTree

        implicit var arbSCharacter : Arbitrary[SCharacter]
            = Arbitrary(for {
                i <- Gen.choose(0,2)
            } yield scman.makeCharacter("as_a" + i))


        def ruleGen(q1 : ControlState,
                    a : SCharacter,
                    q2 :ControlState) : Gen[Rule] = {
            def getRange(i : Int) = 
                    if (i == 1 || i == 3 || i == 4 || i == 6) 
                        Gen.choose(2,MAX_ORDER)
                    else
                        Gen.choose(1,MAX_ORDER)
            for {
                // favour push rules to increase number of reachables
                i <- Gen.frequency( 
                         (3, 1),
                         (1, 2),
                         (1, 3),
                         (2, 4),
                         (2, 5),
                         (2, 6)
                     )
                k <- getRange(i)
                b <- Arbitrary.arbitrary[SCharacter]
            } yield {
                i match {
                    case 1 => new PushRule(q1, a, q2, k.toByte)
                    case 2 => new PopRule(q1, a, q2, k.toByte)
                    case 3 => new CollapseRule(q1, a, q2, k.toByte)
                    case 4 => new CPushRule(q1, a, q2, b, k.toByte)
                    case 5 => new RewRule(q1, a, q2, b)
                    case 6 => new RewLinkRule(q1, a, q2, b, k.toByte)
                    // TODO: shall we do tests for HORS rules getArg and
                    // getOrder0Arg or leave that to HORS testing?
                }
            }
        }

        /**
         * @param n length of run remaining
         * @param csprefix a prefix for control states to ensure unique to path
         * @param conf the config to continue from
         * @param valid whether the run so far actually reaches conf
         * @param run the run so far
         */
        def runLenNGen(n : Int, 
                       csprefix : String,
                       conf : CpdsConfig, 
                       valid : Boolean) : Gen[RunTree] = {
            val endControl = csman.makeControl(Cpds.error_state)
            def getControl(ext : String) = {
                csman.makeControl("as_q_" + 
                                  csprefix + ext)
            }
            def getRule(aguess : SCharacter,
                        left : Boolean) = {
                var q1 = if (valid) conf.getControlState()
                         else getControl("")
                var q2 = if (n == 1) endControl
                         else if (left)
                            getControl("l")
                         else
                            getControl("r")
                var a = if (valid) conf.getStack().getTop1()
                        else aguess
                ruleGen(q1, a, q2)
            }
            def makeRest(n : Int,
                         csprefixNext : String,
                         conf : CpdsConfig, 
                         valid : Boolean,
                         rule : Rule) = {
                val confNext = new CpdsConfig(conf)
                val validNext = valid && 
                                confNext.applyRule(rule) &&
                                confNext.getStack().getTop1() != null
                runLenNGen(n - 1, csprefixNext, confNext, validNext)
            }
            if (n == 0) {
                Gen.const(RunLeaf(valid))
            } else {
                Gen.frequency(
                    (3, for {
                            a <- Arbitrary.arbitrary[SCharacter]
                            rule <- getRule(a, true)
                            rest <- makeRest(n, csprefix + "l", conf, valid, rule)
                        } yield {
                            RunStraight(rest, rule)
                        }),
                    (1, for {
                            a <- Arbitrary.arbitrary[SCharacter]
                            rl <- getRule(a, true)
                            rr <- getRule(a, false)
                            l <- makeRest(n, csprefix + "l", conf, valid, rl)
                            r <- makeRest(n, csprefix + "r", conf, valid, rr)
                        } yield {
                            RunBranch(l, r, rl, rr)
                        })
                )

            }
        }

        def runGen : Gen[RunTree] = for {
            len <- Gen.choose(1, MAX_RUN_LENGTH)
            runGenned <- {
                var conf = new CpdsConfig(csman.makeControl(initControl),
                                          scman.makeCharacter(initCharacter),
                                          MAX_ORDER.toByte)
                runLenNGen(len, "", conf, true)
            }
        } yield {
            runGenned
        }

        implicit val arbRun = Arbitrary(runGen)


        property("Algorithm: test on random loop-free runs") = {
            // recurse over run tree to get string description and whether error
            // is reachable and depth
            def getCpdsInfo(t : RunTree) : (String, Boolean, Int) = {
                t match {
                    case RunLeaf(valid) => ("", valid, 0)
                    case RunStraight(l, rl) => {
                        val (s, v, d) = getCpdsInfo(l)
                        (rl + "\n" + s, v, d + 1)
                    }
                    case RunBranch(l, r, rl, rr) => {
                        val (sl, vl, dl) = getCpdsInfo(l)
                        val (sr, vr, dr) = getCpdsInfo(r)
                        (rl + "\n" + 
                         sl + "\n" +
                         rr + "\n" + 
                         sr, 
                         vl || vr,
                         Math.max(dl, dr) + 1)
                    }
                }
            }
            forAll{(run : RunTree) => 
                var (cpdsS, reaches, depth) = getCpdsInfo(run)
    
                classify(reaches, "reachable", "unreachable") {
                    classify(true, "Random run tree of depth " + depth) {
                        doReachTest(cpdsS, reaches)
                    }
                }
            }
        }



        ///////////////////////////////////////////////////////////////
        // Test cases from failed random tests

        property("Failed random test --- Null pointer in Triple") = {
            // you have to read this one bottom to top
            doReachTest("""(as_q4 as_a1 collapse(4) error)
                           (as_q3 as_a1 push(4) as_q4)
                           (as_q2 as_a1 push(3) as_q3)
                           (as_q1 as_a0 rew(as_a1) as_q2)
                           
                           (as_q4 as_a0 rew(as_a1) error)
                           (as_q3 as_a0 push(as_a0)(3) as_q4)
                           (as_q2 as_a2 pop(3) as_q3)
                           (as_q1 as_a0 rew(as_a2) as_q2)
                           (as_q0 as_a0 push(3) as_q1)
                           
                           (as_q4 as_a2 pop(3) error)
                           (as_q3 as_a0 pop(1) as_q4)
                           (as_q2 as_a0 rew(as_a0) as_q3)
                           
                           (as_q4 as_a1 collapse(3) error)
                           (as_q3 as_a1 push(2) as_q4)
                           (as_q2 as_a0 push(as_a1) as_q3)
                           (as_q1 as_a0 push(4) as_q2)
                           (as_q0 as_a0 push(2) as_q1)""", true)
        }
                        
        property("Failed random test --- Null pointer in Quad") = {
            doReachTest("""(as_q0 as_a0 push(3) error)
                           (as_q0 as_a0 push(4) error)""", true)

        }

        property("Failed random test --- bad rewLink in CpdsStackOps") = {
            doReachTest("""(as_q0 as_a0 push(as_a0)(3) as_q_l)
                           (as_q_l as_a0 push(4) as_q_ll)
                           (as_q_ll as_a0 rew(as_a1)(5) as_q_lll)
                           (as_q_lll as_a1 pop(1) as_q_llll)
                           (as_q_llll as_a0 rew(as_a0)(5) error)""", true)
        }

        property("Failed random test --- bad collapse in CpdsStackOps") = {
            doReachTest("""(as_q0 as_a0 push(as_a2)(5) as_q1)
                           (as_q1 as_a2 rew(as_a1)(3) as_q2)
                           (as_q2 as_a1 rew(as_a1) as_q3)
                           (as_q3 as_a1 rew(as_a0) as_q4)
                           (as_q4 as_a0 push(3) as_q5)
                           (as_q5 as_a0 push(4) as_q6)
                           (as_q6 as_a0 rew(as_a0)(3) as_q7)
                           (as_q7 as_a0 collapse(3) as_q8)
                           (as_q8 as_a0 rew(as_a2)(5) error)""", true)
        }

        property("Failed random test --- bad collapse after push(2) in CpdsStackOps") = {
            doReachTest("""(as_q0 as_a0 push(2) as_q1)
                           (as_q1 as_a0 push(as_a1)(2) as_q2)
                           (as_q2 as_a1 push(2) as_q3)
                           (as_q3 as_a1 collapse(2) as_q4)
                           (as_q4 as_a0 push(4) error)""", true)
        }

    }




}
