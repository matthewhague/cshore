/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



import java.io.StringReader

import java.util.HashSet

import scala.collection._

import org.scalacheck.Properties
import org.scalacheck.Prop._
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Gen

import org.scalacheck.util.Buildable

import ho.structures.cpds._

import ho.minimisers.CpdsRuleCompression

import ho.reachability.Algorithm

import ho.structures.sa._

import ho.managers.Managers

import ho.parsers.CpdsParser;
import ho.parsers.SAParser;


package ho.reachability {

    object AlgorithmSpecification
        extends BaseAlgorithmSpecification("Algorithm") {

        def doTest(sCpds : String, sSa : String) = {
            try {
                reset()
                val cpds = cpdsParser.getCpds(new StringReader(sCpds))
                cpds.trivialiseConstraints
                Algorithm.check(cpds)
                var sa = Algorithm.getSA()
                val sagood = saParser.getSA(new StringReader(sSa))
                val result = (sagood == sa)
                if (!result) {
                    println("\nMismatch!")
                    println("For cpds:\n")
                    println(cpds)
                    println("Algorithm returns:\n")
                    println(sa)
                    println("Test expected:\n")
                    println(sagood)
                }
                result
            } 
            catch {
                case e : Throwable => println("Error in AlgorithmSpecification test: " + e)
                                              e.printStackTrace()
                                              false 
            }
        }

        def doReachTest(cpdsS : String, reaches : Boolean) = {
            try {
                reset()
                val endControl = csman.makeControl(Cpds.error_state)

                var reader = new StringReader(cpdsS)
                var cpds = cpdsParser.getCpds(reader)
                
                var p = csman.makeControl(initControl)
                var a = scman.makeCharacter(initCharacter)
                var conf = new CpdsConfig(p, a, cpds.getOrder())

                // println("Test:")
                // println(cpdsS)
                // println("Reachable: " + reaches)
                // println("")

                var protectedHeads = new HashSet[StackHead]()
                protectedHeads.add(new StackHead(p, a))
                (new CpdsRuleCompression()).minimise(cpds, protectedHeads)
                
                cpds.trivialiseConstraints

                Algorithm.check(cpds)
                var result = Algorithm.reaches(p, a)
                var passed = true
                if (result != reaches) {
                    println("Wrong result " + result)
                    passed = false
                } else if (reaches) {
                    // test counter example generation
                    val witness = Algorithm.getCounterExample(p, a)
                    passed = conf.applyRules(witness.iterator())
                    if (passed) {
                        passed = csman.equal(conf.getControlState(), 
                                             endControl)
                    }
                    if (!passed) {
                        println("Bad witness:")
                        println(witness)
                    }
                }
                if (!passed) {
                    println("Cpds:")
                    println(cpdsS)
                    println("Produced SA:")
                    println(Algorithm.getSA())
                }
                passed
            } catch {
                case e : Throwable => println("Error with:")
                                      println(cpdsS)
                                      e.printStackTrace()
                                      false
            }
        }




        ///////////////////////////////////////////////////////
        // One-step tests

        property("Algorithm: one-step pop(1)") = {
           doTest("""(q1 a pop(1) error)""",
                  """error --- a, { } ---> { }
                     q1 --- a, { } ---> { error }""")
        }

        property("Algorithm: one-step pop(2) and pop(1)") = {
               doTest("""(q1 a pop(1) error)
                         (q2 b pop(2) error)""",
                      """error ------> { }
                         q1 ------> { }
                         q2 ------> { error }
                         (error, { }) --- a, { } ---> { }
                         (error, { }) --- b, { } ---> { }
                         (q1, { }) --- a, { } ---> { (error, { }) }
                         (q2, { error }) --- b, { } ---> { }""");
        }

        property("Algorithm: one-step pop(2) and pop(3)") = {
            doTest("""(q1 a pop(2) error)
                      (q2 a pop(3) error)""",
                   """error ------> { }
                      (error, { }) ------> { }
                      ((error, { }), { }) --- a, { } ---> { }
                      q1 ------> { }
                      (q1, { }) ------> { (error, { }) }
                      ((q1, { }), { (error, { }) }) --- a, { } ---> { }
                      q2 ------> { error }
                      (q2, { error }) ------> { }
                      ((q2, { error }), { }) --- a, { } ---> { }""")
        }

        property("Algorithm: one-step collapse(2)") = {
            doTest("""(q1 a collapse(2) error)""",
                   """error ------> { }
                      q1 ------> { }
                      (error, { }) --- a, { } ---> { }
                      (q1, { }) --- a, { error } ---> { }""");
        }

        property("Algorithm: one-step collapse(2) and pop(3)") = {
           doTest("""(q1 a collapse(2) error)
                     (q2 a pop(3) error)""",
                  """error ------> { }
                     (error, { }) ------> { }
                     ((error, { }), { }) --- a, { } ---> { }
                     q1 ------> { }
                     (q1, { }) ------> { }
                     ((q1, { }), { }) --- a, { (error, { }) } ---> { }
                     q2 ------> { error }
                     (q2, { error }) ------> { }
                     ((q2, { error }), { }) --- a, { } ---> { }""")
        }

        property("Algorithm: one-step rew(b)") = {
            doTest("""(q1 a rew(b) error)""",
                   """error --- a, { } ---> { }
                      error --- b, { } ---> { }
                      q1 --- a, { } ---> { }""")
        }
  
        property("Algorithm: one-step rew(b) and pop(2)") = {
           doTest("""(q1 a rew(b) error)
                     (q2 a pop(2) error)""",
                  """error ------> { }
                     q1 ------> { }
                     q2 ------> { error }
                     (error, { }) --- a, { } ---> { }
                     (error, { }) --- b, { } ---> { }
                     (q1, { }) --- a, { } ---> { }
                     (q2, { error }) --- a, { } ---> { }""")
        }
  
        property("Algorithm: one-step push(b)") = {
            doTest("""(q1 a push(b) error)""",
                   """error --- a, { } ---> { }
                      error --- b, { } ---> { }
                      q1 --- a, { } ---> { }""")
        }

        property("Algorithm: one-step push(b) and pop(2)") = {
            doTest("""(q1 a push(b) error)
                      (q2 a pop(2) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q2 ------> { error }
                      (q2, { error }) --- a, { } ---> { }
                      q1 ------> { }
                      (q1, { }) --- a, { } ---> { }""")
        }

        property("Algorithm: one-step push(2)") = {
            doTest("""(q1 a push(2) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      q1 ------> { }
                      (q1, { }) --- a, { } ---> { }""")
        }

        property("Algorithm: one-step push(2) and pop(3)") = {
            doTest("""(q1 a push(2) error)
                     (q2 a pop(3) error)""",
                   """error ------> { }
                      (error, { }) ------> { }
                      ((error, { }), { }) --- a, { } ---> { }
                      q2 ------> { error }
                      (q2, { error }) ------> { }
                      ((q2, { error }), { }) --- a, { } ---> { }
                      q1 ------> { }
                      (q1, { }) ------> { }
                      ((q1, { }), { }) --- a, { } ---> { }""")
        }

        property("Algorithm: one-step push(b)(2)") = {
            doTest("""(q1 a push(b)(2) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { }
                      (q1, { }) --- a, { } ---> { }""")
        }

        property("Algorithm: one-step push(b)(2) and pop(3)") = {
            doTest("""(q1 a push(b)(2) error)
                     (q2 a pop(3) error)""",
                   """error ------> { }
                      (error, { }) ------> { }
                      ((error, { }), { }) --- a, { } ---> { }
                      ((error, { }), { }) --- b, { } ---> { }
                      q2 ------> { error }
                      (q2, { error }) ------> { }
                      ((q2, { error }), { }) --- a, { } ---> { }
                      q1 ------> { }
                      (q1, { }) ------> { }
                      ((q1, { }), { }) --- a, { } ---> { }""")
        }

        property("Algorithm: one-step rew(b)(1)") = {
            doTest("""(q1 a rew(b)(1) error)""",
                   """error --- a, { } ---> { } 
                      error --- b, { } ---> { }
                      q1 --- a, { } ---> { }""")
                      
        }

        property("Algorithm: one-step rew(b)(1) and pop(2)") = {
            doTest("""(q1 a rew(b)(1) error)
                      (q2 a pop(2) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { }
                      (q1, { }) --- a, { } ---> { }
                      q2 ------> { error }
                      (q2, { error }) --- a, { } ---> { }""")
        }

        property("Algorithm: one-step rew(b)(2)") = {
            doTest("""(q1 a rew(b)(2) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { }
                      (q1, { }) --- a, { } ---> { }""")
        }

        property("Algorithm: one-step rew(b)(2) and pop(3)") = {
            doTest("""(q1 a rew(b)(2) error)
                      (q2 a pop(3) error)""",
                   """error ------> { }
                      (error, { }) ------> { }
                      ((error, { }), { }) --- a, { } ---> { }
                      ((error, { }), { }) --- b, { } ---> { }
                      q1 ------> { }
                      (q1, { }) ------> { }
                      ((q1, { }), { }) --- a, { } ---> { }
                      q2 ------> { error }
                      (q2, { error }) ------> { }
                      ((q2, { error }), { }) --- a, { } ---> { }""")
        }



        //////////////////////////////////////////////////////////////
        // Smattering of two-step tests (too many cases to do them all!)


        property("Algorithm: two-step push(3) push(2)") = {
            doTest("""(q1 a push(3) q2)
                      (q2 a push(2) error)""",
                   """error ------> { }
                      (error, { }) ------> { }
                      ((error, { }), { }) --- a, { } ---> { }
                      q2 ------> { }
                      (q2, { }) ------> { }
                      ((q2, { }), { }) --- a, { } ---> { }
                      q1 ------> { }
                      (q1, { }) ------> { }
                      ((q1, { }), { }) --- a, { } ---> { }""")
        }

        property("Algorithm: two-step pop(2) pop(1)") = {
            doTest("""(q1 a pop(2) q2)
                      (q2 b pop(1) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q2 ------> { }
                      (q2, { }) --- b, { } ---> { (error, { }) }
                      q1 ------> { q2 }
                      (q1, { q2 }) --- a, { } ---> { }""")
        }

        property("Algorithm: two-step col(3) pop(2)") = {
            doTest("""(q1 a collapse(3) q2)
                      (q2 b pop(2) error)""",
                   """error ------> { }
                      (error, { }) ------> { }
                      ((error, { }), { }) --- a, { } ---> { }
                      ((error, { }), { }) --- b, { } ---> { }
                      q2 ------> { }
                      (q2, { }) ------> { (error, { }) }
                      ((q2, { }), { (error, { }) }) --- b, { } ---> { }
                      q1 ------> { }
                      (q1, { }) ------> { }
                      ((q1, { }), { }) --- a, { q2 } ---> { }""")
        }

        property("Algorithm: two-step pop(1) push(a) + pop(2) to make order-2") = {
            doTest("""(q1 b pop(1) q2)
                      (q2 a push(b) error)
                      (q3 a pop(2) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { }
                      (q1, { }) --- b, { } ---> { (q2, { }) }
                      q2 ------> { }
                      (q2, { }) --- a, { } ---> { }
                      q3 ------> { error }
                      (q3, { error }) --- a, { } ---> { }""")
        }

        property("Algorithm: two-step collapse(2) rew(b) (+pop(3) to make order-3)") = {
            doTest("""(q1 a collapse(2) q2)
                      (q2 a rew(b) error)
                      (q3 a pop(3) error)""",
                   """error ------> { }
                      (error, { }) ------> { }
                      ((error, { }), { }) --- a, { } ---> { }
                      ((error, { }), { }) --- b, { } ---> { }
                      q1 ------> { }
                      (q1, { }) ------> { }
                      ((q1, { }), { }) --- a, { (q2, { }) } ---> { }
                      q2 ------> { }
                      (q2, { }) ------> { }
                      ((q2, { }), { }) --- a, { } ---> { }
                      q3 ------> { error }
                      (q3, { error }) ------> { }
                      ((q3, { error }), { }) --- a, { } ---> { }""")
        }

        property("Algorithm: two-step push(b)(2) push(2)") = {
            doTest("""(q1 a push(b)(2) q2)
                      (q2 b push(2) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { }
                      (q1, { }) --- a, { } ---> { }
                      q2 ------> { }
                      (q2, { }) --- b, { } ---> { }""")
        }

        property("Algorithm: two-step push(a) push(b) (+ pop(2) to make order-2)") = {
            doTest("""(q1 a push(a) q2)
                      (q2 a push(b) error)
                      (q3 a pop(2) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { }
                      (q1, { }) --- a, { } ---> { }
                      q2 ------> { }
                      (q2, { }) --- a, { } ---> { }
                      q3 ------> { error }
                      (q3, { error }) --- a, { } ---> { }""")
        }

        property("Algorithm: two-step rew(b) pop(2)") = {
            doTest("""(q1 a rew(b) q2)
                      (q2 b pop(2) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { error }
                      (q1, { error }) --- a, { } ---> { }
                      q2 ------> { error }
                      (q2, { error } ) --- b, { } ---> { }""")
        }

        property("Algorithm: two-step push(3) push(2) (+pop(4) to make order-4)") = { 
            doTest("""(q1 a push(3) q2)
                      (q2 a push(2) error)
                      (q3 a pop(4) error)""",
                   """error ------> { }
                      (error, { }) ------> { }
                      ((error, { }), { }) ------> { }
                      (((error, { }), { }), { }) --- a, { } ---> { }
                      q1 ------> { }
                      (q1, { }) ------> { }
                      ((q1, { }), {}) ------> { }
                      (((q1, { }), { }), { }) --- a, { } ---> { }
                      q2 ------> { }
                      (q2, { }) ------> { }
                      ((q2, { }), { }) ------> { }
                      (((q2, { }), { }), { }) --- a, { } ---> { }
                      q3 ------> { error }
                      (q3, { error }) ------> { }
                      ((q3, { error }), { }) ------> { }
                      (((q3, { error }), { }), { }) --- a, { } ---> { }""")
        } 

        property("Algorithm: two-step pop(2) push(a)") = {
            doTest("""(q1 b pop(2) q2)
                      (q2 a push(a) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { q2 }
                      (q1, { q2 }) --- b, { } ---> { }
                      q2 ------> { }
                      (q2, { }) --- a, { } ---> { }""")
        }

        property("Algorithm: two-step collapse(2) collapse(2) (+pop(3) to make order-3)") = {
            doTest("""(q1 a collapse(2) q2)
                      (q2 b collapse(2) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q2 ------> { }
                      (q2, { }) --- b, { error } ---> { }
                      q1 ------> { }
                      (q1, { }) --- a, { q2 } ---> { }""")
        }

        property("Algorithm: two-step pop(1) push(b)(2)") = {
            doTest("""(q1 a pop(1) q2)
                      (q2 a push(b)(2) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { }
                      (q1, { }) --- a, { } ---> { (q2, { }) }
                      q2 ------> { }
                      (q2, { }) --- a, { } ---> { }""")
        }

        property("Algorithm: two-step collapse(2) pop(1) (+pop(3) to make order-3)") = {
            doTest("""(q1 a collapse(2) q2)
                      (q2 a pop(1) error)
                      (q3 a pop(3) error)""",
                   """error ------> { }
                      (error, { }) ------> { }
                      ((error, { }), { }) --- a, { } ---> { }
                      q1 ------> { }
                      (q1, { }) ------> { }
                      ((q1, { }), { }) --- a, { (q2, { }) } ---> { }
                      q2 ------> { }
                      (q2, { }) ------> { }
                      ((q2, { }), { }) --- a, { } ---> { ((error, { }), { }) }
                      q3 ------> { error }
                      (q3, { error }) ------> { }
                      ((q3, { error }), { }) --- a, { } ---> { }""")
        }

        property("Algorithm: two-step push(a)(2) rew(b)") = {
            doTest("""(q1 a push(a)(2) q2)
                      (q2 a rew(b) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { }
                      (q1, { }) --- a, { } ---> { }
                      q2 ------> { }
                      (q2, { }) --- a, { } ---> { }""")
        }

        property("Algorithm: two-step push(a) collapse(2) (+pop(3) to make order-2)") = {
            doTest("""(q1 a push(a) q2)
                      (q2 a collapse(2) error)
                      (q3 a pop(3) error)""",
                   """error ------> { }
                      (error, { }) ------> { }
                      ((error, { }), { }) --- a, { } ---> { }
                      q2 ------> { }
                      (q2, { }) ------> { }
                      ((q2, { }), { }) --- a, { (error, { }) } ---> { }
                      q3 ------> { error }
                      (q3, { error }) ------> { }
                      ((q3, { error }), { }) --- a, { } ---> { }""")
        }

        property("Algorithm: two-step rew(b) collapse(2)") = {
            doTest("""(q1 a rew(b) q2)
                      (q2 b collapse(2) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { }
                      (q1, { }) --- a, { error } ---> { }
                      q2 ------> { }
                      (q2, { }) --- b, { error } ---> { }""")
        }

        property("Algorithm: two-step push(a)(2) collapse(2) (+pop(3) to make order-3") = {
            doTest("""(q1 a push(a)(2) q2)
                      (q2 a collapse(2) error)
                      (q3 a pop(3) error)""",
                   """error ------> { }
                      (error, { }) ------> { }
                      ((error, { }), { }) --- a, { } ---> { }
                      q1 ------> { }
                      (q1, { }) ------> { (error, { }) }
                      ((q1, { }), { (error, { }) }) --- a, { } ---> { }
                      q2 ------> { }
                      (q2, { }) ------> { }
                      ((q2, { }), { }) --- a, { (error, { }) } ---> { }
                      q3 ------> { error }
                      (q3, { error }) ------> { }
                      ((q3, { error }), { }) --- a, { } ---> { }""")
        }

        property("Algorithm: two-step pop(1) collapse(2)") = {
            doTest("""(q1 a pop(1) q2)
                      (q2 a collapse(2) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      q1 ------> { }
                      (q1, { }) --- a, { } ---> { (q2, { }) }
                      q2 ------> { }
                      (q2, { }) --- a, { error } ---> { }""")
        }

        property("Algorithm: two-step rew(b) pop(1)") = {
            doTest("""(q1 a rew(b) q2)
                      (q2 b pop(1) error)""",
                   """error --- a, { } ---> { }
                      error --- b, { } ---> { }
                      q2 --- b, { } ---> { error }
                      q1 --- a, { } ---> { error }""")
        }

        property("Algorithm: two-step rew(b)(2) collapse(2)") = {
            doTest("""(q1 a rew(b)(2) q2)
                      (q2 b collapse(2) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { error }
                      (q1, { error }) --- a, { } ---> { }
                      q2 ------> { }
                      (q2, { }) --- b, { error } ---> { }""")
        }

        property("Algorithm: two-step rew(b)(2) collapse(2)") = {
            doTest("""(q1 a rew(b)(2) q2)
                      (q2 b collapse(3) error)""",
                   """error ------> { }
                      (error, { }) ------> { }
                      ((error, { }), { }) --- a, { } ---> { }
                      ((error, { }), { }) --- b, { } ---> { }
                      q2 ------> { }
                      (q2, { }) ------> { }
                      ((q2, { }), { }) --- b, { error } ---> { }""")
        }

        /////////////////////////////////////////////////////////////////
        // Miscellaneous interesting tests

// Removed tests that broke due to optimisation
// In general all of these automata comparisons are fragile to optimisation
// Really need a better way...

//        property("Algorithm: three-step push(2) pop(2) pop(2) (+pop(1) for variety)") = {
//            doTest("""(q1 a push(2) q2)
//                      (q2 a pop(2) q3)
//                      (q2 a pop(1) q2)
//                      (q3 a pop(2) error)""",
//                   """error ------> { }
//                      (error, { }) --- a, { } ---> { }
//                      q1 ------> { error }
//                      (q1, { error }) --- a, { } ---> { }
//                      (q1, { error }) --- a, { } ---> { (q2, { q3 }) }
//                      q2 ------> { q3 }
//                      (q2, { q3 }) --- a, { } ---> { }
//                      (q2, { q3 }) --- a, { } ---> { (q2, { q3 }) }
//                      q3 ------> { error }
//                      (q3, { error }) --- a, { } ---> { }""")
//        }

        property("Algorithm: three-step (a, push(b)) (b, pop(1)) (a, pop(1))") = {
            doTest("""(q1 a push(b) q2)
                      (q2 b pop(1) q3)
                      (q3 a pop(1) error)""",
                   """error --- a, { } ---> { }
                      error --- b, { } ---> { }
                      q1 --- a, { } ---> { error }
                      q2 --- b, { } ---> { q3 }
                      q3 --- a, { } ---> { error }""")
        }

        property("Algorithm: three-step (a, push(b)(*2*)) (b, pop(1)) (a, pop(1))") = {
            doTest("""(q1 a push(b)(2) q2)
                      (q2 b pop(1) q3)
                      (q3 a pop(1) error)""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { }
                      (q1, { }) --- a, { } ---> { (error, { }) }
                      q2 ------> { }
                      (q2, { }) --- b, { } ---> { (q3, { }) }
                      q3 ------> { }
                      (q3, { }) --- a, { } ---> { (error, { }) }""")
        }

//        property("Algorithm: build set of two elements") = {
//            doTest("""(q1 a pop(1) error)
//                      (q2 a pop(2) q1)
//                      (q2 a pop(1) q2)
//                      (q3 a push(2) q2)""",
//                   """error ------> { }
//                      (error, { }) --- a, { } ---> { }
//                      q1 ------> { }
//                      (q1, { }) --- a, { } ---> { (error, { }) }
//                      q2 ------> { q1 }
//                      (q2, { q1 }) --- a, { } ---> { }
//                      (q2, { q1 }) --- a, { } ---> { (q2, { q1 }) }
//                      q3 ------> { }
//                      (q3, { }) --- a, { } ---> { (error, { }) }
//                      (q3, { }) --- a, { } ---> { (q2, { q1 }) (error, { }) }""")
//        }

//        property("Algorithm: two elements via collapse rather than pop") = {
//            doTest("""(q1 a pop(3) q2)
//                      (q1 a collapse(2) q1)
//                      (q2 a collapse(2) error)
//                      (q3 a push(3) q1)
//                      (q4 a push(a)(2) q3)""",
//                   """error ------> { }
//                      (error, { }) ------> { }
//                      ((error, { }), { }) --- a, { } ---> { }
//                      q1 ------> { q2 }
//                      (q1, { q2 }) ------> { } 
//                      ((q1, { q2 }), { }) --- a, { } ---> { }
//                      ((q1, { q2 }), { }) --- a, { (q1, { q2 }) } ---> { }
//                      q2 ------> { }
//                      (q2, { }) ------> { }
//                      ((q2, { }), { }) --- a, { (error, { }) } ---> { }
//                      q3 ------> { }
//                      (q3, { }) ------> { }
//                      ((q3, { }), { }) --- a, { (error, { }) } ---> { }
//                      ((q3, { }), { }) --- a, { (q1, { q2 }) (error, { }) } ---> { }
//                      q4 ------> { }
//                      (q4, { }) ------> { (error, { }) }
//                      ((q4, { }), { (error, { }) }) --- a, { } ---> { }
//                      (q4, { }) ------> { (q1, { q2 }) (error, { }) }
//                      ((q4, { }), { (q1, { q2 }) (error, { }) }) --- a, { } ---> { }""")
//        }
    }
}
