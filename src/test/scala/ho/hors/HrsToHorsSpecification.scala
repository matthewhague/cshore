/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.hors

import java.io.StringReader

import scala.collection.mutable.Set

import org.scalacheck.Properties
import org.scalacheck.Prop._

import ho.parsers.HrsParser
import ho.parsers.HorsParser



object HrsToHorsSpecification extends Properties("HrsToHrs") {

    // Because we can't parse _fail
    val hrsFailID = "_fail"
    val horsFailID = "fail"
    val hrsDummyID = "_dummy"
    val horsDummyID = "dummy"


    def doTest(hrsString : String,
               horsString : String,
               expect : Boolean ) : Boolean = {
        val hrsP = HrsParser(new StringReader(hrsString))
        val horsP = HorsParser(new StringReader(horsString))
        (hrsP, horsP) match {
            case (HrsParser.Success(hrs, _), HorsParser.Success(hors2, _)) => {
                val hors1 = HrsToHors(hrs)
                val pass = isCompatible(hors1, hors2)
                if (pass != expect) {
                    println("Failed test from " + 
                            hrsString + 
                            " and " + 
                            horsString +
                            " got mismatched\n" +
                            hors1.toString + 
                            "\n   and  \n" + 
                            hors2.toString)
                }
                (pass == expect)
            }
            case (HrsParser.Failure(msg, _), HorsParser.Success(_, _)) => {
                println("Failed to parse " + hrsString + " with error " + msg)
                false
            }
            case (HrsParser.Success(_, _), HorsParser.Failure(msg, _)) => {
                println("Failed to parse " + horsString + " with error " + msg)
                false
            }
            case (HrsParser.Failure(msg1, _), HorsParser.Failure(msg2, _)) => {
                println("Failed to parse " + 
                        hrsString + 
                        " with error " + 
                        msg1 +
                        " and failed to parse " +
                        horsString + 
                        " with error " +
                        msg2)
                false
            }
        }
    }

    def doPosTest(hrsString : String, horsString : String) = {
        doTest(hrsString, horsString, true)
    }

    def doNegTest(hrsString : String, horsString : String) = {
        doTest(hrsString, horsString, false)
    }

    def isCompatible(hors1 : Hors, hors2 : Hors) = {
        hors1.initNonTerms == hors2.initNonTerms &&
        hors1.failNonTerms == hors2.failNonTerms &&
        matchRuleSets(hors1, hors2) &&
        matchStateSets(hors1.initPropAutStates, hors2.initPropAutStates) &&
        matchTransitionSets(hors1, hors2)
    }

    def matchRuleSets(hors1 : Hors, hors2 : Hors) = {
        // not the most efficient implementation, but hopefully fast enough for
        // all cases we consider
        hors1.rules.forall {case (nt1, rhs1) => hors2.rules.exists {case (nt2, rhs2) => {
            matchRules(nt1, rhs1, nt2, rhs2)
        }}} &&
        hors2.rules.forall {case (nt2, rhs2) => hors1.rules.exists {case (nt1, rhs1) => {
            matchRules(nt1, rhs1, nt2, rhs2)
        }}}
    }

    def matchRules(nt1 : NonTerm, rhs1 : Term, nt2 : NonTerm, rhs2 : Term) = {
        (nt1 == nt2) && matchTerms(rhs1, rhs2)
    }

    def matchTerms(t1 : Term, t2 : Term) : Boolean = {
        (t1.typ == t2.typ) && ((t1, t2) match {
            case (NonTerm(_,_), NonTerm(_,_)) => (t1 == t2)
            case (Terminal(id1,ty1), Terminal(id2,ty2)) => {
                matchTerminals(Terminal(id1,ty1), Terminal(id2,ty2))
            }
            case (Var(_,_,_,_), Var(_,_,_,_)) => (t1 == t2)
            case (App(f1,a1,_), App(f2,a2,_)) => {
                matchTerms(f1, f2) && matchTerms(a1, a2)
            }
            case (NonDet(a1,b1,_), NonDet(a2,b2,_)) => {
                matchTerms(a1, a2) && matchTerms(b1, b2)
            }
            case (DomConst(_,_), DomConst(_,_)) => (t1 == t2)
            case (CaseTerm(c1,cs1,_), CaseTerm(c2,cs2,_)) => {
                matchTerms(c1, c2) &&
                cs1.zip(cs2).forall { case (ct1, ct2) => matchTerms(ct1, ct2) }
            }
            case _ => { 
                println("Mismatch " + t1.toString + " " + t2.toString)
                false
            }
        })
    }

    def matchTerminals(t1 : Terminal, t2 : Terminal) = {
        if (t1.id == hrsDummyID || t1.id == horsDummyID) 
            (t2.id == hrsDummyID || t2.id == horsDummyID) && 
            (t1.typ == t2.typ)
        else 
            (t1 == t2)
    }

    def matchStateSets(qs1 : Set[PropAutState], qs2 : Set[PropAutState]) = {
        qs1.forall(q1 => qs2.exists(q2 => matchStates(q1, q2))) &&
        qs2.forall(q2 => qs1.exists(q1 => matchStates(q1, q2))) 
    }


    def matchStates(q1 : PropAutState, q2 : PropAutState) : Boolean = {
        if (q1.id == hrsFailID || q1.id == horsFailID) 
            (q2.id == hrsFailID || q2.id == horsFailID)
        else 
            (q1.id == q2.id)
    }

    def matchTransitionSets(hors1 : Hors, hors2 : Hors) = {
        // not the most efficient implementation, but hopefully fast enough for
        // all cases we consider
        hors1.propAutRules.forall {case ((q1, a1, i1), qq1) => hors2.propAutRules.exists {
            case ((q2, a2, i2), qq2) => matchTransitions(q1, a1, i1, qq1,
                                                         q2, a2, i2, qq2)
        }} &&
        hors2.propAutRules.forall {case ((q2, a2, i2), qq2) => hors1.propAutRules.exists {
            case ((q1, a1, i1), qq1) => matchTransitions(q1, a1, i1, qq1,
                                                         q1, a2, i2, qq2)
        }}
    }

    def matchTransitions(q1 : PropAutState,
                         a1 : Terminal,
                         i1 : Int,
                         qq1 : PropAutState,
                         q2 : PropAutState,
                         a2 : Terminal,
                         i2 : Int,
                         qq2 : PropAutState) = {
        matchStates(q1, q2) &&
        matchTerminals(a1, a2) &&
        (i1 == i2) &&
        matchStates(qq1, qq2)
    }


    property("Example2-1") = doPosTest("""
        %BEGING
        S -> F c.
        F x -> a x (F (b x)).
        %ENDG
        
        %BEGINA
        q0 a -> q0 q0.
        q0 b -> q1.
        q1 b -> q1.
        q0 c -> .
        q1 c -> .
        %ENDA
    """, """
        nont S : 0;
        nont F : 0 => 0;
        
        tml a : 2;
        tml b : 1;
        tml c : 1;
        tml dummy : 0;
        
        state q0 q1 fail;
        
        initnt S;
        initst q0;
        failst fail;
        
        S = F (c dummy);
        F x = a x (F (b x));
        
        q0 -> a q0 q0;
        q0 -> b q1;
        
        q1 -> a fail fail;
        q1 -> b q1;
    """)

    property("Example2-1, bad hors") = doNegTest("""
        %BEGING
        S -> F c.
        F x -> a x (F (d x)).
        %ENDG
        
        %BEGINA
        q0 a -> q0 q0.
        q0 b -> q1.
        q1 b -> q1.
        q0 c -> .
        q1 c -> .
        %ENDA
    """, """
        nont S : 0;
        nont F : 0 => 0;
        
        tml a : 2;
        tml b : 1;
        tml c : 1;
        tml dummy : 0;
        
        state q0 q1 fail;
        
        initnt S;
        initst q0;
        failst fail;
        
        S = F (c dummy);
        F x = a x (F (b x));
        
        q0 -> a q0 q0;
        q0 -> b q1;
        
        q1 -> a fail fail;
        q1 -> b q1;
    """)

    property("Example2-1, bad hors again") = doNegTest("""
        %BEGING
        S -> F c.
        F x -> a x (F (b x)).
        %ENDG
        
        %BEGINA
        q0 a -> q0 q0.
        q0 b -> q1.
        q1 b -> q1.
        q0 c -> .
        q1 c -> .
        %ENDA
    """, """
        nont S : 0;
        nont F : 0 => 0;
        
        tml a : 2;
        tml b : 1;
        tml c : 1;
        tml dummy : 0;
        
        state q0 q1 fail;
        
        initnt S;
        initst q0;
        failst fail;
        
        S = F (c dummy);
        F x = a x (F (c x));
        
        q0 -> a q0 q0;
        q0 -> b q1;
        
        q1 -> a fail fail;
        q1 -> b q1;
    """)

    property("Example2-1, bad aut") = doNegTest("""
        %BEGING
        S -> F c.
        F x -> a x (F (d x)).
        %ENDG
        
        %BEGINA
        q0 a -> q0 q2.
        q0 b -> q1.
        q1 b -> q1.
        q0 c -> .
        q1 c -> .
        %ENDA
    """, """
        nont S : 0;
        nont F : 0 => 0;
        
        tml a : 2;
        tml b : 1;
        tml c : 1;
        tml dummy : 0;
        
        state q0 q1 fail;
        
        initnt S;
        initst q0;
        failst fail;
        
        S = F (c dummy);
        F x = a x (F (c x));
        
        q0 -> a q0 q0;
        q0 -> b q1;
        
        q1 -> a fail fail;
        q1 -> b q1;
    """)

    property("Example2-1, bad aut again") = doNegTest("""
        %BEGING
        S -> F c.
        F x -> a x (F (b x)).
        %ENDG
        
        %BEGINA
        q0 a -> q0 q0.
        q0 b -> q1.
        q1 b -> q1.
        q0 c -> .
        q1 c -> .
        %ENDA
    """, """
        nont S : 0;
        nont F : 0 => 0;
        
        tml a : 2;
        tml b : 1;
        tml c : 1;
        tml dummy : 0;
        
        state q0 q2 fail;
        
        initnt S;
        initst q0;
        failst fail;
        
        S = F (c dummy);
        F x = a x (F (b x));
        
        q0 -> a q0 q0;
        q0 -> b q2;
        
        q2 -> a fail fail;
        q2 -> b q2;
    """)


    property("Order5") = doPosTest("""
        %BEGING
        S -> GenConsume Newr Read Close (GenConsume Neww Write Close end). /** (GenConsume Neww F2 end). **/
        GenConsume gen use finish k -> gen (Loop use finish k).
        Loop use finish k x -> br (finish x k) (use x (Loop use finish k x)).
        C use k x -> use x k.
        I x y -> x y.
        K x y -> y.
        Newr k -> br (newr (k I)) (k K).
        Neww k -> br (neww (k I)) (k K).
        Close x k -> x close k.
        Read x k -> x read k.
        Write y k -> y write k.
        %ENDG
        
        %BEGINA
        q0 br -> q0 q0.
        qr br -> qr qr.
        qw br -> qw qw.
        qrw br -> qrw qrw.
        q0 newr -> qr.
        qr read -> qr.
        qr close -> qc.
        qc br -> qc qc.
        q0 neww -> qw.
        qw write -> qw.
        qw close -> qc.
        qw newr -> qrw.
        qr neww -> qrw.
        qc newr -> qrw.
        qc neww -> qrw.
        qrw newr -> qrw.
        qrw neww -> qrw.
        qrw read -> qrw.
        qrw write -> qrw.
        qrw close -> qrw.
        qc end ->.
        q0 end ->.
        qrw end ->.
        %ENDA
    """, """
        nont S : 0;
        nont GenConsume : ((((0 => 0) => 0 => 0) => 0) => 0) => (((0 => 0) => 0 => 0) => 0 => 0) => (((0 => 0) => 0 => 0) => 0 => 0) => 0 => 0;
        nont Loop : (((0 => 0) => 0 => 0) => 0 => 0) => (((0 => 0) => 0 => 0) => 0 => 0) => 0 => ((0 => 0) => 0 => 0) => 0;
        nont C : (0 => 0 => 0) => 0 => 0 => 0;
        nont I : (0 => 0) => 0 => 0;
        nont K : (0 => 0) => 0 => 0;
        nont Newr : (((0 => 0) => 0 => 0) => 0) => 0;
        nont Neww : (((0 => 0) => 0 => 0) => 0) => 0;
        nont Close : ((0 => 0) => 0 => 0) => 0 => 0;
        nont Read : ((0 => 0) => 0 => 0) => 0 => 0;
        nont Write : ((0 => 0) => 0 => 0) => 0 => 0;
        
        tml br : 2;
        tml newr : 1;
        tml read : 1;
        tml write : 1;
        tml close : 1;
        tml neww : 1;
        tml end : 1;
        tml dummy : 0;
        
        state q0 qr qw qrw qc fail;
        
        initnt S;
        initst q0;
        failst fail;
        
        
        S = GenConsume Newr Read Close (GenConsume Neww Write Close (end dummy));
        // (GenConsume Neww F2 (end dummy)). 
        GenConsume gen use finish k = gen (Loop use finish k);
        Loop use finish k x = br (finish x k) (use x (Loop use finish k x));
        C use k x = use x k;
        I x y = x y;
        K x y = y;
        Newr k = br (newr (k I)) (k K);
        Neww k = br (neww (k I)) (k K);
        Close x k = x close k;
        Read x k = x read k;
        Write y k = y write k;
        
        q0 -> br q0 q0;
        q0 -> newr qr;
        q0 -> neww qw;
        q0 -> read fail;
        q0 -> write fail;
        q0 -> close fail;
        
        qr -> br qr qr;
        qr -> read qr;
        qr -> close qc;
        qr -> neww qrw;
        qr -> newr fail;
        qr -> write fail;
        qr -> end fail;
        
        qw -> br qw qw;
        qw -> write qw;
        qw -> close qc;
        qw -> newr qrw;
        qw -> read fail;
        qw -> neww fail;
        qw -> end fail;
        
        qrw -> br qrw qrw;
        qrw -> newr qrw;
        qrw -> neww qrw;
        qrw -> read qrw;
        qrw -> write qrw;
        qrw -> close qrw;
        
        qc -> br qc qc;
        qc -> newr qrw;
        qc -> neww qrw;
        qc -> read fail;
        qc -> write fail;
        qc -> close fail;
    """)

    property("fold_right") = doPosTest("""
        %BEGING
        Main_1  -> K_main_7 .
        Bottom0  -> unit .
        Add y0_ y1_ k  ->  y1_  (F_4264 y0_  k  True  ) (F_4264 y0_  k  y0_  ) .
        Br_fold_right b f xs2 acc k  -> br ( b  Bottom0  (l1 Bottom0  ) ) (l0 (xs2 True  (F_3923 f  xs2  acc  k  ) ) ) .
        F_3923 f_3919 xs2_3920 acc_3921 k_3922 x0_3752 x1_3752  ->  x0_3752  (F_4297 f_3919  xs2_3920  acc_3921  k_3922  x0_3752  True  ) (F_4297 f_3919  xs2_3920  acc_3921  k_3922  x0_3752  x1_3752  ) .
        F_3926 f_3925 x0_3749 x1_3749 x2_3749 x_3750  -> F_4410 f_3925  x0_3749  x1_3749  x2_3749  x0_3749  (F_3931 x0_3749  x2_3749  x_3750  ) .
        F_3931 x0_3928 x2_3929 x_3930 x0_3751 x1_3751  ->  x0_3751  (F_4381 x0_3928  x2_3929  x_3930  x0_3751  x1_3751  x0_3928  ) (F_4381 x0_3928  x2_3929  x_3930  x0_3751  x1_3751  False  ) .
        F_3934 xs2_3933 x_3745  -> br (xs2_3933 True  (F_3937 x_3745  ) ) (xs2_3933 False  (F_3937 x_3745  ) ) .
        F_3937 x_3936 x0_3746 x1_3746  ->  x0_3746  (F_4306 x_3936  True  ) (F_4306 x_3936  x1_3746  ) .
        F_3942 xs10__3939 xs12__3940 xs2_3941 x_3756 x_4732  ->  xs12__3940  (F_4431 xs10__3939  xs2_3941  x_3756  True  x_4732  ) (F_4431 xs10__3939  xs2_3941  x_3756  xs10__3939  x_4732  ) .
        F_3945 k_3944 x_3759  ->  x_3759  (F_4451 k_3944  True  ) (br (F_4451 k_3944  True  ) (F_4451 k_3944  False  ) ) .
        F_3948 f_3947 x0_3790 x1_3790 x_3791  -> F_4922 f_3947  x0_3790  x1_3790  (F_3951 x_3791  ) .
        F_3951 x_3950 x0_3792 x1_3792 x2_3792  ->  x1_3792  (F_4500 x_3950  True  ) (br (F_4500 x_3950  True  ) (F_4500 x_3950  False  ) ) .
        F_3954 xs2_3953 x_3786 x_3787  -> F_xs_prime_ xs2_3953  (F_3957 x_3787  ) .
        F_3957 x_3956 x_3788  ->  x_3788  (F_4485 x_3956  True  ) (br (F_4485 x_3956  True  ) (F_4485 x_3956  False  ) ) .
        F_3963 f_3959 k_3960 r0_3961 r2_3962 x0_3782 x1_3782  ->  x0_3782  (F_4567 f_3959  k_3960  r0_3961  r2_3962  x0_3782  x1_3782  r2_3962  ) (F_4567 f_3959  k_3960  r0_3961  r2_3962  x0_3782  x1_3782  False  ) .
        F_3966 f_3965 x0_3779 x1_3779 x_3780  -> F_4642 f_3965  x0_3779  x1_3779  x0_3779  (F_3971 x0_3779  x1_3779  x_3780  ) .
        F_3971 x0_3968 x1_3969 x_3970 x0_3781 x1_3781 x2_3781  ->  x0_3781  (F_4599 x1_3969  x_3970  x0_3781  x1_3781  x2_3781  True  ) ( x1_3781  (F_4599 x1_3969  x_3970  x0_3781  x1_3781  x2_3781  x0_3968  ) (F_4599 x1_3969  x_3970  x0_3781  x1_3781  x2_3781  False  ) ) .
        F_3974 k_3973 x0_3807 x1_3807 x2_3807 x_4822  -> 
         x2_3807  (F_4659 k_3973  True  x_4822  ) 
          (br  (F_4659 k_3973  False  x_4822  ) ( x1_3807  Bottom0 (F_4659 k_3973  True  x_4822  ))) .
        /**
         x2_3807  (F_4659 k_3973  True  x_4822  ) ( x1_3807  (F_4659 k_3973  False  x_4822  ) (br (F_4659 k_3973  True  x_4822  ) (F_4659 k_3973  False  x_4822  ) ) ) .
        **/
        F_3976 x_3812 x_3813  -> F_make_list (br (F_4666 x_3813  True  ) (F_4666 x_3813  False  ) ) .
        F_3979 k_3978 x0_3820 x1_3820 x2_3820 x_3821  -> F_4677 k_3978  x0_3820  x2_3820  x1_3820  True  (F_3985 x_3821  ) .
        F_3982 k_3981 x_3815 x_4844  -> 
        br (F_4683 k_3981  x_3815  True  x_4844  ) ( x_3815   Bottom0 (F_4683 k_3981  x_3815  False  x_4844  )) .
        /**
         x_3815  (F_4683 k_3981  x_3815  True  x_4844  ) (br (F_4683 k_3981  x_3815  True  x_4844  ) (F_4683 k_3981  x_3815  False  x_4844  ) ) .
        **/
        F_3985 x_3984 x_3823  -> br (x_3984 True  (F_3988 x_3823  ) ) (x_3984 False  (F_3988 x_3823  ) ) .
        F_3988 x_3987 x0_3824 x1_3824  ->  x0_3824  (F_4692 x_3987  True  ) (F_4692 x_3987  x1_3824  ) .
        F_4259 y0__4257 k_4258 b_3989  -> br (k_4258 True  b_3989  ) ( y0__4257  unit (k_4258 False  b_3989  ) ) .
        F_4264 y0__4262 k_4263 b_3995  -> br (F_4259 y0__4262  k_4263  True  ) ( b_3995   unit (F_4259 y0__4262  k_4263  False  ) ) .
        F_4273 f_4268 xs2_4269 acc_4270 k_4271 x0_4272 b_3999  -> 
         br (F_4278 f_4268  xs2_4269  k_4271  x0_4272  True  b_3999  ) ( acc_4270 unit (F_4278 f_4268  xs2_4269  k_4271  x0_4272  False  b_3999  ) ) .
        F_4278 f_4274 xs2_4275 k_4276 x0_4277 b_4001 x_4706  -> 
        br (F_4282 f_4274  xs2_4275  k_4276  True  b_4001  x_4706  )
        ( x0_4277 unit  (F_4282 f_4274  xs2_4275  k_4276  False  b_4001  x_4706  ) ) .
        F_4282 f_4279 xs2_4280 k_4281 b_4003 x_4700 x_4701  -> K_fold_right_2 (F_3926 f_4279  ) k_4281  (F_3934 xs2_4280  ) b_4003  x_4700  x_4701  .
        F_4297 f_4292 xs2_4293 acc_4294 k_4295 x0_4296 b_4009  -> 
        br (F_4273 f_4292  xs2_4293  acc_4294  k_4295  x0_4296  True  )
        ( b_4009 unit (F_4273 f_4292  xs2_4293  acc_4294  k_4295  x0_4296  False  ) ) .
        F_4306 x_4305 b_4015  ->  b_4015  (x_4305 True  ) (br (x_4305 True  ) (x_4305 False  ) ) .
        F_4315 x0_4310 x2_4311 x_4312 x0_4313 x1_4314 b_4021  ->  x0_4313  (F_4339 x0_4310  x2_4311  x_4312  x0_4313  True  b_4021  ) (F_4339 x0_4310  x2_4311  x_4312  x0_4313  x1_4314  b_4021  ) .
        F_4320 x0_4316 x2_4317 x_4318 x0_4319 b_4023 x_4720  ->  x0_4319  (F_4326 x2_4317  x_4318  x0_4319  x0_4316  b_4023  x_4720  ) (F_4326 x2_4317  x_4318  x0_4319  False  b_4023  x_4720  ) .
        F_4322 x_4321 b_4027 x_4710 x_4711  -> br (x_4321 True  x_4710  x_4711  ) ( b_4027  unit (x_4321 False  x_4710  x_4711  ) ) .
        F_4326 x2_4323 x_4324 x0_4325 b_4029 x_4714 x_4715  ->  b_4029  (F_4322 x_4324  True  x_4714  x_4715  ) ( x0_4325  (F_4322 x_4324  x2_4323  x_4714  x_4715  ) (F_4322 x_4324  False  x_4714  x_4715  ) ) .
        F_4339 x0_4335 x2_4336 x_4337 x0_4338 b_4035 x_4708  -> 
        br (F_4320 x0_4335  x2_4336  x_4337  x0_4338  True  x_4708  )
        ( b_4035 unit (F_4320 x0_4335  x2_4336  x_4337  x0_4338  False  x_4708  ) ) .
        F_4351 x0_4346 x2_4347 x_4348 x0_4349 x1_4350 b_4039  -> 
        br (F_4315 x0_4346  x2_4347  x_4348  x0_4349  x1_4350  True  )
        ( b_4039 unit (F_4315 x0_4346  x2_4347  x_4348  x0_4349  x1_4350  False  ) ) .
        F_4357 x0_4352 x2_4353 x_4354 x0_4355 x1_4356 b_4041  ->  b_4041  (F_4351 x0_4352  x2_4353  x_4354  x0_4355  x1_4356  True  ) ( x1_4356  (F_4351 x0_4352  x2_4353  x_4354  x0_4355  x1_4356  x2_4353  ) (F_4351 x0_4352  x2_4353  x_4354  x0_4355  x1_4356  False  ) ) .
        F_4369 x0_4364 x2_4365 x_4366 x0_4367 x1_4368 b_4043  ->  b_4043  (F_4357 x0_4364  x2_4365  x_4366  x0_4367  x1_4368  True  ) ( x1_4368  (F_4357 x0_4364  x2_4365  x_4366  x0_4367  x1_4368  x0_4364  ) (F_4357 x0_4364  x2_4365  x_4366  x0_4367  x1_4368  False  ) ) .
        F_4381 x0_4376 x2_4377 x_4378 x0_4379 x1_4380 b_4045  ->  b_4045  (F_4369 x0_4376  x2_4377  x_4378  x0_4379  x1_4380  True  ) ( x0_4379  (F_4369 x0_4376  x2_4377  x_4378  x0_4379  x1_4380  x2_4377  ) (F_4369 x0_4376  x2_4377  x_4378  x0_4379  x1_4380  False  ) ) .
        F_4396 f_4394 x0_4395 b_4055 x_4729  ->  x0_4395  (f_4394 True  b_4055  x_4729  ) (br (f_4394 True  b_4055  x_4729  ) (f_4394 False  b_4055  x_4729  ) ) .
        F_4401 f_4399 x0_4400 b_4061 x_4723  ->  b_4061  (F_4396 f_4399  x0_4400  True  x_4723  ) (br (F_4396 f_4399  x0_4400  True  x_4723  ) (F_4396 f_4399  x0_4400  False  x_4723  ) ) .
        F_4405 f_4402 x0_4403 x2_4404 b_4063 x_4722  ->  b_4063  (F_4401 f_4402  x0_4403  True  x_4722  ) (F_4401 f_4402  x0_4403  x2_4404  x_4722  ) .
        F_4410 f_4406 x0_4407 x1_4408 x2_4409 b_4065 x_4721  ->  b_4065  (F_4405 f_4406  x0_4407  x2_4409  True  x_4721  ) (F_4405 f_4406  x0_4407  x2_4409  x1_4408  x_4721  ) .
        F_4413 k_4412 b_4071 x_4730  ->  b_4071  (k_4412 True  x_4730  ) (br (k_4412 True  x_4730  ) (k_4412 False  x_4730  ) ) .
        F_4417 xs10__4414 xs2_4415 x_4416 b_4073 x_4733  -> 
        br (F_4420 xs10__4414  xs2_4415  True  b_4073  x_4733  ) ( x_4416  unit (F_4420 xs10__4414  xs2_4415  False  b_4073  x_4733  ) ) .
        F_4420 xs10__4418 xs2_4419 b_4075 x_4747 x_4748  ->  xs10__4418  (F_4422 xs2_4419  True  b_4075  x_4747  x_4748  ) (br (F_4422 xs2_4419  True  b_4075  x_4747  x_4748  ) (F_4422 xs2_4419  False  b_4075  x_4747  x_4748  ) ) .
        F_4422 xs2_4421 b_4077 x_4738 x_4739 x_4740  -> br ( x_4738  Bottom0  (l1 (xs2_4421 (F_3945 x_4740  ) ) ) ) (l0 ( x_4739  (F_4443 b_4077  x_4740  True  ) (F_4443 b_4077  x_4740  b_4077  ) ) ) .
        F_4431 xs10__4428 xs2_4429 x_4430 b_4083 x_4749  ->  b_4083  (F_4417 xs10__4428  xs2_4429  x_4430  True  x_4749  ) (br (F_4417 xs10__4428  xs2_4429  x_4430  True  x_4749  ) (F_4417 xs10__4428  xs2_4429  x_4430  False  x_4749  ) ) .
        F_4438 i0__4436 k_4437 b_4087  ->  i0__4436  (k_4437 True  b_4087  ) (br (k_4437 True  b_4087  ) (k_4437 False  b_4087  ) ) .
        F_4443 i0__4441 k_4442 b_4093  ->  b_4093  (F_4438 i0__4441  k_4442  True  ) (br (F_4438 i0__4441  k_4442  True  ) (F_4438 i0__4441  k_4442  False  ) ) .
        F_4451 k_4450 b_4099  -> br (k_4450 True  b_4099  ) (k_4450 False  b_4099  ) .
        F_4464 f_4461 xs2_4462 acc1__4463 b_4109 x_4764  ->  acc1__4463  (Br_fold_right True  f_4461  xs2_4462  b_4109  x_4764  ) (br (Br_fold_right True  f_4461  xs2_4462  b_4109  x_4764  ) (Br_fold_right False  f_4461  xs2_4462  b_4109  x_4764  ) ) .
        F_4473 f_4470 xs2_4471 r1_4472 b_4117 x_4773  ->  r1_4472  (F_4476 f_4470  xs2_4471  True  b_4117  x_4773  ) (br (F_4476 f_4470  xs2_4471  True  b_4117  x_4773  ) (F_4476 f_4470  xs2_4471  False  b_4117  x_4773  ) ) .
        F_4476 f_4474 xs2_4475 b_4119 x_4767 x_4768  -> Fold_right (F_3948 f_4474  ) (F_3954 xs2_4475  ) b_4119  x_4767  x_4768  .
        F_4483 f_4480 xs2_4481 r1_4482 b_4123 x_4765  ->  b_4123  (F_4473 f_4480  xs2_4481  r1_4482  False  x_4765  ) (br (F_4473 f_4480  xs2_4481  r1_4482  True  x_4765  ) (F_4473 f_4480  xs2_4481  r1_4482  False  x_4765  ) ) .
        F_4485 x_4484 b_4125  -> br (x_4484 True  b_4125  ) (x_4484 False  b_4125  ) .
        F_4491 f_4488 x0_4489 x1_4490 b_4131 x_4786  ->  x0_4489  (F_4495 f_4488  True  b_4131  x_4786  ) (F_4495 f_4488  x1_4490  b_4131  x_4786  ) .
        F_4493 f_4492 b_4133 x_4775 x_4776  -> br (f_4492 True  b_4133  x_4775  x_4776  ) (f_4492 False  b_4133  x_4775  x_4776  ) .
        F_4495 f_4494 b_4137 x_4780 x_4781  ->  b_4137  (F_4493 f_4494  True  x_4780  x_4781  ) (br (F_4493 f_4494  True  x_4780  x_4781  ) (F_4493 f_4494  False  x_4780  x_4781  ) ) .
        F_4500 x_4499 b_4141  -> br (x_4499 True  b_4141  ) (x_4499 False  b_4141  ) .
        F_4508 f_4503 k_4504 r0_4505 x0_4506 x1_4507 b_4147  ->  x0_4506  (F_4519 f_4503  k_4504  r0_4505  x1_4507  r0_4505  b_4147  ) (F_4519 f_4503  k_4504  r0_4505  x1_4507  False  b_4147  ) .
        F_4511 f_4509 k_4510 b_4149 x_4792  -> K_fold_right_3 (F_3966 f_4509  ) k_4510  b_4149  x_4792  .
        F_4514 f_4512 k_4513 b_4151 x_4790  ->  b_4151  (F_4511 f_4512  k_4513  True  x_4790  ) (br (F_4511 f_4512  k_4513  True  x_4790  ) (F_4511 f_4512  k_4513  False  x_4790  ) ) .
        F_4519 f_4515 k_4516 r0_4517 x1_4518 b_4153 x_4789  ->  b_4153  (F_4514 f_4515  k_4516  True  x_4789  ) ( x1_4518  (F_4514 f_4515  k_4516  r0_4517  x_4789  ) (F_4514 f_4515  k_4516  False  x_4789  ) ) .
        F_4534 f_4529 k_4530 r0_4531 x0_4532 x1_4533 b_4159  ->  b_4159  (F_4508 f_4529  k_4530  r0_4531  x0_4532  x1_4533  True  ) (br (F_4508 f_4529  k_4530  r0_4531  x0_4532  x1_4533  True  ) (F_4508 f_4529  k_4530  r0_4531  x0_4532  x1_4533  False  ) ) .
        F_4540 f_4535 k_4536 r0_4537 x0_4538 x1_4539 b_4161  ->  b_4161  (F_4534 f_4535  k_4536  r0_4537  x0_4538  x1_4539  True  ) ( x1_4539  (F_4534 f_4535  k_4536  r0_4537  x0_4538  x1_4539  r0_4537  ) (F_4534 f_4535  k_4536  r0_4537  x0_4538  x1_4539  False  ) ) .
        F_4553 f_4547 k_4548 r0_4549 r2_4550 x0_4551 x1_4552 b_4163  ->  b_4163  (F_4540 f_4547  k_4548  r0_4549  x0_4551  x1_4552  True  ) ( x1_4552  (F_4540 f_4547  k_4548  r0_4549  x0_4551  x1_4552  r2_4550  ) (F_4540 f_4547  k_4548  r0_4549  x0_4551  x1_4552  False  ) ) .
        F_4567 f_4561 k_4562 r0_4563 r2_4564 x0_4565 x1_4566 b_4165  ->  b_4165  (F_4553 f_4561  k_4562  r0_4563  r2_4564  x0_4565  x1_4566  True  ) ( x0_4565  (F_4553 f_4561  k_4562  r0_4563  r2_4564  x0_4565  x1_4566  r0_4563  ) (F_4553 f_4561  k_4562  r0_4563  r2_4564  x0_4565  x1_4566  False  ) ) .
        F_4584 x_4582 x0_4583 b_4175  ->  x0_4583  (x_4582 True  b_4175  ) (br (x_4582 True  b_4175  ) (x_4582 False  b_4175  ) ) .
        F_4589 x_4587 x0_4588 b_4181  ->  b_4181  (F_4584 x_4587  x0_4588  True  ) (br (F_4584 x_4587  x0_4588  True  ) (F_4584 x_4587  x0_4588  False  ) ) .
        F_4593 x_4590 x0_4591 x2_4592 b_4183  ->  b_4183  (F_4589 x_4590  x0_4591  True  ) (F_4589 x_4590  x0_4591  x2_4592  ) .
        F_4599 x1_4594 x_4595 x0_4596 x1_4597 x2_4598 b_4185  ->  b_4185  (F_4593 x_4595  x0_4596  x2_4598  True  ) ( x1_4597  (F_4593 x_4595  x0_4596  x2_4598  x1_4594  ) (F_4593 x_4595  x0_4596  x2_4598  False  ) ) .
        F_4622 f_4619 x0_4620 x1_4621 b_4193 x_4812  ->  x0_4620  (F_4630 f_4619  x0_4620  True  b_4193  x_4812  ) (F_4630 f_4619  x0_4620  x1_4621  b_4193  x_4812  ) .
        F_4625 f_4623 x0_4624 b_4195 x_4798 x_4799  ->  x0_4624  (f_4623 True  b_4195  x_4798  x_4799  ) (br (f_4623 True  b_4195  x_4798  x_4799  ) (f_4623 False  b_4195  x_4798  x_4799  ) ) .
        F_4630 f_4628 x0_4629 b_4201 x_4806 x_4807  ->  b_4201  (F_4625 f_4628  x0_4629  True  x_4806  x_4807  ) (br (F_4625 f_4628  x0_4629  True  x_4806  x_4807  ) (F_4625 f_4628  x0_4629  False  x_4806  x_4807  ) ) .
        F_4638 f_4635 x0_4636 x1_4637 b_4205 x_4796  ->  b_4205  (F_4622 f_4635  x0_4636  x1_4637  True  x_4796  ) (br (F_4622 f_4635  x0_4636  x1_4637  True  x_4796  ) (F_4622 f_4635  x0_4636  x1_4637  False  x_4796  ) ) .
        F_4642 f_4639 x0_4640 x1_4641 b_4207 x_4795  ->  b_4207  (F_4638 f_4639  x0_4640  x1_4641  True  x_4795  ) (F_4638 f_4639  x0_4640  x1_4641  x1_4641  x_4795  ) .
        F_4645 r1_4643 r2_4644 b_4209 x_4821  ->  r1_4643  (F_4647 r2_4644  False  b_4209  x_4821  ) (br (F_4647 r2_4644  True  b_4209  x_4821  ) (F_4647 r2_4644  False  b_4209  x_4821  ) ) .
        F_4647 r2_4646 b_4211 x_4815 x_4816  -> Fold_right Add  r2_4646  b_4211  x_4815  x_4816  .
        F_4652 r1_4650 r2_4651 b_4215 x_4813  ->  b_4215  (F_4645 r1_4650  r2_4651  True  x_4813  ) (br (F_4645 r1_4650  r2_4651  True  x_4813  ) (F_4645 r1_4650  r2_4651  False  x_4813  ) ) .
        F_4654 k_4653 b_4217  ->  b_4217  Bottom0  (l1 (Fail True  k_4653  ) ) .
        F_4659 k_4658 b_4221 x_4823  -> F_4652 b_4221  x_4823  b_4221  (K_main_5 k_4658  ) .
        F_4666 x_4665 b_4227  -> br (x_4665 True  b_4227  ) (x_4665 False  b_4227  ) .
        F_4669 k_4667 x0_4668 b_4231 x_4827 x_4828  ->  x0_4668  (F_4671 k_4667  True  b_4231  x_4827  x_4828  ) (br (F_4671 k_4667  True  b_4231  x_4827  x_4828  ) (F_4671 k_4667  False  b_4231  x_4827  x_4828  ) ) .
        F_4671 k_4670 b_4233 x_4835 x_4836 x_4837  -> K_cons_make_list (F_3982 k_4670  ) b_4233  x_4835  x_4836  x_4837  .
        F_4677 k_4674 x0_4675 x2_4676 b_4237 x_4838 x_4839  ->  b_4237  (F_4669 k_4674  x0_4675  True  x_4838  x_4839  ) ( x2_4676  (F_4669 k_4674  x0_4675  False  x_4838  x_4839  ) (br (F_4669 k_4674  x0_4675  True  x_4838  x_4839  ) (F_4669 k_4674  x0_4675  False  x_4838  x_4839  ) ) ) .
        F_4683 k_4681 x_4682 b_4241 x_4845  ->  x_4682  (F_4685 k_4681  False  b_4241  x_4845  ) (br (F_4685 k_4681  True  b_4241  x_4845  ) (F_4685 k_4681  False  b_4241  x_4845  ) ) .
        F_4685 k_4684 b_4243 x_4850 x_4851  -> k_4684 True  b_4243  x_4850  x_4851  .
        F_4692 x_4691 b_4253  ->  b_4253  (x_4691 True  ) (br (x_4691 True  ) (x_4691 False  ) ) .
        F_4922 f_4919 x0_4920 x1_4921 x_4774  -> br (F_4491 f_4919  x0_4920  x1_4921  True  x_4774  ) (F_4491 f_4919  x0_4920  x1_4921  False  x_4774  ) .
        F_make_list k  -> Bottom0 .
        F_xs_prime_ xs2 k  -> xs2 k  .
        Fail b k  -> event_fail k  .
        Fold_right f xs2 acc0_ acc1_ k  -> br ( acc0_  Bottom0  (l1 ( acc1_  (F_4464 f  xs2  acc1_  True  k  ) (br (F_4464 f  xs2  acc1_  True  k  ) (F_4464 f  xs2  acc1_  False  k  ) ) ) ) ) ( acc1_  Bottom0  (l0 (k False  True  ) ) ) .
        K_cons_make_list k r10_ r11_ r12_ r2  -> F_4413 k  r11_  (F_3942 r10_  r12_  r2  ) .
        K_fold_right_2 f k xs2 r0_10 r1_10 r2_10  -> F_4483 f  xs2  r1_10  r1_10  (F_3963 f  k  r0_10  r2_10  ) .
        K_fold_right_3 f k r0_11 r1_11  -> f r0_11  r1_11  k  .
        K_main_5 k r0_ r1_  -> br ( r1_  (F_4654 k  True  ) (F_4654 k  r0_  ) ) (l0 k  ) .
        K_main_6  -> unit .
        K_main_7  -> K_main_8 .
        K_main_8  -> Make_list (F_3974 K_main_6  ) .
        Make_list k  -> br (l1 (Make_list (F_3979 k  ) ) ) (l0 (k False  True  False  F_3976  ) ) .
        True x y -> x.
        False x y -> y.
        %ENDG
        
        %BEGINA
        q0 br -> q0 q0 .
        q0 event_fail -> q1 .
        q0 l0 -> q0 .
        q0 l1 -> q0 .
        q0 unit -> .
        %ENDA
    """, """
        nont Main_1 : 0;
        nont Bottom0 : 0;
        nont Add : (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont Br_fold_right : (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_3923 : ((0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_3926 : ((0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_3931 : (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_3934 : ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => 0) => 0;
        nont F_3937 : ((0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_3942 : (0 => 0 => 0) => (0 => 0 => 0) => (((0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_3945 : ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => 0;
        nont F_3948 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_3951 : ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_3954 : (((0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_3957 : ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => 0;
        nont F_3963 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_3966 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_3971 : (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_3974 : 0 => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0;
        nont F_3976 : (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_3979 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0;
        nont F_3982 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0;
        nont F_3985 : ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => 0) => 0;
        nont F_3988 : ((0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4259 : (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => 0;
        nont F_4264 : (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => 0;
        nont F_4273 : ((0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4278 : ((0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4282 : ((0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4297 : ((0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4306 : ((0 => 0 => 0) => 0) => (0 => 0 => 0) => 0;
        nont F_4315 : (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4320 : (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4322 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4326 : (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4339 : (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4351 : (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4357 : (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4369 : (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4381 : (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4396 : ((0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4401 : ((0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4405 : ((0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4410 : ((0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4413 : ((0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0;
        nont F_4417 : (0 => 0 => 0) => (((0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4420 : (0 => 0 => 0) => (((0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4422 : (((0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4431 : (0 => 0 => 0) => (((0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4438 : (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => 0;
        nont F_4443 : (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => 0;
        nont F_4451 : ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => 0;
        nont F_4464 : ((0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4473 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (((0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4476 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (((0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4483 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (((0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4485 : ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => 0;
        nont F_4491 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4493 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4495 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4500 : ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => 0;
        nont F_4508 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4511 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4514 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4519 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4534 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4540 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4553 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4567 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4584 : ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4589 : ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4593 : ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4599 : (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont F_4622 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4625 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4630 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4638 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4642 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4645 : (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4647 : ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4652 : (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_4654 : 0 => (0 => 0 => 0) => 0;
        nont F_4659 : 0 => (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0;
        nont F_4666 : ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => 0;
        nont F_4669 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (((0 => 0 => 0) => 0) => 0) => 0;
        nont F_4671 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (((0 => 0 => 0) => 0) => 0) => 0;
        nont F_4677 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (((0 => 0 => 0) => 0) => 0) => 0;
        nont F_4683 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0;
        nont F_4685 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0;
        nont F_4692 : ((0 => 0 => 0) => 0) => (0 => 0 => 0) => 0;
        nont F_4922 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont F_make_list : 0 => 0;
        nont F_xs_prime_ : (((0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => 0) => 0;
        nont Fail : (0 => 0 => 0) => 0 => 0;
        nont Fold_right : ((0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0;
        nont K_cons_make_list : ((0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (((0 => 0 => 0) => 0) => 0) => 0;
        nont K_fold_right_2 : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (((0 => 0 => 0) => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont K_fold_right_3 : ((0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont K_main_5 : 0 => (0 => 0 => 0) => (0 => 0 => 0) => 0;
        nont K_main_6 : 0;
        nont K_main_7 : 0;
        nont K_main_8 : 0;
        nont Make_list : ((0 => 0 => 0) => (0 => 0 => 0) => (0 => 0 => 0) => ((0 => 0 => 0) => ((0 => 0 => 0) => (0 => 0 => 0) => 0) => 0) => 0) => 0;
        nont True : 0 => 0 => 0;
        nont False : 0 => 0 => 0;
        tml dummy : 0;
        tml unit : 0 => 0;
        tml l1 : 0 => 0;
        tml l0 : 0 => 0;
        tml event_fail : 0 => 0;
        tml br : 0 => 0 => 0;
        initnt Main_1;
        
        Main_1  = K_main_7;
        Bottom0  = unit dummy;
        Add y0_ y1_ k  = y1_ (F_4264 y0_ k True) (F_4264 y0_ k y0_);
        Br_fold_right b f xs2 acc k  = br (b Bottom0 (l1 Bottom0)) (l0 (xs2 True (F_3923 f xs2 acc k)));
        F_3923 f_3919 xs2_3920 acc_3921 k_3922 x0_3752 x1_3752  = x0_3752 (F_4297 f_3919 xs2_3920 acc_3921 k_3922 x0_3752 True) (F_4297 f_3919 xs2_3920 acc_3921 k_3922 x0_3752 x1_3752);
        F_3926 f_3925 x0_3749 x1_3749 x2_3749 x_3750  = F_4410 f_3925 x0_3749 x1_3749 x2_3749 x0_3749 (F_3931 x0_3749 x2_3749 x_3750);
        F_3931 x0_3928 x2_3929 x_3930 x0_3751 x1_3751  = x0_3751 (F_4381 x0_3928 x2_3929 x_3930 x0_3751 x1_3751 x0_3928) (F_4381 x0_3928 x2_3929 x_3930 x0_3751 x1_3751 False);
        F_3934 xs2_3933 x_3745  = br (xs2_3933 True (F_3937 x_3745)) (xs2_3933 False (F_3937 x_3745));
        F_3937 x_3936 x0_3746 x1_3746  = x0_3746 (F_4306 x_3936 True) (F_4306 x_3936 x1_3746);
        F_3942 xs10__3939 xs12__3940 xs2_3941 x_3756 x_4732  = xs12__3940 (F_4431 xs10__3939 xs2_3941 x_3756 True x_4732) (F_4431 xs10__3939 xs2_3941 x_3756 xs10__3939 x_4732);
        F_3945 k_3944 x_3759  = x_3759 (F_4451 k_3944 True) (br (F_4451 k_3944 True) (F_4451 k_3944 False));
        F_3948 f_3947 x0_3790 x1_3790 x_3791  = F_4922 f_3947 x0_3790 x1_3790 (F_3951 x_3791);
        F_3951 x_3950 x0_3792 x1_3792 x2_3792  = x1_3792 (F_4500 x_3950 True) (br (F_4500 x_3950 True) (F_4500 x_3950 False));
        F_3954 xs2_3953 x_3786 x_3787  = F_xs_prime_ xs2_3953 (F_3957 x_3787);
        F_3957 x_3956 x_3788  = x_3788 (F_4485 x_3956 True) (br (F_4485 x_3956 True) (F_4485 x_3956 False));
        F_3963 f_3959 k_3960 r0_3961 r2_3962 x0_3782 x1_3782  = x0_3782 (F_4567 f_3959 k_3960 r0_3961 r2_3962 x0_3782 x1_3782 r2_3962) (F_4567 f_3959 k_3960 r0_3961 r2_3962 x0_3782 x1_3782 False);
        F_3966 f_3965 x0_3779 x1_3779 x_3780  = F_4642 f_3965 x0_3779 x1_3779 x0_3779 (F_3971 x0_3779 x1_3779 x_3780);
        F_3971 x0_3968 x1_3969 x_3970 x0_3781 x1_3781 x2_3781  = x0_3781 (F_4599 x1_3969 x_3970 x0_3781 x1_3781 x2_3781 True) (x1_3781 (F_4599 x1_3969 x_3970 x0_3781 x1_3781 x2_3781 x0_3968) (F_4599 x1_3969 x_3970 x0_3781 x1_3781 x2_3781 False));
        F_3974 k_3973 x0_3807 x1_3807 x2_3807 x_4822  = x2_3807 (F_4659 k_3973 True x_4822) (br (F_4659 k_3973 False x_4822) (x1_3807 Bottom0 (F_4659 k_3973 True x_4822)));
        F_3976 x_3812 x_3813  = F_make_list (br (F_4666 x_3813 True) (F_4666 x_3813 False));
        F_3979 k_3978 x0_3820 x1_3820 x2_3820 x_3821  = F_4677 k_3978 x0_3820 x2_3820 x1_3820 True (F_3985 x_3821);
        F_3982 k_3981 x_3815 x_4844  = br (F_4683 k_3981 x_3815 True x_4844) (x_3815 Bottom0 (F_4683 k_3981 x_3815 False x_4844));
        F_3985 x_3984 x_3823  = br (x_3984 True (F_3988 x_3823)) (x_3984 False (F_3988 x_3823));
        F_3988 x_3987 x0_3824 x1_3824  = x0_3824 (F_4692 x_3987 True) (F_4692 x_3987 x1_3824);
        F_4259 y0__4257 k_4258 b_3989  = br (k_4258 True b_3989) (y0__4257 (unit dummy) (k_4258 False b_3989));
        F_4264 y0__4262 k_4263 b_3995  = br (F_4259 y0__4262 k_4263 True) (b_3995 (unit dummy) (F_4259 y0__4262 k_4263 False));
        F_4273 f_4268 xs2_4269 acc_4270 k_4271 x0_4272 b_3999  = br (F_4278 f_4268 xs2_4269 k_4271 x0_4272 True b_3999) (acc_4270 (unit dummy) (F_4278 f_4268 xs2_4269 k_4271 x0_4272 False b_3999));
        F_4278 f_4274 xs2_4275 k_4276 x0_4277 b_4001 x_4706  = br (F_4282 f_4274 xs2_4275 k_4276 True b_4001 x_4706) (x0_4277 (unit dummy) (F_4282 f_4274 xs2_4275 k_4276 False b_4001 x_4706));
        F_4282 f_4279 xs2_4280 k_4281 b_4003 x_4700 x_4701  = K_fold_right_2 (F_3926 f_4279) k_4281 (F_3934 xs2_4280) b_4003 x_4700 x_4701;
        F_4297 f_4292 xs2_4293 acc_4294 k_4295 x0_4296 b_4009  = br (F_4273 f_4292 xs2_4293 acc_4294 k_4295 x0_4296 True) (b_4009 (unit dummy) (F_4273 f_4292 xs2_4293 acc_4294 k_4295 x0_4296 False));
        F_4306 x_4305 b_4015  = b_4015 (x_4305 True) (br (x_4305 True) (x_4305 False));
        F_4315 x0_4310 x2_4311 x_4312 x0_4313 x1_4314 b_4021  = x0_4313 (F_4339 x0_4310 x2_4311 x_4312 x0_4313 True b_4021) (F_4339 x0_4310 x2_4311 x_4312 x0_4313 x1_4314 b_4021);
        F_4320 x0_4316 x2_4317 x_4318 x0_4319 b_4023 x_4720  = x0_4319 (F_4326 x2_4317 x_4318 x0_4319 x0_4316 b_4023 x_4720) (F_4326 x2_4317 x_4318 x0_4319 False b_4023 x_4720);
        F_4322 x_4321 b_4027 x_4710 x_4711  = br (x_4321 True x_4710 x_4711) (b_4027 (unit dummy) (x_4321 False x_4710 x_4711));
        F_4326 x2_4323 x_4324 x0_4325 b_4029 x_4714 x_4715  = b_4029 (F_4322 x_4324 True x_4714 x_4715) (x0_4325 (F_4322 x_4324 x2_4323 x_4714 x_4715) (F_4322 x_4324 False x_4714 x_4715));
        F_4339 x0_4335 x2_4336 x_4337 x0_4338 b_4035 x_4708  = br (F_4320 x0_4335 x2_4336 x_4337 x0_4338 True x_4708) (b_4035 (unit dummy) (F_4320 x0_4335 x2_4336 x_4337 x0_4338 False x_4708));
        F_4351 x0_4346 x2_4347 x_4348 x0_4349 x1_4350 b_4039  = br (F_4315 x0_4346 x2_4347 x_4348 x0_4349 x1_4350 True) (b_4039 (unit dummy) (F_4315 x0_4346 x2_4347 x_4348 x0_4349 x1_4350 False));
        F_4357 x0_4352 x2_4353 x_4354 x0_4355 x1_4356 b_4041  = b_4041 (F_4351 x0_4352 x2_4353 x_4354 x0_4355 x1_4356 True) (x1_4356 (F_4351 x0_4352 x2_4353 x_4354 x0_4355 x1_4356 x2_4353) (F_4351 x0_4352 x2_4353 x_4354 x0_4355 x1_4356 False));
        F_4369 x0_4364 x2_4365 x_4366 x0_4367 x1_4368 b_4043  = b_4043 (F_4357 x0_4364 x2_4365 x_4366 x0_4367 x1_4368 True) (x1_4368 (F_4357 x0_4364 x2_4365 x_4366 x0_4367 x1_4368 x0_4364) (F_4357 x0_4364 x2_4365 x_4366 x0_4367 x1_4368 False));
        F_4381 x0_4376 x2_4377 x_4378 x0_4379 x1_4380 b_4045  = b_4045 (F_4369 x0_4376 x2_4377 x_4378 x0_4379 x1_4380 True) (x0_4379 (F_4369 x0_4376 x2_4377 x_4378 x0_4379 x1_4380 x2_4377) (F_4369 x0_4376 x2_4377 x_4378 x0_4379 x1_4380 False));
        F_4396 f_4394 x0_4395 b_4055 x_4729  = x0_4395 (f_4394 True b_4055 x_4729) (br (f_4394 True b_4055 x_4729) (f_4394 False b_4055 x_4729));
        F_4401 f_4399 x0_4400 b_4061 x_4723  = b_4061 (F_4396 f_4399 x0_4400 True x_4723) (br (F_4396 f_4399 x0_4400 True x_4723) (F_4396 f_4399 x0_4400 False x_4723));
        F_4405 f_4402 x0_4403 x2_4404 b_4063 x_4722  = b_4063 (F_4401 f_4402 x0_4403 True x_4722) (F_4401 f_4402 x0_4403 x2_4404 x_4722);
        F_4410 f_4406 x0_4407 x1_4408 x2_4409 b_4065 x_4721  = b_4065 (F_4405 f_4406 x0_4407 x2_4409 True x_4721) (F_4405 f_4406 x0_4407 x2_4409 x1_4408 x_4721);
        F_4413 k_4412 b_4071 x_4730  = b_4071 (k_4412 True x_4730) (br (k_4412 True x_4730) (k_4412 False x_4730));
        F_4417 xs10__4414 xs2_4415 x_4416 b_4073 x_4733  = br (F_4420 xs10__4414 xs2_4415 True b_4073 x_4733) (x_4416 (unit dummy) (F_4420 xs10__4414 xs2_4415 False b_4073 x_4733));
        F_4420 xs10__4418 xs2_4419 b_4075 x_4747 x_4748  = xs10__4418 (F_4422 xs2_4419 True b_4075 x_4747 x_4748) (br (F_4422 xs2_4419 True b_4075 x_4747 x_4748) (F_4422 xs2_4419 False b_4075 x_4747 x_4748));
        F_4422 xs2_4421 b_4077 x_4738 x_4739 x_4740  = br (x_4738 Bottom0 (l1 (xs2_4421 (F_3945 x_4740)))) (l0 (x_4739 (F_4443 b_4077 x_4740 True) (F_4443 b_4077 x_4740 b_4077)));
        F_4431 xs10__4428 xs2_4429 x_4430 b_4083 x_4749  = b_4083 (F_4417 xs10__4428 xs2_4429 x_4430 True x_4749) (br (F_4417 xs10__4428 xs2_4429 x_4430 True x_4749) (F_4417 xs10__4428 xs2_4429 x_4430 False x_4749));
        F_4438 i0__4436 k_4437 b_4087  = i0__4436 (k_4437 True b_4087) (br (k_4437 True b_4087) (k_4437 False b_4087));
        F_4443 i0__4441 k_4442 b_4093  = b_4093 (F_4438 i0__4441 k_4442 True) (br (F_4438 i0__4441 k_4442 True) (F_4438 i0__4441 k_4442 False));
        F_4451 k_4450 b_4099  = br (k_4450 True b_4099) (k_4450 False b_4099);
        F_4464 f_4461 xs2_4462 acc1__4463 b_4109 x_4764  = acc1__4463 (Br_fold_right True f_4461 xs2_4462 b_4109 x_4764) (br (Br_fold_right True f_4461 xs2_4462 b_4109 x_4764) (Br_fold_right False f_4461 xs2_4462 b_4109 x_4764));
        F_4473 f_4470 xs2_4471 r1_4472 b_4117 x_4773  = r1_4472 (F_4476 f_4470 xs2_4471 True b_4117 x_4773) (br (F_4476 f_4470 xs2_4471 True b_4117 x_4773) (F_4476 f_4470 xs2_4471 False b_4117 x_4773));
        F_4476 f_4474 xs2_4475 b_4119 x_4767 x_4768  = Fold_right (F_3948 f_4474) (F_3954 xs2_4475) b_4119 x_4767 x_4768;
        F_4483 f_4480 xs2_4481 r1_4482 b_4123 x_4765  = b_4123 (F_4473 f_4480 xs2_4481 r1_4482 False x_4765) (br (F_4473 f_4480 xs2_4481 r1_4482 True x_4765) (F_4473 f_4480 xs2_4481 r1_4482 False x_4765));
        F_4485 x_4484 b_4125  = br (x_4484 True b_4125) (x_4484 False b_4125);
        F_4491 f_4488 x0_4489 x1_4490 b_4131 x_4786  = x0_4489 (F_4495 f_4488 True b_4131 x_4786) (F_4495 f_4488 x1_4490 b_4131 x_4786);
        F_4493 f_4492 b_4133 x_4775 x_4776  = br (f_4492 True b_4133 x_4775 x_4776) (f_4492 False b_4133 x_4775 x_4776);
        F_4495 f_4494 b_4137 x_4780 x_4781  = b_4137 (F_4493 f_4494 True x_4780 x_4781) (br (F_4493 f_4494 True x_4780 x_4781) (F_4493 f_4494 False x_4780 x_4781));
        F_4500 x_4499 b_4141  = br (x_4499 True b_4141) (x_4499 False b_4141);
        F_4508 f_4503 k_4504 r0_4505 x0_4506 x1_4507 b_4147  = x0_4506 (F_4519 f_4503 k_4504 r0_4505 x1_4507 r0_4505 b_4147) (F_4519 f_4503 k_4504 r0_4505 x1_4507 False b_4147);
        F_4511 f_4509 k_4510 b_4149 x_4792  = K_fold_right_3 (F_3966 f_4509) k_4510 b_4149 x_4792;
        F_4514 f_4512 k_4513 b_4151 x_4790  = b_4151 (F_4511 f_4512 k_4513 True x_4790) (br (F_4511 f_4512 k_4513 True x_4790) (F_4511 f_4512 k_4513 False x_4790));
        F_4519 f_4515 k_4516 r0_4517 x1_4518 b_4153 x_4789  = b_4153 (F_4514 f_4515 k_4516 True x_4789) (x1_4518 (F_4514 f_4515 k_4516 r0_4517 x_4789) (F_4514 f_4515 k_4516 False x_4789));
        F_4534 f_4529 k_4530 r0_4531 x0_4532 x1_4533 b_4159  = b_4159 (F_4508 f_4529 k_4530 r0_4531 x0_4532 x1_4533 True) (br (F_4508 f_4529 k_4530 r0_4531 x0_4532 x1_4533 True) (F_4508 f_4529 k_4530 r0_4531 x0_4532 x1_4533 False));
        F_4540 f_4535 k_4536 r0_4537 x0_4538 x1_4539 b_4161  = b_4161 (F_4534 f_4535 k_4536 r0_4537 x0_4538 x1_4539 True) (x1_4539 (F_4534 f_4535 k_4536 r0_4537 x0_4538 x1_4539 r0_4537) (F_4534 f_4535 k_4536 r0_4537 x0_4538 x1_4539 False));
        F_4553 f_4547 k_4548 r0_4549 r2_4550 x0_4551 x1_4552 b_4163  = b_4163 (F_4540 f_4547 k_4548 r0_4549 x0_4551 x1_4552 True) (x1_4552 (F_4540 f_4547 k_4548 r0_4549 x0_4551 x1_4552 r2_4550) (F_4540 f_4547 k_4548 r0_4549 x0_4551 x1_4552 False));
        F_4567 f_4561 k_4562 r0_4563 r2_4564 x0_4565 x1_4566 b_4165  = b_4165 (F_4553 f_4561 k_4562 r0_4563 r2_4564 x0_4565 x1_4566 True) (x0_4565 (F_4553 f_4561 k_4562 r0_4563 r2_4564 x0_4565 x1_4566 r0_4563) (F_4553 f_4561 k_4562 r0_4563 r2_4564 x0_4565 x1_4566 False));
        F_4584 x_4582 x0_4583 b_4175  = x0_4583 (x_4582 True b_4175) (br (x_4582 True b_4175) (x_4582 False b_4175));
        F_4589 x_4587 x0_4588 b_4181  = b_4181 (F_4584 x_4587 x0_4588 True) (br (F_4584 x_4587 x0_4588 True) (F_4584 x_4587 x0_4588 False));
        F_4593 x_4590 x0_4591 x2_4592 b_4183  = b_4183 (F_4589 x_4590 x0_4591 True) (F_4589 x_4590 x0_4591 x2_4592);
        F_4599 x1_4594 x_4595 x0_4596 x1_4597 x2_4598 b_4185  = b_4185 (F_4593 x_4595 x0_4596 x2_4598 True) (x1_4597 (F_4593 x_4595 x0_4596 x2_4598 x1_4594) (F_4593 x_4595 x0_4596 x2_4598 False));
        F_4622 f_4619 x0_4620 x1_4621 b_4193 x_4812  = x0_4620 (F_4630 f_4619 x0_4620 True b_4193 x_4812) (F_4630 f_4619 x0_4620 x1_4621 b_4193 x_4812);
        F_4625 f_4623 x0_4624 b_4195 x_4798 x_4799  = x0_4624 (f_4623 True b_4195 x_4798 x_4799) (br (f_4623 True b_4195 x_4798 x_4799) (f_4623 False b_4195 x_4798 x_4799));
        F_4630 f_4628 x0_4629 b_4201 x_4806 x_4807  = b_4201 (F_4625 f_4628 x0_4629 True x_4806 x_4807) (br (F_4625 f_4628 x0_4629 True x_4806 x_4807) (F_4625 f_4628 x0_4629 False x_4806 x_4807));
        F_4638 f_4635 x0_4636 x1_4637 b_4205 x_4796  = b_4205 (F_4622 f_4635 x0_4636 x1_4637 True x_4796) (br (F_4622 f_4635 x0_4636 x1_4637 True x_4796) (F_4622 f_4635 x0_4636 x1_4637 False x_4796));
        F_4642 f_4639 x0_4640 x1_4641 b_4207 x_4795  = b_4207 (F_4638 f_4639 x0_4640 x1_4641 True x_4795) (F_4638 f_4639 x0_4640 x1_4641 x1_4641 x_4795);
        F_4645 r1_4643 r2_4644 b_4209 x_4821  = r1_4643 (F_4647 r2_4644 False b_4209 x_4821) (br (F_4647 r2_4644 True b_4209 x_4821) (F_4647 r2_4644 False b_4209 x_4821));
        F_4647 r2_4646 b_4211 x_4815 x_4816  = Fold_right Add r2_4646 b_4211 x_4815 x_4816;
        F_4652 r1_4650 r2_4651 b_4215 x_4813  = b_4215 (F_4645 r1_4650 r2_4651 True x_4813) (br (F_4645 r1_4650 r2_4651 True x_4813) (F_4645 r1_4650 r2_4651 False x_4813));
        F_4654 k_4653 b_4217  = b_4217 Bottom0 (l1 (Fail True k_4653));
        F_4659 k_4658 b_4221 x_4823  = F_4652 b_4221 x_4823 b_4221 (K_main_5 k_4658);
        F_4666 x_4665 b_4227  = br (x_4665 True b_4227) (x_4665 False b_4227);
        F_4669 k_4667 x0_4668 b_4231 x_4827 x_4828  = x0_4668 (F_4671 k_4667 True b_4231 x_4827 x_4828) (br (F_4671 k_4667 True b_4231 x_4827 x_4828) (F_4671 k_4667 False b_4231 x_4827 x_4828));
        F_4671 k_4670 b_4233 x_4835 x_4836 x_4837  = K_cons_make_list (F_3982 k_4670) b_4233 x_4835 x_4836 x_4837;
        F_4677 k_4674 x0_4675 x2_4676 b_4237 x_4838 x_4839  = b_4237 (F_4669 k_4674 x0_4675 True x_4838 x_4839) (x2_4676 (F_4669 k_4674 x0_4675 False x_4838 x_4839) (br (F_4669 k_4674 x0_4675 True x_4838 x_4839) (F_4669 k_4674 x0_4675 False x_4838 x_4839)));
        F_4683 k_4681 x_4682 b_4241 x_4845  = x_4682 (F_4685 k_4681 False b_4241 x_4845) (br (F_4685 k_4681 True b_4241 x_4845) (F_4685 k_4681 False b_4241 x_4845));
        F_4685 k_4684 b_4243 x_4850 x_4851  = k_4684 True b_4243 x_4850 x_4851;
        F_4692 x_4691 b_4253  = b_4253 (x_4691 True) (br (x_4691 True) (x_4691 False));
        F_4922 f_4919 x0_4920 x1_4921 x_4774  = br (F_4491 f_4919 x0_4920 x1_4921 True x_4774) (F_4491 f_4919 x0_4920 x1_4921 False x_4774);
        F_make_list k  = Bottom0;
        F_xs_prime_ xs2 k  = xs2 k;
        Fail b k  = event_fail k;
        Fold_right f xs2 acc0_ acc1_ k  = br (acc0_ Bottom0 (l1 (acc1_ (F_4464 f xs2 acc1_ True k) (br (F_4464 f xs2 acc1_ True k) (F_4464 f xs2 acc1_ False k))))) (acc1_ Bottom0 (l0 (k False True)));
        K_cons_make_list k r10_ r11_ r12_ r2  = F_4413 k r11_ (F_3942 r10_ r12_ r2);
        K_fold_right_2 f k xs2 r0_10 r1_10 r2_10  = F_4483 f xs2 r1_10 r1_10 (F_3963 f k r0_10 r2_10);
        K_fold_right_3 f k r0_11 r1_11  = f r0_11 r1_11 k;
        K_main_5 k r0_ r1_  = br (r1_ (F_4654 k True) (F_4654 k r0_)) (l0 k);
        K_main_6  = unit dummy;
        K_main_7  = K_main_8;
        K_main_8  = Make_list (F_3974 K_main_6);
        Make_list k  = br (l1 (Make_list (F_3979 k))) (l0 (k False True False F_3976));
        True x y  = x;
        False x y  = y;
        
        state fail q0 q1 ;
        initst q0;
        failst fail; 
        q0 -> l1  q0;
        q0 -> l0  q0;
        q0 -> event_fail  q1;
        q0 -> br  q0 q0;
        q1 -> unit  fail;
        q1 -> l1  fail;
        q1 -> l0  fail;
        q1 -> event_fail  fail;
        q1 -> br  fail fail;
    """)

    property("checkpairs") = doPosTest("""
        %BEGING
        S_SSSSS12  -> SSSSS12 c_bot_o c_error_o c_x65.
        KK_c_bot_bool c_bot_bool c_error_bool c_false c_true  -> c_bot_bool.
        KK_c_error_bool c_bot_bool c_error_bool c_false c_true  -> c_error_bool.
        KK_c_false c_bot_bool c_error_bool c_false c_true  -> c_false.
        KK_c_true c_bot_bool c_error_bool c_false c_true  -> c_true.
        KK_c_bot_nat c_bot_nat c_error_nat c_succ_x43 c_z  -> c_bot_nat.
        KK_c_error_nat c_bot_nat c_error_nat c_succ_x43 c_z  -> c_error_nat.
        KK_c_succ_x43 c_bot_nat c_error_nat c_succ_x43 c_z  -> c_succ_x43.
        KK_c_z c_bot_nat c_error_nat c_succ_x43 c_z  -> c_z.
        KK_c_bot_natpair c_bot_natpair c_error_natpair c_pair_x45_y47  -> c_bot_natpair.
        KK_c_error_natpair c_bot_natpair c_error_natpair c_pair_x45_y47  -> c_error_natpair.
        KK_c_pair_x45_y47 c_bot_natpair c_error_natpair c_pair_x45_y47  -> c_pair_x45_y47.
        KK_c_bot_o c_bot_o c_error_o c_x65  -> c_bot_o.
        KK_c_error_o c_bot_o c_error_o c_x65  -> c_error_o.
        KK_c_x65 c_bot_o c_error_o c_x65  -> c_x65.
        SSSSS12 c_bot_o c_error_o c_x65  -> br c_bot_o (SSSSS c_bot_o c_error_o c_x65).
        K_ok c_bot_o c_error_o c_x65  -> c_x65.
        K_false c_bot_bool c_error_bool c_false c_true  -> c_false.
        K_true c_bot_bool c_error_bool c_false c_true  -> c_true.
        K_pair x0 x1 c_bot_natpair c_error_natpair c_pair_x45_y47  -> x0 (x1 c_pair_x45_y47 c_error_natpair c_pair_x45_y47 c_pair_x45_y47) (x1 c_error_natpair c_error_natpair c_error_natpair c_error_natpair) (x1 c_pair_x45_y47 c_error_natpair c_pair_x45_y47 c_pair_x45_y47) (x1 c_pair_x45_y47 c_error_natpair c_pair_x45_y47 c_pair_x45_y47).
        K_succ x0 c_bot_nat c_error_nat c_succ_x43 c_z  -> x0 c_succ_x43 c_error_nat c_succ_x43 c_succ_x43.
        K_z c_bot_nat c_error_nat c_succ_x43 c_z  -> c_z.
        K_error_bool c_bot_bool c_error_bool c_false c_true  -> c_error_bool.
        K_error_nat c_bot_nat c_error_nat c_succ_x43 c_z  -> c_error_nat.
        K_error_natpair c_bot_natpair c_error_natpair c_pair_x45_y47  -> c_error_natpair.
        K_error_o c_bot_o c_error_o c_x65  -> c_error_o.
        SSSSS c_bot_o c_error_o c_x65  -> Main1 S5 c_bot_o c_error_o c_x65.
        Y47 c_bot_nat c_error_nat c_succ_x43 c_z  -> N7 c_bot_nat c_error_nat c_succ_x43 c_z.
        X45 c_bot_nat c_error_nat c_succ_x43 c_z  -> N7 c_bot_nat c_error_nat c_succ_x43 c_z.
        Q39 c_bot_nat c_error_nat c_succ_x43 c_z  -> N7 c_bot_nat c_error_nat c_succ_x43 c_z.
        P37 c_bot_nat c_error_nat c_succ_x43 c_z  -> N7 c_bot_nat c_error_nat c_succ_x43 c_z.
        N c_bot_nat c_error_nat c_succ_x43 c_z  -> br (K_z c_bot_nat c_error_nat c_succ_x43 c_z) (K_succ N7 c_bot_nat c_error_nat c_succ_x43 c_z).
        PairN c_bot_natpair c_error_natpair c_pair_x45_y47  -> K_pair N7 N7 c_bot_natpair c_error_natpair c_pair_x45_y47.
        S c_bot_natpair c_error_natpair c_pair_x45_y47  -> PairN6 c_bot_natpair c_error_natpair c_pair_x45_y47.
        If x0 x1 x2 c_bot_o c_error_o c_x65  -> x0 c_bot_o c_error_o (x2 c_bot_o c_error_o c_x65) (x1 c_bot_o c_error_o c_x65).
        Lt x0 x1 c_bot_bool c_error_bool c_false c_true  -> x0 (x1 c_bot_bool c_error_bool c_bot_bool c_bot_bool) (x1 c_error_bool c_error_bool c_error_bool c_error_bool) (x1 c_bot_bool c_error_bool (Lt3 P378 Q399 c_bot_bool c_error_bool c_false c_true) (K_false c_bot_bool c_error_bool c_false c_true)) (x1 c_bot_bool c_error_bool (K_true c_bot_bool c_error_bool c_false c_true) (K_false c_bot_bool c_error_bool c_false c_true)).
        Check x0 c_bot_o c_error_o c_x65  -> x0 c_bot_o c_error_o (If4 (Lt3 X4510 Y4711) K_ok K_error_o c_bot_o c_error_o c_x65).
        Main x0 c_bot_o c_error_o c_x65  -> Check2 x0 c_bot_o c_error_o c_x65.
        Y4711 c_bot_nat c_error_nat c_succ_x43 c_z  -> br c_bot_nat (Y47 c_bot_nat c_error_nat c_succ_x43 c_z).
        X4510 c_bot_nat c_error_nat c_succ_x43 c_z  -> br c_bot_nat (X45 c_bot_nat c_error_nat c_succ_x43 c_z).
        Q399 c_bot_nat c_error_nat c_succ_x43 c_z  -> br c_bot_nat (Q39 c_bot_nat c_error_nat c_succ_x43 c_z).
        P378 c_bot_nat c_error_nat c_succ_x43 c_z  -> br c_bot_nat (P37 c_bot_nat c_error_nat c_succ_x43 c_z).
        N7 c_bot_nat c_error_nat c_succ_x43 c_z  -> br c_bot_nat (N c_bot_nat c_error_nat c_succ_x43 c_z).
        PairN6 c_bot_natpair c_error_natpair c_pair_x45_y47  -> br c_bot_natpair (PairN c_bot_natpair c_error_natpair c_pair_x45_y47).
        S5 c_bot_natpair c_error_natpair c_pair_x45_y47  -> br c_bot_natpair (S c_bot_natpair c_error_natpair c_pair_x45_y47).
        If4 x0 x1 x2 c_bot_o c_error_o c_x65  -> br c_bot_o (If x0 x1 x2 c_bot_o c_error_o c_x65).
        Lt3 x0 x1 c_bot_bool c_error_bool c_false c_true  -> br c_bot_bool (Lt x0 x1 c_bot_bool c_error_bool c_false c_true).
        Check2 x0 c_bot_o c_error_o c_x65  -> br c_bot_o (Check x0 c_bot_o c_error_o c_x65).
        Main1 x0 c_bot_o c_error_o c_x65  -> br c_bot_o (Main x0 c_bot_o c_error_o c_x65).
        %ENDG
        %BEGINA
        q c_x65 -> .
        q c_bot_o -> .
        q br -> q q .
        %ENDA
    """, """
        nont S_SSSSS12 : 0;
        nont KK_c_bot_bool : 0 => 0 => 0 => 0 => 0;
        nont KK_c_error_bool : 0 => 0 => 0 => 0 => 0;
        nont KK_c_false : 0 => 0 => 0 => 0 => 0;
        nont KK_c_true : 0 => 0 => 0 => 0 => 0;
        nont KK_c_bot_nat : 0 => 0 => 0 => 0 => 0;
        nont KK_c_error_nat : 0 => 0 => 0 => 0 => 0;
        nont KK_c_succ_x43 : 0 => 0 => 0 => 0 => 0;
        nont KK_c_z : 0 => 0 => 0 => 0 => 0;
        nont KK_c_bot_natpair : 0 => 0 => 0 => 0;
        nont KK_c_error_natpair : 0 => 0 => 0 => 0;
        nont KK_c_pair_x45_y47 : 0 => 0 => 0 => 0;
        nont KK_c_bot_o : 0 => 0 => 0 => 0;
        nont KK_c_error_o : 0 => 0 => 0 => 0;
        nont KK_c_x65 : 0 => 0 => 0 => 0;
        nont SSSSS12 : 0 => 0 => 0 => 0;
        nont K_ok : 0 => 0 => 0 => 0;
        nont K_false : 0 => 0 => 0 => 0 => 0;
        nont K_true : 0 => 0 => 0 => 0 => 0;
        nont K_pair : (0 => 0 => 0 => 0 => 0) => (0 => 0 => 0 => 0 => 0) => 0 => 0 => 0 => 0;
        nont K_succ : (0 => 0 => 0 => 0 => 0) => 0 => 0 => 0 => 0 => 0;
        nont K_z : 0 => 0 => 0 => 0 => 0;
        nont K_error_bool : 0 => 0 => 0 => 0 => 0;
        nont K_error_nat : 0 => 0 => 0 => 0 => 0;
        nont K_error_natpair : 0 => 0 => 0 => 0;
        nont K_error_o : 0 => 0 => 0 => 0;
        nont SSSSS : 0 => 0 => 0 => 0;
        nont Y47 : 0 => 0 => 0 => 0 => 0;
        nont X45 : 0 => 0 => 0 => 0 => 0;
        nont Q39 : 0 => 0 => 0 => 0 => 0;
        nont P37 : 0 => 0 => 0 => 0 => 0;
        nont N : 0 => 0 => 0 => 0 => 0;
        nont PairN : 0 => 0 => 0 => 0;
        nont S : 0 => 0 => 0 => 0;
        nont If : (0 => 0 => 0 => 0 => 0) => (0 => 0 => 0 => 0) => (0 => 0 => 0 => 0) => 0 => 0 => 0 => 0;
        nont Lt : (0 => 0 => 0 => 0 => 0) => (0 => 0 => 0 => 0 => 0) => 0 => 0 => 0 => 0 => 0;
        nont Check : (0 => 0 => 0 => 0) => 0 => 0 => 0 => 0;
        nont Main : (0 => 0 => 0 => 0) => 0 => 0 => 0 => 0;
        nont Y4711 : 0 => 0 => 0 => 0 => 0;
        nont X4510 : 0 => 0 => 0 => 0 => 0;
        nont Q399 : 0 => 0 => 0 => 0 => 0;
        nont P378 : 0 => 0 => 0 => 0 => 0;
        nont N7 : 0 => 0 => 0 => 0 => 0;
        nont PairN6 : 0 => 0 => 0 => 0;
        nont S5 : 0 => 0 => 0 => 0;
        nont If4 : (0 => 0 => 0 => 0 => 0) => (0 => 0 => 0 => 0) => (0 => 0 => 0 => 0) => 0 => 0 => 0 => 0;
        nont Lt3 : (0 => 0 => 0 => 0 => 0) => (0 => 0 => 0 => 0 => 0) => 0 => 0 => 0 => 0 => 0;
        nont Check2 : (0 => 0 => 0 => 0) => 0 => 0 => 0 => 0;
        nont Main1 : (0 => 0 => 0 => 0) => 0 => 0 => 0 => 0;
        
        
        tml dummy : 0;
        tml br : 2;
        tml c_bot_o : 1;
        tml c_error_o : 1;
        tml c_x65 : 1;
        
        state q fail;
        
        initnt S_SSSSS12;
        initst q;
        failst fail;
        
        
        S_SSSSS12  = SSSSS12 (c_bot_o dummy) (c_error_o dummy) (c_x65 dummy);
        KK_c_bot_bool c_bot_bool c_error_bool c_false c_true  = c_bot_bool;
        KK_c_error_bool c_bot_bool c_error_bool c_false c_true  = c_error_bool;
        KK_c_false c_bot_bool c_error_bool c_false c_true  = c_false;
        KK_c_true c_bot_bool c_error_bool c_false c_true  = c_true;
        KK_c_bot_nat c_bot_nat c_error_nat c_succ_x43 c_z  = c_bot_nat;
        KK_c_error_nat c_bot_nat c_error_nat c_succ_x43 c_z  = c_error_nat;
        KK_c_succ_x43 c_bot_nat c_error_nat c_succ_x43 c_z  = c_succ_x43;
        KK_c_z c_bot_nat c_error_nat c_succ_x43 c_z  = c_z;
        KK_c_bot_natpair c_bot_natpair c_error_natpair c_pair_x45_y47  = c_bot_natpair;
        KK_c_error_natpair c_bot_natpair c_error_natpair c_pair_x45_y47  = c_error_natpair;
        KK_c_pair_x45_y47 c_bot_natpair c_error_natpair c_pair_x45_y47  = c_pair_x45_y47;
        KK_c_bot_o c_bot_o c_error_o c_x65  = c_bot_o;
        KK_c_error_o c_bot_o c_error_o c_x65  = c_error_o;
        KK_c_x65 c_bot_o c_error_o c_x65  = c_x65;
        SSSSS12 c_bot_o c_error_o c_x65  = br c_bot_o (SSSSS c_bot_o c_error_o c_x65);
        K_ok c_bot_o c_error_o c_x65  = c_x65;
        K_false c_bot_bool c_error_bool c_false c_true  = c_false;
        K_true c_bot_bool c_error_bool c_false c_true  = c_true;
        K_pair x0 x1 c_bot_natpair c_error_natpair c_pair_x45_y47  = x0 (x1 c_pair_x45_y47 c_error_natpair c_pair_x45_y47 c_pair_x45_y47) (x1 c_error_natpair c_error_natpair c_error_natpair c_error_natpair) (x1 c_pair_x45_y47 c_error_natpair c_pair_x45_y47 c_pair_x45_y47) (x1 c_pair_x45_y47 c_error_natpair c_pair_x45_y47 c_pair_x45_y47);
        K_succ x0 c_bot_nat c_error_nat c_succ_x43 c_z  = x0 c_succ_x43 c_error_nat c_succ_x43 c_succ_x43;
        K_z c_bot_nat c_error_nat c_succ_x43 c_z  = c_z;
        K_error_bool c_bot_bool c_error_bool c_false c_true  = c_error_bool;
        K_error_nat c_bot_nat c_error_nat c_succ_x43 c_z  = c_error_nat;
        K_error_natpair c_bot_natpair c_error_natpair c_pair_x45_y47  = c_error_natpair;
        K_error_o c_bot_o c_error_o c_x65  = c_error_o;
        SSSSS c_bot_o c_error_o c_x65  = Main1 S5 c_bot_o c_error_o c_x65;
        Y47 c_bot_nat c_error_nat c_succ_x43 c_z  = N7 c_bot_nat c_error_nat c_succ_x43 c_z;
        X45 c_bot_nat c_error_nat c_succ_x43 c_z  = N7 c_bot_nat c_error_nat c_succ_x43 c_z;
        Q39 c_bot_nat c_error_nat c_succ_x43 c_z  = N7 c_bot_nat c_error_nat c_succ_x43 c_z;
        P37 c_bot_nat c_error_nat c_succ_x43 c_z  = N7 c_bot_nat c_error_nat c_succ_x43 c_z;
        N c_bot_nat c_error_nat c_succ_x43 c_z  = br (K_z c_bot_nat c_error_nat c_succ_x43 c_z) (K_succ N7 c_bot_nat c_error_nat c_succ_x43 c_z);
        PairN c_bot_natpair c_error_natpair c_pair_x45_y47  = K_pair N7 N7 c_bot_natpair c_error_natpair c_pair_x45_y47;
        S c_bot_natpair c_error_natpair c_pair_x45_y47  = PairN6 c_bot_natpair c_error_natpair c_pair_x45_y47;
        If x0 x1 x2 c_bot_o c_error_o c_x65  = x0 c_bot_o c_error_o (x2 c_bot_o c_error_o c_x65) (x1 c_bot_o c_error_o c_x65);
        Lt x0 x1 c_bot_bool c_error_bool c_false c_true  = x0 (x1 c_bot_bool c_error_bool c_bot_bool c_bot_bool) (x1 c_error_bool c_error_bool c_error_bool c_error_bool) (x1 c_bot_bool c_error_bool (Lt3 P378 Q399 c_bot_bool c_error_bool c_false c_true) (K_false c_bot_bool c_error_bool c_false c_true)) (x1 c_bot_bool c_error_bool (K_true c_bot_bool c_error_bool c_false c_true) (K_false c_bot_bool c_error_bool c_false c_true));
        Check x0 c_bot_o c_error_o c_x65  = x0 c_bot_o c_error_o (If4 (Lt3 X4510 Y4711) K_ok K_error_o c_bot_o c_error_o c_x65);
        Main x0 c_bot_o c_error_o c_x65  = Check2 x0 c_bot_o c_error_o c_x65;
        Y4711 c_bot_nat c_error_nat c_succ_x43 c_z  = br c_bot_nat (Y47 c_bot_nat c_error_nat c_succ_x43 c_z);
        X4510 c_bot_nat c_error_nat c_succ_x43 c_z  = br c_bot_nat (X45 c_bot_nat c_error_nat c_succ_x43 c_z);
        Q399 c_bot_nat c_error_nat c_succ_x43 c_z  = br c_bot_nat (Q39 c_bot_nat c_error_nat c_succ_x43 c_z);
        P378 c_bot_nat c_error_nat c_succ_x43 c_z  = br c_bot_nat (P37 c_bot_nat c_error_nat c_succ_x43 c_z);
        N7 c_bot_nat c_error_nat c_succ_x43 c_z  = br c_bot_nat (N c_bot_nat c_error_nat c_succ_x43 c_z);
        PairN6 c_bot_natpair c_error_natpair c_pair_x45_y47  = br c_bot_natpair (PairN c_bot_natpair c_error_natpair c_pair_x45_y47);
        S5 c_bot_natpair c_error_natpair c_pair_x45_y47  = br c_bot_natpair (S c_bot_natpair c_error_natpair c_pair_x45_y47);
        If4 x0 x1 x2 c_bot_o c_error_o c_x65  = br c_bot_o (If x0 x1 x2 c_bot_o c_error_o c_x65);
        Lt3 x0 x1 c_bot_bool c_error_bool c_false c_true  = br c_bot_bool (Lt x0 x1 c_bot_bool c_error_bool c_false c_true);
        Check2 x0 c_bot_o c_error_o c_x65  = br c_bot_o (Check x0 c_bot_o c_error_o c_x65);
        Main1 x0 c_bot_o c_error_o c_x65  = br c_bot_o (Main x0 c_bot_o c_error_o c_x65);
        
        q -> br q q;
        q -> c_error_o fail;
    """)



    // Some horsc examples
    property("example.horsc") = doPosTest("""
        domain B tt ff.
        domain N n1 n2 n3.

        %BEGING
        S -> case(F, good, bad).
        F -> case(G tt, ff, tt, tt).
        G x -> case(x, n1, n2).
        %ENDG

        %BEGINA
        q0 good -> .
        %ENDA
    """, """
        domain B tt ff;
        domain N n1 n2 n3;

        nont S : 0;
        nont F : B;
        nont G : B => N;

        tml bad : 1;
        tml good : 1;
        tml dummy : 0;

        initnt S;

        S = case(F, good(dummy), bad(dummy));
        F = case(G tt, ff, tt, tt);
        G x = case(x, n1, n2);

        states q0 fail;
        initst q0;
        failst fail;

        q0 -> bad fail;
    """)


    property("example.horsc with some type hiding") = doPosTest("""
        domain B tt ff.
        domain N n1 n2 n3.

        %BEGING
        S -> case(F, good, bad).
        F -> case(G tt n1 n2, ff, tt, tt).
        G x y z -> case(x, y, z).
        %ENDG

        %BEGINA
        q0 good -> .
        %ENDA
    """, """
        domain B tt ff;
        domain N n1 n2 n3;

        nont S : 0;
        nont F : B;
        nont G : B => N => N => N;

        tml bad : 1;
        tml good : 1;
        tml dummy : 0;

        initnt S;

        S = case(F, good(dummy), bad(dummy));
        F = case(G tt n1 n2, ff, tt, tt);
        G x y z = case(x, y, z);

        states q0 fail;
        initst q0;
        failst fail;

        q0 -> bad fail;
    """)


    property("example.horsc with some type hiding and neg") = doNegTest("""
        domain B tt ff.
        domain N n1 n2 n3.

        %BEGING
        S -> case(F, good, bad).
        F -> case(G tt n2 n1, ff, tt, tt).
        G x y z -> case(x, y, z).
        %ENDG

        %BEGINA
        q0 good -> .
        %ENDA
    """, """
        domain B tt ff;
        domain N n1 n2 n3;

        nont S : 0;
        nont F : B;
        nont G : B => N => N => N;

        tml bad : 1;
        tml good : 1;
        tml dummy : 0;

        initnt S;

        S = case(F, good(dummy), bad(dummy));
        F = case(G tt n1 n2, ff, tt, tt);
        G x y z = case(x, y, z);

        states q0 fail;
        initst q0;
        failst fail;

        q0 -> bad fail;
    """)


    property("example.horsc with more type hiding") = doPosTest("""
        domain B tt ff.
        domain N n1 n2 n3.

        %BEGING
        S -> case(F, good, bad).
        F -> case(G tt (H n1) (H n2), ff, tt, tt).
        G x y z -> case(x, y, z).
        H x -> x.
        %ENDG

        %BEGINA
        q0 good -> .
        %ENDA
    """, """
        domain B tt ff;
        domain N n1 n2 n3;

        nont S : 0;
        nont F : B;
        nont G : B => N => N => N;
        nont H : N => N;

        tml bad : 1;
        tml good : 1;
        tml dummy : 0;

        initnt S;

        S = case(F, good(dummy), bad(dummy));
        F = case(G tt (H n1) (H n2), ff, tt, tt);
        G x y z = case(x, y, z);
        H x = x;

        states q0 fail;
        initst q0;
        failst fail;

        q0 -> bad fail;
    """)

}
