/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



package ho.hors
import org.scalacheck.Properties
import org.scalacheck.Prop._
import ho.structures.cpds.Rule
import ho.structures.cpds.Cpds
import ho.structures.cpds.CpdsConfig
import ho.structures.cpds.CpdsAnnotatedRunTree
import ho.structures.cpds.RuleAnnotation
import ho.structures.cpds.CARTreeNode
import ho.util.LazyWrapper
import ho.structures.cpds.SCharacterTerm
import ho.structures.cpds.ControlStateHors
import scala.collection.JavaConversions._
import scala.collection.immutable.HashSet
import ho.managers.Managers
import ho.structures.cpds.CpdsEvaluator


object HorsToCPDSSpecification 
	extends Properties("HorsToCpds") with HorsGenerator {
	
	val depth = 6 //depth to which Hors EvalTrees should be explored for equality.
	//WARNING : The 
		property("Correct Hors to CPDS Translation") 
			= forAll { (hors : Hors) =>
			  {
			    try{
			    val cpds = new (ho.structures.cpds.HorsToCPDS).translate(hors)
			    val results = for(propState <- hors.initPropAutStates; nonTerm <- hors.initNonTerms)
			    yield {
			      Managers.saManager.setOrder(hors.getOrder)
			      
			      val initState = Managers.csManager.makeControl(propState, 0, false) 
			      val initChar = Managers.scManager.makeCharacter(nonTerm)
			      val cpdsTree = CpdsEvaluator(cpds, initState, initChar)
			      // println("Done with CpdsEvaluator, CPDS tree is:")
			      // println(cpdsTreeString(cpdsTree, depth * 5))
			      
			      val evalTreeFromCpds = HorsTreeFromCpdsTree(cpdsTree)
			      val evalTreeFromHors = HorsEvaluator(hors, propState, nonTerm)
			      
			      if(evalTreeEq(evalTreeFromCpds, evalTreeFromHors, depth))
			        true
			      else {
			        println("Test Failure with hors: " + hors.toString)
			        println("The original hors generated hors Tree: \n " + evalTreeString(evalTreeFromHors, depth))
			        println("But the cpds induced a hors Tree: \n" + evalTreeString(evalTreeFromCpds, depth))
			        println("Originating from Cpds Run Tree: \n" + cpdsTreeString(cpdsTree, depth * 2))
			        false
			      }
			    	}
			    	results.fold(true)(_ && _)
			    }
			    catch{case e : Throwable => {e.printStackTrace; false}} //Hack because sbt doesn't give full stack trace.

			    
			    
			   
			    
			  }
		}


	// Print EvalTree to given depth
	private def evalTreeString(evt : EvalTree, depth : Int) : String = {
	  if(depth == 0) 
	    "<* ... *>"
	  else {
		  evt match {
		    case NonDetNode(state, term, left, right) 
		    	=> "NonDetNode(\n" + state.toString + ", " + term.toString + "| " +
		    			"\n" + (" " * depth) + evalTreeString(left, depth - 1) + "\n" + (" " * depth)  + 
		    			evalTreeString(right, depth - 1) + ")\n"
		    case NonTermNode(state, term, child)
		    	=> "NonTermNode(\n" + state.toString + ", " + term.toString + "| " +
		    			 "\n" + (" " * depth) + evalTreeString(child, depth - 1) + ") \n"
		    case TerminalNode(state, term, children)
		    	=> "TerminalNode(" + state.toString + ", " + term.toString + "| " +
		    			 children.map(evalTreeString(_, depth -1)).mkString("\n" + " " * depth + ")") + "\n"
		    case DeadEnd() => "<* DeadEnd *>"
		    case Error(state, term) => "Error(" + state.toString + term.toString + ")"
		  }
	  }
	}
	
	//Print CpdsTree to given depth
	private def cpdsTreeString(cpdsTree : CpdsAnnotatedRunTree, depth : Int) : String= {
	  if (depth == 0)
	    "<* ... *>"
	  else
	    cpdsTree match {
	    case CARTreeNode(config, children) 
	    	=> "( + " + config.toString + " | " + children.map({
	    	  		case (an, child) => "<" + an.toString + ", " + cpdsTreeString(child, depth-1) + " >"}
	    					).mkString("\n" + " " * depth) + " )"
	  }
	}
	
	//Test ev1 and ev2 for equlaity up to given depth. 
	private def evalTreeEq(et1 : EvalTree, et2 : EvalTree, depth : Int) : Boolean = {
	  if (depth <= 0)
	    true
	  else
	    (et1, et2) match {
	    case (NonDetNode(state1, term1, left1, right1), NonDetNode(state2, term2, left2, right2)) 
	    	=> state1 == state2 && term1 == term2 &&
	    			evalTreeEq(left1, left2, depth -1) && evalTreeEq(right1, right2, depth -1)
	    case (NonTermNode(state1, term1, child1), NonTermNode(state2, term2, child2)) 
	    	=> state1 == state2 && term1 == term2 &&
	    			evalTreeEq(child1, child2, depth -1)
	    case (TerminalNode(state1, term1, Nil), TerminalNode(state2, term2, Nil)) 
	    	=> state1 == state2 && term1 == term2 
	    case (TerminalNode(state1, term1, child1 :: children1), TerminalNode(state2, term2, child2 :: children2))
	    	=> evalTreeEq(TerminalNode(state1, term1, children1), TerminalNode(state2, term2, children2), depth) &&
	    			evalTreeEq(child1, child2, depth -1 )
	    case (DeadEnd(), DeadEnd()) => true
	    case (Error(state1, term1), Error(state2, term2)) => state1 == state2 && term1 == term2 
	    case _ => false
	  }
	      
	}
		
}

