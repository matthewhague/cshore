/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.hors

import java.lang.Exception
import ho.parsers.HorsParser
import java.io.StringReader
import org.scalacheck.Properties
import org.scalacheck.Prop._
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Gen





/**
 * "Generates" Hors (although they will be specified fixed in the first instance, 
 * will try to make random later on.
 */
trait HorsGenerator {

    // NOTE: removed non-det operator: we don't use it and it doesn't work since
    // we changed the parser to read simple terms first
	val listOfHorsStringDescriptions = List (
	    """ 
			nont F : (0 => 0) => 0;
			nont G : 0 => 0;
			
			
			tml g : 0 => 0 => 0;
			
			tml a : 0;
            tml br : 2;
            tml brr : 4;
			
			F phi = br (br (F(g a)) (F( phi  ))) (g (F G) a);
			
			G x = g (F G) x; 
			
			nont H : 0 => 2 => 0 => 0;
			nont S : 0;
			
			
			S = H a br (F (g a));
			
			H x phi y = H x phi x;
			
			initnt S;
			
			failnt G;
			
			
			
			state q1 q2 q3; 
			
			q1 -> g q2 q3;
			
			q2 -> g q1 q3;
			
			q3 -> g q2 q2 ;
			 // q3 -> g q2 q1; 
			 
			failst q3 q2;
			
			initst q1 q3; 
		""",
		
		"""
		nont F : (0 => 0) => 0;
		nont S : 0;
		F phi = phi (F phi);
		tml f : 0 => 0;
		S = F f;
		initnt S;

		state q1;
		initst q1;
		q1 -> f q1;

		""" // This one has a higher-order variable call
		,
		
		// Kobayashi's example (POPL '09, ported from TReCS example suite):
	"""	
nont S : 0;
nont C1 : (((0 => 0) => 0 => 0) => 0);
nont C2 : ((0 => 0) => 0 => 0) => (((0 => 0) => 0 => 0) => 0);
nont F : ((0 => 0) => 0 => 0) => ((0 => 0) => 0 => 0) => 0 => 0;
nont I : (0 => 0) => 0 => 0;
nont K : (0 => 0) => 0 => 0;
nont Newr : (((0 => 0) => 0 => 0) => 0) => 0;
nont Neww : (((0 => 0) => 0 => 0) => 0) => 0;
nont Close : ((0 => 0) => 0 => 0) => 0 => 0;
nont Read : ((0 => 0) => 0 => 0) => 0 => 0;
nont Write : ((0 => 0) => 0 => 0) => 0 => 0;

tml br : 2;
tml newr : 1;
tml read : 1;
tml close : 1;
tml neww : 1;
tml write : 1;
tml end : 0;

state q0 qr qw qc qrw fail;

initnt S;
initst q0;
failst fail;

S = Newr C1;
C1 x = Neww (C2 x);
C2 x y = F x y end;
F x y k = br (Close x (Close y k)) (Read x (Write y (F x y k)));
I x y = x y;
K x y = y;
Newr k = br (newr (k I)) (k K);
Neww k = br (neww (k I)) (k K);
Close x k = x close k;
Read x k = x read k;
Write y k = y write k;



q0 -> br q0 q0;
qr -> br  qr qr;
qw -> br  qw qw;
qc -> br fail fail;
qrw -> br  qrw qrw;

q0 -> newr  qr;
qr -> newr  fail;
qw -> newr  qrw;
qc -> newr  fail;
qrw -> newr  fail;
 
qr -> read  qr;
q0 -> read  fail;
qw -> read  fail;
qc -> read  fail;
qrw -> read  qrw;

qr -> close  qc;
q0 -> close  fail;
qw -> close  qc;
qc -> close  fail;
qrw -> close  qrw;

q0 -> neww  qw;
qr -> neww  qrw;
qw -> neww  fail;
qc -> neww  fail;
qrw -> neww  fail;

qw -> write  qw;
q0 -> write  fail;
qr -> write  fail;
qc -> write  fail;
qrw -> write  qrw;
"""
	) 
	
	
	val listOfHorsDescriptions = (for(des <- listOfHorsStringDescriptions)
										yield HorsParser(new StringReader(des))).map(
									{   case HorsParser.Success(hors, _) => hors 
										    case fail : HorsParser.Failure 
										    	=> throw new Exception(fail.toString)})
	
	implicit var arbHors : Arbitrary[Hors]
			= Arbitrary(Gen.oneOf(listOfHorsDescriptions.toSeq ))
}
