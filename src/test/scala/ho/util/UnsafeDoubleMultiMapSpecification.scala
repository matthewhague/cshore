/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util
import org.scalacheck.Properties
import org.scalacheck.Prop._
import scala.collection.JavaConversions._

// mapFact here expects function generating fresh map (as no reset method...although
// perhaps there should be...)
// As part of `unsafety' we should only request one iterator at at a time from an Unsafe_Map...
// (as could conceive of `efficient' implementation that reuses a single iterator?)
abstract class UnsafeDoubleMultiMapSpecification ( val mapFact : () => UnsafeDoubleMultiMap[Int, Int, Int],
                                          val smname : String )
	extends Properties("UnsafeDoubleMultiMap") {
			 
		 property(smname + ": multi element add") = 
		   forAll{( testMap : Map[(Int, Int), Set[Int]]) =>
		     val map = mapFact ()
		     for{ (source, targets) <- testMap; target <- targets }
		     		yield map.put(source._1, source._2, target)
		     
		     val notTooMuch = for{ ((source1, source2), targets) <- testMap; target2 <- map.get(source1, source2)}
		     					yield (targets.contains(target2))
		     					 
		 
		 	 val enough =   for{ ((source1, source2), targets) <- testMap; target1 <- targets}
		     					yield {	var temp = false
		     							for{next <- map.get(source1, source2)} temp = (temp || (target1 == next))
		     							temp
		     						} && //should replace with recursive function to make quicker
		     							//by breaking as soon as find match
		     							// would be nice to have let...in syntax...
		     						map.hasInImage(target1) //Also check membership of hasInImage
		     												//(negative check is below)
		     
		     ((notTooMuch./:\ (true)( _ && _ ) ) == true) :| "image of element in domain contains " +
		     		"no more than should" &&
		     //
		     ((enough./:\ (true)( _ && _ ) ) == true) :| "image of element in domain contains " +
		     		"at least what should" &&
		     //
		     forAll{( c : (Int, Int)) => 
		     !testMap.keys.contains(c) ==>
		       map.get(c._1, c._2).isEmpty} :| "non-membership of domain" &&
		     //
		     forAll{(d : Int) =>
		       {	
		    	   var temp = true
		    	   for { targets <- testMap.values; target <- targets } 
		    	   	yield(temp = temp && (target != d))
		    	   temp //should replace with recursive function to make quicker
		     							//by breaking as soon as find match
		     							// would be nice to have let...in syntax...
		       } ==> 
		     		!map.hasInImage(d) :| "non-membership via hasInImage"
		     
		       }
		 }
}
