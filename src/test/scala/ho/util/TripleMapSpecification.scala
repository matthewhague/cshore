/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */




import scala.collection._

import org.scalacheck.Properties
import org.scalacheck.Prop._

import ho.util.TripleMap

package ho.util {

    abstract class TripleMapSpecification(val smname : String) 
        extends Properties("TripleMap") {

        var map : TripleMap[Int,Int,Int,Int] = new TripleMap[Int,Int,Int,Int]

        property(smname + ": basic add") = 
            forAll{(a: Int, b: Int, c: Int, d: Int) => 
                map.put(a, b, c, d);
                map.get(a, b, c) == d;
            }

        property(smname + ": add twice/replace") = 
            forAll{(a: Int, b: Int, c: Int, d: Int, e: Int) => 
                map.put(a, b, c, d);
                map.put(a, b, c, e);
                map.get(a, b, c) == e;
            }

        property(smname + ": not in") = 
            forAll{(abs: Set[(Int,Int,Int)], a: Int, b: Int, c: Int, d: Int) => 
                (!(abs contains (a, b, c))) ==> {
                    map.clear()
                    for ((a1, b1, c1) <- abs) {
                        map.put(a1, b1, c1, d)
                    }
                    (map.get(a, b, c) == null)
                }
            }


    }
}
