/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



import scala.collection._

import org.scalacheck.Properties
import org.scalacheck.Prop._

import ho.util.DoubleMap

package ho.util {

    class DoubleMapSpecification()
        extends Properties("DoubleMap") {

        var map = new DoubleMap[Int,Int,Int]()

        property("basic add") = 
            forAll{(a: Int, b: Int, c: Int) => 
                map.put(a, b, c);
                map.get(a, b) == c;
            }

        property("add twice/replace") = 
            forAll{(a: Int, b: Int, c: Int, d: Int) => 
                map.put(a, b, c);
                map.put(a, b, d);
                map.get(a, b) == d;
            }

        property("not in") = 
            forAll{(abs: Set[(Int,Int)], a: Int, b: Int, c: Int) => 
                (!(abs contains (a, b))) ==> {
                    map.clear()
                    for ((a1, b1) <- abs) {
                        map.put(a1, b1, c)
                    }
                    (map.get(a, b) == null)
                }
            }


    }
}
