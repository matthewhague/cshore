/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util
import org.scalacheck.Properties
import org.scalacheck.Prop._
import scala.collection.JavaConversions._

abstract class UnsafeMultiMapToDoubleSpecification ( val mapFact : () => UnsafeMultiMapToDouble[Int, Int, Int],
									val smname : String )
	extends Properties("UnsafeMultiMapToDouble") {
			 
		 property(smname + ": multi element add") = 
		   forAll{( testMap : Map[Int, Set[(Int, Int)]]) =>
		     val map = mapFact ()
		     for{ (source, targets) <- testMap; (targeta, targetb) <- targets }
		     		yield map.put(source, targeta, targetb)
		     
		     val notTooMuch = for{ (source, targets) <- testMap; target2 <- map.get(source)}
		     					yield (targets.contains((target2.first, target2.second)))
		     					 
		 
		 	 val enough =   for{ (source, targets) <- testMap; (target1a, target1b) <- targets}
		     					yield {	var temp = false
		     							for{next <- map.get(source)} temp = (temp || (target1a == next.first &&
		     																			target1b == next.second))
		     							temp
		     						} && //should replace with recursive function to make quicker
		     							//by breaking as soon as find match
		     							// would be nice to have let...in syntax...
		     						map.hasInImage(target1a, target1b) //Also check membership of hasInImage
		     												//(negative check is below)
		     
		     ((notTooMuch./:\ (true)( _ && _ ) ) == true) :| "image of element in domain contains " +
		     		"no more than should" &&
		     //
		     ((enough./:\ (true)( _ && _ ) ) == true) :| "image of element in domain contains " +
		     		"at least what should" &&
		     //
		     forAll{(c : Int) => 
		     !testMap.keys.contains(c) ==>
		       map.get(c).isEmpty} :| "non-membership of domain" &&
		     //
		     forAll{( d : (Int, Int)) =>
		       {	
		    	   var temp = true
		    	   for { targets <- testMap.values; target <- targets } 
		    	   	yield(temp = temp && (target != d))
		    	   temp //should replace with recursive function to make quicker
		     							//by breaking as soon as find match
		     							// would be nice to have let...in syntax...
		       } ==> 
		     		!map.hasInImage(d._1, d._2) :| "non-membership via hasInImage"
		     
		       }
		 }
}
