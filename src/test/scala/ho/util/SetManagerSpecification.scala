/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



import java.util.HashSet
import java.util.ArrayList

import scala.collection._
import scala.collection.JavaConverters._

import scala.language.implicitConversions
import org.scalacheck.Properties
import org.scalacheck.Prop._

import org.scalacheck.util.Buildable
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Gen

import ho.util.SetManager
import ho.util.HashSetManager

package ho.util {

    abstract class SetManagerSpecification(var man : SetManager[Int], 
                                           val smname : String) 
        extends Properties("SetManager") {

        implicit def buildableHashSet[T] : Buildable[T,HashSet[T]] = new Buildable[T,HashSet[T]] {
            def builder = new mutable.Builder[T,HashSet[T]] {
                val al = new HashSet[T]
                def +=(x: T) = {
                    al.add(x)
                    this
                }
                def clear() = al.clear()
                def result() = al
            }
        }

        implicit def traverseHashSet[T](s : HashSet[T]) : Traversable[T] = s.asScala

        property(smname + ": makeSet -- equal") = 
            forAll{(a: HashSet[Int]) => 
                man.makeSet(a) == a
            }

        property(smname + ": makeSet -- empty") = {
                var a = new HashSet[Int]()
                var ua = man.makeSet(a)
                ua.isEmpty()
            }

        property(smname + ": makeSet -- singleton") = 
            forAll{(e : Int) =>
                var a = new HashSet[Int]()
                a.add(e)
                var ua1 = man.makeSet(a);
                var ua2 = man.makeSingleton(e);
                man.equal(ua1, ua2);
            }


        property(smname + ": makeSet/equals") = 
            forAll{(a: HashSet[Int]) =>
                var a2 = new HashSet[Int]()
                a2.addAll(a)
                var ua = man.makeSet(a)
                var ua2 = man.makeSet(a2)
                man.equal(ua, ua2)
            }

        property(smname + ": union") = 
            forAll{(a : HashSet[Int], b : HashSet[Int]) =>
                var ua = man.makeSet(a)
                var ub = man.makeSet(b)
                var uab = man.union(ua, ub)
                var ab = new HashSet[Int](a)
                ab.addAll(b)
                uab.equals(ab)
            }

        property(smname + ": union -- singleton") = 
            forAll{(a : HashSet[Int], e : Int) =>
                var ua = man.makeSet(a)
                var ub = man.makeSingleton(e)
                var uab = man.union(ua, ub)
                var ab = new HashSet[Int](a)
                ab.add(e)
                uab.equals(ab)
            }


        property(smname + ": minus") = 
            forAll{(a : HashSet[Int], e : Int) =>
                var ua = man.makeSet(a)
                var uaminus = man.subtract(ua, e)
                var aminus = new HashSet[Int](a)
                aminus.remove(e)
                uaminus.equals(aminus)
            }

        property(smname + ": minus -- singleton") = 
            forAll{(e1 : Int, e2 : Int) =>
                var a = new HashSet[Int]();
                a.add(e1);
                a.add(e2);
                var ua = man.makeSet(a)
                var uaminus = man.subtract(ua, e1)
                var aminus = new HashSet[Int](a)
                aminus.remove(e1)
                uaminus.equals(aminus)
            }

        property(smname + ": add") = 
            forAll{(a : HashSet[Int], e : Int) =>
                var ua = man.makeSet(a)
                var uaminus = man.add(ua, e)
                var aminus = new HashSet[Int](a)
                aminus.add(e)
                uaminus.equals(aminus)
            }

        property(smname + ": add -- singleton") = 
            forAll{(e1 : Int, e2 : Int) =>
                var a = new HashSet[Int]();
                a.add(e1);
                a.add(e2);
                var ua = man.makeSingleton(e1)
                ua = man.add(ua, e2)
                ua.equals(a)
            }

    }
}
