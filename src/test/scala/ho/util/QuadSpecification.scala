/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.util
import org.scalacheck.Properties
import org.scalacheck.Prop._

object QuadSpecification
	extends Properties("Quad") {
  
		val quad1 = new Quad[Int, Int, Int, Int]
		val quad2 = new Quad[Int, Int, Int, Int]
		
		property("inequality") =
		  forAll {(a1 : Int, b1 : Int, a2 : Int, b2 : Int, c1 : Int, c2 : Int,
				  	d1 : Int, d2 : Int) =>
		    	(a1 != a2 || b1 != b2 || c1 != c2 || d1 != d2) ==>
		    	  {
		    		quad1.set(a1, b1, c1, d1)
		    		quad2.set(a2, b2, c2, d2)
		    		quad1 != quad2 && quad2 != quad1
		    	  }
		  }
		
		property("equality and hashcode") =
		  forAll {(a1 : Int, b1 : Int, c1:Int, d1 : Int) =>
		    	
		    		quad1.set(a1, b1, c1, d1)
		    		quad2.set(a1, b1, c1, d1)
		    		(quad1 == quad2 && quad2 == quad1) :|"equality" &&
		    		(quad1.hashCode() == quad2.hashCode())  :| "hashcode"
		    	 
		  }
	
}
