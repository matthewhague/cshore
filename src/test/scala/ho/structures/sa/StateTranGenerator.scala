/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.structures.sa

import scalaz._

import scala.collection.mutable.HashSet

import org.scalacheck.Properties
import org.scalacheck.Prop._
import scala.collection._

import org.scalacheck.Properties
import org.scalacheck.Prop._
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Gen

import org.scalacheck.util.Buildable

import ho.managers.Managers
import ho.structures.cpds._
import ho.util.ManagedSet

trait StateTranGenerator {
	// will test at order-3
		var saman = Managers.saManager
		saman.reset(3) 
		
		var setman = Managers.sManager
        var csman = Managers.csManager
        var scman = Managers.scManager
        
        case class SAStateOuter(value : SAState) extends NewType[SAState]
        case class SAStateO2(value : SAState) extends NewType[SAState]
        case class SAStateO1(value : SAState) extends NewType[SAState]
		
        case class SATransitionOuter(value : SATransition) extends NewType[SATransition]
		case class SATransitionO2(value : SATransition) extends NewType[SATransition]
		
		case class SAStateSetOuter(value : ManagedSet[SAState]) extends NewType[ManagedSet[SAState]]
        case class SAStateSetO2(value : ManagedSet[SAState]) extends NewType[ManagedSet[SAState]]
        case class SAStateSetO1(value : ManagedSet[SAState]) extends NewType[ManagedSet[SAState]]
        
        protected def getAddTransition(q : SAState, qs : ManagedSet[SAState]) = {
            var change = saman.addTransition(q, qs)
            (change, saman.getTransition(q, qs))
        }

        protected def getAddTransitionO1(q : SAState, 
                               a : SCharacter, 
                               qsbr : ManagedSet[SAState],
                               qs : ManagedSet[SAState]) = {
            var change = saman.addTransitionO1(q, a, qsbr, qs)
            (change, saman.getTransitionO1(q, a, qsbr, qs))
        }
		
		implicit var arbControlState : Arbitrary[ControlState]
            = Arbitrary(for {
                i <- Gen.choose(1,10)
            } yield csman.makeControl("q" + i))

        implicit var arbSCharacter : Arbitrary[SCharacter]
            = Arbitrary(for {
                i <- Gen.choose(1,10)
            } yield scman.makeCharacter("a" + i))

        
         val genOuterState 
            = for {
                  c <- arbitrary[ControlState]
              } yield {
                saman.addOuterState(c)
                SAStateOuter(saman.getOuterState(c))
            }
         
         val genManagedSetOuterState
            = for {
                  size <- Gen.choose(0,10)
              } yield {
                  var s = setman.makeEmptySet()
                  for (i <- 1 to size) {
                      genOuterState.sample match {
                          case Some(q) => 
                              s = setman.add(s, q)  
                          case None => { }
                      }
                  }
                  SAStateSetOuter(s)
              }
              
        val genO2SAState 
            = for {
                q <- genOuterState
                qs <- genManagedSetOuterState
            } yield {
                var (_, t) = getAddTransition(q, qs)
                SAStateO2(t.getLabellingState())
            }

        val genO2ManagedSetSAState 
            = for {
                  size <- Gen.choose(0,10)
              } yield {
                  var s = setman.makeEmptySet()
                  for (i <- 1 to size) {
                      genO2SAState.sample match {
                          case Some(q) => 
                              s = setman.add(s, q)  
                          case None => { }
                      }
                  }
                  SAStateSetO2(s)
              }
              
          val genO1SAState 
            = for {
                q <- genO2SAState
                qs <- genO2ManagedSetSAState
            } yield {
                var (_, t) = getAddTransition(q, qs)
                SAStateO1(t.getLabellingState())
            }

        val genO1ManagedSetSAState 
            = for {
                  size <- Gen.choose(0,10)
              } yield {
                  var s = setman.makeEmptySet()
                  for (i <- 1 to size) {
                      genO1SAState.sample match {
                          case Some(q) => 
                              s = setman.add(s, q)  
                          case None => { }
                      }
                  }
                  SAStateSetO1(s)
              }
        
       
              
        implicit val arbO1State = Arbitrary(genO1SAState)
        implicit val arbO1StateSet = Arbitrary(genO1ManagedSetSAState)
        
        implicit val arbO2State = Arbitrary(genO2SAState)
        implicit val arbO2StateSet = Arbitrary(genO2ManagedSetSAState)
        
        implicit val arbOuterState = Arbitrary(genOuterState)
        implicit val arbOuterStateSet = Arbitrary(genManagedSetOuterState)
        
        
        val genOuterTransition = 
          genO2SAState map ( x => SATransitionOuter(x.getTransition ))
        
        val genOuterTransitionSet = 
          for {
                  size <- Gen.choose(0,10)
              } yield {
                  var s = new HashSet[SATransitionOuter]
                  for (i <- 1 to size) {
                      genOuterTransition.sample match {
                          case Some(t) => 
                              s.add(t)  
                          case None => { }
                      }
                  }
                  s
              }
        
        val genTransitionO2 = 
          genO1SAState map ( x => SATransitionO2(x.getTransition) )
        
        val genTransitionO2Set = 
          for {
                  size <- Gen.choose(0,10)
              } yield {
                  var s = new HashSet[SATransitionO2]
                  for (i <- 1 to size) {
                      genTransitionO2.sample match {
                          case Some(t) => 
                              s.add(t)  
                          case None => { }
                      }
                  }
                  s
              }
        
        // Generate O1 transition with order-2 branching states.
        val genTransitionO1 = {
          def getO1State : SAState = { genO1SAState.sample match  {
          						case Some(x) => x
          						case None => getO1State
        					}}
          def getSChar : SCharacter = { arbitrary[SCharacter].sample match  {
          						case Some(x) => x
          						case None => getSChar
        					}} 
          def getO2StateSet : ManagedSet[SAState] = { genO2ManagedSetSAState.sample match  {
          						case Some(x) => x
          						case None => getO2StateSet
        					}}
          def getO1StateSet : ManagedSet[SAState] = { genO1ManagedSetSAState.sample match  {
          						case Some(x) => x
          						case None => getO1StateSet
        					}}
          for ( i <- Gen.choose(1, 10) )
        	  yield (saman.getAddTransitionO1(getO1State, getSChar, getO2StateSet, getO1StateSet))
        }
        
        val genTransitionO1Set = 
          for {
                  size <- Gen.choose(0,10)
              } yield {
                  var s = new HashSet[SATransitionO1]
                  for (i <- 1 to size) {
                      genTransitionO1.sample match {
                          case Some(t) => 
                              s.add(t)  
                          case None => { }
                      }
                  }
                  s
              }
              
        implicit val arbO1Transition = Arbitrary(genTransitionO1)
        implicit val arbO1TransitionSet = Arbitrary(genTransitionO1Set)
        
        implicit val arbO2Transition = Arbitrary(genTransitionO2)
        implicit val arbO2TransitionSet = Arbitrary(genTransitionO2Set)
        
        implicit val arbOuterTransition = Arbitrary(genOuterTransition)
        implicit val arbOuterTransitionSet = Arbitrary(genOuterTransitionSet)
         
        
}
