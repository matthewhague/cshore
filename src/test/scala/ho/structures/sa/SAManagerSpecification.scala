/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



import java.util.HashSet

import scala.collection._

import org.scalacheck.Properties
import org.scalacheck.Prop._
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Gen

import org.scalacheck.util.Buildable

import ho.structures.sa._

import ho.structures.cpds._

import ho.util.ManagedSet
import ho.util.SetManager
import ho.util.HashSetManager

import ho.managers.Managers

// for using java iterators like scala ones (exists)
import scala.collection.JavaConversions._


package ho.structures.sa {

    object SAManagerSpecification 
        extends Properties("SAManager") {

        // test order-3
        var saman = Managers.saManager
        saman.reset(3)
        var setman = Managers.sManager
        var csman = Managers.csManager
        var scman = Managers.scManager

        var order3Transitions = new HashSet[SATransition]()
        var order2Transitions = new HashSet[SATransition]()
        var order1Transitions = new HashSet[SATransitionO1]()

        def getAddTransition(q : SAState, qs : ManagedSet[SAState]) = {
            var change = saman.addTransition(q, qs)
            (change, saman.getTransition(q, qs))
        }

        def getAddTransitionO1(q : SAState, 
                               a : SCharacter, 
                               qsbr : ManagedSet[SAState],
                               qs : ManagedSet[SAState]) = {
            var change = saman.addTransitionO1(q, a, qsbr, qs)
            (change, saman.getTransitionO1(q, a, qsbr, qs))
        }

        // give our control states names unique to this batch of tests (to avoid
        // collision with previously run AlgorithmSpecification)

        implicit var arbControlState : Arbitrary[ControlState]
            = Arbitrary(for {
                i <- Gen.choose(1,10)
            } yield csman.makeControl("satest_q" + i))

        implicit var arbSCharacter : Arbitrary[SCharacter]
            = Arbitrary(for {
                i <- Gen.choose(1,10)
            } yield scman.makeCharacter("satest_a" + i))


        val genOuterState 
            = for {
                  c <- arbitrary[ControlState]
              } yield {
                saman.getAddOuterState(c)
              }

        val genManagedSetOuterState
            = for {
                  size <- Gen.choose(0,10)
              } yield {
                  var s = setman.makeEmptySet()
                  for (i <- 1 to size) {
                      genOuterState.sample match {
                          case Some(q) => 
                              s = setman.add(s, q)  
                          case None => { }
                      }
                  }
                  s
              }

        // these are vars because we will redefine them for testing at order-2
        // (they test order-3 at the moment
        implicit var arbSAState : Arbitrary[SAState] 
            = Arbitrary(genOuterState)
        implicit var arbManagedSetSAState : Arbitrary[ManagedSet[SAState]]
            = Arbitrary(genManagedSetOuterState)

        property("SAManager: basic add outer state") = 
            forAll{(c : ControlState) =>
                saman.addOuterState(c)
                (saman.getOuterState(c) != null)
            }

        property("SAManager: basic add tran at order-3 (of 3)") = 
            forAll{(q: SAState, qs : ManagedSet[SAState]) =>
                (getAddTransition(q, qs)._2 != null)
            }

        property("SAManager: test getAddTransition at order-3 (of 3)") = 
            forAll{(q: SAState, qs : ManagedSet[SAState]) =>
                (getAddTransition(q, qs)._2 == saman.getAddTransition(q, qs))
            }

        property("SAManager: test getAddTransition at order-3 (of 3), call saman.getAdd first") = 
            forAll{(q: SAState, qs : ManagedSet[SAState]) =>
                (saman.getAddTransition(q, qs) == getAddTransition(q, qs)._2)
            }

        property("SAManager: new transition appears in Tnew, then Told, order 3") = 
            forAll{(q : SAState, qs : ManagedSet[SAState]) =>
                // by above t is not null...
                var (change, tadded) = getAddTransition(q, qs)
                var t1 : SATransition = null
                if (change) {
                    t1 = saman.getNewTransition()
                    while (t1 != null && !t1.equals(tadded)) {
                        t1 = saman.getNewTransition()
                    }
                    if (t1 != null) {
                        saman.oldTranIterator(q) exists { t2 => t2.equals(t1) } 
                    } else
                        false
                } else // no change
                    true
        } 


        property("SAManager: switch control state, order 3") = 
            forAll{(q : SAState, c : ControlState) =>
                // try {
                    var qnew = saman.switchControlStateAdd(q, c)
                    var qmade = saman.getAddOuterState(c)
                    saman.equal(qnew, qmade)
                // } catch {
                //     case e => e.printStackTrace()
                //               println(q)
                //               println(q.getStateOrder())
                //               var t = q.getTransition()
                //               println(t)
                //               println(t.getSource())
                //               false
                // }
            }



        // redefine for order-2

        val genO2SAState 
            = for {
                q <- genOuterState
                qs <- genManagedSetOuterState
            } yield {
                var (_, t) = getAddTransition(q, qs)
                // just let it throw a null pointer exception if it failed (test
                // above should have made sure it doesn't)
                t.getLabellingState()
            }

        val genO2ManagedSetSAState 
            = for {
                  size <- Gen.choose(0,10)
              } yield {
                  var s = setman.makeEmptySet()
                  for (i <- 1 to size) {
                      genO2SAState.sample match {
                          case Some(q) => 
                              s = setman.add(s, q)  
                          case None => { }
                      }
                  }
                  s
              }

        arbSAState = Arbitrary(genO2SAState)
        arbManagedSetSAState = Arbitrary(genO2ManagedSetSAState)


        property("SAManager: basic add tran at order-2 (of 3)") = 
            forAll{(q: SAState, qs : ManagedSet[SAState]) => 
                (getAddTransition(q, qs)._2 != null)
            }

        property("SAManager: test getAddTransition at order-2 (of 3)") = 
            forAll{(q: SAState, qs : ManagedSet[SAState]) =>
                (getAddTransition(q, qs)._2 == saman.getAddTransition(q, qs))
            }

        property("SAManager: test getAddTransition at order-2 (of 3), call saman.getAdd first") = 
            forAll{(q: SAState, qs : ManagedSet[SAState]) =>
                (saman.getAddTransition(q, qs) == getAddTransition(q, qs)._2)
            }


        property("SAManager: new transition appears in Tnew, then Told, order 2") = 
            forAll{(q : SAState, qs : ManagedSet[SAState]) =>
                // by above t is not null...
                var (change, tadded) = getAddTransition(q, qs)
                var t1 : SATransition = null
                if (change) {
                    t1 = saman.getNewTransition()
                    while (t1 != null && !t1.equals(tadded)) {
                        t1 = saman.getNewTransition()
                    }
                    if (t1 != null) {
                        saman.oldTranIterator(q) exists { t2 => t2.equals(t1) } 
                    } else
                        false
                } else // no change
                    true
        } 

        property("SAManager: switch control state, order 2") = 
            forAll{(q : SAState, c : ControlState) =>
                var qnew = saman.switchControlStateAdd(q, c)
                var q3 = q.getTransition().getSource()
                var qs3 = q.getTransition().getDest()
                var qmade3 = saman.getAddOuterState(c)
                var qmade = saman.getAddTransition(qmade3, qs3).getLabellingState()
                saman.equal(qnew, qmade)
            }




        // finally order-1

        val genO1SAState 
            = for {
                q <- genO2SAState
                qs <- genO2ManagedSetSAState
            } yield {
                var (_, t) = getAddTransition(q, qs)
                // property tested above should make sure t isn't null
                t.getLabellingState()
            }

        val genO1ManagedSetSAState 
            = for {
                  size <- Gen.choose(0,10)
              } yield {
                  var s = setman.makeEmptySet()
                  for (i <- 1 to size) {
                      genO1SAState.sample match {
                          case Some(q) => 
                              s = setman.add(s, q)  
                          case None => { }
                      }
                  }
                  s
              }

        arbSAState = Arbitrary(genO1SAState)
        arbManagedSetSAState = Arbitrary(genO1ManagedSetSAState)

        property("SAManager: basic add tran at order-1 (of 3)") = 
            forAll{(q : SAState, 
                    a : SCharacter,
                    qsbr : ManagedSet[SAState],
                    qs : ManagedSet[SAState]) => 
                (getAddTransitionO1(q, a, qsbr, qs)._2 != null)
            }

        property("SAManager: test getAddTransition at order-1 (of 3)") = 
            forAll{(q : SAState, 
                    a : SCharacter,
                    qsbr : ManagedSet[SAState],
                    qs : ManagedSet[SAState]) => 
                (getAddTransitionO1(q, a, qsbr, qs)._2 ==
                 saman.getAddTransitionO1(q, a, qsbr, qs))
            }

        property("SAManager: test getAddTransition at order-1 (of 3), call saman.getAdd first") = 
            forAll{(q : SAState, 
                    a : SCharacter,
                    qsbr : ManagedSet[SAState],
                    qs : ManagedSet[SAState]) => 
                (saman.getAddTransitionO1(q, a, qsbr, qs) ==
                 getAddTransitionO1(q, a, qsbr, qs)._2)
            }



        
         property("SAManager: new transition appears in Tnew, then Told, order 1") = 
            forAll{(q : SAState, 
                    a : SCharacter,
                    qsbr : ManagedSet[SAState],
                    qs : ManagedSet[SAState]) => 
                // by above t is not null...
                var (change, tadded) = getAddTransitionO1(q, a, qsbr, qs)
                var t1 : SATransitionO1 = null
                if (change) {
                    t1 = saman.getNewTransitionO1()
                    while (t1 != null && !t1.equals(tadded)) {
                        t1 = saman.getNewTransitionO1()
                    }
                    if (t1 != null) {
                        saman.oldTranO1Iterator(q, a) exists { t2 => t2.equals(t1) } 
                    } else
                        false
                } else // no change
                    true
        } 

        property("SAManager: switch control state, order 1") = 
            forAll{(q : SAState, c : ControlState) =>
                var qnew = saman.switchControlStateAdd(q, c)
                var q2 = q.getTransition().getSource()
                var qs2 = q.getTransition().getDest()
                var q3 = q2.getTransition().getSource()
                var qs3 = q2.getTransition().getDest()
                var qmade3 = saman.getAddOuterState(c)
                var qmade2 = saman.getAddTransition(qmade3, qs3).getLabellingState()
                var qmade = saman.getAddTransition(qmade2, qs2).getLabellingState()
                saman.equal(qnew, qmade)
            }




        /*
         * Previously failed test:
         *
         * ARG_0: ((satest_q5, { satest_q7 satest_q9 satest_q6 satest_q10 }), 
         *         { (satest_q8, { satest_q6 satest_q1 satest_q5 })
         *           (satest_q2, { satest_q10 }) 
         *           (satest_q4, { satest_q3 }) 
         *           (satest_q2, { satest_q8 satest_q2 satest_q5 }) 
         *           (satest_q4, { satest_q8 satest_q9 satest_q2 satest_q10 satest_q3 }) 
         *           (satest_q8, { satest_q6 satest_q4 satest_q5 }) 
         *           (satest_q2, { }) 
         *           (satest_q9, { satest_q8 satest_q2 satest_q3 satest_q4 satest_q5 }) 
         *           (satest_q7, { satest_q7 satest_q8 satest_q6 satest_q3 satest_q4 }) 
         *           (satest_q2, { satest_q8 satest_q2 satest_q3 satest_q1 satest_q4 satest_q5 }) })
         * ARG_1: satest_q8
         */
        property("SAManager: switch control state, order 1, previously failed") = {
            def makeSet(states : List[SAState]) : ManagedSet[SAState] = {
                states match {
                    case Nil => setman.makeEmptySet()
                    case q :: qs => {
                        var s = makeSet(qs)
                        setman.add(s, q);
                    }
                }
            }
            def makeState(q : SAState, qs : List[SAState]) = {
                var s = makeSet(qs)
                saman.getAddTransition(q, s).getLabellingState();
            }
            var c1 = csman.makeControl("satest_q1");
            var c2 = csman.makeControl("satest_q2");
            var c3 = csman.makeControl("satest_q3");
            var c4 = csman.makeControl("satest_q4");
            var c5 = csman.makeControl("satest_q5");
            var c6 = csman.makeControl("satest_q6");
            var c7 = csman.makeControl("satest_q7");
            var c8 = csman.makeControl("satest_q8");
            var c9 = csman.makeControl("satest_q9");
            var c10 = csman.makeControl("satest_q10");
            var q1 = saman.getAddOuterState(c1);
            var q2 = saman.getAddOuterState(c2);
            var q3 = saman.getAddOuterState(c3);
            var q4 = saman.getAddOuterState(c4);
            var q5 = saman.getAddOuterState(c5);
            var q6 = saman.getAddOuterState(c6);
            var q7 = saman.getAddOuterState(c7);
            var q8 = saman.getAddOuterState(c8);
            var q9 = saman.getAddOuterState(c9);
            var q10 = saman.getAddOuterState(c10);

            var source1 = makeState(q5, List(q7, q9, q6, q10))
            var source2 = makeState(q8, List(q7, q9, q6, q10))
            var dest1 = makeState(q8, List(q6, q1, q5))
            var dest2 = makeState(q2, List(q10)) 
            var dest3 = makeState(q4, List(q3))
            var dest4 = makeState(q2, List(q8, q2, q5)) 
            var dest5 = makeState(q4, List(q8, q9, q2, q10, q3)) 
            var dest6 = makeState(q8, List(q6, q4, q5)) 
            var dest7 = makeState(q2, List()) 
            var dest8 = makeState(q9, List(q8, q2, q3, q4, q5)) 
            var dest9 = makeState(q7, List(q7, q8, q6, q3, q4)) 
            var dest10 = makeState(q2, List(q8, q2, q3, q1, q4, q5))

            var q = makeState(source1, List(dest1, dest2, dest3, dest4, dest5,
                                            dest6, dest7, dest8, dest9, dest10))

            var switched = saman.switchControlStateAdd(q, c8)
            var handSwitched = makeState(source2, 
                                         List(dest1, dest2, dest3, dest4, dest5,
                                              dest6, dest7, dest8, dest9, dest10))

            saman.equal(switched, handSwitched)
        }



    }

}
