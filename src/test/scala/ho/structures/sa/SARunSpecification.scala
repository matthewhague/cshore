/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



import java.io.StringReader

import org.scalacheck.Properties
import org.scalacheck.Prop._

import ho.structures.cpds._
import ho.structures.sa._

import ho.parsers.CpdsParser
import ho.parsers.SAParser

import ho.managers.Managers


package ho.structures.sa {

    object SARunSpecification 
        extends Properties("SARun") {

        var cpdsParser = new CpdsParser
        var saParser = new SAParser

        def doTest(sConfig : String, 
                   sSa : String,  
                   accepts : Boolean) = {
            try {
                Managers.reset()
                val conf = cpdsParser.getConfig(new StringReader(sConfig))
                Managers.saManager.setOrder(conf.getOrder())
                val sa = saParser.getSA(new StringReader(sSa))
                val run = new SARun(sa.getTransitionsO1())
                val accepted = run.buildRun(conf)
                val result = (accepts == accepted)
                if (!result) {
                    println("\nMismatch!")
                    println("For SA:")
                    println(sSa)
                    println("and config:")
                    println(sConfig)
                    println("Test expected (and didn't get):" + accepts)
                    println("And built the run:")
                    println(run);
                }
                result
            } 
            catch {
                case e : Throwable => println("Error in SARunSpecification test: " + e)
                                      e.printStackTrace()
                                      false 
            }
        }



        property("Accepts basic error stack") = {
            doTest("""<error [[[a](1)](2)](3)>""",
                   """error ------> { }
                      (error, { }) ------> { }
                      ((error, { }), { }) --- a, { } ---> { }""",
                   true)
        }

        property("Accepts error stack with two characters") = {
            doTest("""<error [[[a b](1)](2)](3)>""",
                   """error ------> { }
                      (error, { }) ------> { }
                      ((error, { }), { }) --- a, { } ---> { }""",
                   true)
        }

        property("Accepts error stack with two characters and collapse links") = {
            doTest("""<error [[[(a 3 0) (b 2 0)](1)](2)](3)>""",
                   """error ------> { }
                      (error, { }) ------> { }
                      ((error, { }), { }) --- a, { } ---> { }""",
                   true)
        }

        property("Accepts error stack with two order-2 stacks and collapse links") = {
            doTest("""<error [[[(a 3 0) (c 3 1)](1)](2)
                              [[(a 3 0) (b 2 0)](1)](2)](3)>""",
                   """error ------> { }
                      (error, { }) ------> { }
                      ((error, { }), { }) --- a, { } ---> { }""",
                   true)
        }


        property("Chain of order-2 stacks") = {
            doTest("""<q1 [[a b](1) [a b](1)](2)>""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { error }
                      (q1, { error }) --- a, { } ---> { (q2, { }) }
                      q2 ------> { }
                      (q2, { }) --- b, { } ---> { }""",
                   true)
        }

        property("Chain of order-2 stacks -- not accepted") = {
            doTest("""<q1 [[a b](1) [a b](1)](2)>""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { error }
                      (q1, { error }) --- a, { } ---> { (q2, { }) }
                      q2 ------> { }
                      (q2, { }) --- a, { } ---> { }""",
                   false)
        }

        property("Chain of order-2 stacks with collapse link") = {
            doTest("""<q2 [[(a 2 1) b](1) [c](1) [a b](1)](2)>""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { error }
                      (q1, { error }) --- c, { } ---> {  }
                      q2 ------> { q1 }
                      (q2, { q1 }) --- a, { error } ---> { (q3, { }) }
                      q3 ------> { }
                      (q3, { }) --- b, { } ---> { }""",
                   true)
        }

        property("Chain of order-2 stacks with collapse link -- not accepted") = {
            doTest("""<q2 [[(a 2 1) b](1) [c](1) [a b](1)](2)>""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { error }
                      (q1, { error }) --- c, { } ---> {  }
                      q2 ------> { q1 }
                      (q2, { q1 }) --- a, { q2 } ---> { (q3, { }) }
                      q3 ------> { }
                      (q3, { }) --- b, { } ---> { }""",
                   false)
        }

        property("Chain of order-2 stacks with alternation") = {
            doTest("""<q2 [[(a 2 1) b](1) [c](1) [a b](1)](2)>""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { error }
                      (q1, { error }) --- c, { } ---> {  }
                      q2 ------> { q1 q3 }
                      (q2, { q1 q3 }) --- a, { } ---> { (q3, { }) }
                      q3 ------> { }
                      (q3, { }) --- b, { } ---> { }
                      (q3, { }) --- c, { } ---> { }""",
                   true)
        }

        property("Chain of order-2 stacks with alternation -- not accepted") = {
            doTest("""<q2 [[(a 2 1) b](1) [c](1) [a b](1)](2)>""",
                   """error ------> { }
                      (error, { }) --- a, { } ---> { }
                      (error, { }) --- b, { } ---> { }
                      q1 ------> { error }
                      (q1, { error }) --- c, { } ---> {  }
                      q2 ------> { q1 q3 }
                      (q2, { q1 q3 }) --- a, { } ---> { (q3, { }) }
                      q3 ------> { }
                      (q3, { }) --- b, { } ---> { }""",
                   false)
        }




    }

}
