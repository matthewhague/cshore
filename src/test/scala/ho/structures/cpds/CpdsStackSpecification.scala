/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */




import java.io.StringReader

import java.util.Stack

import org.scalacheck.Properties
import org.scalacheck.Prop._
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Gen

import ho.managers.Managers

import ho.parsers.CpdsParser

package ho.structures.cpds {


    object CpdsStackSpecification
        extends Properties("CpdsStack") {

            var cpdsParser = new CpdsParser

            def doGetNextPositionTest(sStack : String, 
                                      posIn : CpdsStackPosition,
                                      posOut : CpdsStackPosition) = {
                try {
                    Managers.reset()
                    val stack = cpdsParser.getStack(new StringReader(sStack))
                    val nextPos = stack.getNextPosition(posIn)
                    val result = (nextPos == posOut)
                    if (!result) {
                        println("\nMismatch in doGetNextPositionTest!")
                        println("For Stack:")
                        println(sStack)
                        println("and position in:" + posIn)
                        println("Test expected position out:" + posOut)
                        println("But got: " + nextPos)
                    }
                    result
                } 
                catch {
                    case e : Throwable => println("Error in CpdsStackSpecification test: " + e)
                                                  e.printStackTrace()
                                                  false 
                }
            }

            // Basic stack
            //
            // .[.[.[.a.b.].[.c.].].].[.[.a.].].].
            //
            // Positions
            //
            // (-1, -1, -1, -1)
            // (-1, -1, -1, 0)
            // (-1, -1, 0, 0)
            // (-1, 0, 0, 0)
            // (0, 0, 0, 0)
            // (1, 0, 0, 0)
            // (-1, 1, 0, 0)
            // (-1, -1, 1, 0)
            // (-1, 0, 1, 0)
            // (0, 0, 1, 0)
            // (1, 0, 1, 0)
            // (-1, 1, 1, 0)
            // (0, 1, 1, 0)
            // (1, 1, 1, 0)
            // (2, 1, 1, 0)
            // (-1, 2, 1, 0)
            // (-1, -1, 2, 0)
            // (-1, -1, -1, 1)


            property("getFirstPosition") = {
                val sStack = "[[[a b](1)[c](1)](2)[[a](1)](2)](3)"
                val firstPos = new CpdsStackPosition(-1, -1, -1, 1)

                try {
                    Managers.reset()
                    val stack = cpdsParser.getStack(new StringReader(sStack))
                    (stack.getFirstPosition() == firstPos)
                } 
                catch {
                    case e : Throwable => println("Error in CpdsStackSpecification test: " + e)
                                                  e.printStackTrace()
                                                  false 
                }
            }
      

            property("getNextPosition: bottom position stays bottom") = {
                val stack = "[[[a b](1)[c](1)](2)[[a](1)](2)](3)"
                val posIn = new CpdsStackPosition(-1, -1, -1, -1)
                val posOut = new CpdsStackPosition(-1, -1, -1, -1)
                doGetNextPositionTest(stack, posIn, posOut)
            }

            property("getNextPosition: simple order-1 drop") = {
                val stack = "[[[a b](1)[c](1)](2)[[a](1)](2)](3)"
                val posIn = new CpdsStackPosition(2, 1, 1, 0)
                val posOut = new CpdsStackPosition(1, 1, 1, 0)
                doGetNextPositionTest(stack, posIn, posOut)
            }

            property("getNextPosition: simple order-1 drop 2") = {
                val stack = "[[[a b](1)[c](1)](2)[[a](1)](2)](3)"
                val posIn = new CpdsStackPosition(1, 0, 1, 0)
                val posOut = new CpdsStackPosition(0, 0, 1, 0)
                doGetNextPositionTest(stack, posIn, posOut)
            }

            property("getNextPosition: order-1 drop out of stack") = {
                val stack = "[[[a b](1)[c](1)](2)[[a](1)](2)](3)"
                val posIn = new CpdsStackPosition(0, 0, 1, 0)
                val posOut = new CpdsStackPosition(-1, 0, 1, 0)
                doGetNextPositionTest(stack, posIn, posOut)
            }

            property("getNextPosition: order-2 drop out of stack") = {
                val stack = "[[[a b](1)[c](1)](2)[[a](1)](2)](3)"
                val posIn = new CpdsStackPosition(-1, 0, 1, 0)
                val posOut = new CpdsStackPosition(-1, -1, 1, 0)
                doGetNextPositionTest(stack, posIn, posOut)
            }

            property("getNextPosition: order-2 move into order-1 stack") = {
                val stack = "[[[a b](1)[c](1)](2)[[a](1)](2)](3)"
                val posIn = new CpdsStackPosition(-1, 1, 1, 0)
                val posOut = new CpdsStackPosition(1, 0, 1, 0)
                doGetNextPositionTest(stack, posIn, posOut)
            }

            property("getNextPosition: order-3 drop out of stack") = {
                val stack = "[[[a b](1)[c](1)](2)[[a](1)](2)](3)"
                val posIn = new CpdsStackPosition(-1, -1, 0, 0)
                val posOut = new CpdsStackPosition(-1, -1, -1, 0)
                doGetNextPositionTest(stack, posIn, posOut)
            }

            property("getNextPosition: order-3 move into order-1 stack") = {
                val stack = "[[[a b](1)[c](1)](2)[[a](1)](2)](3)"
                val posIn = new CpdsStackPosition(-1, -1, 1, 0)
                val posOut = new CpdsStackPosition(-1, 1, 0, 0)
                doGetNextPositionTest(stack, posIn, posOut)
            }



            // Empty order-1
            //
            // .[.[.[.].[.c.].].].[.[.a.].].].
            //
            // Positions
            //
            // (-1, -1, -1, -1)
            // (-1, -1, -1, 0)
            // (-1, -1, 0, 0)
            // (-1, 0, 0, 0)
            // (0, 0, 0, 0)
            // (1, 0, 0, 0)
            // (-1, 1, 0, 0)
            // (-1, -1, 1, 0)
            // (-1, 0, 1, 0)
            // (0, 0, 1, 0)
            // (1, 0, 1, 0)
            // (-1, 1, 1, 0)
            // (0, 1, 1, 0)
            // (-1, 2, 1, 0)
            // (-1, -1, 2, 0)
            // (-1, -1, -1, 1)

            property("getNextPosition: simple order-1 drop from empty") = {
                val stack = "[[[](1)[c](1)](2)[[a](1)](2)](3)"
                val posIn = new CpdsStackPosition(0, 1, 1, 0)
                val posOut = new CpdsStackPosition(-1, 1, 1, 0)
                doGetNextPositionTest(stack, posIn, posOut)
            }

            property("getNextPosition: simple order-1 move into empty") = {
                val stack = "[[[](1)[c](1)](2)[[a](1)](2)](3)"
                val posIn = new CpdsStackPosition(-1, 2, 1, 0)
                val posOut = new CpdsStackPosition(0, 1, 1, 0)
                doGetNextPositionTest(stack, posIn, posOut)
            }


            // Empty order-2
            //
            // .[.[.].[.[.a.].].].
            //
            // Positions
            //
            // (-1, -1, -1, -1)
            // (-1, -1, -1, 0)
            // (-1, -1, 0, 0)
            // (-1, 0, 0, 0)
            // (0, 0, 0, 0)
            // (1, 0, 0, 0)
            // (-1, 1, 0, 0)
            // (-1, -1, 1, 0)
            // (-1, 0, 1, 0)
            // (-1, -1, 2, 0)
            // (-1, -1, -1, 1)

            property("getNextPosition: order-2 drop from empty") = {
                val stack = "[[](2)[[a](1)](2)](3)"
                val posIn = new CpdsStackPosition(-1, 0, 1, 0)
                val posOut = new CpdsStackPosition(-1, -1, 1, 0)
                doGetNextPositionTest(stack, posIn, posOut)
            }

            property("getNextPosition: order-2 move into empty") = {
                val stack = "[[](2)[[a](1)](2)](3)"
                val posIn = new CpdsStackPosition(-1, -1, 2, 0)
                val posOut = new CpdsStackPosition(-1, 0, 1, 0)
                doGetNextPositionTest(stack, posIn, posOut)
            }

            // Empty order-3
            //
            // .[.].
            //
            // Positions
            //
            // (-1, -1, -1, -1)
            // (-1, -1, -1, 0)
            // (-1, -1, -1, 1)

            property("getNextPosition: order-3 drop from empty") = {
                val stack = "[](3)"
                val posIn = new CpdsStackPosition(-1, -1, -1, 0)
                val posOut = new CpdsStackPosition(-1, -1, -1, -1)
                doGetNextPositionTest(stack, posIn, posOut)
            }

            property("getNextPosition: order-3 move into empty") = {
                val stack = "[](3)"
                val posIn = new CpdsStackPosition(-1, -1, -1, 1)
                val posOut = new CpdsStackPosition(-1, -1, -1, 0)
                doGetNextPositionTest(stack, posIn, posOut)
            }


            // Would be nice to generate random stacks and test whether we can
            // walk from top to bottom using getNextPosition (we can count the
            // size of the stack to know exactly how long this should take)
            // But maybe that's too much like procrastination right now (and
            // will be tested by pruning SARun)


    }
}
