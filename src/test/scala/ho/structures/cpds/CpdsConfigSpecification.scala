/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */



import java.io.StringReader

import java.util.Stack

import org.scalacheck.Properties
import org.scalacheck.Prop._
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Gen

import ho.managers.Managers

import ho.parsers.CpdsParser

package ho.structures.cpds {

    object CpdsConfigSpecification
        extends Properties("CpdsConfig") {
      
            val csman = Managers.csManager
            val scman = Managers.scManager

            val cpdsParser = new CpdsParser

            def canApplySequence(run : List[String],
                                 q0 : String,
                                 a0 : String,
                                 k : Byte) = {
                    var p = csman.makeControl(q0)
                    var a = scman.makeCharacter(a0)
                    var conf = new CpdsConfig(p, a, k)

                    // have to recreate the rules too...
                    var reaches = true
                    for (r <- run if reaches) {
                        var reader = new StringReader(r)
                        var rule = cpdsParser.getRule(reader)
                        reaches &= conf.applyRule(rule)
                    }
                    // because the algorithm has the assumption that we always
                    // have a top(1) character
                    reaches &= (conf.getStack().getTop1() != null)

                    // reaches should be false
                    reaches
                }

            property("Create Config") = {
                forAll{(order : Byte) =>
                    (order >= 1) ==> {
                        var c = csman.makeControl("ccs_q1")
                        var a = scman.makeCharacter("ccs_a")
                        var config = new CpdsConfig(c, a, order)
                        var s = config.getStack()
                        var good = true
                        good &= (c == config.getControlState())
                        for (i <- order.toInt to 2 by -1) {
                            good &= (s.topSize(i.toByte) == 1)
                        }
                        good &= (s.getTop1() == a)
                        if (!good) {
                            println("Failed on " + config)
                        }
                        good
                    }
                }
            }


            def canApplyRuleTest(conf1 : String,
                                 rule : String,
                                 conf2: String) = {
                try {
                    val c1 = cpdsParser.getConfig(new StringReader(conf1))
                    val r = cpdsParser.getRule(new StringReader(rule))
                    val c2 = cpdsParser.getConfig(new StringReader(conf2))
                    val applied = c1.applyRule(r)
                    if (!applied) {
                         println("Could not apply rule:\n" + 
                                "Conf in: " + conf1 + "\n" +
                                "   Rule: " + rule + "\n")
                         false
                   } else {
                        val result = c1.equals(c2)
                        if (!result) {
                            println("Failed:\n" + 
                                    "         Conf in: " + conf1 + "\n" +
                                    "            Rule: " + rule + "\n" +
                                    "   Conf expected: " + conf2 + "\n" +
                                    "Actually reached: " + c1)
                        }
                        result
                    }
                } catch {
                    case e : Throwable => e.printStackTrace()
                                          false
                }
            }

            def cantApplyRuleTest(conf1 : String,
                                  rule : String) = {
                try {
                    val c1 = cpdsParser.getConfig(new StringReader(conf1))
                    val r = cpdsParser.getRule(new StringReader(rule))
                    !c1.applyRule(r)
                } catch {
                    case e : Throwable => e.printStackTrace()
                                          false
                }
            }

            property("Control state mismatch") = {
                cantApplyRuleTest("<ccs_q1 [[[a](1)](2)](3)>",
                                  "(ccs_q2 a push(2) ccs_q2)")
            }

            property("Top char mismatch") = {
                cantApplyRuleTest("<ccs_q1 [[[a](1)](2)](3)>",
                                  "(ccs_q1 b push(2) ccs_q2)")
            }

            property("No top char") = {
                cantApplyRuleTest("<ccs_q1 [[[](1)](2)](3)>",
                                  "(ccs_q1 a push(2) ccs_q2)")
            }


            property("Apply push(2)") = {
                canApplyRuleTest("<ccs_q1 [[[a](1)](2)](3)>",
                                 "(ccs_q1 a push(2) ccs_q2)",
                                 "<ccs_q2 [[[a](1) [a](1)](2)](3)>")
            }

            property("Apply push(3)") = {
                canApplyRuleTest("<ccs_q1 [[[a](1)](2)](3)>",
                                 "(ccs_q1 a push(3) ccs_q2)",
                                 "<ccs_q2 [[[a](1)](2) [[a](1)](2)](3)>")
            }

            property("Apply pop(1)") = {
                canApplyRuleTest("<ccs_q1 [[[a](1)](2)](3)>",
                                 "(ccs_q1 a pop(1) ccs_q2)",
                                 "<ccs_q2 [[[](1)](2)](3)>")
            }

            property("Apply pop(2)") = {
                canApplyRuleTest("<ccs_q1 [[[a](1)](2)](3)>",
                                 "(ccs_q1 a pop(2) ccs_q2)",
                                 "<ccs_q2 [[](2)](3)>")
            }

            property("Apply pop(3)") = {
                canApplyRuleTest("<ccs_q1 [[[a](1)](2)](3)>",
                                 "(ccs_q1 a pop(3) ccs_q2)",
                                 "<ccs_q2 [](3)>")
            }

            property("Apply collapse(3)") = {
                canApplyRuleTest("""<ccs_q1 [[[(a 3 2)](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""",
                                 "(ccs_q1 a collapse(3) ccs_q2)",
                                 """<ccs_q2 [[[a](1)](2)
                                             [[b](1)](2)](3)>""")
            }

            property("Can't apply collapse(3)") = {
                cantApplyRuleTest("""<ccs_q1 [[[(a 2 0)](1)](2)
                                              [[a](1)](2)
                                              [[b](1)](2)](3)>""",
                                  "(ccs_q1 a collapse(3) ccs_q2)")
            }

            property("Apply collapse(2)") = {
                canApplyRuleTest("""<ccs_q1 [[[(a 2 1)](1) [a](1) [b](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""",
                                 "(ccs_q1 a collapse(2) ccs_q2)",
                                 """<ccs_q2 [[[b](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""")
            }

            property("Apply rew(b)") = {
                canApplyRuleTest("<ccs_q1 [[[a](1)](2)](3)>",
                                 "(ccs_q1 a rew(b) ccs_q2)",
                                 "<ccs_q2 [[[b](1)](2)](3)>")
            }

            property("Apply rew(b) with link") = {
                canApplyRuleTest("""<ccs_q1 [[[(a 2 1)](1) [a](1) [b](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""",
                                 "(ccs_q1 a rew(b) ccs_q2)",
                                 """<ccs_q2 [[[(b 2 1)](1) [a](1) [b](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""")
            }

            property("Apply push(b)(3)") = {
                canApplyRuleTest("""<ccs_q1 [[[(a 2 1)](1) [a](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""",
                                 "(ccs_q1 a push(b)(3) ccs_q2)",
                                 """<ccs_q2 [[[(b 3 2) (a 2 1)](1) [a](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""")
            }

            property("Apply push(b)(2)") = {
                canApplyRuleTest("""<ccs_q1 [[[(a 2 1)](1) [a](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""",
                                 "(ccs_q1 a push(b)(2) ccs_q2)",
                                 """<ccs_q2 [[[(b 2 1) (a 2 1)](1) [a](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""")
            }

            property("Apply push(b)") = {
                canApplyRuleTest("""<ccs_q1 [[[(a 2 1)](1) [a](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""",
                                 "(ccs_q1 a push(b) ccs_q2)",
                                 """<ccs_q2 [[[b (a 2 1)](1) [a](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""")
            }

            property("Apply push(b)(3) point to bottom") = {
                canApplyRuleTest("""<ccs_q1 [[[(a 2 1)](1) [a](1)](2)](3)>""",
                                 "(ccs_q1 a push(b)(3) ccs_q2)",
                                 """<ccs_q2 [[[(b 3 0) (a 2 1)](1) [a](1)](2)](3)>""")
            }

            property("Apply push(b)(2) point to bottom") = {
                canApplyRuleTest("""<ccs_q1 [[[(a 2 1)](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""",
                                 "(ccs_q1 a push(b)(2) ccs_q2)",
                                 """<ccs_q2 [[[(b 2 0) (a 2 1)](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""")
            }

            property("Apply pop(1) to order-1 stack") = { 
                canApplyRuleTest("<ccs_q1 [a b](1)>",
                                 "(ccs_q1 a pop(1) ccs_q2)",
                                 "<ccs_q2 [b](1)>")
            }

            property("Apply pop(1) to order-1 stack, reaches empty") = { 
                canApplyRuleTest("<ccs_q1 [a](1)>",
                                 "(ccs_q1 a pop(1) ccs_q2)",
                                 "<ccs_q2 [](1)>")
            }

            property("Apply rew(b) to order-1 stack") = {
                canApplyRuleTest("<ccs_q1 [a b c](1)>",
                                 "(ccs_q1 a rew(b) ccs_q2)",
                                 "<ccs_q2 [b b c](1)>")
            }

            property("Apply push(b) to order-1 stack") = {
                canApplyRuleTest("<ccs_q1 [a b c](1)>",
                                 "(ccs_q1 a push(b) ccs_q2)",
                                 "<ccs_q2 [b a b c](1)>")
            }


            property("Apply rew(b)(3)") = {
                canApplyRuleTest("""<ccs_q1 [[[(a 2 1)](1) [a](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""",
                                 "(ccs_q1 a rew(b)(3) ccs_q2)",
                                 """<ccs_q2 [[[(b 3 2)](1) [a](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""")
            }

            property("Apply rew(b)(2)") = {
                canApplyRuleTest("""<ccs_q1 [[[(a 3 1)](1) [a](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""",
                                 "(ccs_q1 a rew(b)(2) ccs_q2)",
                                 """<ccs_q2 [[[(b 2 1)](1) [a](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""")
            }

//            property("Apply rew(b)(1)") = {
//                canApplyRuleTest("""<ccs_q1 [[[(a 2 1) c](1) [a](1)](2)
//                                             [[a](1)](2)
//                                             [[b](1)](2)](3)>""",
//                                 "(ccs_q1 a rew(b)(1) ccs_q2)",
//                                 """<ccs_q2 [[[(b 1 1) c](1) [a](1)](2)
//                                             [[a](1)](2)
//                                             [[b](1)](2)](3)>""")
//            }
//
            property("Apply rew(b)(3) point to bottom") = {
                canApplyRuleTest("""<ccs_q1 [[[(a 2 1)](1) [a](1)](2)](3)>""",
                                 "(ccs_q1 a rew(b)(3) ccs_q2)",
                                 """<ccs_q2 [[[(b 3 0)](1) [a](1)](2)](3)>""")
            }

            property("Apply rew(b)(2) point to bottom") = {
                canApplyRuleTest("""<ccs_q1 [[[(a 2 1)](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""",
                                 "(ccs_q1 a rew(b)(2) ccs_q2)",
                                 """<ccs_q2 [[[(b 2 0)](1)](2)
                                             [[a](1)](2)
                                             [[b](1)](2)](3)>""")
            }

            property("Apply pop(2) to order-1 stack") = { 
                cantApplyRuleTest("<ccs_q1 [a b](1)>",
                                  "(ccs_q1 a pop(2) ccs_q2)")
            }

            property("Apply push(b)(2) to order-1 stack") = {
                cantApplyRuleTest("<ccs_q1 [a b c](1)>",
                                  "(ccs_q1 a push(b)(2) ccs_q2)")
            }

            property("Can't get top(2)") = {
                var r = new StringReader("<ccs_q1 [[](3)](4)>")
                var c = cpdsParser.getConfig(r)
                !c.getStack().hasTop(2.toByte)
            }

            property("Can't get top(1), empty order-3") = {
                var r = new StringReader("<ccs_q1 [[](3)](4)>")
                var c = cpdsParser.getConfig(r)
                c.getStack().getTop1() == null
            }

            property("Can't get top(1), empty order-2") = {
                var r = new StringReader("<ccs_q1 [[[](2)](3)](4)>")
                var c = cpdsParser.getConfig(r)
                c.getStack().getTop1() == null
            }

            property("Can't get top(1), empty order-1") = {
                var r = new StringReader("<ccs_q1 [[[[](1)](2)](3)](4)>")
                var c = cpdsParser.getConfig(r)
                c.getStack().getTop1() == null
            }

            property("Get top(1)") = {
                var r = new StringReader("<ccs_q1 [[[[(a 2 0)](1)](2)](3)](4)>")
                var c = cpdsParser.getConfig(r)
                var cl = new CharLink(scman.makeCharacter("a"),
                                      2.toByte,
                                      0)
                c.getStack().getTop1CharLink().equals(cl)
            }

            property("Get top(2)") = {
                var r1 = new StringReader("""<ccs_q1 [[[a b](1)](2) 
                                                      [[a](1) [b](1)](2)](3)>""")
                var c1 = cpdsParser.getConfig(r1)
                var r2 = new StringReader("<ccs_q1 [a b](1)>")
                var c2 = cpdsParser.getConfig(r2)
                var s1 = c1.getStack().getTopCopy(2.toByte)
                var s2 = c2.getStack().getTopCopy(2.toByte)
                s1 == s2
            }

            property("Bug found in CPDS simulation 1") =  {
                // cpds that should not be able to reach error state
                var run = List("(as_q0 as_a0 push(3) as_q1)",
                               "(as_q1 as_a0 push(as_a1)(2) as_q2)",
                               "(as_q2 as_a1 pop(3) as_q3)",
                               "(as_q3 as_a1 rew(as_a3) error)")
                !canApplySequence(run, "as_q0", "as_a0", 3.toByte)
            }

    }
}
