/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


package ho.lazystates

import org.scalacheck.Properties
import org.scalacheck.Prop._
import ho.structures.sa.SAManagerSpecification
import ho.structures.sa.SAState
import ho.managers.Managers

import scala.collection._

import ho.structures.sa._

import ho.structures.cpds._

import org.scalacheck.Properties
import org.scalacheck.Prop._
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Gen

import org.scalacheck.util.Buildable

import ho.structures.sa._

import ho.structures.cpds._

import ho.util.ManagedSet



// Perhaps should refactor SAMansgerSpecification
// to extract SAState generation code...

abstract class LazyStateManagerSpecification ( lazyStateManager : LazyStateManager )
	extends Properties("LazyState") with StateTranGenerator {
	
	property("Add Lazy States from Outer") = {
	  
	println("In LazyState Property")
		(forAll { ( q3 : SAStateOuter, p : ControlState, Q3 : SAStateSetOuter, Q2 : SAStateSetO2,
					sChar : SCharacter, Q1 : SAStateSetO1, col : SAStateSetO2) =>
					  
			forAll { (q3b : SAStateOuter, pb : ControlState, Q3b : SAStateSetOuter, Q2b : SAStateSetO2,
								sCharb : SCharacter, Q1b : SAStateSetO1, colb : SAStateSetO2) => 
								  
					( q3b != q3 || pb != p || Q3b != Q3 || Q2b != Q2 ||
						sCharb != sChar || Q1b != Q1 || colb != col ) ==>
				//
				({	
					saman.reset(3)
					
					val lazy3 = lazyStateManager.getAddLazyState(q3, p)
					val lazy2 = lazy3.nextLazyState(Q3)
					val lazy1 = lazy2.nextLazyStateO2(Q2)
					lazy1.complete(sChar, col, Q1, null, null)
					
					val changed3 = saman.addOuterState(p)
					val (changed2, t3) = getAddTransition(saman.getOuterState(p), Q3)
					val (changed1, t2) = getAddTransition(t3.getLabellingState, Q2)
					val (completeTest, _) = getAddTransitionO1(t2.getLabellingState, sChar, col, Q1) 
									
					!changed3  :| "Create Outer State" &&
					!changed2 :| "Create O2 state" &&
					!changed1 :| "Create O1 state" &&
					!completeTest :| "Create Complete O1 transition" &&
					//
					{ val changed3 = saman.addOuterState(pb)
					  val (changed2, t3) = getAddTransition(saman.getOuterState(pb), Q3b)
					  val (changed1, t2) = getAddTransition(t3.getLabellingState, Q2b)
					  !saman.containsTransitionO1(t2.getLabellingState, sCharb, colb, Q1b)
					} :| "Non-existence of O1 transition" 
					
				})
		
	}}) && {saman.reset(3); true} :| "Cleanup" 	
	}
	
	property("Add Lazy States from O2")
		= { println("In Add Lazy States from O2 Property")
	  (forAll { ( q2 : SAStateO2, p : ControlState,  Q2 : SAStateSetO2,
	
					sChar : SCharacter, Q1 : SAStateSetO1, col : SAStateSetO2) =>
					  
			forAll { (q2b : SAStateO2, pb : ControlState, Q2b : SAStateSetO2,
								sCharb : SCharacter, Q1b : SAStateSetO1, colb : SAStateSetO2) => 					
				// 
				({	
					saman.reset(3)
					
					val lazy2 = lazyStateManager.getAddLazyState(q2, p)
					val lazy1 = lazy2.nextLazyStateO2(Q2)
					lazy1.complete(sChar, col, Q1, null, null)
					
					
					val switched = saman.switchControlStateAdd(q2, p)
					
					(   switched !=  q2b || Q2b != Q2 ||
						sCharb != sChar || Q1b != Q1 || colb != col ) ==>
					({
					val (changed1, t2) = getAddTransition(switched, Q2)
					val (completeTest, _) = getAddTransitionO1(t2.getLabellingState, sChar, col, Q1) 
									
					!changed1 :| "Create O1 state" &&
					!completeTest :| "Create Complete O1 transition" &&
					//
					{
					  val (changed2, t2) = getAddTransition(q2b, Q2b)
					  val (changed1, t1) = getAddTransition(t2.getLabellingState, Q1b)
					  !saman.containsTransitionO1(t1.getLabellingState, sCharb, colb, Q1b)
					} :| "Non-existence of O1 transition" 
					})	
				})
		
	}}) && {saman.reset(3); true} :| "Cleanup" 	 }
	
	
	property("Add Lazy States from O1")
		= { println("In Add LAzy States from O1 property")
	  
			(forAll { ( q1 : SAStateO1, p : ControlState, 
					sChar : SCharacter, Q1 : SAStateSetO1, col : SAStateSetO2, mix : SAStateSetO2) =>
					  
			forAll { (q1b : SAStateO1, 
								sCharb : SCharacter, Q1b : SAStateSetO1, colb : SAStateSetO2) => 
				//								  
				//
				// 
				({	
					saman.reset(3)
					
					val lazy1 = lazyStateManager.getAddLazyStateO1(q1, p, 2.toByte, mix)
					lazy1.complete(sChar, col, Q1, null, null)
					
					val switched = saman.switchControlStateUnionOrdkAdd(q1, p, mix, 2.toByte)
					
					( q1b != switched || 
						sCharb != sChar || Q1b != Q1 || colb != col ) ==>
					({
					val (completeTest, _) = 
					  	getAddTransitionO1(switched, sChar, col, Q1) 
									
					!completeTest :| "Create Complete O1 transition" &&
					//
					{
					  !saman.containsTransitionO1(q1b, sCharb, colb, Q1b)
					} :| "Non-existence of O1 transition" 
					
					})
		
	})}}) && {saman.reset(3); true} :| "Cleanup" 	}
	
	
	
      
		
}
