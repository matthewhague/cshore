    
import AssemblyKeys._

name := "HO"

version := "0.2"

scalaVersion := "2.10.4"

libraryDependencies += "log4j" % "log4j" % "1.2.16"

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.12.5" % "test"

libraryDependencies += "org.pcollections" % "pcollections" % "2.1.2"

resolvers += "Scala Tools Snapshots" at "http://scala-tools.org/repo-snapshots/"

libraryDependencies += "org.scalaz" %% "scalaz-core" % "6.0.4"

javacOptions += "-Xlint:unchecked"

javaOptions ++= Seq("-Xmx500M")

// hprof options (amongst others)
// =heap=sites/dump/all
// =cpu=samples/times/old
// and for sampling interval
// interval=
//javaOptions ++= Seq("-agentlib:hprof=cpu=times")


parallelExecution in Test := false

fork := false

// scalacOptions ++= Seq("-unchecked", "-deprecation")

// for "assembly" task that builds one stand-alone jar

assemblySettings

mainClass in assembly := Some("ho.main.Main")
mainClass in (Compile, run) := Some("ho.main.Main")
